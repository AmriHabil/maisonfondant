<?php
    return [
        'raisonsociale' => 'Maison Fondant',
        'mf' => '12345678/X/N/M/000',
        'address' => '025 Rue Ibn Al Jazzar Ryadh Ghazela',
        'phone' => ' 22 561 313',
        'email' => 'contact@maisonfondant.tn',
        'token' => 'y_wIB8pbcdoqSjTkTso60iSk_WIhDsZnJe80BMihuo4s08qWalTNskcADMWNzZUCrkVaURO29gEhXRpTXHxgv2tJY0kDJlqhIqj65oC0GQIlqvnV0ppZpQKKuXgtp-4ClDdTh0KJ3_67yFGPiIHksbjEKrV44ZyJNlwy7pcYdegV-0lu-znNYKJMYyPZx1H3x3ipeCKjZrlY10a0Z2PwFGtU6qlpIA7KAEjkulKq8_6eU2BKuWDwQY_DefvjhwMrXHwuPIQypq5clCzR1XyVXtoFUJSLB5cgnghT4e9UDpZjauEA8wWGv36fd3znQMDr5z57ggswGIfd-_1xJZ8CyFLlk6zrTTXcWTWihCUdCBpNcsOaQju8WUZ4tBp5MrVii_DjvQE4LKzn0kJ5YuRwVOR599Fqh3aWFlYFkvjOCB1ah8Nc1jUfCoVZxpAR0sb9ULyTk7iRHjSI4KiYnJ3dKpPcdLWhcVfu_R2IzRgPvvdtlXUM1q0lhnseE4lqCQQYQyWdBBN-AhMQHDh3EI53vVt5HrpkEBTYWeir8VTPOHo',
        'image' => '/assets/images/logo-dark.png'
        ,
        'status' => [
            'pending' => '<span class="badge badge-primary">en Attente</span>',
            'in_progress' => '<span class="badge badge-warning">en Cours</span>',
            'returned' => '<span class="badge badge-pink">Retourné</span>',
            'delivered' => '<span class="badge badge-success">Livré</span>',
            'cancelled' => '<span class="badge badge-danger">Annulé</span>',
        ],
        

    ];