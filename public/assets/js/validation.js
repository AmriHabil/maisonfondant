

    $('.numeric').keyup(function(e){
        var max=$(this).attr('maxlenght');
        if (/\D/g.test(this.value))
        {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
        }
        if (e.which < 0x20) {
            // e.which < 0x20, then it's not a printable character
            // e.which === 0 - Not a character
            return; // Do nothing
        }
        if ($(this).val().toString().length == max) {
            e.preventDefault();
        } else if ($(this).val().toString().length > max) {
            // Maximum exceeded
            this.value = this.value.substring(0, max);
        }
        
    });
    $(".numeric").maxlength({

        warningClass: "badge badge-danger",
        limitReachedClass: "badge badge-success"
    });
    $('#ID_Governorate ').change(function(){
        var ID_Governorate=$('#ID_Governorate').val();
        $('#Governorate').val($("#ID_Governorate option:selected").text());
        $("#ID_Delegation").val("");
        $("#ID_Delegation").select2({
            templateResult: function(option, container) {
                if ($(option.element).attr("data-governorates") != ID_Governorate){
                    $(container).css("display","none");
                }
                return option.text;
            }
        });
        $("#ID_Locality").val("");
        $("#ID_Locality").select2({
            templateResult: function(option, container) {
                if ($(option.element).attr("data-governorates") != ID_Governorate){
                    $(container).css("display","none");
                }
                return option.text;
            }
        });
    });
    $('#ID_Locality').change(function(){
        if($('#ID_Governorate').val()>0) {
            if($('#ID_Delegation').val()>0) {
                $('#Locality').val($("#ID_Locality .tohide:selected").text());

            }else{
                swal({
                    title: "Fail!",
                    text: "Please Select Delegation first",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });
                $('#ID_Locality').val('');
            }
        }else{
            swal({
                title: "Fail!",
                text: "Please Select Governorate first",
                type: "error",
                confirmButtonClass: "btn btn-confirm mt-2"
            });

            $("#ID_Locality").val("");
            $("#ID_Locality").select2({});
        }

        // $('#Locality').val($("#ID_Locality .tohide:selected").text());
        // var ID_Delegation=$('#ID_Delegation').att('data-delegation');
        // var ID_Governorates=$('#ID_Delegation').att('data-governorates');
        // $("#ID_Delegation").select2({
        //     templateResult: function(option, container) {
        //         if ($(option.element).attr("data-governorates") != ID_Governorates){
        //             $(container).css("display","none");
        //         }
        //         return option.text;
        //     }
        // });
        // $("#ID_Delegation option[value="+ID_Delegation+"]").prop("selected", "selected");
        // $('#ID_Delegation').val(ID_Delegation); // Select the option with a value of '1'
        // $('#ID_Delegation').trigger('change');
        //
        //
        // $("#ID_Governorate option[value="+ID_Governorates+"]").prop("selected", "selected");
        // $('#ID_Governorate').val(ID_Governorates); // Select the option with a value of '1'
        // $('#ID_Governorate').trigger('change');
    });
    $('#ID_Delegation').change(function(){
        if($('#ID_Governorate').val()>0){
            var ID_Delegation=$('#ID_Delegation').val();
            // var ID_Governorates=$('#ID_Delegation').att('data-governorates');
            $('#Delegation').val($("#ID_Delegation .tohide:selected").text());
            $("#ID_Locality").val("");
            $("#ID_Locality").select2({
                templateResult: function(option, container) {
                    if ($(option.element).attr("data-delegation") != ID_Delegation){
                        $(container).css("display","none");
                    }
                    return option.text;
                }
            });
            // $("#ID_Governorate option[value="+ID_Governorates+"]").prop("selected", "selected");
            // $('#ID_Governorate').val(ID_Governorates); // Select the option with a value of '1'
            // $('#ID_Governorate').trigger('change');
        }else{
            swal({
                title: "Fail!",
                text: "Please Select Governorate first",
                type: "error",
                confirmButtonClass: "btn btn-confirm mt-2"
            });
            $("#ID_Delegation").val("");
            $("#ID_Delegation").select2({});
        }

    });
