<?php
    
    use Illuminate\Support\Facades\Route;
    
    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */
    
    /*LaravelLocalization*/
    
    
    Route::group(
        [
            'prefix' => LaravelLocalization::setLocale(),
            'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
        ], function(){ //...
    
    
        
        Route::get('/admin/login', [\App\Http\Controllers\Auth\LoginController::class,'showAdminLoginForm'])->name('showAdminLoginForm');
        Route::post('/admin/login', [\App\Http\Controllers\Auth\LoginController::class,'adminLogin'])->name('admin_login');
    
        
    
        Route::group(['middleware' => ['admin','auth:admin'],'prefix' => 'admin'], function(){
    
    
    
            /*products*/
            Route::group(['prefix'=>'products'],function (){
        
                Route::get('/statistics', [\App\Http\Controllers\Admin\ProductController::class,'statistics'])->name('admin.products.statistics');
                Route::get('/list/{type?}', [\App\Http\Controllers\Admin\ProductController::class,'index'])->name('admin.products');
                Route::get('/getData/{type?}', [\App\Http\Controllers\Admin\ProductController::class,'getData'])->name('admin.products.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\ProductController::class,'create'])->name('admin.products.create');
                Route::get('/settings/{product}', [\App\Http\Controllers\Admin\ProductController::class,'settings'])->name('admin.products.settings');
                Route::post('/settings/{product}', [\App\Http\Controllers\Admin\ProductController::class,'updateSettings'])->name('admin.products.updateSettings');
                Route::post('/store', [\App\Http\Controllers\Admin\ProductController::class,'store'])->name('admin.products.store');
                Route::get('/edit/{product}', [\App\Http\Controllers\Admin\ProductController::class,'edit'])->name('admin.products.edit');
                Route::post('/reOrdering', [\App\Http\Controllers\Admin\ProductController::class,'reOrdering'])->name('admin.products.reOrdering');
                Route::post('/update/{product}', [\App\Http\Controllers\Admin\ProductController::class,'update'])->name('admin.products.update');
                Route::get('/show/{product}', [\App\Http\Controllers\Admin\ProductController::class,'show'])->name('admin.products.show');
                Route::delete('/delete/{product}', [\App\Http\Controllers\Admin\ProductController::class,'destroy'])->name('admin.products.delete');
                Route::get('/getDataAchat/{type?}', [\App\Http\Controllers\Admin\ProductController::class,'getDataAchat'])->name('admin.products.getDataAchat');
                Route::get('/getDataLog/{product}/{type?}', [\App\Http\Controllers\Admin\ProductController::class,'getDataLog'])->name('admin.products.getDataLog');
                Route::get('/getDataPos/{type?}', [\App\Http\Controllers\Admin\ProductController::class,'getDataPos'])->name('admin.products.getDataPos');
                Route::get('/getDataBs/{type?}', [\App\Http\Controllers\Admin\ProductController::class,'getDataBs'])->name('admin.products.getDataBs');
            });
            /*products*/
            
            /*categories*/
            Route::group(['prefix'=>'categories'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\CategoryController::class,'index'])->name('admin.categories');
                Route::get('/getData', [\App\Http\Controllers\Admin\CategoryController::class,'getData'])->name('admin.categories.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\CategoryController::class,'create'])->name('admin.categories.create');
                Route::post('/store', [\App\Http\Controllers\Admin\CategoryController::class,'store'])->name('admin.categories.store');
                Route::get('/edit/{category}', [\App\Http\Controllers\Admin\CategoryController::class,'edit'])->name('admin.categories.edit');
                Route::post('/update/{category}', [\App\Http\Controllers\Admin\CategoryController::class,'update'])->name('admin.categories.update');
                Route::delete('/delete/{category}', [\App\Http\Controllers\Admin\CategoryController::class,'destroy'])->name('admin.categories.delete');
            });
            /*categories*/
    
            /*brands*/
            Route::group(['prefix'=>'brands'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\BrandController::class,'index'])->name('admin.brands');
                Route::get('/getData', [\App\Http\Controllers\Admin\BrandController::class,'getData'])->name('admin.brands.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\BrandController::class,'create'])->name('admin.brands.create');
                Route::post('/store', [\App\Http\Controllers\Admin\BrandController::class,'store'])->name('admin.brands.store');
                Route::get('/edit/{brand}', [\App\Http\Controllers\Admin\BrandController::class,'edit'])->name('admin.brands.edit');
                Route::post('/update/{brand}', [\App\Http\Controllers\Admin\BrandController::class,'update'])->name('admin.brands.update');
                Route::delete('/delete/{brand}', [\App\Http\Controllers\Admin\BrandController::class,'destroy'])->name('admin.brands.delete');
            });
            /*brands*/
            
            /*tastes*/
            Route::group(['prefix'=>'tastes'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\TasteController::class,'index'])->name('admin.tastes');
                Route::get('/getData', [\App\Http\Controllers\Admin\TasteController::class,'getData'])->name('admin.tastes.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\TasteController::class,'create'])->name('admin.tastes.create');
                Route::post('/store', [\App\Http\Controllers\Admin\TasteController::class,'store'])->name('admin.tastes.store');
                Route::get('/edit/{taste}', [\App\Http\Controllers\Admin\TasteController::class,'edit'])->name('admin.tastes.edit');
                Route::post('/update/{taste}', [\App\Http\Controllers\Admin\TasteController::class,'update'])->name('admin.tastes.update');
                Route::delete('/delete/{taste}', [\App\Http\Controllers\Admin\TasteController::class,'destroy'])->name('admin.tastes.delete');
            });
            /*tastes*/
            
            /*stores*/
            Route::group(['prefix'=>'stores'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\StoreController::class,'index'])->name('admin.stores');
                Route::get('/getData', [\App\Http\Controllers\Admin\StoreController::class,'getData'])->name('admin.stores.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\StoreController::class,'create'])->name('admin.stores.create');
                Route::post('/store', [\App\Http\Controllers\Admin\StoreController::class,'store'])->name('admin.stores.store');
                Route::get('/edit/{store}', [\App\Http\Controllers\Admin\StoreController::class,'edit'])->name('admin.stores.edit');
                Route::post('/update/{store}', [\App\Http\Controllers\Admin\StoreController::class,'update'])->name('admin.stores.update');
                Route::delete('/delete/{store}', [\App\Http\Controllers\Admin\StoreController::class,'destroy'])->name('admin.stores.delete');
            });
            /*stores*/
            
            /*roles*/
            Route::group(['prefix'=>'roles'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\RoleController::class,'index'])->name('admin.roles');
                Route::get('/getData', [\App\Http\Controllers\Admin\RoleController::class,'getData'])->name('admin.roles.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\RoleController::class,'create'])->name('admin.roles.create');
                Route::post('/store', [\App\Http\Controllers\Admin\RoleController::class,'store'])->name('admin.roles.store');
                Route::get('/edit/{role}', [\App\Http\Controllers\Admin\RoleController::class,'edit'])->name('admin.roles.edit');
                Route::post('/update/{role}', [\App\Http\Controllers\Admin\RoleController::class,'update'])->name('admin.roles.update');
                Route::delete('/delete/{role}', [\App\Http\Controllers\Admin\RoleController::class,'destroy'])->name('admin.roles.delete');
            });
            /*roles*/
            
            /*admins*/
            Route::group(['prefix'=>'admins'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\AdminController::class,'index'])->name('admin.admins');
                Route::get('/getData', [\App\Http\Controllers\Admin\AdminController::class,'getData'])->name('admin.admins.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\AdminController::class,'create'])->name('admin.admins.create');
                Route::post('/store', [\App\Http\Controllers\Admin\AdminController::class,'store'])->name('admin.admins.store');
                Route::get('/edit/{admin}', [\App\Http\Controllers\Admin\AdminController::class,'edit'])->name('admin.admins.edit');
                Route::post('/update/{admin}', [\App\Http\Controllers\Admin\AdminController::class,'update'])->name('admin.admins.update');
                Route::post('/updateRole/{admin}', [\App\Http\Controllers\Admin\AdminController::class,'updateRole'])->name('admin.admins.updateRole');
                Route::delete('/delete/{admin}', [\App\Http\Controllers\Admin\AdminController::class,'destroy'])->name('admin.admins.delete');
            });
            /*admins*/
            
            /*Invoices*/
            Route::group(['prefix'=>'invoices'],function (){
                Route::get('/list/', [\App\Http\Controllers\Admin\InvoiceController::class,'index'])->name('admin.invoices');
                Route::get('/getData', [\App\Http\Controllers\Admin\InvoiceController::class,'getData'])->name('admin.invoices.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\InvoiceController::class,'create'])->name('admin.invoices.create');
                Route::post('/store', [\App\Http\Controllers\Admin\InvoiceController::class,'store'])->name('admin.invoices.store');
                Route::get('/edit/{invoice}', [\App\Http\Controllers\Admin\InvoiceController::class,'edit'])->name('admin.invoices.edit');
                Route::get('/show/{invoice}', [\App\Http\Controllers\Admin\InvoiceController::class,'show'])->name('admin.invoices.show');
                Route::post('/update/{invoice}', [\App\Http\Controllers\Admin\InvoiceController::class,'update'])->name('admin.invoices.update');
                Route::delete('/delete/{invoice}', [\App\Http\Controllers\Admin\InvoiceController::class,'destroy'])->name('admin.invoices.delete');
            });
            /*Invoices*/
            
            Route::delete('/resources/delete/{image}', [\App\Http\Controllers\Admin\InvoiceController::class,'deleteResources'])->name('admin.resources.delete');
            
            /*BS*/
            Route::group(['prefix'=>'bs'],function (){
                Route::get('/list/{type?}', [\App\Http\Controllers\Admin\BsController::class,'index'])->name('admin.bs');
                Route::get('/getJournal/{type?}', [\App\Http\Controllers\Admin\BsController::class,'journal'])->name('admin.bs.journal');
                Route::get('/journal/{type?}', [\App\Http\Controllers\Admin\BsController::class,'getJournal'])->name('admin.bs.getJournal');
                Route::get('/getData/{type?}', [\App\Http\Controllers\Admin\BsController::class,'getData'])->name('admin.bs.getData');
                Route::get('/getRecap/', [\App\Http\Controllers\Admin\BsController::class,'getRecap'])->name('admin.bs.getRecap');
                Route::get('/charts/', [\App\Http\Controllers\Admin\BsController::class,'charts'])->name('admin.bs.charts');
                Route::get('/reports/', [\App\Http\Controllers\Admin\BsController::class,'reports'])->name('admin.bs.reports');
                Route::get('/daily/', [\App\Http\Controllers\Admin\BsController::class,'daily'])->name('admin.bs.daily');
                Route::get('/getDaily/', [\App\Http\Controllers\Admin\BsController::class,'getDaily'])->name('admin.bs.getDaily');
                Route::get('/create/{order?}', [\App\Http\Controllers\Admin\BsController::class,'create'])->name('admin.bs.create');
                Route::post('/store', [\App\Http\Controllers\Admin\BsController::class,'store'])->name('admin.bs.store');
                Route::get('/edit/{bs}/{type?}', [\App\Http\Controllers\Admin\BsController::class,'edit'])->name('admin.bs.edit');
                Route::get('/accept/{bs}', [\App\Http\Controllers\Admin\BsController::class,'accept'])->name('admin.bs.accept');
                Route::get('/printA4/{bs}', [\App\Http\Controllers\Admin\BsController::class,'printA4'])->name('admin.bs.printA4');
                Route::get('/printTicket/{bs}', [\App\Http\Controllers\Admin\BsController::class,'printTicket'])->name('admin.bs.printTicket');
                Route::post('/update/{bs}', [\App\Http\Controllers\Admin\BsController::class,'update'])->name('admin.bs.update');
                Route::delete('/delete/{bs}', [\App\Http\Controllers\Admin\BsController::class,'destroy'])->name('admin.bs.delete');
            });
            /*BS*/
            
            /*orders*/
            Route::group(['prefix'=>'orders'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\OrderController::class,'index'])->name('admin.orders');
                Route::get('/getData', [\App\Http\Controllers\Admin\OrderController::class,'getData'])->name('admin.orders.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\OrderController::class,'create'])->name('admin.orders.create');
                Route::post('/store', [\App\Http\Controllers\Admin\OrderController::class,'store'])->name('admin.orders.store');
                Route::get('/edit/{order}', [\App\Http\Controllers\Admin\OrderController::class,'edit'])->name('admin.orders.edit');
                Route::get('/printA4/{order}', [\App\Http\Controllers\Admin\OrderController::class,'printA4'])->name('admin.orders.printA4');
                Route::get('/printTicket/{order}', [\App\Http\Controllers\Admin\OrderController::class,'printTicket'])->name('admin.orders.printTicket');
                Route::post('/update/{order}', [\App\Http\Controllers\Admin\OrderController::class,'update'])->name('admin.orders.update');
                Route::get('/chnageStatus/{order}', [\App\Http\Controllers\Admin\OrderController::class,'chnageStatus'])->name('admin.orders.chnageStatus');
                Route::delete('/delete/{order}', [\App\Http\Controllers\Admin\OrderController::class,'destroy'])->name('admin.orders.delete');
                Route::post('/send/', [\App\Http\Controllers\Admin\OrderController::class,'send'])->name('admin.orders.send');
            });
            /*orders*/
    
            /*Bon de Livraison*/
            Route::group(['prefix'=>'bl'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\BlController::class,'index'])->name('admin.bl');
                Route::get('/daily', [\App\Http\Controllers\Admin\BlController::class,'daily'])->name('admin.bl.daily');
                Route::post('/updatebonus/{bl_detail}', [\App\Http\Controllers\Admin\BlController::class,'payBonus'])->name('admin.bl.payBonus');
                Route::post('/payBulkBonus/', [\App\Http\Controllers\Admin\BlController::class,'payBulkBonus'])->name('admin.bl.payBulkBonus');
                Route::get('/getData/{client?}', [\App\Http\Controllers\Admin\BlController::class,'getData'])->name('admin.bl.getData');
                Route::get('/getDaily', [\App\Http\Controllers\Admin\BlController::class,'getDaily'])->name('admin.bl.getDaily');
                Route::get('/roulement', [\App\Http\Controllers\Admin\BlController::class,'roulement'])->name('admin.bl.roulement');
                Route::get('/getRoulement', [\App\Http\Controllers\Admin\BlController::class,'getRoulement'])->name('admin.bl.getRoulement');
                Route::get('/create', [\App\Http\Controllers\Admin\BlController::class,'create'])->name('admin.bl.create');
                Route::post('/store', [\App\Http\Controllers\Admin\BlController::class,'store'])->name('admin.bl.store');
                Route::get('/edit/{bl}', [\App\Http\Controllers\Admin\BlController::class,'edit'])->name('admin.bl.edit');
                Route::get('/avoir/{bl}', [\App\Http\Controllers\Admin\BlController::class,'printA4'])->name('admin.bl.avoir');
                Route::get('/printA4/{bl}', [\App\Http\Controllers\Admin\BlController::class,'printA4'])->name('admin.bl.printA4');
                Route::get('/printTicket/{bl}', [\App\Http\Controllers\Admin\BlController::class,'printTicket'])->name('admin.bl.printTicket');
                Route::post('/update/{bl}', [\App\Http\Controllers\Admin\BlController::class,'update'])->name('admin.bl.update');
                Route::delete('/delete/{bl}', [\App\Http\Controllers\Admin\BlController::class,'destroy'])->name('admin.bl.delete');
            });
            /*Bon de Livraison*/
            /*Facture*/
            Route::group(['prefix'=>'facture'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\FactureController::class,'index'])->name('admin.facture');
                Route::get('/getData/{client?}', [\App\Http\Controllers\Admin\FactureController::class,'getData'])->name('admin.facture.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\FactureController::class,'create'])->name('admin.facture.create');
                Route::post('/store', [\App\Http\Controllers\Admin\FactureController::class,'store'])->name('admin.facture.store');
                Route::get('/edit/{facture}', [\App\Http\Controllers\Admin\FactureController::class,'edit'])->name('admin.facture.edit');
                Route::get('/printA4/{facture}', [\App\Http\Controllers\Admin\FactureController::class,'printA4'])->name('admin.facture.printA4');
                Route::get('/printTicket/{facture}', [\App\Http\Controllers\Admin\FactureController::class,'printTicket'])->name('admin.facture.printTicket');
                Route::post('/update/{facture}', [\App\Http\Controllers\Admin\FactureController::class,'update'])->name('admin.facture.update');
                Route::post('/delete/{facture}', [\App\Http\Controllers\Admin\FactureController::class,'destroy'])->name('admin.facture.delete');
            });
            /*Facture*/
            /*clients*/
            Route::group(['prefix'=>'clients'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\ClientController::class,'index'])->name('admin.clients');
                Route::get('/getData/{type?}', [\App\Http\Controllers\Admin\ClientController::class,'getData'])->name('admin.clients.getData');
                Route::get('/show/{client}/{year?}', [\App\Http\Controllers\Admin\ClientController::class,'show'])->name('admin.clients.show');
                Route::get('/create', [\App\Http\Controllers\Admin\ClientController::class,'create'])->name('admin.clients.create');
                Route::post('/store', [\App\Http\Controllers\Admin\ClientController::class,'store'])->name('admin.clients.store');
                Route::get('/edit/{client}', [\App\Http\Controllers\Admin\ClientController::class,'edit'])->name('admin.clients.edit');
                Route::post('/update/{client}', [\App\Http\Controllers\Admin\ClientController::class,'update'])->name('admin.clients.update');
                Route::delete('/delete/{client}', [\App\Http\Controllers\Admin\ClientController::class,'destroy'])->name('admin.clients.delete');
                Route::post('/sms/', [\App\Http\Controllers\Admin\ClientController::class,'sms'])->name('admin.clients.sms');
            });
            /*clients*/
            
            
            /*POINT OF SALE*/
            
            Route::group(['prefix'=>'pos'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\PosController::class,'index'])->name('admin.pos');
                Route::get('/daily', [\App\Http\Controllers\Admin\PosController::class,'daily'])->name('admin.pos.daily');
                Route::get('/performance', [\App\Http\Controllers\Admin\PosController::class,'performance'])->name('admin.pos.performance');
                Route::get('/performance/search', [\App\Http\Controllers\Admin\PosController::class,'search'])->name('admin.pos.performance.search');
                Route::get('/statistics/{year?}', [\App\Http\Controllers\Admin\PosController::class,'statistics'])->name('admin.pos.statistics');
                Route::get('/getData/{client?}', [\App\Http\Controllers\Admin\PosController::class,'getData'])->name('admin.pos.getData');
                Route::get('/getDaily', [\App\Http\Controllers\Admin\PosController::class,'getDaily'])->name('admin.pos.getDaily');
                Route::get('/roulement', [\App\Http\Controllers\Admin\PosController::class,'roulement'])->name('admin.pos.roulement');
                Route::get('/getRoulement', [\App\Http\Controllers\Admin\PosController::class,'getRoulement'])->name('admin.pos.getRoulement');
                Route::get('/create', [\App\Http\Controllers\Admin\PosController::class,'create'])->name('admin.pos.create');
                Route::post('/store', [\App\Http\Controllers\Admin\PosController::class,'store'])->name('admin.pos.store');
                Route::get('/edit/{pos}', [\App\Http\Controllers\Admin\PosController::class,'edit'])->name('admin.pos.edit');
                Route::get('/printA4/{pos}', [\App\Http\Controllers\Admin\PosController::class,'printA4'])->name('admin.pos.printA4');
                Route::get('/printTicket/{pos}', [\App\Http\Controllers\Admin\PosController::class,'printTicket'])->name('admin.pos.printTicket');
                Route::post('/update/{pos}', [\App\Http\Controllers\Admin\PosController::class,'update'])->name('admin.pos.update');
                Route::delete('/delete/{pos}', [\App\Http\Controllers\Admin\PosController::class,'destroy'])->name('admin.pos.delete');
            });
            /*POINT OF SALE*/
            
            /*financialcommitment*/
            Route::group(['prefix' => 'financialcommitment',], function () {
                Route::get('/list', [\App\Http\Controllers\Admin\FinancialCommitmentController::class, 'index'])->name('admin.financialcommitment.index');
                Route::get('/getData', [\App\Http\Controllers\Admin\FinancialCommitmentController::class,'getData'])->name('admin.financialcommitment.getData');
                Route::get('/edit/{fc}', [\App\Http\Controllers\Admin\FinancialCommitmentController::class, 'edit'])->name('admin.financialcommitment.edit');
                Route::post('/update/{fc}', [\App\Http\Controllers\Admin\FinancialCommitmentController::class, 'update'])->name('admin.financialcommitment.update');
                Route::delete('/delete/{fc}', [\App\Http\Controllers\Admin\FinancialCommitmentController::class,'destroy'])->name('admin.financialcommitment.delete');
            });
            /*financialcommitment*/
            
            /*caisse*/
            Route::group(['prefix' => 'caisse',], function () {
                Route::get('/', [\App\Http\Controllers\Admin\CaisseController::class, 'index'])->name('admin.caisse.index');
                Route::get('/status', [\App\Http\Controllers\Admin\CaisseController::class, 'status'])->name('admin.caisse.status');
                Route::post('/decaissement', [\App\Http\Controllers\Admin\CaisseController::class, 'decaissement'])->name('admin.decaissement.store');
                Route::post('/receive/{blPayment}', [\App\Http\Controllers\Admin\CaisseController::class, 'updatestatuts'])->name('admin.payments.updatestatuts');
            });
            /*caisse*/
            
            
            /*Facture*/
            Route::group(['prefix'=>'facture'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\FactureController::class,'index'])->name('admin.facture');
                Route::get('/getData/{client?}', [\App\Http\Controllers\Admin\FactureController::class,'getData'])->name('admin.facture.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\FactureController::class,'create'])->name('admin.facture.create');
                Route::post('/store', [\App\Http\Controllers\Admin\FactureController::class,'store'])->name('admin.facture.store');
                Route::get('/edit/{facture}', [\App\Http\Controllers\Admin\FactureController::class,'edit'])->name('admin.facture.edit');
                Route::get('/printA4/{facture}', [\App\Http\Controllers\Admin\FactureController::class,'printA4'])->name('admin.facture.printA4');
                Route::get('/printTicket/{facture}', [\App\Http\Controllers\Admin\FactureController::class,'printTicket'])->name('admin.facture.printTicket');
                Route::post('/update/{facture}', [\App\Http\Controllers\Admin\FactureController::class,'update'])->name('admin.facture.update');
                Route::post('/delete/{facture}', [\App\Http\Controllers\Admin\FactureController::class,'destroy'])->name('admin.facture.delete');
            });
            /*Facture*/
            
            
            
            
            /*staffRequests*/
            Route::group(['prefix'=>'staffRequests'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\StaffRequestController::class,'index'])->name('admin.staffRequests');
                Route::get('/getData', [\App\Http\Controllers\Admin\StaffRequestController::class,'getData'])->name('admin.staffRequests.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\StaffRequestController::class,'create'])->name('admin.staffRequests.create');
                Route::post('/store', [\App\Http\Controllers\Admin\StaffRequestController::class,'store'])->name('admin.staffRequests.store');
                Route::get('/edit/{staffRequest}/{status}', [\App\Http\Controllers\Admin\StaffRequestController::class,'edit'])->name('admin.staffRequests.edit');
                Route::post('/update/{category}', [\App\Http\Controllers\Admin\StaffRequestController::class,'update'])->name('admin.staffRequests.update');
                Route::delete('/delete/{category}', [\App\Http\Controllers\Admin\StaffRequestController::class,'destroy'])->name('admin.staffRequests.delete');
            });
            /*staffRequests*/
            
            
            
            /*profile*/
            Route::group(['prefix' => 'profile',], function () {
                Route::get('/', [\App\Http\Controllers\Admin\ProfileController::class,'edit'])->name('admin.profile.edit');
                Route::post('/', [\App\Http\Controllers\Admin\ProfileController::class,'update'])->name('admin.profile.update');});
            /*profile*/
    
    
            /*Dashboard*/
            Route::get('/', [\App\Http\Controllers\Admin\DashboardController::class,'index'])->name('admin.dashboard');
            Route::get('/statistics', [\App\Http\Controllers\Admin\DashboardController::class,'statistics'])->name('admin.statistics');
            Route::get('/daily', [\App\Http\Controllers\Admin\DashboardController::class,'daily'])->name('admin.daily');
            Route::get('/daily/search', [\App\Http\Controllers\Admin\DashboardController::class,'search'])->name('admin.daily.search');
            /*Dashboard*/
            /*banks*/
            Route::group(['prefix'=>'banks'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\BankController::class,'index'])->name('admin.banks');
                Route::get('/getData', [\App\Http\Controllers\Admin\BankController::class,'getData'])->name('admin.banks.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\BankController::class,'create'])->name('admin.banks.create');
                Route::post('/store', [\App\Http\Controllers\Admin\BankController::class,'store'])->name('admin.banks.store');
                Route::get('/edit/{bank}', [\App\Http\Controllers\Admin\BankController::class,'edit'])->name('admin.banks.edit');
                Route::post('/update/{bank}', [\App\Http\Controllers\Admin\BankController::class,'update'])->name('admin.banks.update');
                Route::delete('/delete/{bank}', [\App\Http\Controllers\Admin\BankController::class,'destroy'])->name('admin.banks.delete');
            });
            /*brands*/
    
    
            /*providers*/
            Route::group(['prefix'=>'providers'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\ProviderController::class,'index'])->name('admin.providers');
                Route::get('/getData', [\App\Http\Controllers\Admin\ProviderController::class,'getData'])->name('admin.providers.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\ProviderController::class,'create'])->name('admin.providers.create');
                Route::post('/store', [\App\Http\Controllers\Admin\ProviderController::class,'store'])->name('admin.providers.store');
                Route::get('/edit/{provider}', [\App\Http\Controllers\Admin\ProviderController::class,'edit'])->name('admin.providers.edit');
                Route::post('/update/{provider}', [\App\Http\Controllers\Admin\ProviderController::class,'update'])->name('admin.providers.update');
                Route::delete('/delete/{provider}', [\App\Http\Controllers\Admin\ProviderController::class,'destroy'])->name('admin.providers.delete');
            });
            /*providers*/
            /*Bon de Achat*/
            Route::group(['prefix'=>'achat'],function (){
                Route::get('/list/{status?}', [\App\Http\Controllers\Admin\AchatController::class,'index'])->name('admin.achat');
                Route::get('/getData', [\App\Http\Controllers\Admin\AchatController::class,'getData'])->name('admin.achat.getData');
                Route::get('/create', [\App\Http\Controllers\Admin\AchatController::class,'create'])->name('admin.achat.create');
                Route::post('/store', [\App\Http\Controllers\Admin\AchatController::class,'store'])->name('admin.achat.store');
                Route::get('/edit/{achat}', [\App\Http\Controllers\Admin\AchatController::class,'edit'])->name('admin.achat.edit');
                Route::get('/avoir/{achat}', [\App\Http\Controllers\Admin\AchatController::class,'avoir'])->name('admin.achat.avoir');
                Route::get('/printA4/{achat}', [\App\Http\Controllers\Admin\AchatController::class,'printA4'])->name('admin.achat.printA4');
                Route::get('/printTicket/{achat}', [\App\Http\Controllers\Admin\AchatController::class,'printTicket'])->name('admin.achat.printTicket');
                Route::post('/update/{achat}', [\App\Http\Controllers\Admin\AchatController::class,'update'])->name('admin.achat.update');
                Route::delete('/delete/{achat}', [\App\Http\Controllers\Admin\AchatController::class,'destroy'])->name('admin.achat.delete');
            });
            /*Bon de Achat*/
            /*financialcommitment*/
            Route::group(['prefix' => 'achatfinancialcommitment',], function () {
                Route::get('/list', [\App\Http\Controllers\Admin\AchatFinancialCommitmentController::class, 'index'])->name('admin.achatfinancialcommitment.index');
                Route::get('/getData', [\App\Http\Controllers\Admin\AchatFinancialCommitmentController::class,'getData'])->name('admin.achatfinancialcommitment.getData');
                Route::get('/edit/{fc}', [\App\Http\Controllers\Admin\AchatFinancialCommitmentController::class, 'edit'])->name('admin.achatfinancialcommitment.edit');
                Route::post('/update/{fc}', [\App\Http\Controllers\Admin\AchatFinancialCommitmentController::class, 'update'])->name('admin.achatfinancialcommitment.update');
                Route::delete('/delete/{fc}', [\App\Http\Controllers\Admin\AchatFinancialCommitmentController::class,'destroy'])->name('admin.achatfinancialcommitment.delete');
            });
            /*financialcommitment*/
        });
    });
    /*LaravelLocalization*/
    
    Route::get('admin/logout', function () {
        // Get a list of all of the defined guards.
        $guards = array_keys(config('auth.guards'));
        // Loop through each guard and logout.
        foreach ($guards as $guard) {
            $guard = app('auth')->guard($guard);
            // Not all guard types have a logout method. The SessionGuard (web) does,
            // the TokenGuard (api) does not. Only call the method if it exists
            // on the guard.
            if (method_exists($guard, 'logout')) {
                $guard->logout();
            }
        }
        return redirect()->route('admin_login');
    })->name('admin.logout');
    
    
