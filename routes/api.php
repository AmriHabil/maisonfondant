<?php
    
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Route;
    
    /*
    |--------------------------------------------------------------------------
    | API Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register API routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | is assigned the "api" middleware group. Enjoy building your API!
    |
    */

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
    ////Public Routes
    Route::post('/auth/login', [\App\Http\Controllers\API\AuthController::class, 'loginUser']);
    
    
    
    ////Protected Routes
    Route::group(['middleware'=>['auth:sanctum']],function () {
        Route::post('/orders/store', [\App\Http\Controllers\API\OrdersController::class, 'store'])->middleware('auth:sanctum');
        Route::post('/orders/status/', [\App\Http\Controllers\API\OrdersController::class, 'getstatus'])->middleware('auth:sanctum');
        Route::post('/logout', [\App\Http\Controllers\API\AuthController::class, 'logout']);
    });
    