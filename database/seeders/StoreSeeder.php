<?php
    
    namespace Database\Seeders;
    
    use App\Models\Store;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\DB;

    class StoreSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            DB::table('stores')->delete();
            
            Store::create(
                [
                    'name' => 'ShopiaX',
                    'email' => 'shopiax@matjar.tn',
                    'phone' => '56525990',
                    'address' => 'Jardin El Menzah',
                    'manager_id' => 1
                ]
            );
            Store::create(
                [
                    'name' => 'Wooden Touch',
                    'email' => 'wt@matjar.tn',
                    'phone' => '52613632',
                    'address' => 'Omrane Supérieur',
                    'manager_id' => 1
                ]
            );
            
            
        }
    }
