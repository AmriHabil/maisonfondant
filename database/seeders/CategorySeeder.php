<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = [
            [
                'name' => 'Catégorie1',
            ],
            [
                'name' => 'Catégorie2',
            ],
            [
                'name' => 'Catégorie3',
            ],
            [
                'name' => 'Sous Catégorie1-1',
                'parent_id' => 1,
            ],
            [
                'name' => 'Sous Catégorie1-2',
                'parent_id' => 1,
            ],
        ];
    
        DB::table('categories')->delete();
    
        foreach ($categories as $key => $category) {
        
            Category::create($category);
        }
    }
}
