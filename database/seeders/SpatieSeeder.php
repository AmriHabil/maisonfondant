<?php
    
    namespace Database\Seeders;
    
    use App\Models\Admin;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\DB;
    use App\Models\Permission;
    use Spatie\Permission\Models\Role;
    
    class SpatieSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            // reset cached roles and permissions
            app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
            
            DB::table('roles')->delete();
            DB::table('permissions')->delete();
            
            $roleSuperAdmin = Role::create(['name' => 'SuperAdmin', 'guard_name' => 'admin']);
            
            $models = [
                [
                    'name' => 'produits',
                    'permissions' => ['Quantités','Forcer Les Quantités']
                ],
                [
                    'name' => 'facture',
                    'permissions' => []
                ],
                [
                    'name' => 'facture avoir',
                    'permissions' => []
                ],
                [
                    'name' => 'bon de livraison',
                    'permissions' => ['Genrer Facture']
                ],
                [
                    'name' => 'bon de retour de vente',
                    'permissions' => []
                ],
                [
                    'name' => 'bon d\'achat',
                    'permissions' => []
                ],
                [
                    'name' => 'bon de retour achat',
                    'permissions' => []
                ],
                [
                    'name' => 'encaissement',
                    'permissions' => ['Détails échéance encaissement']
                ],
                [
                    'name' => 'décaissement',
                    'permissions' => ['Détails échéance décaissement']
                ],
                [
                    'name' => 'caisse',
                    'permissions' => ['Retirer', 'Supprimer retrait']
                ],
                [
                    'name' => 'inventaire',
                    'permissions' => []
                ],
                [
                    'name' => 'paramètre',
                    'permissions' => []
                ],
                [
                    'name' => 'bon de sortie',
                    'permissions' => []
                ],
    
                [
                    'name' => 'bon de réception',
                    'permissions' => []
                ],
                [
                    'name' => 'boutiques',
                    'permissions' => []
                ],
                [
                    'name' => 'categories',
                    'permissions' => []
                ],
                [
                    'name' => 'brands',
                    'permissions' => []
                ],
                [
                    'name' => 'goûts',
                    'permissions' => []
                ],
                [
                    'name' => 'admins',
                    'permissions' => []
                ],
                [
                    'name' => 'roles',
                    'permissions' => []
                ],
                [
                    'name' => 'orders',
                    'permissions' => []
                ],
               
               
                
            ];
            
            $defaultPermissions = ['Liste', 'Ajouter', 'Modifier', 'Supprimer', 'Détails', 'Imprimer'];
            
            foreach ($models as $key => $model) {
                if(!empty($model['permissions'])){
                    foreach ($model['permissions'] as $key => $permission) {
                        $permission = Permission::create(['name' => $permission, 'guard_name' => 'admin', 'type' => $model['name']]);
                        $roleSuperAdmin->givePermissionTo($permission);
                    }
                }
                foreach ($defaultPermissions as $key => $perm) {
                    $permission = Permission::create(['name' => $perm . ' ' . $model['name'], 'guard_name' => 'admin', 'type' => $model['name']]);
                    $roleSuperAdmin->givePermissionTo($permission);
                }
            }
            $admin = Admin::create(['name' => 'Admin', 'email' => 'superadmin@ah.com', 'password' => bcrypt('password'),'phone'=>'56525990']);
            $roleAdmin = Role::where(['name' => 'SuperAdmin', 'guard_name' => 'admin'])->first();
            $admin->assignRole($roleAdmin);
        }
    }
