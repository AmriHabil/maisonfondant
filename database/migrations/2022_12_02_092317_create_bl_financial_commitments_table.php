<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bl_financial_commitments', function (Blueprint $table) {
            $table->id();
            $table->integer('bl_id');
            $table->decimal('amount',20,3);
            $table->date('date');
            $table->boolean('payment_status')->default(false);
            $table->timestamps();
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bl_financial_commitments');
    }
};
