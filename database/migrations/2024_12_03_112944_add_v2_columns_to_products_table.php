<?php
    
    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;
    
    class AddV2ColumnsToProductsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('products', function (Blueprint $table) {
                $table->string('image', 255)->nullable();
                $table->integer('tva')->nullable();
                $table->integer('buying')->nullable();
               
            });
        }
        
        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn(['image']);
            });
        }
    }
