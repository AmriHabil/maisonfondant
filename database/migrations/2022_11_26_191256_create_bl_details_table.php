<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bl_details', function (Blueprint $table) {
            $table->id();
            $table->integer('bl_id');
            $table->integer('product_id');
            $table->string('product_ref')->nullable();
            $table->integer('product_category')->nullable();
            $table->string('product_name');
            $table->integer('product_quantity')->default(0);
            $table->decimal('product_tva',20,3)->default(0);
            $table->string('product_unity')->nullable();
            $table->decimal('product_remise',20,3)->default(0);
            $table->decimal('product_price_buying',20,3)->default(0);
            $table->decimal('product_price_selling',20,3)->default(0);
            $table->decimal('product_bonus',20,3)->default(0);
            $table->string('options')->nullable();
            $table->softDeletes();
            $table->timestamps();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bl_details');
    }
};
