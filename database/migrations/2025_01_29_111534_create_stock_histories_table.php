<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id');
            $table->foreignId('store_id');
            $table->integer('quantity_before'); // Quantity before the update
            $table->integer('quantity_after');  // Quantity after the update
            $table->enum('action', ['in', 'out']); // 'in' for adding, 'out' for reducing stock
            $table->string('reference')->nullable(); // Can store related order ID or reason
            $table->string('event')->nullable(); // Can store related order ID or reason
            $table->string('type')->nullable(); // Can store related order ID or reason
            $table->nullableMorphs('stockable'); // Polymorphic relation
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_histories');
    }
};
