<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bs_details', function (Blueprint $table) {
            $table->id();
            $table->integer('bs_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->string('product_ref')->nullable();
            $table->integer('product_category')->nullable();
            $table->string('product_name')->nullable();
            $table->integer('product_quantity_sent')->nullable()->default(0);
            $table->integer('product_quantity_received')->nullable()->default(0);
            $table->integer('product_quantity_delivered')->nullable()->default(0);
            $table->float('product_price')->nullable()->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bs_details');
    }
};
