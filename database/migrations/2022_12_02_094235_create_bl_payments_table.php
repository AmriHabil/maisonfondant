<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bl_payments', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->integer('store_id');
            $table->integer('bl_financial_commitment_id');
            $table->string('type')->nullable();
            $table->json('details')->nullable();
            $table->date('received_at')->nullable();
            $table->date('due_at')->nullable();
            $table->integer('number')->nullable();
            $table->string('bank')->nullable();
            $table->decimal('amount',20,3);
            $table->string('status')->nullable();
            $table->timestamps();
        });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bl_payments');
    }
};
