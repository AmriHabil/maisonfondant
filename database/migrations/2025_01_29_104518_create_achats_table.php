<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achats', function (Blueprint $table) {
            $table->id();
            $table->integer('provider_id')->nullable();
            $table->integer('store_id')->nullable();
            $table->integer('admin_id')->nullable();
            $table->string('status')->nullable()->default(NULL);
            $table->boolean('payment_status')->default(false);
            $table->date('date')->nullable();
            $table->json('provider_details')->nullable();
            $table->string('number',255)->default(0);
            $table->decimal('total',20,3)->default(0);
            $table->boolean('timbre')->default(false);
            $table->boolean('declared')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achats');
    }
};
