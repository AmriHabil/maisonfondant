<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achat_payments', function (Blueprint $table) {
            $table->id();
            $table->integer('provider_id');
            $table->integer('store_id');
            $table->integer('achat_financial_commitment_id');
            $table->string('type')->nullable();
            $table->json('details')->nullable();
            $table->date('received_at')->nullable();
            $table->date('due_at')->nullable();
            $table->integer('number')->nullable();
            $table->string('bank')->nullable();
            $table->decimal('amount',20,3);
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achat_payments');
    }
};
