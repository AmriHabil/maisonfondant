<?php
    
    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;
    
    return new class extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('orders', function (Blueprint $table) {
                $table->id();
                $table->integer('store_id')->nullable();
                $table->integer('admin_id')->nullable();
                $table->time('hour')->nullable();
                $table->date('date')->nullable();
                $table->string('name')->nullable();
                $table->integer('phone')->nullable();
                $table->integer('address')->nullable();
                $table->string('status')->nullable()->default(NULL);
                $table->text('note')->nullable();
                $table->float('total')->nullable();
                $table->timestamps();
            });
        }
        
        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('orders');
        }
    };
