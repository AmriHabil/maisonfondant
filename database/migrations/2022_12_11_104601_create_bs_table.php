<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bs', function (Blueprint $table) {
            $table->id();
            $table->integer('from_store_id')->nullable();
            $table->integer('to_store_id')->nullable();
            $table->json('sender_report')->default(
                json_encode(
                    [
                        'Fondant'=>'',
                        'Mini Fondant'=>'',
                        'Polysterene'=>'',
                        'Fondant Geant'=>'',
                        'Commandes'=>'',
                        'Consommables'=>'',
                    ]
                )
            )->nullable();
            $table->json('receiver_report')->default(
                json_encode(
                    [
                        'Fondant'=>'',
                        'Mini Fondant'=>'',
                        'Polysterene'=>'',
                        'Fondant Geant'=>'',
                        'Commandes'=>'',
                        'Consommables'=>'',
                    ]
                )
            )->nullable();
            $table->json('driver_report')->default(
                json_encode(
                    [
                        'Fondant'=>'',
                        'Mini Fondant'=>'',
                        'Polysterene'=>'',
                        'Fondant Geant'=>'',
                        'Commandes'=>'',
                        'Consommables'=>'',
                    ]
                )
            )->nullable();
            $table->integer('sender_id')->nullable();
            $table->integer('receiver_id')->nullable();
            $table->integer('driver_id')->nullable();
            $table->string('status')->nullable()->default(NULL);
            $table->date('date')->nullable();
            $table->float('total')->nullable();
            $table->float('recette')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bs');
    }
};
