<?php
    
    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;
    
    return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id')->nullable();
            $table->integer('store_id')->nullable();
            $table->integer('invoice_id')->nullable()->default(NULL);
            $table->integer('admin_id')->nullable();
            $table->string('status')->nullable()->default(NULL);
            $table->boolean('payment_status')->default(false);
            $table->date('date')->nullable();
            $table->json('client_details')->nullable();
            $table->integer('number')->default(0);
            $table->decimal('total',20,3)->default(0);
            $table->boolean('timbre')->default(false);
            $table->timestamps();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos');
    }
};
