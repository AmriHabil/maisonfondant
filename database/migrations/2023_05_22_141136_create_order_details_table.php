<?php
    
    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;
    
    return new class extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('order_details', function (Blueprint $table) {
                $table->id();
                $table->integer('order_id')->nullable();
                $table->integer('product_id')->nullable();
                $table->integer('product_ref')->nullable();
                $table->string('product_name')->nullable();
                $table->integer('product_quantity')->nullable()->default(0);
                $table->float('product_price')->nullable()->default(0);
                $table->softDeletes();
                $table->timestamps();
            });
        }
        
        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('order_details');
        }
    };
