<?php
    
    namespace App\Http\Controllers\Auth;
    
    use App\Http\Controllers\Controller;
    use App\Models\User;
    use Illuminate\Foundation\Auth\AuthenticatesUsers;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;


    class LoginController extends Controller
    {
        
        /*
        |--------------------------------------------------------------------------
        | Login Controller
        |--------------------------------------------------------------------------
        |
        | This controller handles authenticating users for the application and
        | redirecting them to your home screen. The controller uses a trait
        | to conveniently provide its functionality to your applications.
        |
        */
        
        use AuthenticatesUsers;
        
        /**
         * Where to redirect users after login.
         *
         * @var string
         */
        protected $redirectTo = '/user/dashboard';
        
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        
        public function __construct()
        {
            $this->middleware('guest')->except('logout');
            $this->middleware('guest:admin')->except('logout');
            $this->middleware('guest:web')->except('logout');
        }
        
        public function showAdminLoginForm()
        {
            return view('admin.auth.login');
        }
        
        public function adminLogin(Request $request)
        {
            
            $this->validate($request, [
                'email'   => 'required',
                'password' => 'required|min:6'
            ]);
    
    
            $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
            if (Auth::guard('admin')->attempt([$fieldType => $request->email, 'password' => $request->password])) {
    
               
                return redirect()->route('admin.dashboard');
            }
           
            return back()->withInput($request->only('Email'));
        }
        
        
        
        
        
        
        
        
        
        public function showUserLoginForm()
        {
            return view('user.auth.login');
        }
        
        public function userLogin(Request $request)
        {
            $this->validate($request, [
                'email'   => 'required|email',
                'password' => 'required'
            ]);
            $User=User::where('email',$request->email)->first();
            
            if(isset($User->Status) && $User->Status!='active')
                return redirect()->back()->with('fail', 'Votre compte n\'est pas activé encore. Veuillez contacter le service commerciale');
            if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password])) {
        
                
                return redirect()->route('user.dashboard');
            }else{
                return redirect()->back()->with('fail', 'Email ou Password invalide');
            }
    
            //return back()->withInput($request->only('email'));
            
        }
        
        
    }