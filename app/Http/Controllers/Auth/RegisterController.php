<?php
    
    namespace App\Http\Controllers\Auth;
    
    
    
    use App\Http\Controllers\Controller;
    use App\Http\Traits\CustomFunctionsTrait;
    use App\Models\Regions\Governorates;
    use App\Models\Settings\Prices;
    use App\Models\User;
    use Illuminate\Auth\Events\Registered;
    use Illuminate\Foundation\Auth\RegistersUsers;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Validator;


    class RegisterController extends Controller
    {
        /*
        |--------------------------------------------------------------------------
        | Register Controller
        |--------------------------------------------------------------------------
        |
        | This controller handles the registration of new users as well as their
        | validation and creation. By default this controller uses a trait to
        | provide this functionality without requiring any additional code.
        |
        */
        use CustomFunctionsTrait;
        use RegistersUsers;
        
        /**
         * Where to redirect users after registration.
         *
         * @var string
         */
        protected $redirectTo = '/';
        
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {
            $this->middleware('guest');
            $this->middleware('guest:admin');
            $this->middleware('guest:web');
        }
        
        /**
         * Get a validator for an incoming registration request.
         *
         * @param  array  $data
         * @return \Illuminate\Contracts\Validation\Validator
         */
        protected function validator(array $data)
        {
            return Validator::make($data, [
             
                'FullName' => 'required|string|max:50',
                'BName' => 'required|string|max:50|unique:users',
                'Number' => 'required|numeric:8|unique:users',
                'Address' => 'required|max:255',
                'Region' => 'required',
                'PatenteStatus' => 'required',
                //'CIN' => 'required|numeric:8',
                //'CIN_Copy' => 'required',
                //'RIB' => 'numeric:20',
                'email' => 'required|string|email|max:100|unique:users',
                'password' => 'required|same:c_password',
                'c_password' => 'required|same:password',
            ]);
        }
        
        
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function showUserRegisterForm()
        {
            
            $Governorates=Governorates::all();
            return view('user.auth.register',compact('Governorates'));
        }
        
        /**
         * @param array $data
         *
         * @return mixed
         */
        protected function create(array $data)
        {
            
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
        }
        
        
        /**
         * @param Request $request
         *
         * @return \Illuminate\Http\RedirectResponse
         */
        protected function createUser(Request $request)
        {
           
            $this->validator($request->all())->validate();
            
            
            $CIN_Copy='';
            $Patente='';
            $RIB_Copy='';
            $Prices=Prices::find(1);
            $Governorates=Governorates::find($request->Region);
            $user=User::create([
                'FullName' => $request->FullName,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'name' => $request->FullName,
                'BName' => $request->BName,
                'Number' => $request->Number,
                'Address' => $request->Address,
                'Region' => $request->Region,
                'PatenteStatus' => $request->PatenteStatus,
                'CIN' => $request->CIN,
                'CIN_Copy' => $CIN_Copy,
                'RIB' => $request->RIB,
                'RIB_Copy' => $RIB_Copy,
                'Patente' => $Patente,
                'HUB_id' => $Governorates->HUB_id,
                'Status' => 'pending',
                'DeliveringFees'=>$Prices->DeliveringFees,
                'ReturningFees'=>$Prices->ReturningFees,
                'ExchangingFees'=>$Prices->ExchangingFees,
                'CollectingFees'=>$Prices->CollectingFees,
                'WeightFees'=>$Prices->WeightFees,
                'SizeFees'=>$Prices->SizeFees,
                'PackageFees'=>$Prices->PackageFees,
                'WeightLimit'=>$Prices->WeightLimit,
                'CoverageFees'=>$Prices->CoverageFees
            
            ]);
            event(new Registered($user));
            return $user;
        }
    }