<?php

namespace App\Http\Controllers\Printer;

use App\Http\Controllers\Controller;
use App\Models\Orders\Manifest;
use App\Models\Orders\MU;
use App\Models\Orders\Orders;
use Illuminate\Http\Request;

class PrintOrdersController extends Controller
{
    public function print_orders(Request $request){
        $toprint = explode(",", $request->print_data);
        $Orders=Orders::whereIn('id', $toprint)->get();
        return view('print.orders',compact('Orders'));
    }
    public function print_manifest($id){
        $Manifest=Manifest::find($id);
        $Orders=Orders::where('Manifest', $id)->get();
        return view('user.manifests.show',compact('Manifest','Orders'));
    }
    
    public function print_mu(Request $request){
      
        $toprint = explode(",", $request->print_data);
        $MUS=MU::with('orders')->whereIn('id', $toprint)->get();
        return view('print.show',compact('MUS'));
    }
}
