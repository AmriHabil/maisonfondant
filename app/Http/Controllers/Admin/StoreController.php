<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStoreRequest;
use App\Http\Requests\UpdateStoreRequest;
use App\Models\Category;
use App\Models\Store;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('admin.stores.index');
    }
    
    public function getData()
    {
        $products = DB::table('stores')
            ->get();
        if (request()->ajax()) {
            return datatables()->of($products)
                ->rawColumns(['action'])
                ->addColumn('action', function ($row) {
                    $row_id = $row->id;
                    $form_id = "delete_Alert_form_" . $row_id;
                    $btn = '    <div style="display:inline-block; width: 210px;">
                                    <a class="icon-btn text-success p-1" href="edit/' . $row_id . '"><i class="fa fa-pencil-alt text-view"></i></a>
                                    <form id="'.$form_id.'" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'stores\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>
                                </div>';
                    return $btn;
                })
                ->setRowId(function ($row) {
                    return 'row'.$row->id;
                })
            
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStoreRequest $request)
    {
        
        try {
            DB::beginTransaction();
            $data=$request->all();
            $store=Store::create($data);
            DB::commit();
            return $store;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        return view('admin.stores.edit',compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStoreRequest  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStoreRequest $request, Store $store)
    {
        try {
            $data=$request->all();
            
            $store->update($data);
            return $store;
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        try {
            $store->delete();
            return 'success';
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
        
    }
}
