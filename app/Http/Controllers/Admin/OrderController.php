<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Order;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Product;
use App\Models\Quantity;
use App\Models\Store;
use App\Notifications\MinQtyNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('admin.orders.index');
    }
    
    public function getData()
    {
        $records = DB::table('orders')
            ->leftJoin('stores', 'orders.store_id', '=', 'stores.id')
            ->leftJoin('admins', 'orders.admin_id', '=', 'admins.id');
            
        if (auth()->user()->type == 'franchise') $records->where('orders.store_id', auth()->user()->store_id);
        $records->select('orders.*', 'stores.name as from','admins.name as receiver');
        $records->groupBy('orders.id');
        if (request()->ajax()) {
            return datatables()->of($records)
                ->rawColumns(['action'])
                ->addColumn('action', function ($row)  {
                    $row_id = $row->id;
                    $form_id = "delete_Alert_form_" . $row_id;
                    $printTicket = route('admin.bs.printA4', $row_id);
                    $printA4 = route('admin.bs.printA4', $row_id);
                    $btn='';
                    if($row->status!=1){
                        if (auth()->user()->can('Modifier commande'))
                            $btn.=  '<a class="icon-btn text-success p-1"  href="/admin/orders/edit/' . $row_id . '"><i class="fa fa-pencil-alt"></i></a>';
                        if (auth()->user()->can('Modifier commande'))
                            $btn.=  '<a class="icon-btn text-info p-1"  href="/admin/orders/chnageStatus/' . $row_id . '"><i class="fa fa-check"></i></a>';
                    }
                    $btn .= '<a class="icon-btn text-danger p-1"  href="/admin/orders/printA4/' . $row_id . '"><i class="fa fa-file-pdf"></i></a>';
                 
                    return $btn;
                })
                
                ->rawColumns(['from'])
                ->addColumn('from', function ($row) {
                    return $row->from;
                })
             
                
                ->editColumn('id', function ($row) {
                    return str_pad($row->id, 6, 0, STR_PAD_LEFT);
                })
                ->setRowId(function ($row) {
                    return $row->id;
                })
                ->editColumn('status', function ($row) {
                    if($row->status==1)
                    return ' <span class="badge badge-success">Livré</span>';
                    else
                    return ' <span class="badge badge-danger">En Attente</span>';
        
                })
                ->editColumn('updated_at', function ($row) {
                    $formattedDate = Carbon::parse($row->updated_at)->format('d-m-y h:i:s');
                    return $formattedDate;
                })
                ->editColumn('created_at', function ($row) {
                    $formattedDate = Carbon::parse($row->created_at)->format('d-m-y h:i:s');
                    return $formattedDate;
                })
                
                ->rawColumns(['action', 'from','status'])
                ->addIndexColumn()
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $products = DB::table('products')
            ->select('*')
            ->get();
        $stores = Store::get();
        return view('admin.orders.create', compact('products', 'stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        try {
            DB::beginTransaction();
            $order = Order::create($data);
            $total=0;
            if (isset($data['details'])) {
                foreach ($data['details'] as $key=>$value){
                    if ($data['details'][$key]['product_quantity']==0){
                        unset($data['details'][$key]);
                    }else{
                        $total+=$data['details'][$key]['product_quantity']*$data['details'][$key]['product_price'];
                    }
                }
                $data['admin_id'] = auth()->user()->id;
                $data['store_id'] = auth()->user()->store_id;
                $order->details()->createMany($data["details"]);
                $order->total=$total;
                $order->save();
            }
            DB::commit();
            return redirect()->route('admin.orders.printA4', $order->id);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }
    public function printA4(Order $order)
    {
    
        $order->load('details');
        return AppHelper::printOrderA4($order,'order');
    }
    public function printTicket(Order $order)
    {
        $order->load('details');
        return AppHelper::printTicket($order,'order');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $products = DB::table('products')
            ->select('*')
            ->get();
        $stores = Store::get();
        return view('admin.orders.edit', compact('order', 'products', 'stores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOrderRequest  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderRequest $request, Order $order)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $order->details()->delete();
            $order->update($data);
            $total=0;
            if (isset($data['details'])) {
                foreach ($data['details'] as $key=>$value){
                    if ($data['details'][$key]['product_quantity']==0){
                        unset($data['details'][$key]);
                    }else{
                        $total+=$data['details'][$key]['product_quantity']*$data['details'][$key]['product_price'];
                    }
                }
                $order->details()->createMany($data["details"]);
                $order->total=$total;
                $order->save();
            }
            DB::commit();
            return redirect()->route('admin.orders.printA4', $order->id);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
        
    }
    public function chnageStatus(Order $order)
    {
        try {
            $order->status=1;
            $order->save();
            return redirect()->back();
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
    
}
