<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Invoice;
use App\Http\Requests\StoreInvoiceRequest;
use App\Http\Requests\UpdateInvoiceRequest;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.invoices.index');
    }
    public function getData(Request $request){
        $records=Invoice::leftJoin('stores','stores.id','=','invoices.store_id');
        if (auth()->user()->type == 'franchise') $records->where('store_id', auth()->user()->store_id);
            $records->select('invoices.*','stores.name as store');
        if(request()->ajax()) {
            $dataTable=datatables()->of($records)
                ->editColumn('type', function($row){
                   if($row->type=='ras') return '<span class="badge badge-warning">RAS</span>';
                   else  return '<span class="badge badge-success">FACTURE</span>';
        
                  
                })
                ->rawColumns(['action'])
                ->addColumn('action', function($row){
                    $row_id=$row->id;
                    $form_id="delete_Alert_form_".$row_id;
                    
                    $btn = '';
                    if(auth()->user()->can('Modifier facture')) $btn.= '<a class="icon-btn text-success p-1" href="'.route('admin.invoices.edit',$row_id).'"><i class="fa fa-pencil-alt text-view"></i></a>';
                    if(auth()->user()->can('Imprimer facture') ) $btn.= '<a class="icon-btn text-info p-1" href="'.route('admin.invoices.show',$row_id).'"   target="_blank"><i class="fa fa-cloud-download-alt text-view"></i></a>';
                    if(auth()->user()->can('Supprimer facture')) $btn.=
                        '<form id="'.$form_id.'" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'invoices\');"><i class="fa fa-trash text-delete"></i></a>
                        </form>';
                        
                    return $btn;
                })
                ->setRowId(function ($row) {
                    return 'row'.$row->id;
                })
                ->rawColumns(['action','type'])
                ->addIndexColumn()
                ->make(true);
            return $dataTable;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores=Store::all();
        return view('admin.invoices.create',compact('stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreInvoiceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInvoiceRequest $request)
    {
        try {
            DB::beginTransaction();
            $data=$request->all();
            $data['created_by']=auth()->user()->name;
    
            $invoice=Invoice::create($data);
            if ($request->file('images')) {
                foreach ($request->file('images') as $key => $imagefile) {
                    $path = ImageHelper::storeFiles($imagefile,'facture/');
                    $invoice->images()->create(['url' => $path['path'],'type' => $path['extension'],'description' => $data['description'][$key], 'is_default' => $key == 0 ? true : false]);
                }
            }
                DB::commit();
            return $invoice;
        } catch (\Exception $e) {
//            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        return view('admin.invoices.show',compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        $stores=Store::all();
        return view('admin.invoices.edit',compact('invoice','stores'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateInvoiceRequest  $request
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInvoiceRequest $request, Invoice $invoice)
    {
        try {
            $data=$request->all();
            $invoice->update($data);
    
            if ($request->file('images')) {
                
                foreach ($request->file('images') as $key => $imagefile) {
                    echo 1;
                    $path = ImageHelper::storeFiles($imagefile,'facture/');
                    $invoice->images()->create(['url' => $path['path'],'type' => $path['extension'],'description' => $data['description'][$key], 'is_default' => $key == 0 ? true : false]);
                }
            }
            
            return 'success';
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        try {
            if($invoice->images){
                foreach ($invoice->images as $image){
                    ImageHelper::deleteFiles($image);
                    $image->delete();
                }
            }
            $invoice->delete();
            
            return 'success';
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    
    
    public function deleteResources(Image $image)
    {
        try {
            DB::beginTransaction();
            
            ImageHelper::deleteFiles($image);
            $image->delete();
            DB::commit();
            return 'success';
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
