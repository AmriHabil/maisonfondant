<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Http\Requests\StoreBankRequest;
use App\Http\Requests\UpdateBankRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.banks.index');
    }
    public function getData(Request $request){
        
        if(request()->ajax()) {
            $dataTable=datatables()->of(Bank::all())
                ->rawColumns(['action'])
                ->addColumn('action', function($row){
                    $row_id=$row->id;
                    $form_id="delete_Alert_form_".$row_id;
                     $btn='    <div style="display:inline-block; width: 210px;">';
    
                    if(auth()->user()->can('Modifier banque')) $btn.='      <a class="icon-btn text-success p-1" href="'.route('admin.banks.edit',$row_id).'"><i class="fa fa-pencil-alt text-view"></i></a>';
                    if(auth()->user()->can('Supprimer banque')) $btn.='<form id="'.$form_id.'" method="POST" action="" style="display:inline">
                                <input name="_method" type="hidden" value="DELETE">
                                <input name="_token" type="hidden" value="'.csrf_token().'">
                                <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'banks\');"><i class="fa fa-trash text-delete"></i></a>
                            </form>';
                     $btn.='           </div>';
                     return $btn;
                })
                ->setRowId(function ($row) {
                    return 'row'.$row->id;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
            return $dataTable;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBankRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBankRequest $request)
    {
        try {
            DB::beginTransaction();
            $data=$request->all();
            $bank=Bank::create($data);
          
            DB::commit();
            return $bank;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function show(Bank $bank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function edit(Bank $bank)
    {
        return view('admin.banks.edit',compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBankRequest  $request
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBankRequest $request, Bank $bank)
    {
        try {
            $data=$request->all();
            $bank->update($data);
            return $bank;
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        try {
            $bank->delete();
            return 'success';
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
