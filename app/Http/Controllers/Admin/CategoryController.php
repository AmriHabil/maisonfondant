<?php
    
    namespace App\Http\Controllers\Admin;
    
    use App\Helpers\ImageHelper;
    use App\Http\Controllers\Controller;
    use App\Http\Requests\StoreCategoryRequest;
    use App\Http\Requests\UpdateCategoryRequest;
    use App\Models\Category;
    use App\Models\Image;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Storage;
    
    class CategoryController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('admin.categories.index');
        }
        public function getData(Request $request){
            
            if(request()->ajax()) {
                $dataTable=datatables()->of(Category::with('parent')->select('*'))
                    ->editColumn('name',function($row){
                        if($row->parent_id==NULL) return $row->name;
                        else return $row->parent->name.' - '.$row->name;
                    })
                    ->rawColumns(['action'])
                    ->addColumn('action', function($row){
                        $row_id=$row->id;
                        $form_id="delete_Alert_form_".$row_id;
                        $btn='    <div style="display:inline-block; width: 210px;">
                                   
                                    <a class="icon-btn text-success p-1" href="'.route('admin.categories.edit',$row_id).'"><i class="fa fa-pencil-alt text-view"></i></a>
                                    <form id="'.$form_id.'" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'categories\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>
                                </div>';
                        return $btn;
                    })
                    ->setRowId(function ($row) {
                        return 'row'.$row->id;
                    })
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);
                return $dataTable;
            }
        }
        
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $categories=Category::where('parent_id', NULL)->get();
            return view('admin.categories.create',compact('categories'));
        }
        
        /**
         * Store a newly created resource in storage.
         *
         * @param  \App\Http\Requests\StoreCategoryRequest  $request
         * @return \Illuminate\Http\Response
         */
        public function store(StoreCategoryRequest $request)
        {
            try {
                DB::beginTransaction();
                $data=$request->all();
                $category=Category::create($data);
                if ($request->file('images')) {
                    foreach ($request->file('images') as $key => $imagefile) {
                        $path = ImageHelper::addAttachment($imagefile);
                        $category->images()->create(['url' => $path, 'is_default' => $key == 0 ? true : false]);
                    }
                }
                DB::commit();
                return $category;
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        /**
         * Display the specified resource.
         *
         * @param  \App\Models\Category  $category
         * @return \Illuminate\Http\Response
         */
        public function show(Category $category)
        {
            //
        }
        
        /**
         * Show the form for editing the specified resource.
         *
         * @param  \App\Models\Category  $category
         * @return \Illuminate\Http\Response
         */
        public function edit(Category $category)
        {
            $categories=Category::where('parent_id', NULL)->get();
            return view('admin.categories.edit',compact('categories','category'));
        }
        
        /**
         * Update the specified resource in storage.
         *
         * @param  \App\Http\Requests\UpdateCategoryRequest  $request
         * @param  \App\Models\Category  $category
         * @return \Illuminate\Http\Response
         */
        public function update(UpdateCategoryRequest $request, Category $category)
        {
            try {
                $data=$request->all();
                $category->update($data);
                $oldImages=$category->load('images');
                foreach ($oldImages->images as $oldImage): ImageHelper::deleteFiles($oldImage,'category'); endforeach;
                if ($request->file('images')) {
                    foreach ($request->file('images') as $key => $imagefile) {
                        $path = ImageHelper::storeFiles($imagefile,'uploads/categories/images/');
                        $category->images()->create(['url' => $path, 'is_default' => $key == 0 ? true : false]);
                    }
                }
                return $category;
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
            
            
        }
        
        /**
         * Remove the specified resource from storage.
         *
         * @param  \App\Models\Category  $category
         * @return \Illuminate\Http\Response
         */
        public function destroy(Category $category)
        {
            try {
                $category->delete();
                return 'success';
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
            
        }
    }
