<?php
    
    namespace App\Http\Controllers\Admin;
    
    use App\Helpers\AppHelper;
    use App\Helpers\QuantityHelper;
    use App\Helpers\QuantityHelperHelper;
    use App\Http\Controllers\Controller;
    use App\Http\Requests\StoreBsRequest;
    use App\Http\Requests\UpdateBsRequest;
    use App\Models\Bl;
    use App\Models\BlDetail;
    use App\Models\Bs;
    use App\Models\BsDetail;
    use App\Models\Category;
    use App\Models\Client;
    use App\Models\FinancialCommitment;
    use App\Models\Order;
    use App\Models\Product;
    use App\Models\Quantity;
    use App\Models\Quotation;
    use App\Models\Store;
    use App\Notifications\MinQtyNotification;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use function Yajra\DataTables\Html\orderBy;
    
    class BsController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index($type = 'bs')
        {
            $stores = Store::get();
            return view('admin.bs.index', compact('type', 'stores'));
        }
        
        public function journal($type = 'bs')
        {
            $stores = Store::get();
            return view('admin.bs.journal', compact('type', 'stores'));
        }
        
        
        public function getRecap(Request $request)
        {
            if (!auth()->user()->can('Historique bon de sortie')) {
                $date = [date('Y-m-d', strtotime(date('Y-m-d') . ' - 3 days')), date('Y-m-d')];
            } else {
                if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                    $date = [$request->query('from'), $request->query('to')];
                } else {
                    $date = [date('Y-m-d'), date('Y-m-d')];
                }
            }
            
            $filtred_stores = [];
            if (!isset($request->stores)) $request->stores = [];
            if ($request->has('stores')) {
                $filtred_stores = $request->stores;
            }
            $records = DB::table('stores')
                ->select(
                    'stores.name',
                    'bs.date',
                    DB::raw('SUM(CASE WHEN bs.from_store_id = stores.id THEN bs.total ELSE 0 END) AS total_sent'),
                    DB::raw('SUM(CASE WHEN bs.to_store_id = stores.id THEN bs.total ELSE 0 END) AS total_received'),
                    DB::raw('SUM(CASE WHEN bs.to_store_id = stores.id THEN bs.total ELSE 0 END) - SUM(CASE WHEN bs.from_store_id = stores.id THEN bs.total ELSE 0 END) AS total')
                )
                ->leftJoin('bs', function ($join) {
                    $join->on('stores.id', '=', 'bs.from_store_id')
                        ->orOn('stores.id', '=', 'bs.to_store_id');
                })
                ->whereIn('stores.id', $filtred_stores)
                ->whereBetween('bs.date', $date)
                ->groupBy('stores.id', 'stores.name', 'bs.date')
                ->get();
            /*if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $records->whereBetween('bs.date', [$request->query('from') , $request->query('to')]);
            }else{
                $records->whereBetween('bs.date', [date('Y-m-d'), date('Y-m-d')]);
            }
    
            if ($request->has('stores')) {
                $records->whereIn('stores.id',$request->stores);
            }*/
//            $records->groupBy('name');
            $stores = Store::get();
            return view('admin.bs.recap', compact('records', 'stores', 'request'));
        }
        
        public function charts(Request $request)
        {
            if (!isset($request->stores)) $request->stores = [];
            if ($request->has('stores')) {
                $filtred_stores = $request->stores;
            }
            $stores = Store::get();
            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $date = [$request->query('from'), $request->query('to')];
            } else {
                $date = [date('Y-m-d'), date('Y-m-d')];
            }
            $filtred_stores = [];
            if (!isset($request->stores)) $request->stores = [];
            if ($request->has('stores')) {
                $filtred_stores = $request->stores;
            } else {
                $request->stores = [];
            }
            $records = DB::table('stores')
                ->select(
                    'stores.name',
                    'bs.date',
                    DB::raw('SUM(CASE WHEN bs.from_store_id = stores.id THEN bs.total ELSE 0 END) AS total_sent'),
                    DB::raw('SUM(CASE WHEN bs.to_store_id = stores.id THEN bs.total ELSE 0 END) AS total_received'),
                    DB::raw('SUM(CASE WHEN bs.to_store_id = stores.id THEN bs.total ELSE 0 END) - SUM(CASE WHEN bs.from_store_id = stores.id THEN bs.total ELSE 0 END) AS total')
                )
                ->leftJoin('bs', function ($join) {
                    $join->on('stores.id', '=', 'bs.from_store_id')
                        ->orOn('stores.id', '=', 'bs.to_store_id');
                })
                ->whereIn('stores.id', $filtred_stores)
                ->whereBetween('bs.date', $date)
                ->groupBy('bs.date')
                ->get();


//            $daily = DB::table('bs_details')
//                ->join('bs', 'bs_details.bs_id', '=', 'bs.id')
//                ->join('products', 'bs_details.product_id', '=', 'products.id');
//            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
//                $daily->whereBetween('bs.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
//            }else{
//                $daily->whereBetween('bs.date', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59']);
//            }
//
//
//            if(auth()->user()->type == 'franchise') $request->store=auth()->user()->store_id;
//
//
            /*$daily->select(
                'bs_details.product_name','bs_details.product_price as product_price','products.value','products.category_id','bs.date',
                DB::raw('SUM(CASE WHEN bs.to_store_id = '.$request->stores[0].'  THEN bs_details.product_quantity_sent ELSE 0 END) AS total_quantity_sent'),
                DB::raw('SUM(CASE WHEN bs.from_store_id = '.$request->stores[0].'  THEN bs_details.product_quantity_sent ELSE 0 END) AS total_quantity_returned'),
                DB::raw('(SUM(CASE WHEN bs.to_store_id = 1 THEN bs_details.product_quantity_sent ELSE 0 END) - SUM(CASE WHEN bs.from_store_id = 1 THEN bs_details.product_quantity_sent ELSE 0 END)) as total_quantity')
            );*/
            
            $daily = DB::table('bs_details')
                ->join('bs', 'bs_details.bs_id', '=', 'bs.id')
                ->join('products', 'bs_details.product_id', '=', 'products.id')
                ->select(
                    'bs_details.product_name', 'bs_details.product_price as product_price', 'products.value', 'products.category_id', 'bs.date',
                    DB::raw('SUM(CASE WHEN bs.to_store_id = 1  THEN bs_details.product_quantity_sent ELSE 0 END) AS total_quantity_sent'),
                    DB::raw('SUM(CASE WHEN bs.from_store_id = 1  THEN bs_details.product_quantity_sent ELSE 0 END) AS total_quantity_returned')
                )->groupBy('products.category_id', 'bs.date')->get();
            
            return view('admin.bs.charts', compact('stores', 'request', 'records', 'daily'));
        }
        
        public function getData($type = 'bs')
        {
            $records = DB::table('bs');
            $records->leftJoin('stores As from', 'bs.from_store_id', '=', 'from.id')
                ->leftJoin('stores AS to', 'bs.to_store_id', '=', 'to.id')
                ->leftJoin('admins AS sender', 'bs.sender_id', '=', 'sender.id')
                ->leftJoin('admins AS receiver', 'bs.receiver_id', '=', 'receiver.id')
                ->leftJoin('admins AS driver', 'bs.driver_id', '=', 'driver.id')
                ->select('bs.*', 'from.name as from', 'to.name as to', 'receiver.name as receiver', 'driver.name as driver', 'sender.name as sender');
            if (!auth()->user()->can('Historique bon de sortie')) {
                $date = [date('Y-m-d', strtotime(date('Y-m-d') . ' - 3 days')), date('Y-m-d')];
                $records->whereBetween('bs.date', $date);
            }
            if (auth()->user()->type == 'franchise' && $type == 'bs')
                $records->where('bs.from_store_id', auth()->user()->store_id)
                    ->orWhere('bs.to_store_id', auth()->user()->store_id);
            elseif (auth()->user()->type == 'franchise' && $type == 'br')
                $records->orWhere('bs.from_store_id', auth()->user()->store_id);
            
            $records->groupBy('bs.id');
            // if ($type == 'bs') $records->where('from_store_id', auth()->user()->store_id);
            // else $records->where('to_store_id', auth()->user()->store_id);
            
            if (request()->ajax()) {
                return datatables()->of($records)
                    ->rawColumns(['action'])
                    ->addColumn('action', function ($row) use ($type) {
                        $row_id = $row->id;
                        $form_id = "delete_Alert_form_" . $row_id;
                        $printTicket = route('admin.bs.printA4', $row_id);
                        $printA4 = route('admin.bs.printA4', $row_id);
                        $btn = '';
                        if (auth()->user()->type == 'admin')
                            $btn .= '<a class="icon-btn text-success p-1"  href="/admin/bs/edit/' . $row_id . '/admin"><i class="fa fa-pencil-alt"></i></a>';
                        if (auth()->user()->type == 'livreur' && $row->driver_id == NULL)
                            $btn .= '<a class="icon-btn text-primary p-1"  href="/admin/bs/edit/' . $row_id . '/driver"><i class="fa fa-truck-moving"></i></a>';
                        if ((auth()->user()->type == 'franchise' || auth()->user()->type == 'admin') && $row->to_store_id == auth()->user()->store_id && $row->receiver_id == NULL)
                            $btn .= '<a class="icon-btn text-info p-1"  href="/admin/bs/edit/' . $row_id . '/receiver"><i class="fa fa-dolly-flatbed"></i></a>';
                        $btn .= '<a class="icon-btn text-danger p-1"  href="/admin/bs/printA4/' . $row_id . '"><i class="fa fa-file-pdf"></i></a>';
                        if($row->status !=1 && auth()->user()->can('Accepter bon de sortie'))
                        $btn .= '<a class="icon-btn text-warning p-1"  href="' . route('admin.bs.accept',$row_id) . '"><i class="fa fa-check"></i></a>';
                        
                        if (auth()->user()->can('Supprimer bon de sortie'))
                            $btn .= '<form id="' . $form_id . '" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="' . $row_id . '" onclick="deleteRecord(this,\'bs\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>';
                        return $btn;
                    })
                    ->rawColumns(['to'])
                    ->addColumn('to', function ($row) {
                        return $row->to;
                    })
                    ->rawColumns(['from'])
                    ->addColumn('from', function ($row) {
                        return $row->from;
                    })
                    ->editColumn('bl_id', function ($row) {
                        if($row->bl_id) return '<a href="'.route('admin.bl.printA4',$row->bl_id).'" target="_blank"><span class="badge badge-info">'.$row->bl_id.'</span></a>';
                        return '';
                    })
                    ->rawColumns(['to'])
                    ->editColumn('id', function ($row) {
                        return str_pad($row->id, 6, 0, STR_PAD_LEFT);
                    })
                    ->setRowId(function ($row) {
                        return '#row' . $row->id;
                    })
                    ->editColumn('updated_at', function ($row) {
                        $formattedDate = Carbon::parse($row->updated_at)->format('d-m-y H:i:s');
                        return $formattedDate;
                    })
                    ->editColumn('created_at', function ($row) {
                        $formattedDate = Carbon::parse($row->created_at)->format('d-m-y H:i:s');
                        return $formattedDate;
                    })
                    ->rawColumns(['action', 'from', 'to','bl_id'])
                    ->addIndexColumn()
                    ->make(true);
            }
        }
        
        public function getJournal(Request $request, $type = 'bs')
        {
            $records = Bs::leftJoin('stores As from', 'bs.from_store_id', '=', 'from.id')
                ->leftJoin('stores AS to', 'bs.to_store_id', '=', 'to.id')
                ->leftJoin('admins AS sender', 'bs.sender_id', '=', 'sender.id')
                ->leftJoin('admins AS receiver', 'bs.receiver_id', '=', 'receiver.id')
                ->leftJoin('admins AS driver', 'bs.driver_id', '=', 'driver.id');
            
            $records->select('bs.*', 'from.name as from', 'to.name as to', 'receiver.name as receiver', 'driver.name as driver', 'sender.name as sender')
                ->groupBy('bs.id');
            // if ($type == 'bs') $records->where('from_store_id', auth()->user()->store_id);
            // else $records->where('to_store_id', auth()->user()->store_id);
            
            if (request()->ajax()) {
                return datatables()->of($records)
                    ->rawColumns(['action'])
                    ->addColumn('action', function ($row) use ($type) {
                        $row_id = $row->id;
                        $form_id = "delete_Alert_form_" . $row_id;
                        $printTicket = route('admin.bs.printA4', $row_id);
                        $printA4 = route('admin.bs.printA4', $row_id);
                        $btn = '
                               <a class="icon-btn text-success p-1"  href="/admin/bs/edit/' . $row_id . '/admin"><i class="fa fa-pencil-alt"></i></a>
                               ';
                        if (auth()->user()->can('Supprimer bon de sortie'))
                            $btn .= '<form id="' . $form_id . '" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="' . $row_id . '" onclick="deleteRecord(this,\'bs\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>';
                        
                        
                        return $btn;
                    })
                    ->rawColumns(['to'])
                    ->addColumn('to', function ($row) {
                        return $row->to;
                    })
                    ->rawColumns(['from'])
                    ->addColumn('from', function ($row) {
                        return $row->from;
                    })
                    ->rawColumns(['to'])
                    ->editColumn('status', function ($row) {
                        if (
                            isset($row->sender_report['Fondant']) && isset($row->driver_report['Fondant']) && isset($row->receiver_report['Fondant']) &&
                            isset($row->sender_report['Fondor']) && isset($row->driver_report['Fondor']) && isset($row->receiver_report['Fondor']) &&
                            isset($row->sender_report['Rates']) && isset($row->driver_report['Rates']) && isset($row->receiver_report['Rates']) &&
                            isset($row->sender_report['Mini Fondant']) && isset($row->driver_report['Mini Fondant']) && isset($row->receiver_report['Mini Fondant']) &&
                            isset($row->sender_report['Polysterene']) && isset($row->driver_report['Polysterene']) && isset($row->receiver_report['Polysterene']) &&
                            isset($row->sender_report['Fondant Geant']) && isset($row->driver_report['Fondant Geant']) && isset($row->receiver_report['Fondant Geant']) &&
                            isset($row->sender_report['Commandes']) && isset($row->driver_report['Fondant']) && isset($row->receiver_report['Commandes']) &&
                            isset($row->sender_report['Consommables']) && isset($row->driver_report['Consommables']) && isset($row->receiver_report['Consommables'])
                        
                        
                        )
                            if (
                                $row->sender_report['Fondant'] == $row->driver_report['Fondant'] && $row->driver_report['Fondant'] == $row->receiver_report['Fondant']
                                &&
                                $row->sender_report['Fondor'] == $row->driver_report['Fondor'] && $row->driver_report['Fondor'] == $row->receiver_report['Fondor']
                                &&
                                $row->sender_report['Rates'] == $row->driver_report['Rates'] && $row->driver_report['Rates'] == $row->receiver_report['Rates']
                                &&
                                $row->sender_report['Mini Fondant'] == $row->driver_report['Mini Fondant'] && $row->driver_report['Mini Fondant'] == $row->receiver_report['Mini Fondant']
                                &&
                                $row->sender_report['Polysterene'] == $row->driver_report['Polysterene'] && $row->driver_report['Polysterene'] == $row->receiver_report['Polysterene']
                                &&
                                $row->sender_report['Fondant Geant'] == $row->driver_report['Fondant Geant'] && $row->driver_report['Fondant Geant'] == $row->receiver_report['Fondant Geant']
                                &&
                                $row->sender_report['Commandes'] == $row->driver_report['Commandes'] && $row->driver_report['Commandes'] == $row->receiver_report['Commandes']
                                &&
                                $row->sender_report['Consommables'] == $row->driver_report['Consommables'] && $row->driver_report['Consommables'] == $row->receiver_report['Consommables']
                            )
                                
                                return ' <span class="badge badge-success">Correcte</span>';
                        return ' <span class="badge badge-danger">Incorrecte</span>';
                        
                    })
                    ->editColumn('updated_at', function ($row) {
                        $formattedDate = Carbon::parse($row->updated_at)->format('d-m-y H:i:s');
                        return $formattedDate;
                    })
                    ->editColumn('created_at', function ($row) {
                        $formattedDate = Carbon::parse($row->created_at)->format('d-m-y H:i:s');
                        return $formattedDate;
                    })
                    ->editColumn('id', function ($row) {
                        return str_pad($row->id, 6, 0, STR_PAD_LEFT);
                    })
                    ->setRowId(function ($row) {
                        return '#row' . $row->id;
                    })
                    ->rawColumns(['resume'])
                    ->addColumn('resume', function ($row) {
                        $output = '
                        <table class="table">
                            <thead>
                            <tr>

                                    <th>Type</th>
                    
                                    <th>Expéditeur</th>
                                    <th>Livreur</th>
                                    <th>Receveur</th>
                                    <th>Etat</th>
                            </tr>
                            </thead>
                            <tbody>';
                        $output .= '
                            <tr>
                                <td>Fondant</td>
                                <td>' . ($row->sender_report['Fondant'] ?? "") . '</td>
                       
                                <td>' . ($row->driver_report['Fondant'] ?? "") . '</td>
                        
                                <td>' . ($row->receiver_report['Fondant'] ?? "") . '</td>
                      
                                <td>';
                        if (isset($row->sender_report['Fondant']) && isset($row->driver_report['Fondant']) && isset($row->receiver_report['Fondant'])) {
                            if ($row->sender_report['Fondant'] == $row->driver_report['Fondant'] && $row->driver_report['Fondant'] == $row->receiver_report['Fondant']):
                                $output .= ' <i class="fa fa-check-circle text-success"></i>';
                            else:
                                $output .= ' <i class="fa fa-minus-circle text-danger"></i>';
                            endif;
                        }
                        $output .= '
                                </td>
                    
                            </tr>';
                        $output .= '
                            <tr>
                                <td>Mini Fondant</td>
                                <td>' . ($row->sender_report['Mini Fondant'] ?? "") . '</td>
                       
                                <td>' . ($row->driver_report['Mini Fondant'] ?? "") . '</td>
                        
                                <td>' . ($row->receiver_report['Mini Fondant'] ?? "") . '</td>
                      
                                <td>';
                        if (isset($row->sender_report['Mini Fondant']) && isset($row->driver_report['Mini Fondant']) && isset($row->receiver_report['Mini Fondant'])) {
                            if ($row->sender_report['Mini Fondant'] == $row->driver_report['Mini Fondant'] && $row->driver_report['Mini Fondant'] == $row->receiver_report['Mini Fondant']):
                                $output .= ' <i class="fa fa-check-circle text-success"></i>';
                            else:
                                $output .= ' <i class="fa fa-minus-circle text-danger"></i>';
                            endif;
                        }
                        
                        $output .= '
                                </td>
                    
                            </tr>';
                        $output .= '
                            <tr>
                                <td>Polysterene</td>
                                <td>' . ($row->sender_report['Polysterene'] ?? "") . '</td>
                       
                                <td>' . ($row->driver_report['Polysterene'] ?? "") . '</td>
                        
                                <td>' . ($row->receiver_report['Polysterene'] ?? "") . '</td>
                      
                                <td>';
                        if (isset($row->sender_report['Polysterene']) && isset($row->driver_report['Polysterene']) && isset($row->receiver_report['Polysterene'])) {
                            if ($row->sender_report['Polysterene'] == $row->driver_report['Polysterene'] && $row->driver_report['Polysterene'] == $row->receiver_report['Polysterene']):
                                $output .= ' <i class="fa fa-check-circle text-success"></i>';
                            else:
                                $output .= ' <i class="fa fa-minus-circle text-danger"></i>';
                            endif;
                        }
                        $output .= '
                                </td>
                    
                            </tr>';
                        $output .= '
                            <tr>
                                <td>Fondant Geant</td>
                                <td>' . ($row->sender_report['Fondant Geant'] ?? "") . '</td>
                       
                                <td>' . ($row->driver_report['Fondant Geant'] ?? "") . '</td>
                        
                                <td>' . ($row->receiver_report['Fondant Geant'] ?? "") . '</td>
                      
                                <td>';
                        if (isset($row->sender_report['Fondant Geant']) && isset($row->driver_report['Fondant Geant']) && isset($row->receiver_report['Fondant Geant'])) {
                            if ($row->sender_report['Fondant Geant'] == $row->driver_report['Fondant Geant'] && $row->driver_report['Fondant Geant'] == $row->receiver_report['Fondant Geant']):
                                $output .= ' <i class="fa fa-check-circle text-success"></i>';
                            else:
                                $output .= ' <i class="fa fa-minus-circle text-danger"></i>';
                            endif;
                        }
                        $output .= '
                                </td>
                    
                            </tr>';
                        $output .= '
                            <tr>
                                <td>Fondor</td>
                                <td>' . ($row->sender_report['Fondor'] ?? "") . '</td>
                       
                                <td>' . ($row->driver_report['Fondor'] ?? "") . '</td>
                        
                                <td>' . ($row->receiver_report['Fondor'] ?? "") . '</td>
                      
                                <td>';
                        if (isset($row->sender_report['Fondor']) && isset($row->driver_report['Fondor']) && isset($row->receiver_report['Fondor'])) {
                            if ($row->sender_report['Fondor'] == $row->driver_report['Fondor'] && $row->driver_report['Fondor'] == $row->receiver_report['Fondor']):
                                $output .= ' <i class="fa fa-check-circle text-success"></i>';
                            else:
                                $output .= ' <i class="fa fa-minus-circle text-danger"></i>';
                            endif;
                        }
                        $output .= '
                                </td>
                    
                            </tr>';
                        $output .= '
                            <tr>
                                <td>Rates</td>
                                <td>' . ($row->sender_report['Rates'] ?? "") . '</td>
                       
                                <td>' . ($row->driver_report['Rates'] ?? "") . '</td>
                        
                                <td>' . ($row->receiver_report['Rates'] ?? "") . '</td>
                      
                                <td>';
                        if (isset($row->sender_report['Rates']) && isset($row->driver_report['Rates']) && isset($row->receiver_report['Rates'])) {
                            if ($row->sender_report['Rates'] == $row->driver_report['Rates'] && $row->driver_report['Rates'] == $row->receiver_report['Rates']):
                                $output .= ' <i class="fa fa-check-circle text-success"></i>';
                            else:
                                $output .= ' <i class="fa fa-minus-circle text-danger"></i>';
                            endif;
                        }
                        $output .= '
                                </td>
                    
                            </tr>';
                        $output .= '
                            <tr>
                                <td>Commandes</td>
                                <td>' . ($row->sender_report['Commandes'] ?? "") . '</td>
                       
                                <td>' . ($row->driver_report['Commandes'] ?? "") . '</td>
                        
                                <td>' . ($row->receiver_report['Commandes'] ?? "") . '</td>
                      
                                <td>';
                        if (isset($row->sender_report['Commandes']) && isset($row->driver_report['Commandes']) && isset($row->receiver_report['Commandes'])) {
                            if ($row->sender_report['Commandes'] == $row->driver_report['Commandes'] && $row->driver_report['Commandes'] == $row->receiver_report['Commandes']):
                                $output .= ' <i class="fa fa-check-circle text-success"></i>';
                            else:
                                $output .= ' <i class="fa fa-minus-circle text-danger"></i>';
                            endif;
                        }
                        $output .= '
                                </td>
                    
                            </tr>';
                        $output .= '
                            <tr>
                                <td>Consommables</td>
                                <td>' . ($row->sender_report['Consommables'] ?? "") . '</td>
                       
                                <td>' . ($row->driver_report['Consommables'] ?? "") . '</td>
                        
                                <td>' . ($row->receiver_report['Consommables'] ?? "") . '</td>
                      
                                <td>';
                        if (isset($row->sender_report['Consommables']) && isset($row->driver_report['Consommables']) && isset($row->receiver_report['Consommables'])) {
                            if ($row->sender_report['Consommables'] == $row->driver_report['Consommables'] && $row->driver_report['Consommables'] == $row->receiver_report['Consommables']):
                                $output .= ' <i class="fa fa-check-circle text-success"></i>';
                            else:
                                $output .= ' <i class="fa fa-minus-circle text-danger"></i>';
                            endif;
                        }
                        $output .= '
                                </td>
                    
                            </tr>';
                        $output .= '
                            <tr>
                                    <td>' . $row->from . ' -></td>
                                    <td>' . $row->to . '</td>
                                    <td>Livré par</td>
                                    <td>' . $row->driver . '</td>
                                    <td>' . $row->created_at . '</td>
                            </tr>';
                        $output .= ' </tbody>
                        </table>';
                        return $output;
                    })
                    ->editColumn('updated_at', function ($row) {
                        $formattedDate = Carbon::parse($row->updated_at)->format('d-m-y H:i:s');
                        return $formattedDate;
                    })
                    ->editColumn('created_at', function ($row) {
                        $formattedDate = Carbon::parse($row->created_at)->format('d-m-y H:i:s');
                        return $formattedDate;
                    })
                    ->rawColumns(['action', 'from', 'to', 'status', 'resume'])
                    ->addIndexColumn()
                    ->make(true);
            }
        }
        
        
        public function daily()
        {
            $stores = Store::all();
            $categories = Category::all();
            $details = BsDetail::paginate(10);
            return view('admin.bs.daily', compact('categories', 'stores', 'details'));
            
        }
        
        public function getDaily(Request $request)
        {
            
            $records = DB::table('bs_details')
                ->join('bs', 'bs_details.bs_id', '=', 'bs.id')
                ->join('products', 'bs_details.product_id', '=', 'products.id');
            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $records->whereBetween('bs.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
            } else {
                
                $records->whereBetween('bs.date', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59']);
            }
            if ($request->has('category') && $request->category != '') {
                $records->whereIn('bs_details.product_category', explode(',', $request->category));
            }
            if (!$request->has('store')) {
                $request->store = 1;
            }
            if (auth()->user()->type == 'franchise') $request->store = auth()->user()->store_id;
            
            
            $records->select(
                'bs_details.product_name', 'bs_details.product_price as product_price', 'products.value',
                DB::raw('SUM(CASE WHEN bs.to_store_id = ' . $request->store . '  THEN bs_details.product_quantity_sent ELSE 0 END) AS total_quantity_sent'),
                DB::raw('SUM(CASE WHEN bs.from_store_id = ' . $request->store . '  THEN bs_details.product_quantity_sent ELSE 0 END) AS total_quantity_returned'),
                DB::raw('(SUM(CASE WHEN bs.to_store_id = ' . $request->store . ' THEN bs_details.product_quantity_sent ELSE 0 END) - SUM(CASE WHEN bs.from_store_id = ' . $request->store . ' THEN bs_details.product_quantity_sent ELSE 0 END)) * bs_details.product_price')
            );
            
            $records->groupBy('bs_details.product_name');
            if (request()->ajax()) {
                return datatables()->of($records)
                    ->addColumn('total', function ($row) {
                        return abs($row->total_quantity_sent - $row->total_quantity_returned);
                    })
                    ->addColumn('totalPrice', function ($row) {
                        return abs(($row->total_quantity_sent - $row->total_quantity_returned) * $row->product_price);
                    })
                    ->addIndexColumn()
                    /*->withQuery('bonus', function ($filteredQuery) use($request){
                        return $filteredQuery->sum( DB::raw('(SUM(CASE WHEN bs.to_store_id = 1 THEN bs_details.product_quantity_sent ELSE 0 END) - SUM(CASE WHEN bs.from_store_id = 1 THEN bs_details.product_quantity_sent ELSE 0 END)) * bs_details.product_price'));
                    })*/
                    ->make(true);
            }
            
        }
        
        public function reports(Request $request)
        {
            $stores = Store::all();
            $categories = Category::all();
            
            $records = DB::table('bs_details')
                ->join('bs', 'bs_details.bs_id', '=', 'bs.id')
                ->join('products', 'bs_details.product_id', '=', 'products.id');
            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $records->whereBetween('bs.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
            } else {
                
                $records->whereBetween('bs.date', [date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59']);
            }
            if (isset($request->category[0]) && $request->category[0] != NULL) {
                $records->whereIn('bs_details.product_category', $request->category);
                $sortedCategories = $categories->whereIn('id', $request->category);
            } else {
                $sortedCategories = $categories;
            }
            if (!$request->has('store')) {
                $request->store = 1;
            }
            if (auth()->user()->type == 'franchise') $request->store = auth()->user()->store_id;
            
            
            $records = $records->select(
                'bs_details.product_name', 'bs_details.product_price as product_price', 'products.value', 'bs_details.product_category',
                DB::raw('SUM(CASE WHEN bs.to_store_id = ' . $request->store . '  THEN bs_details.product_quantity_sent ELSE 0 END) AS total_quantity_sent'),
                DB::raw('SUM(CASE WHEN bs.from_store_id = ' . $request->store . '  THEN bs_details.product_quantity_sent ELSE 0 END) AS total_quantity_returned'),
                DB::raw('(SUM(CASE WHEN bs.to_store_id = ' . $request->store . ' THEN bs_details.product_quantity_sent ELSE 0 END) - SUM(CASE WHEN bs.from_store_id = ' . $request->store . ' THEN bs_details.product_quantity_sent ELSE 0 END)) * bs_details.product_price as ca')
            )
                ->orderBy('total_quantity_sent')->groupBy('bs_details.product_name')->get();
            $colors = ["#1abc9c", "#4fc6e1", "#4a81d4", "#f672a7", "#f7b84b", "#6658dd", "#675aa9"];
            return view('admin.bs.reports', compact('records', 'stores', 'request', 'categories', 'sortedCategories', 'colors'));
        }
        
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create($order = 0)
        {
            
            $order = Order::find($order);
            $products = DB::table('products')
                ->select('*')
                ->orderBy('value')
                ->get();
            $stores = Store::get();
            return view('admin.bs.create', compact('products', 'stores', 'order'));
        }
        
        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\StoreBsRequest $request
         * @return \Illuminate\Http\Response
         */
        public function store(StoreBsRequest $request)
        {
            $data = $request->all();
            try {
                DB::beginTransaction();
                
                
                $data = $request->all();
                $data['sender_id'] = auth()->user()->id;
                
                $total = 0;
                $data['sender_report']['Polysterene'] = 0;
                $data['sender_report']['Fondant'] = 0;
                $data['sender_report']['Mini Fondant'] = 0;
                $data['sender_report']['Fondant Geant'] = 0;
                $data['sender_report']['Fondor'] = 0;
                $data['sender_report']['Consommables'] = 0;
                $data['sender_report']['Commandes'] = 0;
                $data['driver_report']['Polysterene'] = 0;
                $data['driver_report']['Fondant'] = 0;
                $data['driver_report']['Mini Fondant'] = 0;
                $data['driver_report']['Fondant Geant'] = 0;
                $data['driver_report']['Fondor'] = 0;
                $data['driver_report']['Consommables'] = 0;
                $data['driver_report']['Commandes'] = 0;
                $data['receiver_report']['Polysterene'] = 0;
                $data['receiver_report']['Fondant'] = 0;
                $data['receiver_report']['Mini Fondant'] = 0;
                $data['receiver_report']['Fondant Geant'] = 0;
                $data['receiver_report']['Fondor'] = 0;
                $data['receiver_report']['Consommables'] = 0;
                $data['receiver_report']['Commandes'] = 0;
                
                
                foreach ($data['details'] as $key => $value) {
                    if ($data['details'][$key]['product_quantity_sent'] > 0) {
                        $total += $data['details'][$key]['product_quantity_sent'] * $data['details'][$key]['product_price'];
                        $qt = $data['details'][$key]['product_quantity_sent'];
                        $coef = $data['details'][$key]['product_coefficient'];
                        $category = $data['details'][$key]['product_category'];
                        if ($category == 1) {
                            $data['sender_report']['Polysterene'] += $qt / $coef;
                            $data['sender_report']['Fondant'] += $qt;
                        } else if ($category == 2) {
                            $data['sender_report']['Mini Fondant'] += $qt;
                        } else if ($category == 3) {
                            $data['sender_report']['Fondant Geant'] += $qt;
                        } else if ($category == 6) {
                            $data['sender_report']['Fondor'] += $qt;
                        } else if ($category == 4) {
                            $data['sender_report']['Consommables'] += $qt;
                        } else if ($category == 5) {
                            $data['sender_report']['Commandes'] += $qt;
                        }
                        
                    } else {
                        unset($data['details'][$key]);
                    }
                }
                $bs = Bs::create($data);
                if (isset($data['details'])) {
                    $bs->details()->createMany($data["details"]);
                }
                $bs->total = $total;
                $bs->save();
                if ($data["order"] = !0) {
                    $order = Order::find($data['order']);
                    if ($order) {
                        $order->status = 1;
                        $order->save();
                    }
                    
                }
                if (isset($data['create_bl']) && $data['create_bl']==1) {
                    if($data['from_store_id']!=1 && $data['to_store_id']==1){
                       $this->createBL($request->date,$data['from_store_id']);
                    }
                }
                foreach ($bs->details as $key => $product) {
                    $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$bs->from_store_id);
                    QuantityHelper::updateQuantity($product['product_quantity_sent'],$quantity,'out',$bs,'create');
                }
                DB::commit();
                
                return redirect()->route('admin.bs.printA4', $bs->id);
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                Log::error($e->getLine());
                return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
            
            
        }
        
        /**
         * Display the specified resource.
         *
         * @param \App\Models\Bs $bs
         * @return \Illuminate\Http\Response
         */
        public function show(Bs $bs)
        {
            //
        }
        
        public function printA4(Bs $bs)
        {
            
            $bs = Bs::with('details', 'source', 'destination')->find($bs->id);
            return AppHelper::printBs($bs, 'bs');
        }
        
        
        /**
         * Show the form for editing the specified resource.
         *
         * @param \App\Models\Bs $bs
         * @return \Illuminate\Http\Response
         */
        public function edit(Bs $bs, $type = 'admin')
        {
            
            $products = DB::table('products')
                ->select('*')
                ->get();
            $stores = Store::get();
            return view('admin.bs.edit', compact('bs', 'products', 'stores', 'type'));
        }
        
        /**
         * Update the specified resource in storage.
         *
         * @param \App\Http\Requests\UpdateBsRequest $request
         * @param \App\Models\Bs $bs
         * @return \Illuminate\Http\Response
         */
        public function update(UpdateBsRequest $request, Bs $bs)
        {
            $data = $request->all();
            
            if (isset($data['details'])){
                $old_details = $bs->load('details');
                foreach ($old_details->details as $key => $product) {
                    $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$bs->from_store_id);
                    QuantityHelper::updateQuantity($product['product_quantity_sent'],$quantity,'in',$bs,'update');
                }
                $bs->details()->delete();
            }
            

            $total = 0;
            if (isset($data['details'])) {
                $data['sender_report']['Polysterene'] = 0;
                $data['sender_report']['Fondant'] = 0;
                $data['sender_report']['Mini Fondant'] = 0;
                $data['sender_report']['Fondant Geant'] = 0;
                $data['sender_report']['Fondor'] = 0;
                $data['sender_report']['Consommables'] = 0;
                $data['sender_report']['Commandes'] = 0;
                $data['driver_report']['Polysterene'] = 0;
                $data['driver_report']['Fondant'] = 0;
                $data['driver_report']['Mini Fondant'] = 0;
                $data['driver_report']['Fondant Geant'] = 0;
                $data['driver_report']['Fondor'] = 0;
                $data['driver_report']['Consommables'] = 0;
                $data['driver_report']['Commandes'] = 0;
                $data['receiver_report']['Polysterene'] = 0;
                $data['receiver_report']['Fondant'] = 0;
                $data['receiver_report']['Mini Fondant'] = 0;
                $data['receiver_report']['Fondant Geant'] = 0;
                $data['receiver_report']['Fondor'] = 0;
                $data['receiver_report']['Consommables'] = 0;
                $data['receiver_report']['Commandes'] = 0;
                foreach ($data['details'] as $key => $value) {
//                    if ($data['details'][$key]['product_quantity_sent']==0){
//                        unset($data['details'][$key]);
//                    }else{
//                        $total+=$data['details'][$key]['product_quantity_sent']*$data['details'][$key]['product_price'];
//                    }
                    $total += $data['details'][$key]['product_quantity_sent'] * $data['details'][$key]['product_price'];
                    $qt_sent = $data['details'][$key]['product_quantity_sent'];
                    $qt_delivered = $data['details'][$key]['product_quantity_delivered'];
                    $qt_received = $data['details'][$key]['product_quantity_received'];
                    $coef = $data['details'][$key]['product_coefficient'];
                    $category = $data['details'][$key]['product_category'];
                    if ($category == 1) {
                        $data['sender_report']['Polysterene'] += $qt_sent / $coef;
                        $data['driver_report']['Polysterene'] += $qt_delivered / $coef;
                        $data['receiver_report']['Polysterene'] += $qt_received / $coef;
                        $data['sender_report']['Fondant'] += $qt_sent;
                        $data['driver_report']['Fondant'] += $qt_delivered;
                        $data['receiver_report']['Fondant'] += $qt_received;
                    } else if ($category == 2) {
                        $data['sender_report']['Mini Fondant'] += $qt_sent;
                        $data['driver_report']['Mini Fondant'] += $qt_delivered;
                        $data['receiver_report']['Mini Fondant'] += $qt_received;
                    } else if ($category == 3) {
                        $data['sender_report']['Fondant Geant'] += $qt_sent;
                        $data['driver_report']['Fondant Geant'] += $qt_delivered;
                        $data['receiver_report']['Fondant Geant'] += $qt_received;
                    } else if ($category == 6) {
                        $data['sender_report']['Fondor'] += $qt_sent;
                        $data['driver_report']['Fondor'] += $qt_delivered;
                        $data['receiver_report']['Fondor'] += $qt_received;
                    } else if ($category == 4) {
                        $data['sender_report']['Consommables'] += $qt_sent;
                        $data['driver_report']['Consommables'] += $qt_delivered;
                        $data['receiver_report']['Consommables'] += $qt_received;
                    } else if ($category == 5) {
                        $data['sender_report']['Commandes'] += $qt_sent;
                        $data['driver_report']['Commandes'] += $qt_delivered;
                        $data['receiver_report']['Commandes'] += $qt_received;
                    }
                }
            }
            $bs->update($data);
            if (isset($data['details'])){
                $bs->details()->createMany($data["details"]);
                foreach ($bs->details as $key => $product) {
                    $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$bs->from_store_id);
                    QuantityHelper::updateQuantity($product['product_quantity_sent'],$quantity,'out',$bs,'update');
                }
            }
            if (auth()->user()->type == 'franchise' && $bs->to_store_id == auth()->user()->store_id) $bs->receiver_id = auth()->user()->id;
            if (auth()->user()->type == 'livreur') $bs->driver_id = auth()->user()->id;
            if (auth()->user()->type == 'admin' && $bs->to_store_id == auth()->user()->store_id) $bs->receiver_id = auth()->user()->id;
            $bs->total = $total;
            $bs->save();
            return redirect()->route('admin.bs');
        }
        
        public function accept(Bs $bs)
        {
            $old_details = $bs->load('details');
            try {
                DB::beginTransaction();
                
                foreach ($old_details->details as $key => $product) {
                    $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$bs->to_store_id);
                    QuantityHelper::updateQuantity($product['product_quantity_sent'],$quantity,'in',$bs,'accept');
                }
                $bs->status = 1;
                $bs->save();
                DB::commit();
                
                return redirect()->route('admin.bs.printA4', $bs->id);
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                Log::error($e->getLine());
                return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        /**
         * Remove the specified resource from storage.
         *
         * @param \App\Models\Bs $bs
         * @return \Illuminate\Http\Response
         */
        public function destroy(Bs $bs)
        {
            try {
                DB::beginTransaction();
                $old_details = $bs->load('details');
                foreach ($old_details->details as $key => $product) {
                    $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$bs->from_store_id);
                    QuantityHelper::updateQuantity($product['product_quantity_sent'],$quantity,'in',$bs,'delete');
                }
                foreach ($old_details->details as $key => $product) {
                    $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$bs->to_store_id);
                    QuantityHelper::updateQuantity($product['product_quantity_sent'],$quantity,'out',$bs,'delete');
                }
                $bs->details()->delete();
                $bs->delete();
                DB::commit();
                return 'success';
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return "File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage();
            }
        }
    
    
       
    
        private function createBL($date,$from_store)
        {
            // Create a BL document (could be a new table or document)
            $bl = new Bl;
            $bl->date = $date;
            $bl->store_id = $from_store;
            $bl->admin_id = auth()->user()->id;
            $bl->save();
            $client=Client::where('store_id',$from_store)->first();
            $data['client_details']['client_name']= $client->name??'';
            $data['client_details']['client_phone']= $client->phone??'';
            $data['client_details']['client_mf']= $client->mf??'';
            $data['client_details']['client_address']= $client->address??'';
            
            $bl->client_details = $data['client_details'];
            $bl->client_id = $client->id??0;
            $bl->save();
            // Fetch all the details for both sent and returned products
            $sentFromLabo = BsDetail::whereHas('bs', function ($query) use ($date, $from_store) {
                $query->where('from_store_id', 1)
                    ->where('to_store_id', $from_store)
                    ->where('date', $date)
                    ->where('bl_id', 0);
            })->get();
    
            $sentToLabo = BsDetail::whereHas('bs', function ($query) use ($date,$from_store) {
                $query->where('from_store_id', $from_store)
                    ->where('to_store_id', 1)
                    ->where('date', $date)
                    ->where('bl_id', 0);
            })->get();
    
            // Create arrays to track the quantities sent and received per product
            $sentQuantities = [];
            $receivedQuantities = [];
    
            // Loop over the sent products to accumulate the quantities sent
            foreach ($sentFromLabo as $sentDetail) {
                if (!isset($sentQuantities[$sentDetail->product_id])) {
                    $sentQuantities[$sentDetail->product_id] = 0;
                }
                $sentQuantities[$sentDetail->product_id] += $sentDetail->product_quantity_sent;
            }
    
            // Loop over the returned products to accumulate the quantities received
            foreach ($sentToLabo as $returnDetail) {
                if (!isset($receivedQuantities[$returnDetail->product_id])) {
                    $receivedQuantities[$returnDetail->product_id] = 0;
                }
                $receivedQuantities[$returnDetail->product_id] += $returnDetail->product_quantity_sent;
            }
    
    
            $total_TTC=0;
            // Now loop through the sent quantities and calculate the differences
            foreach ($sentQuantities as $productId => $sentQuantity) {
                // Get the received quantity for this product (if any)
                $receivedQuantity = isset($receivedQuantities[$productId]) ? $receivedQuantities[$productId] : 0;
        
                // Calculate the quantity difference (sent - received)
                $quantityDifference = $sentQuantity - $receivedQuantity;
        
                if ($quantityDifference > 0) {
                    // Create BL Detail only if there is a positive quantity difference
                    $sentDetail = $sentFromLabo->firstWhere('product_id', $productId);
            
                    $blDetail = new BlDetail;
                    $blDetail->bl_id = $bl->id;
                    $blDetail->product_ref = $sentDetail->product_ref;
                    $blDetail->product_id = $productId;
                    $blDetail->product_name = $sentDetail->product_name;
                    $blDetail->product_quantity = $quantityDifference;
                    $blDetail->product_tva = 19; // Example static value, replace with actual
                    $blDetail->product_remise = 0; // Example static value, replace with actual
                    $blDetail->product_price_buying = $sentDetail->product_price_buying; // Example static value, replace with actual
                    $blDetail->product_price_selling = $sentDetail->product_price;
                    $blDetail->save();
                    $total_TTC += ($sentDetail->product_price * $quantityDifference);
                }
            }
    
            $bl->total=$total_TTC;
            $bl->save();
            Bs::whereIn('id', $sentFromLabo->pluck('bs_id'))
                ->orWhereIn('id', $sentToLabo->pluck('bs_id'))
                ->update(['bl_id' => $bl->id]);
            return response()->json(['message' => 'BL generated successfully'], 200);
            
        }
    
    
    }
