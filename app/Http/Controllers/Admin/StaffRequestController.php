<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\StaffRequest;
use App\Http\Requests\StoreStaffRequestRequest;
use App\Http\Requests\UpdateStaffRequestRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StaffRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.staffRequests.index');
    }
    public function getData(Request $request){
        $records=StaffRequest::leftJoin('stores', 'staff_requests.store_id', '=', 'stores.id')->select('stores.name','staff_requests.*');
        if(auth()->user()->type="franchise") $records->where('store_id',auth()->user()->store_id);
        if(request()->ajax()) {
            $dataTable=datatables()->of($records)
               
                ->rawColumns(['action'])
                ->addColumn('action', function($row){
                    $row_id=$row->id;
                    $btn='    <div style="display:inline-block; width: 210px;">
                                   
                                    <a class="icon-btn text-success p-1" href="'.route('admin.staffRequests.edit',['staffRequest' => $row_id, 'status' => '1']).'"><i class="fa fa-check text-view"></i></a>
                                    <a class="icon-btn text-danger p-1" href="'.route('admin.staffRequests.edit',['staffRequest' => $row_id, 'status' => '2']).'"><i class="fa fa-times text-view"></i></a>
                                    
                                </div>';
                    return $btn;
                })
                ->setRowId(function ($row) {
                    return 'row'.$row->id;
                })
                ->editColumn('updated_at', function ($row) {
                    $formattedDate = Carbon::parse($row->updated_at)->format('d-m-y H:i:s');
                    return $formattedDate;
                })
                ->editColumn('created_at', function ($row) {
                    $formattedDate = Carbon::parse($row->created_at)->format('d-m-y H:i:s');
                    return $formattedDate;
                })
                ->editColumn('status', function ($row) {
                    
                        switch($row->status){
                            case 1 : return ' <span class="badge badge-success">Accepté</span>'; break;
                            case 2 : return ' <span class="badge badge-danger">Refusé</span>'; break;
                            default : return ' <span class="badge badge-warning">En Attente</span>'; break;
                            
                        }
                
                        
        
                })
                ->rawColumns(['action','status'])
                ->addIndexColumn()
                ->make(true);
            return $dataTable;
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.staffRequests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStaffRequestRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStaffRequestRequest $request)
    {
        try {
            DB::beginTransaction();
            $data=$request->all();
            $data['admin_id']=auth()->user()->id;
            $data['store_id']=auth()->user()->store_id;
            $category=StaffRequest::create($data);
           
            DB::commit();
            return $category;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StaffRequest  $staffRequest
     * @return \Illuminate\Http\Response
     */
    public function show(StaffRequest $staffRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StaffRequest  $staffRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(StaffRequest $staffRequest,$status)
    {
        $staffRequest->status=$status;
        $staffRequest->save();
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStaffRequestRequest  $request
     * @param  \App\Models\StaffRequest  $staffRequest
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStaffRequestRequest $request, StaffRequest $staffRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StaffRequest  $staffRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaffRequest $staffRequest)
    {
        //
    }
}
