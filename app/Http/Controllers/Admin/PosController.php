<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AppHelper;
use App\Helpers\QuantityHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlRequest;
use App\Http\Requests\UpdateBlRequest;
use App\Models\Bl;
use App\Models\Expense;
use App\Models\Pos;
use App\Models\PosDetail;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Client;
use App\Models\Facture;
use App\Models\FinancialCommitment;
use App\Models\Order;
use App\Models\Pack;
use App\Models\Product;
use App\Models\Quantity;
use App\Models\Quotation;
use App\Models\Store;
use App\Notifications\MinQtyNotification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('admin.pos.index');
    }
    public function daily()
    {
        
        return view('admin.pos.daily');
    }
    public function performance(){
        try {
            $Hubs=Store::all();
            
            $mus=Pos::select('id','admin_id','total','store_id','date')->withCount('details')->withCount(
                [
                    'details as sumBuying' => function($query) {
                        $query->select(DB::raw('SUM(`product_price_buying`*`product_quantity`)'));
                    }
                ])->whereDate('date', Carbon::today())->get();
           
           
            $days=30;
            return view('admin.dashboard.daily',compact('mus','Hubs','days'));
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
        
    }
    
    public function search(Request $request){
        $from = date("Y-m-d", time() - 604800);
        $to = date("Y-m-d", time() + 86400);
        if ($request->from) {
            $from = date($request->from);
        }
        if ($request->to) {
            $to = date($request->to, time() + 86400);
        }
        $Hubs=Store::all();
        $mus=Pos::select('id','admin_id','total','store_id','date')->withCount('details')->withCount(['details as sumBuying' => function($query) {
            $query->select(DB::raw('SUM(`product_price_buying`*`product_quantity`)'));
        }
        ])->whereBetween('date', [$from . ' 00:00:00', $to . ' 23:59:59']);
       
        if ($request->Hubs != NULL && !empty($request->Hubs)) {
            $mus->whereIn('store_id', $request->Hubs);
        }
        $mus=$mus->get();
      
        $days=round(( strtotime($to . ' 23:59:59') - strtotime($from . ' 00:00:00') ) / (60 * 60 * 24));
        return view('admin.dashboard.daily',compact('mus','Hubs','days'));
        
    }
    public function statistics($year=2024){
        try {
            
          
           
          
            
            $now=Carbon::now();
//            $topproducts = DB::table('products')
//                ->leftJoin('bl_details','products.id','=','bl_details.product_id')
//                ->selectRaw('products.name , products.detail, COALESCE(sum(bl_details.product_quantity),0) total')
//                ->groupBy('products.id')
//                ->orderBy('total','desc')
//                ->take(20)
//                ->get();
            
            
            $stores=Store::all();
//            return DB::table('bls')->selectRaw('sum(total)')->groupBy('store_id')->get();
            $bls = Pos::select(
                DB::raw('year(date) as year'),
                DB::raw('month(date) as month'),
                DB::raw('sum(total) as total'),
                'store_id'
            )
                ->where(DB::raw('year(date)'), '=', $year)
                ->groupBy('year','store_id')
                ->groupBy('month')
                ->get();
            
            $monthly = Pos::select(
                DB::raw('day(date) as day'),
                DB::raw('month(date) as month'),
                DB::raw('sum(total) as total'),
                'store_id'
            )
                ->where(DB::raw('month(date)'), '=', $now->month)
                ->where(DB::raw('year(date)'), '=', $year)
                ->groupBy('store_id')
                ->groupBy('date')
                ->get();
         
            
          
            return view('admin.pos.statistics',compact('bls','stores','monthly','now'));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
        
        
        
    }
    
    public function getData(Request $request,$client=0)
    {
        $pos=DB::table('pos')
            ->leftJoin('stores', 'pos.store_id', '=', 'stores.id')
            ->leftJoin('admins', 'pos.admin_id', '=', 'admins.id')
            ->select('pos.*','stores.name as store','admins.name as admin');
        if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
            $pos->whereBetween('pos.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
        }
        if ($request->has('status') && $request->status!='all') {
            $pos->where('payment_status',$request->status );
        }
        if ($request->has('client') && $request->client!='') {
            $pos->whereRaw('lower(stores.name) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->client))))]);
        }
        if($client!=0) $pos->where('pos.client_id',$client);
        if(!in_array(auth()->user()->phone,['56525990'])) $pos->where('pos.store_id',auth()->user()->store_id);
        if(auth()->user()->hasRole('Vendeur')) $pos->where('date',date('Y-m-d'));
        $pos->groupBy('pos.id');
        $blsToCount=$pos;
        
        if (request()->ajax()) {
            return datatables()->of($pos)
                ->rawColumns(['action'])
                ->addColumn('action', function ($row) {
                    $row_id = $row->id;
                    $form_id = "delete_Alert_form_" . $row_id;
                    $printTicket=route('admin.pos.printA4',$row_id );
                    $printA4=route('admin.pos.printA4',$row_id );
                    $btn = '<div style="display:inline-block; width: 210px;">';
                    if(auth()->user()->can('Imprimer bon de livraison'))
                        $btn.='<a class="icon-btn text-danger p-1"  href="' . route('admin.pos.printA4',$row_id) . '"><i class="fa fa-file-pdf"></i></a><a class="icon-btn text-info p-1"  href="' . route('admin.pos.printTicket',$row_id) . '"><i class="fa fa-tag"></i></a>';
                   
                        if(auth()->user()->can('Modifier bon de livraison'))
                            $btn .='<a class="icon-btn text-success p-1" href="' . route('admin.pos.edit',$row_id)  . '"><i class="fa fa-pencil-alt text-view"></i></a>';
                    
                    if($row->payment_status!=0){
                           if(auth()->user()->can('Ajouter bon de retour'))
                           $btn .='<a class="icon-btn text-success p-1" href="avoir/' . $row_id . '"><i class="fa fa-redo text-view"></i></a>';
                    }
                        if($row->invoice_id!=NULL)
                        if(auth()->user()->can('Imprimer bon de livraison'))
                        $btn .='<a class="icon-btn text-success p-1" href="/admin/facture/printA4/' . $row->invoice_id . '"><i class="fa fa-file-excel text-view"></i></a>';
                    if(auth()->user()->can('Supprimer bon de livraison'))
                        $btn .='<form id="' . $form_id . '" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'bl\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>';
                         $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['checkbox'])
                ->addColumn('checkbox', function ($row) {
                    if($row->invoice_id!=NULL)
                    return '';
                    return ' <div class="checkbox checkbox-info mb-2">
                                 <input id="checkbox-'.$row->id.'" type="checkbox" class="order_check"
                                                   name="bls[]" data-id="'.$row->id.'" value="'.$row->id.'">
                                 <label for="checkbox-'.$row->id.'"></label>
                             </div>';
                })
           
                ->editColumn('client_details', function ($row) {
                    $array= json_decode($row->client_details);
                    return strtoupper($array->client_name);
                })
                ->rawColumns(['store'])
                ->addColumn('store', function ($row) {
                    return $row->store;
                })
    
                ->editColumn('payment_status', function ($row) {
                    $btn = ' <div>';
                    if ($row->payment_status=='0') $btn.= '<span class="badge badge-danger">Non Payé</span>';
                    elseif ($row->payment_status=='1') $btn.= '<span class="badge badge-warning">Partiellement Payé</span>';
                    else $btn.= '<span class="badge badge-success">Totalement Payé</span>';
                    return $btn.'</div>';
                })
                ->editColumn('id', function ($row) {
                    return str_pad($row->id,6,0,STR_PAD_LEFT);
                })
                ->editColumn('date', function ($row) {
                    return \Carbon\Carbon::parse($row->date)->format('d/m/Y');
                })
                ->setRowId(function ($row) {
                    return 'row'.$row->id;
                })
                ->with('order', function ()  use($request,$pos){
                    $blsToCount=new Pos();
                    $blsToCount=$blsToCount->leftJoin('stores','stores.id','=','pos.store_id');
                    $all=$blsToCount;
                    $paid=$blsToCount;
                    $_paid=$blsToCount;
                    $unpaid=$blsToCount;
                    if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                        $all=$all->whereBetween('pos.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                    }
                    if ($request->has('status') && $request->status!='all') {
                        $all=$all->where('payment_status',$request->status );
                    }
                    if ($request->has('client') && $request->client!='') {
                        $all=$all->whereRaw('lower(stores.name) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->client))))]);
                    }
                        $order['all']=$all->sum('total');
                    if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                        $paid=$paid->whereBetween('pos.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                    }
                    if ($request->has('status') && $request->status!='all') {
                        $paid=$paid->where('payment_status',$request->status );
                    }
                    if ($request->has('client') && $request->client!='') {
                        $paid=$paid->whereRaw('lower(stores.name) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->client))))]);
                    }
                        $order['paid']=$paid->where('payment_status','2')->sum('total');
                    if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                        $_paid=$_paid->whereBetween('pos.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                    }
                    if ($request->has('status') && $request->status!='all') {
                        $_paid=$_paid->where('payment_status',$request->status );
                    }
                    if ($request->has('client') && $request->client!='') {
                        $_paid=$_paid->whereRaw('lower(stores.name) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->client))))]);
                    }
                        $order['p_paid']=$_paid->where('payment_status','1')->sum('total');
                    if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                        $unpaid=$unpaid->whereBetween('pos.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                    }
                    if ($request->has('status') && $request->status!='all') {
                        $unpaid=$unpaid->where('payment_status',$request->status );
                    }
                    if ($request->has('client') && $request->client!='') {
                        $unpaid=$unpaid->whereRaw('lower(stores.name) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->client))))]);
                    }
                        $order['unpaid']=$unpaid->where('payment_status','0')->sum('total');
                       
                        return $order;
                    
                })
                ->rawColumns(['action','payment_status','store','checkbox'])
                ->addIndexColumn()
                ->make(true);
        }
    }
    public function getDaily(Request $request)
    {
        
         $bl= Pos::where('store_id',auth()->user()->store_id)->with('details');
        if(auth()->user()->hasRole('Vendeur')) $bl=$bl->where('date',date('Y-m-d'));
//        if($request->has('details')){
//            $bl->whereHas('details',function($query) use ($request){
//                $query->where('name','like','%'.$request->columns[2]['search']['value'].'%');
//            });
//        }
        if (request()->ajax()) {
            return datatables()->of(
                $bl
            )
                ->rawColumns(['action'])
                ->addColumn('action', function ($row) {
                    $row_id = $row->id;
                    $form_id = "delete_Alert_form_" . $row_id;
                    $printTicket=route('admin.pos.printA4',$row_id );
                    $printA4=route('admin.pos.printA4',$row_id );
                    $btn = '<div style="display:inline-block; width: 210px;">';
                    if(auth()->user()->can('Imprimer bon de livraison'))
                        $btn.='<a class="icon-btn text-danger p-1"  href="printA4/' . $row_id . '"><i class="fa fa-file-pdf"></i></a><a class="icon-btn text-info p-1"  href="printTicket/' . $row_id . '"><i class="fa fa-tag"></i></a>';
    
                    if(auth()->user()->can('Modifier bon de livraison'))
                        $btn .='<a class="icon-btn text-success p-1" href="' . route('admin.pos.edit',$row_id)  . '"><i class="fa fa-pencil-alt text-view"></i></a>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['details'])
                ->addColumn('details', function ($row) {
                    $btn='<table class="table  table-bordered mb-0">';
                    foreach ($row->details as $detail){
                        $btn.='<tr>
                        <td width="500px">'.$detail->product_name.'</td>
                        <td width="50px">'.$detail->product_quantity.'</td>
                        <td width="50px">'.($detail->product_price_selling-$detail->product_remise)*$detail->product_quantity.'</td> </tr>';
                    }
                    $btn.='</table>';
                    return $btn;
                })
    
                ->editColumn('client_details', function ($row) {
                    
                    return strtoupper($row->client_details['client_name']);
                })
                
                
               
                ->editColumn('id', function ($row) {
                    return str_pad($row->id,6,0,STR_PAD_LEFT);
                })
                ->editColumn('date', function ($row) {
                    return \Carbon\Carbon::parse($row->date)->format('d/m/Y');
                })
                ->setRowId(function ($row) {
                    return 'row'.$row->id;
                })
                ->rawColumns(['action','details'])
                ->addIndexColumn()
                ->make(true);
        }
        
    }
    public function roulement (Request $request){
        $categories = Category::all();
        $brands = Brand::all();
        $stores = Store::all();
        return view('admin.pos.roulement', compact('categories', 'stores','brands','stores'));
    
    }
    public function getRoulement(Request $request) {
        // Default date range
        if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
            $from = $request->query('from') . ' 00:00:00';
            $to = $request->query('to') . ' 23:59:59';
        } else {
            $from = date('Y-m-d') . ' 00:00:00';
            $to = date('Y-m-d') . ' 23:59:59';
        }
        
        $products = DB::table('products AS P')
            ->select(
                'P.name as product_name',
                DB::raw('COALESCE(SUM(DetailTotals.Total), 0) AS total_pos_pieces_sold')
            )
            ->groupBy('P.id'); // Group by product ID to avoid combining totals of different products
        
        // Apply store filter if provided
        if ($request->has('store_id') && $request->store_id != '') {
            $products = $products->leftJoin(DB::raw('(SELECT pos_details.product_id, SUM(pos_details.product_quantity) AS Total
            FROM pos_details
            WHERE pos_details.created_at BETWEEN ? AND ?
            AND pos_details.pos_id IN (SELECT id FROM pos WHERE pos.store_id = ?)
            GROUP BY pos_details.product_id) AS DetailTotals'), function($join) {
                $join->on('DetailTotals.product_id', '=', 'P.id');
            });
            $products = $products->setBindings([$from, $to, $request->store_id]);
        } else {
            $products = $products->leftJoin(DB::raw('(SELECT pos_details.product_id, SUM(pos_details.product_quantity) AS Total
            FROM pos_details
            WHERE pos_details.created_at BETWEEN ? AND ?
            GROUP BY pos_details.product_id) AS DetailTotals'), 'DetailTotals.product_id', '=', 'P.id');
            $products = $products->setBindings([$from, $to]);
        }
        
        // Apply category filter if provided
        if ($request->has('category_id') && $request->category_id != '') {
            $products = $products->whereIn('P.category_id', $request->category_id);
        }
        
        // Apply group name search filter if provided
        if ($request->has('group_name') && $request->group_name != '') {
            $groupName = strtolower(implode(' *', str_split(str_replace(' ', '', $request->group_name))));
            $products = $products->whereRaw('lower(P.name) regexp ?', [$groupName]);
        }
        
        // Ordering and returning results
        $products = $products->orderByDesc('total_pos_pieces_sold');
        
        if (request()->ajax()) {
            return datatables()->of($products)
                ->addIndexColumn()
                ->make(true);
        }
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
     
        $products = Product::leftJoin('quantities', 'products.id', '=', 'quantities.product_id')
            ->where('quantities.store_id', auth()->user()->store_id)
            ->select('products.*','quantities.quantity')
            ->orderBy('value')->get();
        $categories = Category::where('status',1)->get();
        $clients = Client::get();
        
        
        return view('admin.pos.create',compact('products','categories','clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBlRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBlRequest $request)
    {
        $data = $request->all();
        $client=Client::where('phone',$data['client_details']['client_phone'])->first();
        if(!$client){
            $client['phone']=$data['client_details']['client_phone'];
            $client['mf']=$data['client_details']['client_mf'];
            $client['address']=$data['client_details']['client_address'];
            $client['name']=$data['client_details']['client_name'];
            $client=Client::create($client);
            $data['client_id']=$client->id;
        }
      
        try {
            DB::beginTransaction();
            
            if (isset($data['details'])) {
                $number = count($data["details"]);
                if($request->piece_type!='facture')
                $data['number'] = $number;
                $data['store_id'] = auth()->user()->store_id;
                $data['admin_id'] = auth()->user()->id;
                $cart = Pos::create($data);
                $total_brut=0;
                $total_TTC=0;
                foreach($data['details'] as $item){
                    $total_brut+= ($item['product_price_selling']* $item['product_quantity']);
                    $after_discount= ($item['product_price_selling']-$item['product_remise']);
                    $total_TTC += ($after_discount * $item['product_quantity']);
                }
                $cart->details()->createMany($data["details"]);
                $cart->client_details['client_name'];
                $cart->total=$total_TTC;
                $cart->save();
               
                    /*Update Quantities*/
                    if (isset($data['details'])){
                        foreach ($cart->details as $key => $product) {
                            $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,auth()->user()->store_id);
                            QuantityHelper::updateQuantity($product['product_quantity'],$quantity,'out',$cart,'create');
                            
                        }
                    }
                    
                    /*Update Quantities*/
                    /*Store Payment Details*/
                    if (isset($data['commitmentType']) && $data['commitmentType'] == 'echeance') {
                        $cart->commitments()->createMany($data['echeance']);
                        
                     
                    }
                    elseif (isset($data['commitmentType']) && $data['commitmentType'] == 'comptant') {
                        $finacial_data['pos_id'] = $cart->id;
                        $finacial_data['date'] = date('Y-m-d H:i:s');
                        $finacial_data['amount'] = $total_TTC;
                        
                        $finacial = FinancialCommitment::create($finacial_data);
                        $cash = 0;
                        $check = 0;
                        $transfer = 0;
                        $sodexo = 0;
                        $payment = collect($request->payments);
        
                        if (isset($data['payments']['cash'])) {
                            $count = count($data['payments']['cash']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['cash'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['cash'][$i + 1]['type'] = 'cash';
                                $data['payments']['cash'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['cash']);
                            $cashs = collect($payment['cash']);
                            $cash = $cashs->sum('amount');
                        }
                        if (isset($data['payments']['check'])) {
                            $count = count($data['payments']['check']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['check'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['check'][$i + 1]['type'] = 'check';
                                $data['payments']['check'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['check']);
                            $checks = collect($payment['check']);
                            $check = $checks->sum('amount');
                        }
                        if (isset($data['payments']['transfer'])) {
                            $count = count($data['payments']['transfer']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['transfer'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['transfer'][$i + 1]['type'] = 'transfer';
                                $data['payments']['transfer'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
                            
                            $finacial->payments()->createMany($data['payments']['transfer']);
                            $transfers = collect($payment['transfer']);
                            $transfer = $transfers->sum('amount');
                        }
                        if (isset($data['payments']['sodexo'])) {
                            $count = count($data['payments']['sodexo']);
                            for ($i = 0; $i < $count; $i++) {
                                
                                $data['payments']['sodexo'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['sodexo'][$i + 1]['type'] = 'sodexo';
                                $data['payments']['sodexo'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['sodexo']);
                            $sodexos = collect($payment['sodexo']);
                            $sodexo = $sodexos->sum('amount');
                        }
                        $total_payments = ($cash + $check + $transfer + $sodexo);
                        if ($total_payments == $total_TTC){
                            $cart->update(['payment_status' => 2]);
                            $finacial->update(['payment_status' => 2]);
                        }
                        elseif ($total_payments<$total_TTC && $total_payments!=0 )
                        {
                            $cart->update(['payment_status' => 1]);
                            $finacial->update(['payment_status' => 1]);
                        }else{
                            $cart->update(['payment_status' => 0]);
                            $finacial->update(['payment_status' => 0]);
                        }
                    }
                    /*Store Payment Details*/
                
            }
            DB::commit();
            if($request->submit=='a4')
                return redirect()->route('admin.pos.printA4',$cart->id);
            else
                return redirect()->route('admin.pos.printTicket',$cart->id);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
        
        
    }
 
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pos  $bl
     * @return \Illuminate\Http\Response
     */
    public function show(Pos $bl)
    {
        //
    }
    public function printA4(Pos $bl)
    {
        
        $bl->load('details');
        return AppHelper::printA4($bl,'bl');
    }
    public function printTicket(Pos $pos)
    {
        $pos->load('details');
        return AppHelper::printTicket($pos,'bl');
    }
    
    public function payBonus (PosDetail $bl_detail){
        try {
            $bl_detail->options = 'paid';
            $bl_detail->save();
            return 'success';
        }catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
     
        }
    }
    
    public function payBulkBonus (Request $request){
        try {
            $bl_detail =PosDetail::whereIn('id' , explode(",", $request->payBonus))->update(['options'=>'paid']);
            return 'success';
        }catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pos  $bl
     * @return \Illuminate\Http\Response
     */
    public function edit(Pos $bl)
    {
        $bonus = Product::with('boosters')->whereHas('boosters', function ($query) {
            $query->where('admin_id', auth()->user()->id);
        })->get();
        $products = DB::table('products')
            ->leftJoin('quantities', 'products.id', '=', 'quantities.product_id')
            ->leftJoin('product_boosters', function ($join) {
                $join->on('products.id', '=', 'product_boosters.product_id')
                    ->where('product_boosters.admin_id', '=', auth()->user()->id);
            })
            ->select('products.id','products.name','products.ref','products.barcode','products.buying','products.unity','products.tva', 'quantities.quantity', 'products.detail', 'products.sgros', 'products.gros', 'product_boosters.bonus as bonus', 'products.min_price', 'products.bonus_for')
            ->where('quantities.store_id', auth()->user()->store_id)
            ->where('quantities.quantity', '>',0)
        
            ->groupBy('products.id')
            ->get();
        $clients = DB::table('clients')
            ->select('clients.id', 'clients.name', 'clients.phone', 'clients.mf', 'clients.address', 'clients.plafond','clients.remise','clients.price',  DB::raw('SUM(`bls`.`total`) as `encours`' ))
            ->leftJoin('bls', 'clients.id', '=', 'pos.client_id')
            ->groupBy('clients.id', 'clients.name', 'clients.mf')->limit(1)
            ->get();
        $packs = Pack::all();
        
        
        return view('admin.pos.edit',compact('bl','products','clients','packs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBlRequest  $request
     * @param  \App\Models\Pos  $bl
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBlRequest $request, Pos $bl)
    {
        $data = $request->all();
        $client=Client::where('phone',$data['client_details']['client_phone'])->first();
        if(!$client){
            $client['phone']=$data['client_details']['client_phone'];
            $client['mf']=$data['client_details']['client_mf'];
            $client['address']=$data['client_details']['client_address'];
            $client['name']=$data['client_details']['client_name'];
            $client=Client::create($client);
            $data['client_id']=$client->id;
        }
        try {
            DB::beginTransaction();
        
        
            if (isset($data['details'])) {
                $old_details = $bl->load('details');
                foreach ($bl->details as $key => $product) {
                    $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$bl->store_id);
                    QuantityHelper::updateQuantity($product['product_quantity'],$quantity,'in',$bl,'update');
                }
                $bl->details()->delete();
                $bl->payments()->delete();
                $bl->commitments()->delete();
                
                $number = count($data["details"]);
                $data['number'] = $number;
               $data['store_id'] = $old_details->store_id;
                //$data['admin_id'] = auth()->user()->id;
                $bl->update($data);
    
                
                $bl->client_details['client_name'];
                $total_brut=0;
                $total_TTC=0;
    
                foreach($data['details'] as $item){
                    //$after_discount= ($item['product_price_selling']*(100-$item['product_remise']))/100;
                    $total_brut+= ($item['product_price_selling']* $item['product_quantity']);
                    $after_discount= ($item['product_price_selling']-$item['product_remise']);
                    $total_TTC += ($after_discount * $item['product_quantity']);
                }
                $bonus_status=1;
                if($total_brut!=0)
                {
                    $remise_total=$total_TTC/$total_brut;
                    if($remise_total < 0.9  && $remise_total > 0.85 ){
                        $bonus_status=0.5;
                    }
                    elseif($remise_total < 0.85   ){
                        $bonus_status=0;
                    }else{
                        $bonus_status=1;
                    }
                }
          
    
                $bl->details()->createMany($data["details"]);
                $bl->total=$total_TTC;
                $bl->save();
                if ($request->piece_type=='bl') {
                    /*Update Quantities*/
    
    
                    if (isset($data['details'])){
                        foreach ($bl->details as $key => $product) {
                            $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$bl->store_id);
                            QuantityHelper::updateQuantity($product['product_quantity'],$quantity,'out',$bl,'update');
                        }
                    }
                    /*Update Quantities*/
                    /*Store Payment Details*/
                    if (isset($data['commitmentType']) && $data['commitmentType'] == 'echeance') {
                        $bl->commitments()->createMany($data['echeance']);
                        $bl->update(['payment_status' => 0]);
                    }
                    elseif (isset($data['commitmentType']) && $data['commitmentType'] == 'comptant'){
                        $finacial_data['pos_id'] = $bl->id;
                        $finacial_data['date'] = date('Y-m-d H:i:s');
                        $finacial_data['amount'] = $total_TTC;
                        $finacial_data['payment_status'] = 1;
                        $finacial = FinancialCommitment::create($finacial_data);
                        $cash = 0;
                        $check = 0;
                        $transfer = 0;
                        $sodexo = 0;
                        $payment = collect($request->payments);
                    
                        if (isset($data['payments']['cash'])) {
                            $count = count($data['payments']['cash']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['cash'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['cash'][$i + 1]['type'] = 'cash';
                                $data['payments']['cash'][$i + 1]['store_id'] = $old_details->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['cash']);
                            $cashs = collect($payment['cash']);
                            $cash = $cashs->sum('amount');
                        }
                        if (isset($data['payments']['check'])) {
                            $count = count($data['payments']['check']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['check'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['check'][$i + 1]['type'] = 'check';
                                $data['payments']['check'][$i + 1]['store_id'] = $old_details->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['check']);
                            $checks = collect($payment['check']);
                            $check = $checks->sum('amount');
                        }
                        if (isset($data['payments']['transfer'])) {
                            $count = count($data['payments']['transfer']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['transfer'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['transfer'][$i + 1]['type'] = 'transfer';
                                $data['payments']['transfer'][$i + 1]['store_id'] = $old_details->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['transfer']);
                            $transfers = collect($payment['transfer']);
                            $transfer = $transfers->sum('amount');
                        }
                        if (isset($data['payments']['sodexo'])) {
                            $count = count($data['payments']['sodexo']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['sodexo'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['sodexo'][$i + 1]['type'] = 'sodexo';
                                $data['payments']['sodexo'][$i + 1]['store_id'] = $old_details->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['sodexo']);
                            $sodexos = collect($payment['sodexo']);
                            $sodexo = $sodexos->sum('amount');
                        }
                        $total_payments = ($cash + $check + $transfer + $sodexo);
                        if ($total_payments == $total_TTC){
                            $bl->update(['payment_status' => 2]);
                            $finacial->update(['payment_status' => 2]);
                        }
                        elseif ($total_payments<$total_TTC && $total_payments!=0 )
                        {
                            $bl->update(['payment_status' => 1]);
                            $finacial->update(['payment_status' => 1]);
                        }else{
                            $bl->update(['payment_status' => 0]);
                            $finacial->update(['payment_status' => 0]);
                        }
                    }
                    /*Store Payment Details*/
                }
            }
            DB::commit();
            if($request->submit=='a4')
                return redirect()->route('admin.'.$request->piece_type.'.printA4',$bl->id);
            else
                return redirect()->route('admin.'.$request->piece_type.'.printTicket',$bl->id);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pos  $bl
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pos $bl)
    {
        try {
            DB::beginTransaction();
        $old_details= $bl->load('details');
            foreach ($old_details->details as $key => $product) {
                $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$bl->store_id);
                QuantityHelper::updateQuantity($product['product_quantity'],$quantity,'in',$bl,'delete');
            }
        $bl->details()->delete();
        $bl->payments()->delete();
        $bl->commitments()->delete();
            $bl->delete();
            DB::commit();
            return 'success';
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    
    
    public function updateQty($product, $line, $action='out')
    
    {
        
        if ($action == 'out') {
            $updatedQty = (int)($line->quantity - $product['product_quantity']);
        } else {
            $updatedQty = (int)($line->quantity + $product['product_quantity']);
        }
        
            
            $line->quantity=$updatedQty;
            $line->save();
        
        
        return true;
    }
    
    
}
