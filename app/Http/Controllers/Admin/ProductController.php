<?php
    
    namespace App\Http\Controllers\Admin;
    
    use App\Helpers\ImageHelper;
    use App\Http\Controllers\Controller;
    use App\Http\Requests\StoreProductRequest;
    use App\Http\Requests\UpdateProductRequest;
    use App\Models\AchatDetail;
    use App\Models\PosDetail;
    use App\Models\Brand;
    use App\Models\BsDetail;
    use App\Models\Category;
    use App\Models\Image;
    use App\Models\Price;
    use App\Models\Product;
    use App\Models\Quantity;
    use App\Models\StockHistory;
    use App\Models\Store;
    use App\Models\Taste;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Storage;
    use Yajra\DataTables\DataTables;

    class ProductController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index($type='all')
        {
            $categories=Category::all();
            $tastes=Taste::all();
            $brands=Brand::all();
            return view('admin.products.index',compact('tastes','brands','categories','type'));
        }
        
        public function getData(Request $request,$type='all')
        {
            
            if (request()->ajax()) {
                $products=DB::table('products')
                    ->leftJoin('categories','categories.id','=','products.category_id');
                if($type!='all') $products->where('category_id',$type);
                $products->select('products.*','categories.name as category');
                return datatables()->of($products)
                    ->rawColumns(['action'])
                    ->addColumn('action', function ($row) {
                        $row_id = $row->id;
                        $form_id = "delete_Alert_form_" . $row_id;
                        $btn = '    <div style="display:inline-block; width: 210px;">
                                    <a class="icon-btn text-success p-1" href="' .route('admin.products.edit',$row_id)  . '"><i class="fa fa-pencil-alt text-view"></i></a>
                                    <a class="icon-btn text-info p-1" href="' .route('admin.products.show',$row_id)  . '"><i class="fa fa-eye text-view"></i></a>
                                    <form id="'.$form_id.'" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'products\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>
                                </div>';
                        return $btn;
                    })
                 
//                    ->editColumn('category_id', function ($row) {
//                        return $row->category_name;
//                    })
//
//                    ->editColumn('taste_id', function ($row) {
//                        return $row->taste_name;
//                    })
//
//                    ->editColumn('brand_id', function ($row) {
//                        return $row->brand_name;
//                    })
                    ->setRowId(function ($row) {
                        return 'row'.$row->id;
                    })
                    
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);
            }
        }
        
        public function setDefaultImage(Image $image)
        {
            try {
                DB::beginTransaction();
                $image->product->images()->update(['is_default' => false]);
                $image->update(['is_default' => true]);
                DB::commit();
                return response()->json(['success' => true], 200);
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
    
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
            $categories = Category::where('parent_id', NULL)->get();
            $brands = Brand::get();
            $tastes = Taste::get();
            return view('admin.products.create', compact('categories','brands','tastes'));
        }
        
        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\StoreProductRequest $request
         * @return \Illuminate\Http\Response
         */
        public function store(StoreProductRequest $request)
        {
            
            try {
    
                $data = $request->all();
                $stores = Store::all();
                
    
                
                if ($request->has('image')) {
                    $file = ImageHelper::storeFiles($request->file('image'), 'product/');
                    $data['image']=$file['path'];
                }
                $product = Product::create($data);
    
                if (isset($data['components'])) {
                    // Add new components
                    foreach ($request->components as $component) {
                        $product->components()->attach($component['child_product_id'], [
                            'quantity' => $component['quantity'], // Save the quantity
                        ]);
                    }
                }
                foreach ($stores as $store) {
                    Quantity::create([
                        'product_id' => $product->id,
                        'store_id' => $store->id
                    ]);
        
                }
                return response()->json('Product is created successfully',201);
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return response()->json('Product is not created ',500);
            }
        }
    
    
        /**
         * Display the specified resource.
         *
         * @param \App\Models\Product $product
         * @return \Illuminate\Http\Response
         */
        public function show(Product $product)
        {
    
            $stores = Store::all();
            $stores_details = DB::table('stores')
                //->leftJoin('prices', 'stores.id', '=', 'prices.store_id')
                ->leftJoin('quantities', 'stores.id', '=', 'quantities.store_id')
                //->where('prices.product_id', '=', $product->id)
                ->where('quantities.product_id', '=', $product->id)
                ->select('stores.*', 'quantities.quantity', 'quantities.id as quantity_id')
                ->groupBy('stores.id')
                ->get();
            return view('admin.products.show',compact('product','stores','stores_details'));
        }
        
        /**
         * Show the form for editing the specified resource.
         *
         * @param \App\Models\Product $product
         * @return \Illuminate\Http\Response
         */
        public function edit(Product $product)
        {
            
            $categories = Category::where('parent_id', NULL)->get();
            $brands = Brand::get();
            $tastes = Taste::get();
            $products=Product::whereNot('id',$product->id)->select('id','name')->get();
            return view('admin.products.edit',compact('product','categories','tastes','brands','products'));
        }
        
        /**
         * Update the specified resource in storage.
         *
         * @param \App\Http\Requests\UpdateProductRequest $request
         * @param \App\Models\Product $product
         * @return \Illuminate\Http\Response
         */
        public function update(UpdateProductRequest $request, Product $product)
        {
    
            try {
                DB::beginTransaction();
                $data = $request->all();
               
    
                if ($request->has('image')) {
                    $file = ImageHelper::storeFiles($request->file('image'), 'product/');
                    $data['image']=$file['path'];
                }
                $product ->update($data);
                // Remove old components if updating
                $product->components()->detach();
                if(isset($data['components'])){
                    // Add new components
                    foreach ($request->components as $component) {
                        $product->components()->attach($component['child_product_id'], [
                            'quantity' => $component['quantity'], // Save the quantity
                        ]);
                    }
                }
                DB::commit();
                return response()->json('Product is updated successfully',200);
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return response()->json('Product is not updated successfully',500);
            }
        }
        public function  reOrdering(Request $request){
            foreach ($request->page_id_array as $key=>$value){
                Product::where('id',ltrim($value, 'row'))->update(['value'=>$key+1]);
            }
        }
        /**
         * Remove the specified resource from storage.
         *
         * @param \App\Models\Product $product
         * @return \Illuminate\Http\Response
         */
        public function destroy(Product $product)
        {
            try {
                if ($product->images()->exists()) {
                    foreach ($product->images as $key => $image) {
                        $fileuri = str_replace('/storage', '', $image->url);
                        Storage::disk('public')->delete($fileuri);
                        $image->delete();
                    }
                }
                $product->delete();
                return response()->json('Product is deleted successfully',200);
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return response()->json('Product is not deleted successfully',500);
            }
        }
        
        
        public function settings(Product $product)
        {
            $stores = Store::all();
            $stores_details = DB::table('stores')
                //->leftJoin('prices', 'stores.id', '=', 'prices.store_id')
                ->leftJoin('quantities', 'stores.id', '=', 'quantities.store_id')
                //->where('prices.product_id', '=', $product->id)
                ->where('quantities.product_id', '=', $product->id)
                ->select('stores.*', 'quantities.quantity', 'quantities.id as quantity_id')
                ->groupBy('stores.id')
                ->get();
            return view('admin.products.settings', compact('stores', 'product', 'stores_details'));
        }
        
        public function updateSettings(UpdateProductRequest $request, Product $product)
        {
            
            try {
                DB::beginTransaction();
                $i = 0;
                foreach ($request->store_id as $store_id) {
                    if ($request->quantity_id[$i] == 0) {
                        Quantity::create([
                            'product_id' => $product->id,
                            'store_id' => $store_id,
                            'quantity' => $request->quantity[$i],
                        ]);
                    } else {
                        $quantity = Quantity::find($request->quantity_id[$i]);
                        $quantity->update([
                            'quantity' => $request->quantity[$i],
                        ]);
                    }
//                    if ($request->price_id[$i] == 0) {
//                        Price::create([
//                            'product_id' => $product->id,
//                            'store_id' => $store_id,
//                            'selling1' => $request->selling1[$i],
//                            'selling2' => $request->selling2[$i],
//                            'selling3' => $request->selling3[$i],
//                        ]);
//                    } else {
//                        $price = Price::find($request->price_id[$i]);
//                        $price->update([
//                            'selling1' => $request->selling1[$i],
//                            'selling2' => $request->selling2[$i],
//                            'selling3' => $request->selling3[$i],
//                        ]);
//
//                    }
                    $i++;
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
    
    
        public function getDataAchat(Request $request,$productId=null)
        {
        
            if (request()->ajax()) {
                $records=AchatDetail::where('product_id',$productId);
                return datatables()->of($records)
                    ->editColumn('achat_id',function($row){
                        return '<a href="'.route('admin.achat.printA4',$row->achat_id).'"></a>';
                    })
                    ->setRowId(function ($row) {
                        return 'row'.$row->id;
                    })
                    ->rawColumns(['achat_id'])
                    ->addIndexColumn()
                    ->make(true);
            }
        }
        public function getDataLog(Request $request,Product $product)
        {
            if ($request->ajax()) {
                $query = StockHistory::with(['stockable'])->leftJoin('stores', 'stock_histories.store_id', '=', 'stores.id')->select('stock_histories.*','stores.name as store');
            
                return DataTables::of($query)
                    ->addColumn('operation', function ($row) {
                        return class_basename($row->stockable); // Affiche BL, Achat, BS, Inventaire, etc.
                    })
                    ->addColumn('reference', function ($row) {
                        return $row->stockable ? ($row->stockable->id ?? '-') : '-';
                    })
                    ->editColumn('action', function ($row) {
                        return $row->action === 'out' ? '<span class="badge bg-danger"> Sortie </span>' : '<span class="badge bg-success"> Entrée </span>';
                    })
                    ->addColumn('change', function ($row) {
                        return ($row->action === 'out' ? '-' : '+') . abs($row->quantity_after - $row->quantity_before);
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
        }
        
    }
