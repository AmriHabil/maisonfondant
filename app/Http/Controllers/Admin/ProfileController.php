<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Hubs\Hubs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    //
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'password' => 'required|same:c_password',
            'c_password' => 'required|same:password',
        ]);
    }
    public function edit(){
        return view('admin.profile.edit');
    }
    public function update(Request $request){
        $this->validator($request->all())->validate();
        $User=Admin::where('id',Auth::user()->id)
            ->update([
                'password' => Hash::make($request->password)
        ]);
        return redirect()->back()->with('message', 'Vous avez changé votre mot de passe avec succès!');
    }
    
}
