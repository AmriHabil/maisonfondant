<?php
    
    namespace App\Http\Controllers\Admin;
    
    use App\Helpers\ImageHelper;
    use App\Http\Controllers\Controller;
    use App\Models\Brand;
    use App\Http\Requests\StoreBrandRequest;
    use App\Http\Requests\UpdateBrandRequest;
    use App\Models\Image;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Storage;
    
    class BrandController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('admin.brands.index');
        }
        public function getData(Request $request){
            
            if(request()->ajax()) {
                $dataTable=datatables()->of(Brand::get())
                    ->rawColumns(['action'])
                    ->addColumn('action', function($row){
                        $row_id=$row->id;
                        $form_id="delete_Alert_form_".$row_id;
                        $btn='    <div style="display:inline-block; width: 210px;">
                                   
                                    <a class="icon-btn text-success p-1" href="'.route('admin.brands.edit',$row_id).'"><i class="fa fa-pencil-alt text-view"></i></a>
                                    <form id="'.$form_id.'" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'brands\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>
                                </div>';
                        return $btn;
                    })
                    ->setRowId(function ($row) {
                        return 'row'.$row->id;
                    })
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);
                return $dataTable;
            }
        }
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.brands.create');
        }
        
        /**
         * Store a newly created resource in storage.
         *
         * @param  \App\Http\Requests\StoreBrandRequest  $request
         * @return \Illuminate\Http\Response
         */
        public function store(StoreBrandRequest $request)
        {
            try {
                DB::beginTransaction();
                $data=$request->all();
                $brand=Brand::create($data);
                if ($request->file('images')) {
                    foreach ($request->file('images') as $key => $imagefile) {
                        $path = ImageHelper::storeFiles($imagefile,'uploads/brands/images/');
                        $brand->images()->create(['url' => $path, 'is_default' => $key == 0 ? true : false]);
                    }
                }
                DB::commit();
                return $brand;
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        
        /**
         * Display the specified resource.
         *
         * @param  \App\Models\Brand  $brand
         * @return \Illuminate\Http\Response
         */
        public function show(Brand $brand)
        {
            //
        }
        
        /**
         * Show the form for editing the specified resource.
         *
         * @param  \App\Models\Brand  $brand
         * @return \Illuminate\Http\Response
         */
        public function edit(Brand $brand)
        {
            return view('admin.brands.edit',compact('brand'));
        }
        
        /**
         * Update the specified resource in storage.
         *
         * @param  \App\Http\Requests\UpdateBrandRequest  $request
         * @param  \App\Models\Brand  $brand
         * @return \Illuminate\Http\Response
         */
        public function update(UpdateBrandRequest $request, Brand $brand)
        {
            try {
                $data=$request->all();
                
                $brand->update($data);
                $oldImages=$brand->load('images');
                foreach ($oldImages->images as $oldImage): ImageHelper::deleteFiles($oldImage,'brand'); endforeach;
                if ($request->file('images')) {
                    foreach ($request->file('images') as $key => $imagefile) {
                        $path = ImageHelper::storeFiles($imagefile,'uploads/brands/images/');
                        $brand->images()->create(['url' => $path, 'is_default' => $key == 0 ? true : false]);
                    }
                }
                return $brand;
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        /**
         * Remove the specified resource from storage.
         *
         * @param  \App\Models\Brand  $brand
         * @return \Illuminate\Http\Response
         */
        public function destroy(Brand $brand)
        {
            try {
                $brand->delete();
                return 'success';
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
    }
