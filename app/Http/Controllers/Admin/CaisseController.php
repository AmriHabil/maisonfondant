<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;

use App\Models\Bl;
use App\Models\PosPayment;
use App\Models\Decaissement;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use function Yajra\DataTables\Html\Editor\Fields\table;

class CaisseController extends Controller
{
    //
    public function index(Request $request){
        $caisse['cash']=PosPayment::where('type','cash')->where('store_id',auth()->user()->store_id)->sum('amount')-Decaissement::where('store_id',auth()->user()->store_id)->sum('amount');
        $caisse['sodexo']=PosPayment::where('type','sodexo')->where('store_id',auth()->user()->store_id)->whereNot('status',1)->sum('amount');
        $caisse['check']=PosPayment::where('type','check')->where('store_id',auth()->user()->store_id)->whereNot('status',1)->sum('amount');
        $caisse['transfer']=PosPayment::where('type','transfer')->where('store_id',auth()->user()->store_id)->whereNot('status',1)->sum('amount');
        $payments=PosPayment::get();
        $payments=DB::table("pos_payments")
        ->leftJoin('financial_commitments','financial_commitments.id','=','pos_payments.financial_commitment_id')
        ->leftJoin('pos','pos.id','=','financial_commitments.pos_id')
        ->where('pos.store_id',auth()->user()->store_id)
        ->select('pos_payments.*','pos.id as pos_number')->get();
        $decaissements=Decaissement::where('store_id',auth()->user()->store_id)->get();
        return view('admin.caisse.index',compact('caisse','payments','decaissements'));
    }
    public function decaissement(Request $request){
    
        try {
            DB::beginTransaction();
            $data=$request->all();
            $data['admin_id']=auth()->user()->id;
            $data['store_id']=auth()->user()->store_id;
            $decaissement=Decaissement::create($data);
            DB::commit();
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function updatestatuts(Request $request,PosPayment $posPayment){
        
        try {
            DB::beginTransaction();
            $posPayment->status=1;
            $posPayment->save();
            DB::commit();
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    public function status(Request $request){
        
        $caisse['fond']=PosPayment::where('type','cash')->where('store_id',auth()->user()->store_id)->whereDate('created_at', '<=', $request->date)->sum('amount')-Decaissement::where('store_id',auth()->user()->store_id)->whereDate('created_at', '<', $request->date)->sum('amount');
        $caisse['cash']=PosPayment::where('type','cash')->where('store_id',auth()->user()->store_id)->whereDate('created_at', '=', $request->date)->sum('amount');
        $caisse['sodexo']=PosPayment::where('type','sodexo')->where('store_id',auth()->user()->store_id)->whereDate('created_at', '=', $request->date)->sum('amount');
        $caisse['check']=PosPayment::where('type','check')->where('store_id',auth()->user()->store_id)->whereDate('created_at', '=', $request->date)->sum('amount');
        $caisse['transfer']=PosPayment::where('type','transfer')->where('store_id',auth()->user()->store_id)->whereDate('created_at', '=', $request->date)->sum('amount');
        $store=Store::find(auth()->user()->store_id);
        $payments=DB::table("pos_payments")
            ->leftJoin('financial_commitments','financial_commitments.id','=','pos_payments.financial_commitment_id')
            ->leftJoin('pos','pos.id','=','financial_commitments.pos_id')
            ->where('pos.store_id',auth()->user()->store_id)->whereDate('pos_payments.created_at', '=', $request->date)
            ->select('pos_payments.*','pos.id as pos_number')->get();
        $decaissements=Decaissement::where('store_id',auth()->user()->store_id)->whereDate('created_at', '=', $request->date)->get();
        return AppHelper::printCaisse($caisse,$payments,$decaissements,$request->date,$store);
    }
    
}
