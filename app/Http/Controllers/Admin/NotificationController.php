<?php
    
    namespace App\Http\Controllers;
    
    use App\Models\Admin;
    use App\Models\User;
    use Illuminate\Http\Request;
    use Illuminate\Notifications\DatabaseNotification;
    use Illuminate\Support\Facades\Log;
    
    class NotificationController extends Controller
    {
        public function index()
        {
            return view('admin.notification.index');
        }
        
        public function markAsRead(DatabaseNotification $notification)
        {
            try {
                $notification->markAsRead();
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return response()->json(['success' => false], 500);
            }
            return response()->json(['success' => true]);
        }
        
        public function markAllAsRead(Admin $admin)
        {
            try {
                foreach ($admin->unreadNotifications as $notification) {
                    $notification->markAsRead();
                }
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return response()->json(['success' => false], 500);
            }
            return response()->json(['success' => true]);
        }
    }
