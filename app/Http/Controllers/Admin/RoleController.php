<?php
    
    namespace App\Http\Controllers\Admin;
    
    
    use App\Http\Controllers\Controller;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use Spatie\Permission\Models\Permission;
    use Spatie\Permission\Models\Role;
    use Yajra\DataTables\Facades\DataTables;
    use App\Http\Requests\StoreRoleRequest;
    use App\Http\Requests\UpdateRoleRequest;
    class RoleController extends Controller
    {
        /**
         * Show the list of available roles.
         *
         * @param Request $request
         * @return mixed
         */
        public function index()
        {
            return view('admin.roles.index');
        }
    
        public function getData()
        {
            $roles = DB::table('roles')
                ->get();
            if (request()->ajax()) {
                return datatables()->of($roles)
                    ->rawColumns(['action'])
                    ->addColumn('action', function ($row) {
                        $row_id = $row->id;
                        $form_id = "delete_Alert_form_" . $row_id;
                        $btn = '    <div style="display:inline-block; width: 210px;">
                                    <a class="icon-btn text-success p-1" href="edit/' . $row_id . '"><i class="fa fa-pencil-alt text-view"></i></a>
                                    <form id="'.$form_id.'" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'roles\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>
                                </div>';
                        return $btn;
                    })
                    ->setRowId(function ($row) {
                        return 'row'.$row->id;
                    })
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);
            }
        }
        
        public function create()
        {
            
            $permissions = Permission::all()->groupBy('type');
            return view('admin.roles.create', compact('permissions'));
        }
        
        public function store(StoreRoleRequest $request)
        {
            try {
                DB::beginTransaction();
                $data = $request->all();
                $data['guard_name'] = 'admin';
                $role = Role::create($data);
                if (isset($data['permissions'])) {
                    foreach ($data['permissions'] as $key => $permission) {
                        $role->givePermissionTo($permission);
                    }
                }
                DB::commit();
                return $role;
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        public function edit(Role $role)
        {
            
            $permissions = Permission::all()->groupBy('type');
            return view('admin.roles.edit', compact('role', 'permissions'));
        }
        
        public function update(UpdateRoleRequest $request, Role $role)
        {
            try {
                DB::beginTransaction();
                $data = $request->all();
                $role->update($data);
                $permissions = isset($data['permissions']) ? $data['permissions'] : [];
                $role->syncPermissions($permissions);
                DB::commit();
                return $role;
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        public function delete(Role $role)
        {
            try {
                $role->delete();
                return 'success';
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
    }
