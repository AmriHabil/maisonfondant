<?php
    
    namespace App\Http\Controllers\Admin;
    
    use App\Helpers\AppHelper;
    use App\Helpers\QuantityHelper;
    use App\Http\Controllers\Controller;
    use App\Http\Requests\StoreAchatRequest;
    use App\Http\Requests\UpdateAchatRequest;
    use App\Models\Achat;
    use App\Models\AchatFinancialCommitment;
    use App\Models\Provider;
    use App\Models\FinancialCommitment;
    use App\Models\Product;
    use App\Models\Quantity;
    use App\Models\Quotation;
    use App\Notifications\MinQtyNotification;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    
    class AchatController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
           
            return view('admin.achat.index');
        }
        
        public function getData(Request $request)
        {
            $achats=DB::table('achats')
                ->leftJoin('achat_details', 'achats.id', '=', 'achat_details.achat_id')
                ->leftJoin('providers', 'achats.provider_id', '=', 'providers.id')
                ->leftJoin('stores', 'achats.store_id', '=', 'stores.id')
                ->select('achats.*','providers.name as provider','stores.name as store')
                ->groupBy('achats.id');
            if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                $achats->whereBetween('achats.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
            }
            if ($request->has('status') && $request->status!='all') {
                $achats->where('payment_status',$request->status );
            }
            if ($request->has('provider') && $request->provider!='') {
                $achats->whereRaw('lower(achats.provider_details) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->provider))))]);
            }
            if (request()->ajax()) {
                return datatables()->of($achats)
                    ->rawColumns(['action'])
                    ->addColumn('action', function ($row) {
                        $row_id = $row->id;
                        $form_id = "delete_Alert_form_" . $row_id;
                        $printTicket=route('admin.achat.printA4',$row_id );
                        $printA4=route('admin.achat.printA4',$row_id );
                        $btn = '<div style="display:inline-block; width: 210px;">';
                        if(auth()->user()->can('Imprimer bon d\'achat'))
                            $btn.='<a class="icon-btn text-danger p-1"  href="printA4/' . $row_id . '"><i class="fa fa-file-pdf"></i></a><a class="icon-btn text-info p-1"  href="printTicket/' . $row_id . '"><i class="fa fa-tag"></i></a>';
        
                        if(auth()->user()->can('Modifier bon d\'achat'))
                            $btn .='<a class="icon-btn text-success p-1" href="edit/' . $row_id . '"><i class="fa fa-pencil-alt text-view"></i></a>';
        
                        if($row->payment_status!=0){
                            if(auth()->user()->can('Ajouter bon de retour'))
                                $btn .='<a class="icon-btn text-success p-1" href="avoir/' . $row_id . '"><i class="fa fa-redo text-view"></i></a>';
                        }
                        if(auth()->user()->can('Supprimer bon d\'achat'))
                            $btn .='<form id="' . $form_id . '" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'achat\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>';
                        $btn .='</div>';
                        if($row_id=='880') $btn='<a class="icon-btn text-danger p-1"  href="printA4/' . $row_id . '"><i class="fa fa-file-pdf"></i></a><a class="icon-btn text-info p-1"  href="printTicket/' . $row_id . '"><i class="fa fa-tag"></i></a>';;
                        return $btn;
                    })
                   
                    ->rawColumns(['provider'])
                    ->addColumn('provider', function ($row) {
                        $provider_details=json_decode($row->provider_details);
                        return strtoupper($provider_details->provider_name);
                    })
                    
                    ->rawColumns(['store'])
                    ->addColumn('store', function ($row) {
                        return $row->store;
                    })
                    ->editColumn('id', function ($row) {
                        return str_pad($row->id,6,0,STR_PAD_LEFT);
                    })
                    ->setRowId(function ($row) {
                        return 'row'.$row->id;
                    })
                    ->editColumn('payment_status', function ($row) {
                        $btn = ' <div>';
                        if ($row->payment_status=='0') $btn.= '<span class="badge badge-danger">Non Payé</span>';
                        elseif ($row->payment_status=='1') $btn.= '<span class="badge badge-warning">Partiellement Payé</span>';
                        else $btn.= '<span class="badge badge-success">Totalement Payé</span>';
                        return $btn.'</div>';
                    })
                    ->rawColumns(['action','provider','store','payment_status'])
                    ->addIndexColumn()
                    ->with('order', function ()  use($request,$achats){
                        $achatsToCount=new Achat();
        
                        $all=$achatsToCount;
                        $paid=$achatsToCount;
                        $_paid=$achatsToCount;
                        $unpaid=$achatsToCount;
                        if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                            $all=$all->whereBetween('achats.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                        }
                        if ($request->has('status') && $request->status!='all') {
                            $all=$all->where('payment_status',$request->status );
                        }
                        if ($request->has('provider') && $request->provider!='') {
                            $all=$all->whereRaw('lower(achats.provider_details) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->provider))))]);
                        }
                        $order['all']=$all->sum('total');
                        if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                            $paid=$paid->whereBetween('achats.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                        }
                        if ($request->has('status') && $request->status!='all') {
                            $paid=$paid->where('payment_status',$request->status );
                        }
                        if ($request->has('provider') && $request->provider!='') {
                            $paid=$paid->whereRaw('lower(achats.provider_details) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->provider))))]);
                        }
                        $order['paid']=$paid->where('payment_status','2')->sum('total');
                        if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                            $_paid=$_paid->whereBetween('achats.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                        }
                        if ($request->has('status') && $request->status!='all') {
                            $_paid=$_paid->where('payment_status',$request->status );
                        }
                        if ($request->has('provider') && $request->provider!='') {
                            $_paid=$_paid->whereRaw('lower(achats.provider_details) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->provider))))]);
                        }
                        $order['p_paid']=$_paid->where('payment_status','1')->sum('total');
                        if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                            $unpaid=$unpaid->whereBetween('achats.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                        }
                        if ($request->has('status') && $request->status!='all') {
                            $unpaid=$unpaid->where('payment_status',$request->status );
                        }
                        if ($request->has('provider') && $request->provider!='') {
                            $unpaid=$unpaid->whereRaw('lower(achats.provider_details) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->provider))))]);
                        }
                        $order['unpaid']=$unpaid->where('payment_status','0')->sum('total');
        
                        return $order;
        
                    })
                    ->make(true);
            }
        }
        
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
            $products = DB::table('products')
                ->leftJoin('quantities', 'products.id', '=', 'quantities.product_id')
                ->select('products.id','products.name','products.ref','products.barcode','products.buying','products.unity','products.tva', 'quantities.quantity', 'products.price as detail')
//            ->where('quantities.store_id', auth()->user()->store_id)
//            ->where('prices.store_id', auth()->user()->store_id)
                ->groupBy('products.id')
                ->get();
            $providers = DB::table('providers')
                ->select('providers.id', 'providers.name', 'providers.phone', 'providers.mf', 'providers.address', 'providers.plafond','providers.remise',  DB::raw('SUM(`achats`.`total`) as `encours`' ))
                ->leftJoin('achats', 'providers.id', '=', 'achats.provider_id')
                ->groupBy('providers.id', 'providers.name', 'providers.mf')
                ->get();
            return view('admin.achat.create',compact('products','providers'));
        }
        
        /**
         * Store a newly created resource in storage.
         *
         * @param  \App\Http\Requests\StoreAchatRequest  $request
         * @return \Illuminate\Http\Response
         */
        public function store(StoreAchatRequest $request)
        {
            $data = $request->all();
            try {
                DB::beginTransaction();
                
                if (isset($data['details'])) {
                    $number = count($data["details"]);
                    $data['number'] = $number;
                    $data['store_id'] = auth()->user()->store_id;
                    $data['admin_id'] = auth()->user()->id;
                    $cart = Achat::create($data);
                    $cart->details()->createMany($data["details"]);
                    $cart->provider_details=$data['provider_details'];
                    $total_TTC=0;
                    foreach($data['details'] as $item){
                        $after_discount= ($item['product_price_buying']*(100-$item['product_remise']))/100;
                        $total_TTC += ($after_discount * $item['product_quantity']);
                    }
                    $cart->total=$total_TTC;
                    $cart->save();
               
                        /*Update Quantities*/
                        foreach ($cart->details as $key => $product) {
                            $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$cart->store_id);
                            QuantityHelper::updateQuantity($product['product_quantity'],$quantity,'in',$cart,'create');
                        }
                        /*Update Quantities*/
                    /*Store Payment Details*/
                    if (isset($data['commitmentType']) && $data['commitmentType'] == 'echeance') {
                        $cart->commitments()->createMany($data['echeance']);
        
        
                    }
                    elseif (isset($data['commitmentType']) && $data['commitmentType'] == 'comptant') {
                        $finacial_data['achat_id'] = $cart->id;
                        $finacial_data['date'] = date('Y-m-d H:i:s');
                        $finacial_data['amount'] = $total_TTC;
        
                        $finacial = AchatFinancialCommitment::create($finacial_data);
                        $cash = 0;
                        $check = 0;
                        $transfer = 0;
                        $sodexo = 0;
                        $payment = collect($request->payments);
        
                        if (isset($data['payments']['cash'])) {
                            $count = count($data['payments']['cash']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['cash'][$i + 1]['provider_id'] = $request->provider_id;
                                $data['payments']['cash'][$i + 1]['type'] = 'cash';
                                $data['payments']['cash'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['cash']);
                            $cashs = collect($payment['cash']);
                            $cash = $cashs->sum('amount');
                        }
                        if (isset($data['payments']['check'])) {
                            $count = count($data['payments']['check']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['check'][$i + 1]['provider_id'] = $request->provider_id;
                                $data['payments']['check'][$i + 1]['type'] = 'check';
                                $data['payments']['check'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['check']);
                            $checks = collect($payment['check']);
                            $check = $checks->sum('amount');
                        }
                        if (isset($data['payments']['transfer'])) {
                            $count = count($data['payments']['transfer']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['transfer'][$i + 1]['provider_id'] = $request->provider_id;
                                $data['payments']['transfer'][$i + 1]['type'] = 'transfer';
                                $data['payments']['transfer'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
            
                            $finacial->payments()->createMany($data['payments']['transfer']);
                            $transfers = collect($payment['transfer']);
                            $transfer = $transfers->sum('amount');
                        }
                        if (isset($data['payments']['sodexo'])) {
                            $count = count($data['payments']['sodexo']);
                            for ($i = 0; $i < $count; $i++) {
                
                                $data['payments']['sodexo'][$i + 1]['provider_id'] = $request->provider_id;
                                $data['payments']['sodexo'][$i + 1]['type'] = 'sodexo';
                                $data['payments']['sodexo'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['sodexo']);
                            $sodexos = collect($payment['sodexo']);
                            $sodexo = $sodexos->sum('amount');
                        }
                        $total_payments = ($cash + $check + $transfer + $sodexo);
                        if ($total_payments == $total_TTC){
                            $cart->update(['payment_status' => 2]);
                            $finacial->update(['payment_status' => 2]);
                        }
                        elseif ($total_payments<$total_TTC && $total_payments!=0 )
                        {
                            $cart->update(['payment_status' => 1]);
                            $finacial->update(['payment_status' => 1]);
                        }else{
                            $cart->update(['payment_status' => 0]);
                            $finacial->update(['payment_status' => 0]);
                        }
                    }
                    /*Store Payment Details*/
                    
                }
                DB::commit();
                
                return redirect()->route('admin.achat.printA4',$cart->id);
                
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                Log::error($e->getLine());
                return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
            
            
        }
        
        /**
         * Display the specified resource.
         *
         * @param  \App\Models\Achat  $achat
         * @return \Illuminate\Http\Response
         */
        public function show(Achat $achat)
        {
            //
        }
        public function printA4(Achat $achat)
        {
            
            $achat->load('details');
            return AppHelper::printAchat($achat,'achat');
        }
        public function printTicket(Achat $achat)
        {
            $achat->load('details');
            return AppHelper::printTicket($achat,'achat');
        }
        
        /**
         * Show the form for editing the specified resource.
         *
         * @param  \App\Models\Achat  $achat
         * @return \Illuminate\Http\Response
         */
        public function edit(Achat $achat)
        {
            $products = DB::table('products')
                ->leftJoin('quantities', 'products.id', '=', 'quantities.product_id')
                ->leftJoin('prices', 'products.id', '=', 'prices.product_id')
                ->select('products.id','products.name','products.ref','products.barcode','products.buying','products.unity','products.tva', 'quantities.quantity', 'prices.selling1')
//            ->where('quantities.store_id', auth()->user()->store_id)
//            ->where('prices.store_id', auth()->user()->store_id)
                ->groupBy('products.id')
                ->get();
            $providers = DB::table('providers')
                ->select('providers.id', 'providers.name', 'providers.phone', 'providers.mf', 'providers.address', 'providers.plafond','providers.remise',  DB::raw('SUM(`achats`.`total`) as `encours`' ))
                ->leftJoin('achats', 'providers.id', '=', 'achats.provider_id')
                ->groupBy('providers.id', 'providers.name', 'providers.mf')
                ->get();
            
            
            return view('admin.achat.edit',compact('achat','products','providers'));
        }
        
        /**
         * Update the specified resource in storage.
         *
         * @param  \App\Http\Requests\UpdateAchatRequest  $request
         * @param  \App\Models\Achat  $achat
         * @return \Illuminate\Http\Response
         */
        public function update(UpdateAchatRequest $request, Achat $achat)
        {
            $data = $request->all();
            try {
                DB::beginTransaction();
                
                
                if (isset($data['details'])) {
                    $old_details = $achat->load('details');
                    foreach ($achat->details as $key => $product) {
                        $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$achat->store_id);
                        QuantityHelper::updateQuantity($product['product_quantity'],$quantity,'out',$achat,'update');
                    }
                    $achat->details()->delete();
                    $achat->payments()->delete();
                    $achat->commitments()->delete();
                    $number = count($data["details"]);
                    $data['number'] = $number;
                    $data['store_id'] = 1;
                    $data['admin_id'] = auth()->user()->id;
                    $achat->update($data);
                    
                    $achat->details()->createMany($data["details"]);
                    //$achat->client_details['client_name'];
                    $total_TTC=0;
                    foreach($data['details'] as $item){
                        $after_discount= ($item['product_price_buying']*(100-$item['product_remise']))/100;
                        $total_TTC += ($after_discount * $item['product_quantity']);
                    }
                    $achat->total=$total_TTC;
                    $achat->save();
                 
                        /*Update Quantities*/
    
    
                   
                        foreach ($achat->details as $key => $product) {
                            $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$achat->store_id);
                            QuantityHelper::updateQuantity($product['product_quantity'],$quantity,'in',$achat,'update');
                        }
                        /*Update Quantities*/
    
                    /*Store Payment Details*/
                    if (isset($data['commitmentType']) && $data['commitmentType'] == 'echeance') {
                        $achat->commitments()->createMany($data['echeance']);
        
        
                    }
                    elseif (isset($data['commitmentType']) && $data['commitmentType'] == 'comptant') {
                        $finacial_data['achat_id'] = $achat->id;
                        $finacial_data['date'] = date('Y-m-d H:i:s');
                        $finacial_data['amount'] = $total_TTC;
        
                        $finacial = AchatFinancialCommitment::create($finacial_data);
                        $cash = 0;
                        $check = 0;
                        $transfer = 0;
                        $sodexo = 0;
                        $payment = collect($request->payments);
        
                        if (isset($data['payments']['cash'])) {
                            $count = count($data['payments']['cash']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['cash'][$i + 1]['provider_id'] = $request->provider_id;
                                $data['payments']['cash'][$i + 1]['type'] = 'cash';
                                $data['payments']['cash'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['cash']);
                            $cashs = collect($payment['cash']);
                            $cash = $cashs->sum('amount');
                        }
                        if (isset($data['payments']['check'])) {
                            $count = count($data['payments']['check']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['check'][$i + 1]['provider_id'] = $request->provider_id;
                                $data['payments']['check'][$i + 1]['type'] = 'check';
                                $data['payments']['check'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['check']);
                            $checks = collect($payment['check']);
                            $check = $checks->sum('amount');
                        }
                        if (isset($data['payments']['transfer'])) {
                            $count = count($data['payments']['transfer']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['transfer'][$i + 1]['provider_id'] = $request->provider_id;
                                $data['payments']['transfer'][$i + 1]['type'] = 'transfer';
                                $data['payments']['transfer'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
            
                            $finacial->payments()->createMany($data['payments']['transfer']);
                            $transfers = collect($payment['transfer']);
                            $transfer = $transfers->sum('amount');
                        }
                        if (isset($data['payments']['sodexo'])) {
                            $count = count($data['payments']['sodexo']);
                            for ($i = 0; $i < $count; $i++) {
                
                                $data['payments']['sodexo'][$i + 1]['provider_id'] = $request->provider_id;
                                $data['payments']['sodexo'][$i + 1]['type'] = 'sodexo';
                                $data['payments']['sodexo'][$i + 1]['store_id'] = auth()->user()->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['sodexo']);
                            $sodexos = collect($payment['sodexo']);
                            $sodexo = $sodexos->sum('amount');
                        }
                        $total_payments = ($cash + $check + $transfer + $sodexo);
                        if ($total_payments == $total_TTC){
                            $achat->update(['payment_status' => 2]);
                            $finacial->update(['payment_status' => 2]);
                        }
                        elseif ($total_payments<$total_TTC && $total_payments!=0 )
                        {
                            $achat->update(['payment_status' => 1]);
                            $finacial->update(['payment_status' => 1]);
                        }else{
                            $achat->update(['payment_status' => 0]);
                            $finacial->update(['payment_status' => 0]);
                        }
                    }
                    /*Store Payment Details*/
                    
                }
                DB::commit();
                if($request->submit=='a4')
                    return redirect()->route('admin.achat.printA4',$achat->id);
                else
                    return redirect()->route('admin.achat.printTicket',$achat->id);
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                Log::error($e->getLine());
                return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        /**
         * Remove the specified resource from storage.
         *
         * @param  \App\Models\Achat  $achat
         * @return \Illuminate\Http\Response
         */
        public function destroy(Achat $achat)
        {
            try {
                DB::beginTransaction();
                $old_details= $achat->load('details');
                foreach ($old_details->details as $key => $product) {
                    $quantity = QuantityHelper::getQuantityOrCreate($product->product_id,$achat->store_id);
                    QuantityHelper::updateQuantity($product['product_quantity'],$quantity,'out',$achat,'delete');
                }
                $achat->details()->delete();
                $achat->payments()->delete();
                $achat->commitments()->delete();
                $achat->delete();
                DB::commit();
                return 'success';
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        
       
        
        
    }
