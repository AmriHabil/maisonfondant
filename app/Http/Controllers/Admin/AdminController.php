<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAdminRequest;
use App\Models\Admin;
use App\Models\Regions\Governorates;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.admins.index');
    }
    public function getData(Request $request){
        
        if(request()->ajax()) {
            $dataTable=datatables()->of(Admin::get())
                ->rawColumns(['action'])
                ->addColumn('action', function($row){
                    $row_id=$row->id;
                    $form_id="delete_Alert_form_".$row_id;
                    $btn='    <div style="display:inline-block; width: 210px;">';
    
                    if(auth()->user()->can('Modifier admins'))  $btn.='<a class="icon-btn text-success p-1" href="'.route('admin.admins.edit',$row_id).'"><i class="fa fa-pencil-alt text-view"></i></a>';
                    if(auth()->user()->can('Modifier admins'))  $btn.='                <form id="'.$form_id.'" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'admins\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>';
                    $btn.='          </div>';
                    return $btn;
                })
                ->setRowId(function ($row) {
                    return 'row'.$row->id;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
            return $dataTable;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles=Role::all();
        $stores=Store::all();
        return view('admin.admins.create',compact('roles','stores'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAdminRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdminRequest $request)
    {
    
        try {
            DB::beginTransaction();
            $data=$request->all();
            $data['password']=Hash::make($data['password']);
            $admin=Admin::create($data);
            $role=Role::findByName($request->role);
            $admin->assignRole($role);
            $admin->save();
            DB::commit();
            return $admin;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
        
    }
    
    public function edit(Admin $admin){
        $roles=Role::all();
        $stores=Store::all();
        return view('admin.admins.edit',compact('admin','roles','stores'));
    }
    
    public function update(Request $request,Admin $admin)
    {
        
        try {
            DB::beginTransaction();
            $data=$request->all();
            if(isset($data['password']) && $data['password']!=NULL && $data['password']!=''){
                $data['password']=Hash::make($data['password']);
            }elseif(isset($data['password'])){
                unset ($data['password']);
            }
            
            
    
            
            $role=Role::findByName($request->role);
            $admin->syncRoles($role);
            $admin->update($data);
            DB::commit();
          
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
        
    }
    public function updateRole(Request $request,Admin $admin)
    {
        try {
            DB::beginTransaction();
            $data=$request->all();
            $admin->update($data);
            $admin->save();
            DB::commit();
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
    
        try {
            $admin->delete();
            return 'success';
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
