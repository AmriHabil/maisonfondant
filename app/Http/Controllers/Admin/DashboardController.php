<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Pos;
use App\Models\PosDetail;
use App\Models\Client;
use App\Models\Facture;
use App\Models\Hubs\Hubs;
use App\Models\Order;
use App\Models\Orders\MU;
use App\Models\Product;
use App\Models\Quotation;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{
    //
    public function index(Request $request){
        
    
    
        
        return view('admin.dashboard.index');
    }
    public function daily(){
        try {
            $Hubs=Store::all();
           
             $mus=Pos::withCount('details')->withCount(['details as sumBuying' => function($query) {
                    $query->select(DB::raw('SUM(product_price_buying)'));
                },'details as bonus' => function($query) {
                 $query->select(DB::raw('SUM(product_bonus)'));
             }
                ])->whereDate('date', Carbon::today())->get();
            return view('admin.dashboard.daily',compact('mus','Hubs'));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
        
    }
    
    public function search(Request $request){
        $from = date("Y-m-d", time() - 604800);
        $to = date("Y-m-d", time() + 86400);
        if ($request->from) {
            $from = date($request->from);
        }
        if ($request->to) {
            $to = date($request->to, time() + 86400);
        }
        $Hubs=Store::all();
        $mus=Pos::withCount('details')->withCount(['details as sumBuying' => function($query) {
            $query->select(DB::raw('SUM(product_price_buying)'));
        },'details as bonus' => function($query) {
            $query->select(DB::raw('SUM(product_bonus)'));
        }
        ])->whereBetween('created_at', [$from . ' 00:00:00', $to . ' 23:59:59']);
        
        if ($request->Hubs != NULL && !empty($request->Hubs)) {
            $mus->whereIn('store_id', $request->Hubs);
        }
        $mus=$mus->get();
        return view('admin.dashboard.daily',compact('mus','Hubs'));
        
    }
    public function statistics($year=2023){
        try {
            
            $factures_number=Facture::count();
            $bl_number=Pos::count();
            $devis_number=Quotation::count();
            $online_number=Order::count();
            $site_number=0;
            $comptoir_number=0;
            $product_number=Product::count();
            $clients_number=Client::count();
            
            $now=Carbon::now();
            $topproducts = DB::table('products')
                ->leftJoin('bl_details','products.id','=','bl_details.product_id')
                ->selectRaw('products.name , products.detail, COALESCE(sum(bl_details.product_quantity),0) total')
                ->groupBy('products.id')
                ->orderBy('total','desc')
                ->take(20)
                ->get();
            
            
            $stores=Store::all();
//            return DB::table('bls')->selectRaw('sum(total)')->groupBy('store_id')->get();
            $bls = Pos::select(
                DB::raw('year(date) as year'),
                DB::raw('month(date) as month'),
                DB::raw('sum(total) as total'),
                'store_id'
                 )
                ->where(DB::raw('year(date)'), '=', $year)
                ->groupBy('year','store_id')
                ->groupBy('month')
                ->get();
    
            $monthly = Pos::select(
                DB::raw('day(date) as day'),
                DB::raw('month(date) as month'),
                DB::raw('sum(total) as total'),
                'store_id'
            )
                ->where(DB::raw('month(date)'), '=', $now->month)
                ->where(DB::raw('year(date)'), '=', $year)
                ->groupBy('store_id')
                ->groupBy('date')
                ->get();
            return view('admin.dashboard.statistics',compact('factures_number','bl_number','devis_number','online_number','site_number','comptoir_number','product_number','clients_number','product_number','clients_number','bls','stores','topproducts','monthly','now'));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
        
    }
}
