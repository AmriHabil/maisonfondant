<?php
    
    namespace App\Http\Controllers\Admin;
    
    use App\Http\Controllers\Controller;
    use App\Models\Taste;
    use App\Http\Requests\StoreTasteRequest;
    use App\Http\Requests\UpdateTasteRequest;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    
    class TasteController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('admin.tastes.index');
        }
        public function getData(Request $request){
            
            if(request()->ajax()) {
                $dataTable=datatables()->of(Taste::get())
                    ->rawColumns(['action'])
                    ->addColumn('action', function($row){
                        $row_id=$row->id;
                        $form_id="delete_Alert_form_".$row_id;
                        $btn='    <div style="display:inline-block; width: 210px;">
                                   
                                    <a class="icon-btn text-success p-1" href="'.route('admin.tastes.edit',$row_id).'"><i class="fa fa-pencil-alt text-view"></i></a>
                                    <form id="'.$form_id.'" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'tastes\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>
                                </div>';
                        return $btn;
                    })
                    ->setRowId(function ($row) {
                        return 'row'.$row->id;
                    })
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);
                return $dataTable;
            }
        }
        
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.tastes.create');
        }
        
        /**
         * Store a newly created resource in storage.
         *
         * @param  \App\Http\Requests\StoreTasteRequest  $request
         * @return \Illuminate\Http\Response
         */
        public function store(StoreTasteRequest $request)
        {
            try {
                DB::beginTransaction();
                $data=$request->all();
                $taste=Taste::create($data);
                
                DB::commit();
                return $taste;
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        /**
         * Display the specified resource.
         *
         * @param  \App\Models\Taste  $taste
         * @return \Illuminate\Http\Response
         */
        public function show(Taste $taste)
        {
            //
        }
        
        /**
         * Show the form for editing the specified resource.
         *
         * @param  \App\Models\Taste  $taste
         * @return \Illuminate\Http\Response
         */
        public function edit(Taste $taste)
        {
            return view('admin.tastes.edit',compact('taste'));
        }
        
        /**
         * Update the specified resource in storage.
         *
         * @param  \App\Http\Requests\UpdateTasteRequest  $request
         * @param  \App\Models\Taste  $taste
         * @return \Illuminate\Http\Response
         */
        public function update(UpdateTasteRequest $request, Taste $taste)
        {
            try {
                $data=$request->all();
                $taste->update($data);
                return $taste;
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        /**
         * Remove the specified resource from storage.
         *
         * @param  \App\Models\Taste  $taste
         * @return \Illuminate\Http\Response
         */
        public function destroy(Taste $taste)
        {
            try {
                $taste->delete();
                return 'success';
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
    }
