<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\Bl;
use App\Models\BlDetail;
use App\Models\Pos;
use App\Models\PosDetail;
use App\Models\Client;
use App\Models\Facture;
use App\Http\Requests\StoreFactureRequest;
use App\Http\Requests\UpdateFactureRequest;
use App\Models\Pack;
use App\Models\Product;
use App\Models\Store;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FactureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('admin.factures.index');
    }
    
    public function getData(Request $request, $client=0)
    {   $query=DB::table('factures')
        ->leftJoin('facture_details', 'factures.id', '=', 'facture_details.facture_id')
        ->leftJoin('clients', 'factures.client_id', '=', 'clients.id')
    
        ->leftJoin('stores', 'factures.store_id', '=', 'stores.id')
        ->select('factures.*','clients.name as client','stores.name as store')
        ->groupBy('factures.id');
        if($client!=0) $query=$query->where('factures.client_id',$client);
        if (request()->ajax()) {
            return datatables()->of($query)
                ->rawColumns(['action'])
                ->addColumn('action', function ($row) {
                    $row_id = $row->id;
                    $form_id = "delete_Alert_form_" . $row_id;
                    $printA4=route('admin.bl.printA4',$row_id );
                    $btn = '
                                    <a class="icon-btn text-danger p-1"  href="printA4/' . $row_id . '"><i class="fa fa-file-pdf"></i></a>
                                 ';
                    if(auth()->user()->can('Supprimer bon de livraison'))
                        $btn .='<form id="' . $form_id . '" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'facture\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>';
                    $btn .='</div>';
                    
                        $btn .='                <a class="icon-btn text-success p-1" href="' . route('admin.facture.edit',$row_id) . '"><i class="fa fa-pencil-alt text-view"></i></a>';
                  
                    return $btn;
                })
               
                ->rawColumns(['client'])
//                ->addColumn('client', function ($row) {
//                    if ($row->client_id=='0') return 'Passager';
//                    return $row->client;
//                })
                ->addColumn('client', function ($row) {
                    $client_details=json_decode($row->client_details);
                    return strtoupper($client_details->client_name);
                })
                ->rawColumns(['store'])
                ->addColumn('store', function ($row) {
                    return $row->store;
                })
                
               
                ->editColumn('id', function ($row) {
                    return str_pad($row->id,6,0,STR_PAD_LEFT);
                })
                ->setRowId(function ($row) {
                    return $row->id;
                })
                ->rawColumns(['action','client','store'])
                ->addIndexColumn()
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = DB::table('products')
            ->select('*')
            ->orderBy('value')
            ->get();
        $clients = DB::table('clients')
            ->select('clients.*',  DB::raw('SUM(`factures`.`total`) as `encours`' ))
            ->leftJoin('factures', 'clients.id', '=', 'factures.client_id')
            ->groupBy('clients.id')->get();
        $stores=Store::get();
        // Récupérer la dernière facture
        $lastFacture = Facture::latest()->first();
    
        // Assume $storeId is the current store's ID
        $storeId = auth()->user()->store_id; // Replace with your actual store ID

// Initialiser le numéro de facture
        $newNumero = '0001/' . date('y'); // Valeur par défaut si aucune facture n'existe pour ce store

// Récupérer la dernière facture pour ce store
        $lastFacture = DB::table('factures')
            ->where('store_id', $storeId)
            ->orderBy('date', 'desc')
            ->first();
    
        if ($lastFacture) {
            // Extraire le numéro de la dernière facture
            $lastNumero = $lastFacture->number;
        
            // Diviser le numéro en partie avant et après le '/'
            list($prefix, $year) = explode('/', $lastNumero);
        
            // Convertir en format d'année complet
            $fullYear = '20' . $year;
        
            // Obtenir la date de la dernière facture
            $lastDate = \Carbon\Carbon::parse($lastFacture->date);
            $today = \Carbon\Carbon::today();
        
            // Calculer les jours écoulés depuis la dernière facture
            $daysMissed = $lastDate->diffInDays($today);
        
            // Si l'année a changé, réinitialiser le préfixe
            if ($fullYear != date('Y')) {
                $newPrefix = '0001'; // Réinitialiser le numéro pour la nouvelle année
            } else {
                // Incrémenter le numéro en tenant compte des jours manqués
                $newPrefix = str_pad((int)$prefix + max(1, $daysMissed), 4, '0', STR_PAD_LEFT);
            }
        
            // Générer le nouveau numéro avec l'année en cours
            $newNumero = "{$newPrefix}/" . date('y');
        }
        return view('admin.factures.create',compact('products','clients','stores','newNumero'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFactureRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        try {
            DB::beginTransaction();
            
            // Fetch the BL data
            $bl = Bl::whereIn('id', $data['bls']);
            
            // Check if clients are the same across the selected BLs
            if($bl->groupBy('client_id')->get()->count() > 1) {
                return redirect()->back()->with('global_error', 'Vous ne pouvez pas générer cette facture : Des clients différents séléctionnés!');
            }
            
            // Get all the BL details
            $bl_details = BlDetail::whereIn('bl_id', $data['bls'])->get();
            
            // Group bl_details by product_id and sum the quantities
            $grouped_details = $bl_details->groupBy('product_id')->map(function ($details) {
                $total_quantity = $details->sum('product_quantity');
                $product_price = $details->first()->product_price_selling; // Assuming all prices are the same for the same product_id
                
                return [
                    'product_id' => $details->first()->product_id,
                    'product_ref' => $details->first()->product_ref,
                    'product_name' => $details->first()->product_name,
                    'product_quantity' => $total_quantity,
                    'product_price_selling' => $product_price,
                    'product_tva' => 19, // Static value, adjust if necessary
                    'product_remise' => 0, // Static value, adjust if necessary
                    'product_price_buying' => 19, // Static value, adjust if necessary
                ];
            });
            
            // Prepare facture data
            $facture_data = [
                'admin_id' => auth()->user()->id,
                'store_id' => $bl->first()->store_id,
                'timbre' => 1,
                'date' => $data['date'],
                'total' => $bl->sum('total') + 1, // Adding the timbre to total
                'client_id' => $bl->first()->client_id,
                'client_details' => $bl->first()->client_details
            ];
            
            // Create facture
            $facture = Facture::create($facture_data);
            $facture->update(['number' => str_pad($facture->id, 2, 0, STR_PAD_LEFT) . '/' . date("Y")]);
            
            // Update Pos records
            Bl::whereIn('id', $data['bls'])->update(['invoice_id' => $facture->id]);
            
            // Create facture details from the grouped data
            $facture->details()->createMany($grouped_details->toArray());
            
            // Commit the transaction
            DB::commit();
            
            // Redirect to the facture print page
            return redirect()->route('admin.facture.printA4', $facture->id);
            
        } catch (\Exception $e) {
            // Rollback in case of error
            DB::rollBack();
            
            // Log the error for debugging purposes
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            
            // Return the error message for debugging
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Facture  $facture
     * @return \Illuminate\Http\Response
     */
    public function show(Facture $facture)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Facture  $facture
     * @return \Illuminate\Http\Response
     */
    public function edit(Facture $facture)
    {
        $products = DB::table('products')
            ->select('*')
            ->orderBy('value')
            ->get();
        $clients = DB::table('clients')
            ->select('clients.id', 'clients.name', 'clients.phone', 'clients.mf', 'clients.address',  DB::raw('SUM(`bls`.`total`) as `encours`' ))
            ->leftJoin('bls', 'clients.id', '=', 'bls.client_id')
            ->groupBy('clients.id', 'clients.name', 'clients.mf')
            ->get();
       
       
        return view('admin.factures.edit',compact('products','clients','facture'));
    }
    public function printA4(Facture $facture)
    {
    
        $facture->load('details');
        return AppHelper::printA4($facture,'facture');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFactureRequest  $request
     * @param  \App\Models\Facture  $facture
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facture $facture)
    {
        $data = $request->all();
        $client=Client::where('phone',$data['client_details']['client_phone'])->first();
        if(!$client){
            $client['phone']=$data['client_details']['client_phone'];
            $client['mf']=$data['client_details']['client_mf'];
            $client['address']=$data['client_details']['client_address'];
            $client['name']=$data['client_details']['client_name'];
            $client=Client::create($client);
            $data['client_id']=$client->id;
        }
        try {
            DB::beginTransaction();
        
        
            if (isset($data['details'])) {
                $old_details= $facture->load('details');
    
                $facture->details()->delete();
               
                
              
                //$data['admin_id'] = auth()->user()->id;
                $facture->update($data);
    
    
                $facture->client_details['client_name'];
                $total_brut=0;
                $total_TTC=0;
            
                foreach($data['details'] as $item){
                    //$after_discount= ($item['product_price_selling']*(100-$item['product_remise']))/100;
                    $total_brut+= ($item['product_price_selling']* $item['product_quantity']);
                    $after_discount= ($item['product_price_selling']-$item['product_remise']);
                    $total_TTC += ($after_discount * $item['product_quantity']);
                }
                $bonus_status=1;
                $remise_total=$total_TTC/$total_brut;
                if($remise_total < 0.9  && $remise_total > 0.85 ){
                    $bonus_status=0.5;
                }
                elseif($remise_total < 0.85   ){
                    $bonus_status=0;
                }
                foreach($data['details'] as $k=>$v){
                    $data['details'][$k]['product_bonus']=$data['details'][$k]['product_bonus']*$bonus_status;
                }
    
                $facture->details()->createMany($data["details"]);
//                $facture->total=$total_TTC-$data["remise"];
                $facture->save();
             
            }
            DB::commit();
            if($request->submit=='a4')
                return redirect()->route('admin.facture.printA4',$facture->id);
            else
                return redirect()->route('admin.facture.printA4',$facture->id);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Facture  $facture
     * @return \Illuminate\Http\Response
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Facture  $facture
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facture $facture)
    {
        $id=$facture->id;
        try {
            $facture->details()->delete();
            if($facture->delete()) return response(['status'=>'success','message'=>'Invoice deleted successfully','id'=>$id],200);
            
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return response(['status'=>'fail','message'=>'Invoice is not deleted'],500);
        }
    }
}
