<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Jobs\SendBulkSms;
use App\Models\Bl;
use App\Models\BlPayment;
use App\Models\Client;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use function Yajra\DataTables\Html\Editor\Fields\table;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.clients.index');
    }
    
    public function getData(Request $request,$type=null)
    {
        
        if ($request->has('type')) {
            if ($request->type=='Boutique') $clients = Client::has('bls');
                elseif($request->type=='Livraison') $clients = Client::has('orders');
        }else{
            $clients = Client::query();
        }
        if($type=='bl'){
            $clients = DB::table('clients')
                ->select('clients.*');
               
        }
        if (request()->ajax()) {
            return datatables()->of($clients)
                ->rawColumns(['action'])
                ->addColumn('action', function ($row) use($type) {
                    if($type=='bl'){
               
                            return  '
                            <a class="addClient"
                                                        data-id="'.$row->id.'"
                                                        data-name="'.$row->name.'"
                                                        data-phone="'.$row->phone.'"
                                                        data-mf="'.$row->mf.'"
                                                        data-address="'.$row->address.'"
                                                        data-plafond="0"
                                               
                            ><i class="fa fa-cart-plus"></i>
                            </a>';
        
                      
                    }else{
                        $row_id = $row->id;
                        $form_id = "delete_Alert_form_" . $row_id;
                        $btn = '    <div style="display:inline-block; width: 210px;">
                                    <a class="icon-btn text-success p-1" href="edit/' . $row_id . '"><i class="fa fa-pencil-alt text-view"></i></a>
                                    <a class="icon-btn info p-1" href="show/' . $row_id . '"><i class="fa fa-eye text-view"></i></a>
                                    <form id="' . $form_id . '" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'clients\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>
                                </div>';
                        return $btn;
                    }
                    
                })
//                ->addColumn('credit', function ($row) {
//
//                    return $row->encours-$row->paid;
//                })
                ->rawColumns(['checkbox'])
                ->addColumn('checkbox', function ($row) {
        
                    return ' <div class="checkbox checkbox-info mb-2">
                                            <input id="checkbox-'.$row->id.'" type="checkbox" class="order_check"
                                                   name="clients[]" data-id="'.$row->id.'" value="'.$row->id.'">
                                            <label for="checkbox-'.$row->id.'" class="label_checkbox">
                                            </label>
                                        </div>';
                })
                ->setRowId(function ($row) {
                    return 'row'.$row->id;
                })
                ->editColumn('name',function($row){
                    return strtoupper($row->name);
                })
                ->rawColumns(['action','checkbox'])
                ->addIndexColumn()
                ->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores=Store::get();
        return view('admin.clients.create',compact('stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreClientRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        DB::beginTransaction();
        try {
            $client = Client::create($data);
            DB::commit();
            return response()->json(['data' => $client, 'success' => true], 201);
        } catch (\Exception $e) {
            
            DB::rollback();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return response()->json([
                'error' => $e->getMessage(),
            ], 500);
        }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client,$year=2024)
    {
        $now=Carbon::now();
        $bls = Bl::select(
            DB::raw('year(date) as year'),
            DB::raw('month(date) as month'),
            DB::raw('sum(total) as total'),
            'store_id'
        )
            ->where(DB::raw('year(date)'), '=', $year)
            ->where('client_id', '=', $client->id)
            ->groupBy('year','store_id')
            ->groupBy('month')
            ->get();
    
        $payments = BlPayment::select(
            DB::raw('year(created_at) as year'),
            DB::raw('month(created_at) as month'),
            DB::raw('sum(amount) as total')
        )
            ->where(DB::raw('year(created_at)'), '=', $year)
            ->where('client_id', '=', $client->id)
            ->groupBy('month')
            ->get();
    
        $topproducts = DB::table('products')
            ->leftJoin('bl_details','products.id','=','bl_details.product_id')
            ->leftJoin('bls','bls.id','=','bl_details.bl_id')
            ->selectRaw('products.name , products.price, COALESCE(sum(bl_details.product_quantity),0) total')
            ->where('bls.client_id', $client->id)
            ->groupBy('products.id')
            ->orderBy('total','desc')
            ->take(15)
            ->get();
        return view('admin.clients.show',$client,compact('client','payments','bls','year','topproducts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('admin.clients.edit',compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateClientRequest  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
       
        try {
            $data=$request->all();
            $client->update($data);
            return $client;
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        try {
            $client->delete();
            return 'success';
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    
}
