<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlRequest;
use App\Http\Requests\UpdateBlRequest;
use App\Models\Bl;
use App\Models\BlDetail;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Client;
use App\Models\Facture;
use App\Models\BlFinancialCommitment;
use App\Models\Order;
use App\Models\Product;
use App\Models\Quantity;
use App\Models\Store;
use App\Notifications\MinQtyNotification;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('admin.bl.index');
    }
    public function daily()
    {
        
        return view('admin.bl.daily');
    }
    
    
    public function getData(Request $request,$client=0)
    {
        $bls=DB::table('bls')
            ->leftJoin('stores', 'bls.store_id', '=', 'stores.id')
            ->leftJoin('admins', 'bls.admin_id', '=', 'admins.id')
            ->leftJoin('factures', 'bls.invoice_id', '=', 'factures.id')
            ->select('bls.*','stores.name as store','admins.name as admin','factures.number as facture_number');
        if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
            $bls->whereBetween('bls.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
        }
        if ($request->has('status') && $request->status!='all') {
            $bls->where('payment_status',$request->status );
        }
        if ($request->has('client') && $request->client!='') {
            $bls->whereRaw('lower(bls.client_details) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->client))))]);
        }
        if($client!=0) $bls->where('bls.client_id',$client);
        
        $bls->groupBy('bls.id');
        $blsToCount=$bls;
        
        if (request()->ajax()) {
            return datatables()->of($bls)
                ->rawColumns(['action'])
                ->addColumn('action', function ($row) {
                    $row_id = $row->id;
                    $form_id = "delete_Alert_form_" . $row_id;
                    $printTicket=route('admin.bl.printA4',$row_id );
                    $printA4=route('admin.bl.printA4',$row_id );
                    $btn = '<div style="display:inline-block; width: 210px;">';
                    if(auth()->user()->can('Imprimer bon de livraison'))
                        $btn.='<a class="icon-btn text-danger p-1"  href="' . route('admin.bl.printA4',$row_id) . '"><i class="fa fa-file-pdf"></i></a><a class="icon-btn text-info p-1"  href="' . route('admin.bl.printTicket',$row_id) . '"><i class="fa fa-tag"></i></a>';
                   
                        if(auth()->user()->can('Modifier bon de livraison'))
                            $btn .='<a class="icon-btn text-success p-1" href="' . route('admin.bl.edit',$row_id)  . '"><i class="fa fa-pencil-alt text-view"></i></a>';
                    
                    if($row->payment_status!=0){
                           if(auth()->user()->can('Ajouter bon de retour'))
                           $btn .='<a class="icon-btn text-success p-1" href="avoir/' . $row_id . '"><i class="fa fa-redo text-view"></i></a>';
                    }
                        if($row->invoice_id!=NULL)
                        if(auth()->user()->can('Imprimer bon de livraison'))
                        $btn .='<a class="icon-btn text-success p-1" href="/admin/facture/printA4/' . $row->invoice_id . '"><i class="fa fa-file-excel text-view"></i></a>';
                    if(auth()->user()->can('Supprimer bon de livraison'))
                        $btn .='<form id="' . $form_id . '" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'bl\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>';
                         $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['checkbox'])
                ->addColumn('checkbox', function ($row) {
                    if($row->invoice_id!=NULL)
                    return '';
                    return ' <div class="checkbox checkbox-info mb-2">
                                 <input id="checkbox-'.$row->id.'" type="checkbox" class="order_check"
                                                   name="bls[]" data-id="'.$row->id.'" value="'.$row->id.'">
                                 <label for="checkbox-'.$row->id.'"></label>
                             </div>';
                })
           
                ->editColumn('client_details', function ($row) {
                    $array= json_decode($row->client_details);
                    return strtoupper($array->client_name);
                })
                ->editColumn('facture_number', function ($row) {
                    if($row->facture_number) return '<a href="'.route('admin.facture.printA4',$row->invoice_id).'" target="_blank"><span class="badge badge-danger">'.$row->facture_number.'</span></a>';
                    return '';
                })
                ->rawColumns(['store'])
                ->addColumn('store', function ($row) {
                    return $row->store;
                })
    
                ->editColumn('payment_status', function ($row) {
                    $btn = ' <div>';
                    if ($row->payment_status=='0') $btn.= '<span class="badge badge-danger">Non Payé</span>';
                    elseif ($row->payment_status=='1') $btn.= '<span class="badge badge-warning">Partiellement Payé</span>';
                    else $btn.= '<span class="badge badge-success">Totalement Payé</span>';
                    return $btn.'</div>';
                })
                ->editColumn('id', function ($row) {
                    return str_pad($row->id,6,0,STR_PAD_LEFT);
                })
                ->editColumn('date', function ($row) {
                    return \Carbon\Carbon::parse($row->date)->format('d/m/Y');
                })
                ->setRowId(function ($row) {
                    return 'row'.$row->id;
                })
                ->with('order', function ()  use($request,$bls){
                    $blsToCount=new Bl();
                    
                    $all=$blsToCount;
                    $paid=$blsToCount;
                    $_paid=$blsToCount;
                    $unpaid=$blsToCount;
                    if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                        $all=$all->whereBetween('bls.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                    }
                    if ($request->has('status') && $request->status!='all') {
                        $all=$all->where('payment_status',$request->status );
                    }
                    if ($request->has('client') && $request->client!='') {
                        $all=$all->whereRaw('lower(bls.client_details) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->client))))]);
                    }
                        $order['all']=$all->sum('total');
                    if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                        $paid=$paid->whereBetween('bls.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                    }
                    if ($request->has('status') && $request->status!='all') {
                        $paid=$paid->where('payment_status',$request->status );
                    }
                    if ($request->has('client') && $request->client!='') {
                        $paid=$paid->whereRaw('lower(bls.client_details) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->client))))]);
                    }
                        $order['paid']=$paid->where('payment_status','2')->sum('total');
                    if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                        $_paid=$_paid->whereBetween('bls.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                    }
                    if ($request->has('status') && $request->status!='all') {
                        $_paid=$_paid->where('payment_status',$request->status );
                    }
                    if ($request->has('client') && $request->client!='') {
                        $_paid=$_paid->whereRaw('lower(bls.client_details) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->client))))]);
                    }
                        $order['p_paid']=$_paid->where('payment_status','1')->sum('total');
                    if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
                        $unpaid=$unpaid->whereBetween('bls.date', [$request->query('from') . ' 00:00:00', $request->query('to') . ' 23:59:59']);
                    }
                    if ($request->has('status') && $request->status!='all') {
                        $unpaid=$unpaid->where('payment_status',$request->status );
                    }
                    if ($request->has('client') && $request->client!='') {
                        $unpaid=$unpaid->whereRaw('lower(bls.client_details) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->client))))]);
                    }
                        $order['unpaid']=$unpaid->where('payment_status','0')->sum('total');
                       
                        return $order;
                    
                })
                ->rawColumns(['action','payment_status','store','checkbox','facture_number'])
                ->addIndexColumn()
                ->make(true);
        }
    }
    public function getDaily(Request $request)
    {
        
         $bl= Bl::where('store_id',auth()->user()->store_id)->with('details');
        if(auth()->user()->hasRole('Vendeur')) $bl=$bl->where('date',date('Y-m-d'));
//        if($request->has('details')){
//            $bl->whereHas('details',function($query) use ($request){
//                $query->where('name','like','%'.$request->columns[2]['search']['value'].'%');
//            });
//        }
        if (request()->ajax()) {
            return datatables()->of(
                $bl
            )
                ->rawColumns(['action'])
                ->addColumn('action', function ($row) {
                    $row_id = $row->id;
                    $form_id = "delete_Alert_form_" . $row_id;
                    $printTicket=route('admin.bl.printA4',$row_id );
                    $printA4=route('admin.bl.printA4',$row_id );
                    $btn = '<div style="display:inline-block; width: 210px;">';
                    if(auth()->user()->can('Imprimer bon de livraison'))
                        $btn.='<a class="icon-btn text-danger p-1"  href="printA4/' . $row_id . '"><i class="fa fa-file-pdf"></i></a><a class="icon-btn text-info p-1"  href="printTicket/' . $row_id . '"><i class="fa fa-tag"></i></a>';
    
                    if(auth()->user()->can('Modifier bon de livraison'))
                        $btn .='<a class="icon-btn text-success p-1" href="' . route('admin.bl.edit',$row_id)  . '"><i class="fa fa-pencil-alt text-view"></i></a>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['details'])
                ->addColumn('details', function ($row) {
                    $btn='<table class="table  table-bordered mb-0">';
                    foreach ($row->details as $detail){
                        $btn.='<tr>
                        <td width="500px">'.$detail->product_name.'</td>
                        <td width="50px">'.$detail->product_quantity.'</td>
                        <td width="50px">'.($detail->product_price_selling-$detail->product_remise)*$detail->product_quantity.'</td>';
                        if(auth()->user()->can('Forcer Les Quantités')) $btn.='<td width="50px">'.$detail->quantity_left()->sum('quantity').'</td>';
                        $btn.='<td width="50px">'.$detail->product_bonus.'</td>
                        </tr>';
                    }
                    $btn.='</table>';
                    return $btn;
                })
    
                ->editColumn('client_details', function ($row) {
                    
                    return strtoupper($row->client_details['client_name']);
                })
                
                
               
                ->editColumn('id', function ($row) {
                    return str_pad($row->id,6,0,STR_PAD_LEFT);
                })
                ->editColumn('date', function ($row) {
                    return \Carbon\Carbon::parse($row->date)->format('d/m/Y');
                })
                ->setRowId(function ($row) {
                    return 'row'.$row->id;
                })
                ->rawColumns(['action','details'])
                ->addIndexColumn()
                ->make(true);
        }
        
    }
    public function roulement (Request $request){
        $categories = Category::all();
        $brands = Brand::all();
        $stores = Store::all();
        return view('admin.bl.roulement', compact('categories', 'stores','brands','stores'));
    
    }
    public function getRoulement(Request $request){
        if ($request->has('from') && $request->query('from') != '' && $request->has('to') && $request->query('to') != '') {
            $from = $request->query('from') . ' 00:00:00';
            $to = $request->query('to') . ' 23:59:59';
        } else {
            $from = date('Y-m-d') . ' 00:00:00';
            $to = date('Y-m-d') . ' 23:59:59';
        }

        $products = DB::table('products AS P')
            ->select(
                'P.group_name as product_name',
                DB::raw('COALESCE(SUM(DetailTotals.Total), 0) AS total_bl_pieces_sold'),
                DB::raw('COALESCE(SUM(OrderTotals.Total), 0) AS total_order_pieces_sold'),
                DB::raw('COALESCE(DetailTotals.Total, 0) + COALESCE(OrderTotals.Total, 0) AS total')
            )
            ->leftJoin(DB::raw('(SELECT O2.product_id, SUM(O2.product_quantity) AS Total FROM order_details AS O2
            WHERE O2.created_at BETWEEN ? AND ? AND O2.order_id IN (SELECT id FROM orders WHERE orders.status = "delivered")
            GROUP BY O2.product_id) AS OrderTotals'), function($join) {
                $join->on('OrderTotals.product_id', '=', 'P.Id');
            });
            if ($request->has('store_id') && $request->store_id != '' ){
                $products=$products->leftJoin(DB::raw('(SELECT O1.product_id, SUM(O1.product_quantity) AS Total FROM bl_details AS O1
            WHERE O1.created_at BETWEEN ? AND ? AND O1.bl_id IN (SELECT id FROM bls WHERE bls.store_id = "'.$request->store_id.'" )
            GROUP BY O1.product_id) AS DetailTotals'), function($join) {
                    $join->on('DetailTotals.product_id', '=', 'P.Id');
                });
            }else{
                $products=$products->leftJoin(DB::raw('(SELECT O1.product_id, SUM(O1.product_quantity) AS Total FROM bl_details AS O1
             WHERE O1.created_at BETWEEN ? AND ? GROUP BY O1.product_id) AS DetailTotals'), 'DetailTotals.product_id', '=', 'P.Id');
            }
     
    
        $products=$products->groupBy('P.preference_id')
            ->orderByDesc('total')
            ->setBindings([$from, $to, $from, $to]);
        /*O2.order_id IN (SELECT id FROM orders WHERE status = "delivered")*/
        if ($request->has('brand_id') && $request->brand_id != '' ){
            $products=$products->where('P.brand_id',$request->brand_id);
        }
        if ($request->has('group_name') && $request->group_name != '' ){
            $products=$products->whereRaw('lower(P.group_name) regexp (?)',[strtolower(implode(' *',str_split(str_replace(' ','',$request->group_name))))]);
        }
     
        if (request()->ajax()) {
            return datatables()->of($products)
//                ->addColumn('total', function ($row) {
//                    return $row->total_bl_pieces_sold + $row->total_order_pieces_sold;
//                })
                ->editColumn('total', function ($row) {
                    return $row->total_bl_pieces_sold + $row->total_order_pieces_sold;
                })
                
                ->addIndexColumn()
                /*->withQuery('bonus', function ($filteredQuery) use($request){
                    return $filteredQuery->sum( DB::raw('(SUM(CASE WHEN bs.to_store_id = 1 THEN bs_details.product_quantity_sent ELSE 0 END) - SUM(CASE WHEN bs.from_store_id = 1 THEN bs_details.product_quantity_sent ELSE 0 END)) * bs_details.product_price'));
                })*/
                ->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        $products = DB::table('products')
            ->select('*')
            ->orderBy('value')
            ->get();
        $clients = DB::table('clients')
            ->select('clients.*',  DB::raw('SUM(`bls`.`total`) as `encours`' ))
            ->leftJoin('bls', 'clients.id', '=', 'bls.client_id')
            ->groupBy('clients.id', 'clients.name', 'clients.mf')
            ->get();
        $stores=Store::get();
        return view('admin.bl.create',compact('products','clients','stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBlRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBlRequest $request)
    {
        $data = $request->all();
        $client=Client::where('phone',$data['client_details']['client_phone'])->first();
        if(!$client){
            $client['phone']=$data['client_details']['client_phone'];
            $client['mf']=$data['client_details']['client_mf'];
            $client['address']=$data['client_details']['client_address'];
            $client['name']=$data['client_details']['client_name'];
            $client=Client::create($client);
            $data['client_id']=$client->id;
        }
      
        try {
            DB::beginTransaction();
            
            if (isset($data['details'])) {
                $number = count($data["details"]);
                if($request->piece_type!='facture')
                $data['number'] = $number;
               
                $data['admin_id'] = auth()->user()->id;
                if ($request->piece_type=='bl'){
                    $cart = Bl::create($data);
                }elseif($request->piece_type=='devis'){
                    $cart = Quotation::create($data);
                }elseif($request->piece_type=='facture'){
                    $cart = Facture::create($data);
                }
                $total_brut=0;
                $total_TTC=0;
                
                foreach($data['details'] as $item){
                    //$after_discount= ($item['product_price_selling']*(100-$item['product_remise']))/100;
                    $total_brut+= ($item['product_price_selling']* $item['product_quantity']);
                    $after_discount= ($item['product_price_selling']-$item['product_remise']);
                    $total_TTC += ($after_discount * $item['product_quantity']);
                }
    
    
    
                
                $cart->details()->createMany($data["details"]);
                $cart->client_details['client_name'];
              
            
                $cart->total=$total_TTC;
                $cart->save();
                if ($request->piece_type=='bl') {
                    /*Update Quantities*/
//                    foreach ($data['details'] as $key => $item) {
//                        $qt = $data['details'][$key]['product_quantity'];
//                        if ($qt > 0) {
//
//                            $quantity=Quantity::firstOrCreate(
//                                [
//                                    'store_id'=>$data['store_id'],
//                                    'product_id'=>$data['details'][$key]['product_id']
//                                ]
//                            );
//
//                            $quantity_update = $this->updateQty($data['details'][$key], $quantity);
//                        }
//
//                    }
                    /*Update Quantities*/
                    /*Store Payment Details*/
                    if (isset($data['commitmentType']) && $data['commitmentType'] == 'echeance') {
                        $cart->commitments()->createMany($data['echeance']);
                    }
                    elseif (isset($data['commitmentType']) && $data['commitmentType'] == 'comptant') {
                        $finacial_data['bl_id'] = $cart->id;
                        $finacial_data['date'] = date('Y-m-d H:i:s');
                        $finacial_data['amount'] = $total_TTC;
                        
                        $finacial = BlFinancialCommitment::create($finacial_data);
                        $cash = 0;
                        $check = 0;
                        $transfer = 0;
                        $sodexo = 0;
                        $payment = collect($request->payments);
        
                        if (isset($data['payments']['cash'])) {
                            $count = count($data['payments']['cash']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['cash'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['cash'][$i + 1]['type'] = 'cash';
                                $data['payments']['cash'][$i + 1]['store_id'] = $data['store_id'];
                            }
                            $finacial->payments()->createMany($data['payments']['cash']);
                            $cashs = collect($payment['cash']);
                            $cash = $cashs->sum('amount');
                        }
                        if (isset($data['payments']['check'])) {
                            $count = count($data['payments']['check']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['check'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['check'][$i + 1]['type'] = 'check';
                                $data['payments']['check'][$i + 1]['store_id'] = $data['store_id'];
                            }
                            $finacial->payments()->createMany($data['payments']['check']);
                            $checks = collect($payment['check']);
                            $check = $checks->sum('amount');
                        }
                        if (isset($data['payments']['transfer'])) {
                            $count = count($data['payments']['transfer']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['transfer'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['transfer'][$i + 1]['type'] = 'transfer';
                                $data['payments']['transfer'][$i + 1]['store_id'] = $data['store_id'];
                            }
                            
                            $finacial->payments()->createMany($data['payments']['transfer']);
                            $transfers = collect($payment['transfer']);
                            $transfer = $transfers->sum('amount');
                        }
                        if (isset($data['payments']['sodexo'])) {
                            $count = count($data['payments']['sodexo']);
                            for ($i = 0; $i < $count; $i++) {
                                
                                $data['payments']['sodexo'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['sodexo'][$i + 1]['type'] = 'sodexo';
                                $data['payments']['sodexo'][$i + 1]['store_id'] = $data['store_id'];
                            }
                            $finacial->payments()->createMany($data['payments']['sodexo']);
                            $sodexos = collect($payment['sodexo']);
                            $sodexo = $sodexos->sum('amount');
                        }
                        $total_payments = ($cash + $check + $transfer + $sodexo);
                        if ($total_payments == $total_TTC){
                            $cart->update(['payment_status' => 2]);
                            $finacial->update(['payment_status' => 2]);
                        }
                        elseif ($total_payments<$total_TTC && $total_payments!=0 )
                        {
                            $cart->update(['payment_status' => 1]);
                            $finacial->update(['payment_status' => 1]);
                        }else{
                            $cart->update(['payment_status' => 0]);
                            $finacial->update(['payment_status' => 0]);
                        }
                    }
                    /*Store Payment Details*/
                }
            }
            DB::commit();
            if($request->submit=='a4')
                return redirect()->route('admin.'.$request->piece_type.'.printA4',$cart->id);
            else
                return redirect()->route('admin.'.$request->piece_type.'.printTicket',$cart->id);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
        
        
    }
 
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bl  $bl
     * @return \Illuminate\Http\Response
     */
    public function show(Bl $bl)
    {
        //
    }
    public function printA4(Bl $bl)
    {
        
        $bl->load('details');
        return AppHelper::printA4($bl,'bl');
    }
    public function printTicket(Bl $bl)
    {
        $bl->load('details');
        return AppHelper::printTicket($bl,'bl');
    }
    
    public function payBonus (BlDetail $bl_detail){
        try {
            $bl_detail->options = 'paid';
            $bl_detail->save();
            return 'success';
        }catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
     
        }
    }
    
    public function payBulkBonus (Request $request){
        try {
            $bl_detail =BlDetail::whereIn('id' , explode(",", $request->payBonus))->update(['options'=>'paid']);
            return 'success';
        }catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bl  $bl
     * @return \Illuminate\Http\Response
     */
    public function edit(Bl $bl)
    {
    
        $products = DB::table('products')
            ->leftJoin('quantities', 'products.id', '=', 'quantities.product_id')
            ->select('products.id','products.name','products.ref','products.barcode','products.buying','products.unity','products.tva', 'quantities.quantity', 'products.price as detail', 'products.price as sgros', 'products.price as gros')
        
        
            ->groupBy('products.id')
            ->get();
        $clients = DB::table('clients')
            ->select('clients.*',  DB::raw('SUM(`factures`.`total`) as `encours`' ))
            ->leftJoin('factures', 'clients.id', '=', 'factures.client_id')
            ->groupBy('clients.id')->get();
        
        
        return view('admin.bl.edit',compact('bl','products','clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBlRequest  $request
     * @param  \App\Models\Bl  $bl
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBlRequest $request, Bl $bl)
    {
        $data = $request->all();
        $client=Client::where('phone',$data['client_details']['client_phone'])->first();
        if(!$client){
            $client['phone']=$data['client_details']['client_phone'];
            $client['mf']=$data['client_details']['client_mf'];
            $client['address']=$data['client_details']['client_address'];
            $client['name']=$data['client_details']['client_name'];
            $client=Client::create($client);
            $data['client_id']=$client->id;
        }
        try {
            DB::beginTransaction();
        
        
            if (isset($data['details'])) {
                $old_details= $bl->load('details');
//                foreach ($old_details->details as $key => $product) {
//                    $quantity=Quantity::firstOrCreate(
//                        [
//                            'store_id'=>$old_details->store_id,
//                            'product_id'=>$product->product_id
//                        ]
//                    );
//
//                        $quantity_update = $this->updateQty($product, $quantity, 'in');
//                }
                $bl->details()->delete();
                $bl->payments()->delete();
                $bl->commitments()->delete();
                
                $number = count($data["details"]);
                $data['number'] = $number;
               $data['store_id'] = $old_details->store_id;
                //$data['admin_id'] = auth()->user()->id;
                $bl->update($data);
    
                
                $bl->client_details['client_name'];
                $total_brut=0;
                $total_TTC=0;
    
                foreach($data['details'] as $item){
                    //$after_discount= ($item['product_price_selling']*(100-$item['product_remise']))/100;
                    $total_brut+= ($item['product_price_selling']* $item['product_quantity']);
                    $after_discount= ($item['product_price_selling']-$item['product_remise']);
                    $total_TTC += ($after_discount * $item['product_quantity']);
                }
                $bonus_status=1;
                if($total_brut!=0)
                {
                    $remise_total=$total_TTC/$total_brut;
                    if($remise_total < 0.9  && $remise_total > 0.85 ){
                        $bonus_status=0.5;
                    }
                    elseif($remise_total < 0.85   ){
                        $bonus_status=0;
                    }else{
                        $bonus_status=1;
                    }
                }
                foreach($data['details'] as $k=>$v){
                    $data['details'][$k]['product_bonus']=$data['details'][$k]['product_bonus']*$bonus_status;
                }
    
                $bl->details()->createMany($data["details"]);
                $bl->total=$total_TTC;
                $bl->save();
                if ($request->piece_type=='bl') {
                    /*Update Quantities*/
                    
                    
//                    foreach ($data['details'] as $key => $item) {
//                        $qt = $data['details'][$key]['product_quantity'];
//                        if ($qt > 0) {
//
//                            $quantity=Quantity::firstOrCreate(
//                                [
//                                    'store_id'=>$old_details->store_id,
//                                    'product_id'=>$data['details'][$key]['product_id']
//                                ]
//                            );
//                            $quantity_update = $this->updateQty($data['details'][$key], $quantity);
//                        }
//
//                    }
                    /*Update Quantities*/
                    /*Store Payment Details*/
                    if (isset($data['commitmentType']) && $data['commitmentType'] == 'echeance') {
                        $bl->commitments()->createMany($data['echeance']);
                        $bl->update(['payment_status' => 0]);
                    }
                    elseif (isset($data['commitmentType']) && $data['commitmentType'] == 'comptant'){
                        $finacial_data['bl_id'] = $bl->id;
                        $finacial_data['date'] = date('Y-m-d H:i:s');
                        $finacial_data['amount'] = $total_TTC;
                        $finacial_data['payment_status'] = 1;
                        $finacial = BlFinancialCommitment::create($finacial_data);
                        $cash = 0;
                        $check = 0;
                        $transfer = 0;
                        $sodexo = 0;
                        $payment = collect($request->payments);
                    
                        if (isset($data['payments']['cash'])) {
                            $count = count($data['payments']['cash']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['cash'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['cash'][$i + 1]['type'] = 'cash';
                                $data['payments']['cash'][$i + 1]['store_id'] = $old_details->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['cash']);
                            $cashs = collect($payment['cash']);
                            $cash = $cashs->sum('amount');
                        }
                        if (isset($data['payments']['check'])) {
                            $count = count($data['payments']['check']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['check'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['check'][$i + 1]['type'] = 'check';
                                $data['payments']['check'][$i + 1]['store_id'] = $old_details->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['check']);
                            $checks = collect($payment['check']);
                            $check = $checks->sum('amount');
                        }
                        if (isset($data['payments']['transfer'])) {
                            $count = count($data['payments']['transfer']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['transfer'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['transfer'][$i + 1]['type'] = 'transfer';
                                $data['payments']['transfer'][$i + 1]['store_id'] = $old_details->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['transfer']);
                            $transfers = collect($payment['transfer']);
                            $transfer = $transfers->sum('amount');
                        }
                        if (isset($data['payments']['sodexo'])) {
                            $count = count($data['payments']['sodexo']);
                            for ($i = 0; $i < $count; $i++) {
                                $data['payments']['sodexo'][$i + 1]['client_id'] = $request->client_id;
                                $data['payments']['sodexo'][$i + 1]['type'] = 'sodexo';
                                $data['payments']['sodexo'][$i + 1]['store_id'] = $old_details->store_id;
                            }
                            $finacial->payments()->createMany($data['payments']['sodexo']);
                            $sodexos = collect($payment['sodexo']);
                            $sodexo = $sodexos->sum('amount');
                        }
                        $total_payments = ($cash + $check + $transfer + $sodexo);
                        if ($total_payments == $total_TTC){
                            $bl->update(['payment_status' => 2]);
                            $finacial->update(['payment_status' => 2]);
                        }
                        elseif ($total_payments<$total_TTC && $total_payments!=0 )
                        {
                            $bl->update(['payment_status' => 1]);
                            $finacial->update(['payment_status' => 1]);
                        }else{
                            $bl->update(['payment_status' => 0]);
                            $finacial->update(['payment_status' => 0]);
                        }
                    }
                    /*Store Payment Details*/
                }
            }
            DB::commit();
            if($request->submit=='a4')
                return redirect()->route('admin.'.$request->piece_type.'.printA4',$bl->id);
            else
                return redirect()->route('admin.'.$request->piece_type.'.printTicket',$bl->id);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            Log::error($e->getLine());
            return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bl  $bl
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bl $bl)
    {
        try {
            DB::beginTransaction();
        $old_details= $bl->load('details');
//        foreach ($old_details->details as $key => $product) {
//            $quantity=Quantity::firstOrCreate(
//                [
//                    'store_id'=>$bl->store_id,
//                    'product_id'=>$product->product_id
//                ]
//            );
//            $quantity_update = $this->updateQty($product, $quantity, 'in');
//        }
        $bl->details()->delete();
        $bl->payments()->delete();
        $bl->commitments()->delete();
            $bl->delete();
            DB::commit();
            return 'success';
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
    
    
    public function updateQty($product, $line, $action='out')
    
    {
        
        if ($action == 'out') {
            $updatedQty = (int)($line->quantity - $product['product_quantity']);
        } else {
            $updatedQty = (int)($line->quantity + $product['product_quantity']);
        }
      
            
            $line->quantity=$updatedQty;
            $line->save();
        
        
        return true;
    }
    
    
}
