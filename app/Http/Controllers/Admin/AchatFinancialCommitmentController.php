<?php
    
    namespace App\Http\Controllers\Admin;
    
    use App\Http\Controllers\Controller;
    use App\Models\Achat;
    use App\Models\AchatFinancialCommitment;
    use App\Models\Bl;
    use App\Models\Client;
    use App\Models\FinancialCommitment;
    use App\Models\BlPayment;
    use Carbon\Carbon;
    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Validation\Rule;
    
    class AchatFinancialCommitmentController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('admin.achatfinancialcommitment.index');
        }
    
        public function getData()
        {
            
            if (request()->ajax()) {
                return datatables()->of(DB::table('achat_financial_commitments')
                    ->leftJoin('achats', 'achats.id', '=', 'achat_financial_commitments.achat_id')
                    ->leftJoin('providers', 'achats.provider_id', '=', 'providers.id')
                    ->leftJoin('stores', 'achats.store_id', '=', 'stores.id')
                    ->select('achat_financial_commitments.*','providers.name as provider','providers.id as provider_id','stores.name as store','achats.provider_details as provider_details')
                    ->groupBy('achat_financial_commitments.id'))
                    ->rawColumns(['action'])
                    ->addColumn('action', function ($row) {
                        $row_id = $row->id;
                        $form_id = "delete_Alert_form_" . $row_id;
                        $printTicket=route('admin.bl.printA4',$row_id );
                        $printA4=route('admin.bl.printA4',$row_id );
                        $btn = ' <div style="display:inline-block; width: 210px;">
                                    <a class="icon-btn text-success p-1" href="edit/' . $row_id . '"><i class="fa fa-pencil-alt text-view"></i></a>
                                </div>';
                        return $btn;
                    })
                    ->rawColumns(['provider'])
                    ->addColumn('provider', function ($row) {
                        if ($row->provider_id=='0') return 'Passager';
    
                        $array= json_decode($row->provider_details);
                        return $array->provider_name;
                    })
                    ->rawColumns(['store'])
                    ->addColumn('store', function ($row) {
                        return $row->store;
                    })
                
                    ->editColumn('payment_status', function ($row) {
                        $btn = ' <div>';
                        if ($row->payment_status=='0') $btn.= '<span class="badge badge-danger">Non Payé</span>';
                        elseif ($row->payment_status=='1') $btn.= '<span class="badge badge-warning">Partiellement Payé</span>';
                        else $btn.= '<span class="badge badge-success">Totalement Payé</span>';
                        return $btn.'</div>';
                    })
                    ->editColumn('achat_id', function ($row) {
                        
                        return '<a href="'.route('admin.achat.printA4',$row->achat_id).'"><span class="badge badge-success">'.str_pad($row->achat_id,3,0,STR_PAD_LEFT).'</span></a>';
                        
                    })
                    ->editColumn('id', function ($row) {
                        return str_pad($row->id,6,0,STR_PAD_LEFT);
                    })
                    ->setRowId(function ($row) {
                        return $row->id;
                    })
                    ->rawColumns(['action','payment_status','provider','store','achat_id'])
                    ->addIndexColumn()
                    ->make(true);
            }
        }
        
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }
        
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
        }
        
        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show(Request $request, $cart)
        {
        
        }
        
        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit(Request $request, AchatFinancialCommitment $fc)
        {
            $fc->load('payments');
            
            return view('admin.achatfinancialcommitment.edit', compact('fc'));
        }
        
        
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, AchatFinancialCommitment $fc)
        {
            try {
                DB::beginTransaction();
                $data = $request->all();
                $cash = 0;
                $check = 0;
                $transfer = 0;
                $sodexo = 0;
                $payment = collect($request->payments);
                if (isset($data['payments']['cash'])) {
                    $count = count($data['payments']['cash']);
                    for ($i = 0; $i < $count; $i++) {
                        $data['payments']['cash'][$i + 1]['provider_id'] = $fc->provider_id;
                        $data['payments']['cash'][$i + 1]['type'] = 'cash';
                        $data['payments']['cash'][$i + 1]['store_id'] = auth()->user()->store_id;
                    }
                    $fc->payments()->createMany($data['payments']['cash']);
                    $cashs = collect($payment['cash']);
                    $cash = $cashs->sum('amount');
                }
                if (isset($data['payments']['check'])) {
                    $count = count($data['payments']['check']);
                    for ($i = 0; $i < $count; $i++) {
                        $data['payments']['check'][$i + 1]['provider_id'] = $fc->provider_id;
                        $data['payments']['check'][$i + 1]['type'] = 'check';
                        $data['payments']['check'][$i + 1]['store_id'] = auth()->user()->store_id;
                    }
                    $fc->payments()->createMany($data['payments']['check']);
                    $checks = collect($payment['check']);
                    $check = $checks->sum('amount');
                }
                if (isset($data['payments']['transfer'])) {
                    $count = count($data['payments']['transfer']);
                    for ($i = 0; $i < $count; $i++) {
                        $data['payments']['transfer'][$i + 1]['provider_id'] = $fc->provider_id;
                        $data['payments']['transfer'][$i + 1]['type'] = 'transfer';
                        $data['payments']['transfer'][$i + 1]['store_id'] = auth()->user()->store_id;
                    }
                    $fc->payments()->createMany($data['payments']['transfer']);
                    $transfers = collect($payment['transfer']);
                    $transfer = $transfers->sum('amount');
                }
                if (isset($data['payments']['sodexo'])) {
                    $count = count($data['payments']['sodexo']);
                    for ($i = 0; $i < $count; $i++) {
                        $data['payments']['sodexo'][$i + 1]['provider_id'] = $fc->provider_id;
                        $data['payments']['sodexo'][$i + 1]['type'] = 'sodexo';
                        $data['payments']['sodexo'][$i + 1]['store_id'] = auth()->user()->store_id;
                    }
                    $fc->payments()->createMany($data['payments']['sodexo']);
                    $sodexos = collect($payment['sodexo']);
                    $sodexo = $sodexos->sum('amount');
                }
                $achat=Achat::find($fc->achat_id);
                $achat->load('payments');
                $total_payments=$achat->payments->sum('amount');
                if ($total_payments == $achat->total){
                    $achat->update(['payment_status' => 2]);
                    $fc->update(['payment_status' => 2]); }
                else{
                    $achat->update(['payment_status' => 1]);
                    $fc->load('payments');
                    $total_payments_fc = $fc->payments->sum('amount');
                    if ($total_payments == $fc->amount){$fc->update(['payment_status' => 2]);}
                    else {$fc->update(['payment_status' => 1]);}
                }
                
                DB::commit();
                // $commitment ;
                return redirect()->back()->with('success', "Ajouté avec succès!");
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
     
        
        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        
       
    }
