<?php
    
    namespace App\Http\Controllers\Admin;
    
    use App\Http\Controllers\Controller;
    use App\Models\Pos;
    use App\Models\Client;
    use App\Models\FinancialCommitment;
    use App\Models\PosPayment;
    use Carbon\Carbon;
    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Validation\Rule;
    
    class FinancialCommitmentController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('admin.financialcommitment.index');
        }
    
        public function getData()
        {
            
            if (request()->ajax()) {
                return datatables()->of(DB::table('financial_commitments')
                    ->leftJoin('pos', 'pos.id', '=', 'financial_commitments.pos_id')
                    ->leftJoin('clients', 'pos.client_id', '=', 'clients.id')
                    ->leftJoin('stores', 'pos.store_id', '=', 'stores.id')
                    ->select('financial_commitments.*','clients.name as client','clients.id as client_id','stores.name as store','pos.client_details as client_details')
                    ->groupBy('financial_commitments.id'))
                    ->rawColumns(['action'])
                    ->addColumn('action', function ($row) {
                        $row_id = $row->id;
                        $form_id = "delete_Alert_form_" . $row_id;
                        $printTicket=route('admin.pos.printA4',$row_id );
                        $printA4=route('admin.pos.printA4',$row_id );
                        $btn = ' <div style="display:inline-block; width: 210px;">
                                    <a class="icon-btn text-success p-1" href="edit/' . $row_id . '"><i class="fa fa-pencil-alt text-view"></i></a>
                                </div>';
                        return $btn;
                    })
                    ->rawColumns(['client'])
                    ->addColumn('client', function ($row) {
                        if ($row->client_id=='0') return 'Passager';
    
                        $array= json_decode($row->client_details);
                        return $array->client_name;
                    })
                    ->rawColumns(['store'])
                    ->addColumn('store', function ($row) {
                        return $row->store;
                    })
                
                    ->editColumn('payment_status', function ($row) {
                        $btn = ' <div>';
                        if ($row->payment_status=='0') $btn.= '<span class="badge badge-danger">Non Payé</span>';
                        elseif ($row->payment_status=='1') $btn.= '<span class="badge badge-warning">Partiellement Payé</span>';
                        else $btn.= '<span class="badge badge-success">Totalement Payé</span>';
                        return $btn.'</div>';
                    })
                    ->editColumn('pos_id', function ($row) {
                        
                        return '<a href="'.route('admin.pos.printA4',$row->pos_id).'"><span class="badge badge-success">'.str_pad($row->pos_id,3,0,STR_PAD_LEFT).'</span></a>';
                        
                    })
                    ->editColumn('id', function ($row) {
                        return str_pad($row->id,6,0,STR_PAD_LEFT);
                    })
                    ->setRowId(function ($row) {
                        return $row->id;
                    })
                    ->rawColumns(['action','payment_status','client','store','pos_id'])
                    ->addIndexColumn()
                    ->make(true);
            }
        }
        
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }
        
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
        }
        
        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show(Request $request, $cart)
        {
        
        }
        
        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit(Request $request, FinancialCommitment $fc)
        {
            $fc->load('payments');
            
            return view('admin.financialcommitment.edit', compact('fc'));
        }
        
        
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, FinancialCommitment $fc)
        {
            try {
                DB::beginTransaction();
                $data = $request->all();
                $cash = 0;
                $check = 0;
                $transfer = 0;
                $sodexo = 0;
                $payment = collect($request->payments);
                if (isset($data['payments']['cash'])) {
                    $count = count($data['payments']['cash']);
                    for ($i = 0; $i < $count; $i++) {
                        $data['payments']['cash'][$i + 1]['client_id'] = $fc->client_id;
                        $data['payments']['cash'][$i + 1]['type'] = 'cash';
                        $data['payments']['cash'][$i + 1]['store_id'] = auth()->user()->store_id;
                    }
                    $fc->payments()->createMany($data['payments']['cash']);
                    $cashs = collect($payment['cash']);
                    $cash = $cashs->sum('amount');
                }
                if (isset($data['payments']['check'])) {
                    $count = count($data['payments']['check']);
                    for ($i = 0; $i < $count; $i++) {
                        $data['payments']['check'][$i + 1]['client_id'] = $fc->client_id;
                        $data['payments']['check'][$i + 1]['type'] = 'check';
                        $data['payments']['check'][$i + 1]['store_id'] = auth()->user()->store_id;
                    }
                    $fc->payments()->createMany($data['payments']['check']);
                    $checks = collect($payment['check']);
                    $check = $checks->sum('amount');
                }
                if (isset($data['payments']['transfer'])) {
                    $count = count($data['payments']['transfer']);
                    for ($i = 0; $i < $count; $i++) {
                        $data['payments']['transfer'][$i + 1]['client_id'] = $fc->client_id;
                        $data['payments']['transfer'][$i + 1]['type'] = 'transfer';
                        $data['payments']['transfer'][$i + 1]['store_id'] = auth()->user()->store_id;
                    }
                    $fc->payments()->createMany($data['payments']['transfer']);
                    $transfers = collect($payment['transfer']);
                    $transfer = $transfers->sum('amount');
                }
                if (isset($data['payments']['sodexo'])) {
                    $count = count($data['payments']['sodexo']);
                    for ($i = 0; $i < $count; $i++) {
                        $data['payments']['sodexo'][$i + 1]['client_id'] = $fc->client_id;
                        $data['payments']['sodexo'][$i + 1]['type'] = 'sodexo';
                        $data['payments']['sodexo'][$i + 1]['store_id'] = auth()->user()->store_id;
                    }
                    $fc->payments()->createMany($data['payments']['sodexo']);
                    $sodexos = collect($payment['sodexo']);
                    $sodexo = $sodexos->sum('amount');
                }
                $bl=Bl::find($fc->bl_id);
                $bl->load('payments');
                $total_payments=$bl->payments->sum('amount');
                if ($total_payments == $bl->total){
                    $bl->update(['payment_status' => 2]);
                    $fc->update(['payment_status' => 2]); }
                else{
                    $bl->update(['payment_status' => 1]);
                    $fc->load('payments');
                    $total_payments_fc = $fc->payments->sum('amount');
                    if ($total_payments == $fc->amount){$fc->update(['payment_status' => 2]);}
                    else {$fc->update(['payment_status' => 1]);}
                }
                
                DB::commit();
                // $commitment ;
                return redirect()->back()->with('success', "Ajouté avec succès!");
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        public function updateCommitments(Request $request, $cart)
        {
            try {
                DB::beginTransaction();
                $data = $request->all();
                // $model = null; $commitment_model = null; $foreign = null;
                $type = $request->query('type');
                switch ($type) {
                    case 'cart':
                        $model = 'App\Models\Cart';
                        $commitment_model = 'App\Models\FinancialCommitment';
                        $foreign = 'cart_id';
                        break;
                    case 'creditnote':
                        $model = 'App\Models\CreditNote';
                        $commitment_model = 'App\Models\CreditnoteCommitment';
                        $foreign = 'credit_note_id';
                        break;
                    case 'returnnote':
                        $model = 'App\Models\ReturnNote';
                        $commitment_model = 'App\Models\ReturnNoteCommitment';
                        $foreign = 'return_note_id';
                        break;
                    
                    default:
                        break;
                }
                
                $cart = $model::find($cart);
                
                $cart->commitments()->delete();
                
                if (isset($data['Deadline_check'])) {
                    $cart->commitments()->createMany($data['Deadline']);
                } else {
                    $financial_data[$foreign] = $cart->id;
                    $financial_data['date'] = date('Y-m-d H:i:s');
                    $financial_data['amount'] = $data['total'];
                    $financial_data['paiment_status'] = 1;
                    $finacial = $commitment_model::create($financial_data);
                    if (isset($data['paiements']['cash'])) {
                        $finacial->paiements()->createMany($data['paiements']['cash']);
                    }
                    if (isset($data['paiements']['check'])) {
                        $finacial->paiements()->createMany($data['paiements']['check']);
                    }
                    if (isset($data['paiements']['transfer'])) {
                        $finacial->paiements()->createMany($data['paiements']['transfer']);
                    }
                    if (isset($data['paiements']['exchange'])) {
                        $finacial->paiements()->createMany($data['paiements']['exchange']);
                    }
                    $cart->update(['paiment_status' => 1]);
                }
                DB::commit();
                return redirect()->route('financialcommitment.index')->with('success', "Ajouté avec succès!");
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
                return ("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        
        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function delete(Cart $cart)
        {
            try {
                DB::beginTransaction();
                $cart->commitments()->delete();
                DB::commit();
                return redirect()->route('financialcommitment.index')->with('success', "Supprimé avec succès!");
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
        public function deleteReturn(CreditnoteCommitment $commitment)
        {
            try {
                $commitment->delete();
                return redirect()->route('financialcommitment.creditnote.index')->with('success', "Supprimé avec succès!");
            } catch (\Exception $e) {
                Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            }
        }
    }
