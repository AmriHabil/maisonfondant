<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProviderRequest;
use App\Http\Requests\UpdateProviderRequest;
use App\Models\Provider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('admin.providers.index');
    }
    
    public function getData()
    {
        $providers = DB::table('providers')
            ->get();
        if (request()->ajax()) {
            return datatables()->of($providers)
                ->rawColumns(['action'])
                ->addColumn('action', function ($row) {
                    $row_id = $row->id;
                    $form_id = "delete_Alert_form_" . $row_id;
                    $btn = '    <div style="display:inline-block; width: 210px;">
                                    <a class="icon-btn text-info p-1" href="show/' . $row_id . '"><i class="fa fa-eye"></i></a>
                                    <a class="icon-btn text-success p-1" href="edit/' . $row_id . '"><i class="fa fa-pencil-alt text-view"></i></a>
                                    <a class="icon-btn text-warning p-1" href="settings/' . $row_id . '"><i class="fa fa-cogs text-view"></i></a>
                                    <form id="' . $form_id . '" method="POST" action="" style="display:inline">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" deleteid="'.$row_id.'" onclick="deleteRecord(this,\'providers\');"><i class="fa fa-trash text-delete"></i></a>
                                    </form>
                                </div>';
                    return $btn;
                })
                ->setRowId(function ($row) {
                    return $row->id;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.providers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProviderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProviderRequest $request)
    {
        $data = $request->all();
        DB::beginTransaction();
        try {
            $provider = Provider::create($data);
            DB::commit();
            return response()->json(['data' => $provider, 'success' => true], 201);
        } catch (\Exception $e) {
            
            DB::rollback();
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
            return response()->json([
                'error' => $e->getMessage(),
            ], 500);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show(Provider $provider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function edit(Provider $provider)
    {
        return view('admin.providers.edit',compact('provider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProviderRequest  $request
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProviderRequest $request, Provider $provider)
    {
        try {
            $data=$request->all();
            $provider->update($data);
            return $provider;
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provider $provider)
    {
        try {
            $provider->delete();
            return 'success';
        } catch (\Exception $e) {
            Log::error("File: " . $e->getFile() . " \nLine: " . $e->getLine() . " Error: " . $e->getMessage());
        }
    }
}
