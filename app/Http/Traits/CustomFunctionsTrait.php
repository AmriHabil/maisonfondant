<?php
    
    namespace App\Http\Traits;
    
    trait CustomFunctionsTrait
    {
        
        public  static function int2str($a){
            $millimes = substr(number_format($a, 3), -3);
            $convert = explode('.', $a);
            if (isset($convert[1]) && $convert[1] != '') {
                return CustomFunctionsTrait::int2str($convert[0]) . ' Dinars' . ' et ' .CustomFunctionsTrait::int2str($millimes) . ' Millimes';
            }
            
            if ($a<0) return 'moins '.CustomFunctionsTrait::int2str(-$a);
            if ($a<17){
                switch ($a){
                    case 0: return 'zero';
                    case 1: return 'un';
                    case 2: return 'deux';
                    case 3: return 'trois';
                    case 4: return 'quatre';
                    case 5: return 'cinq';
                    case 6: return 'six';
                    case 7: return 'sept';
                    case 8: return 'huit';
                    case 9: return 'neuf';
                    case 10: return 'dix';
                    case 11: return 'onze';
                    case 12: return 'douze';
                    case 13: return 'treize';
                    case 14: return 'quatorze';
                    case 15: return 'quinze';
                    case 16: return 'seize';
                }
            } else if ($a<20){
                return 'dix-'.CustomFunctionsTrait::int2str($a-10);
            } else if ($a<100){
                if ($a%10==0){
                    switch ($a){
                        case 20: return 'vingt';
                        case 30: return 'trente';
                        case 40: return 'quarante';
                        case 50: return 'cinquante';
                        case 60: return 'soixante';
                        case 70: return 'soixante-dix';
                        case 80: return 'quatre-vingt';
                        case 90: return 'quatre-vingt-dix';
                    }
                } elseif (substr($a, -1)==1){
                    if( ((int)($a/10)*10)<70 ){
                        return CustomFunctionsTrait::int2str((int)($a/10)*10).'-et-un';
                    } elseif ($a==71) {
                        return 'soixante-et-onze';
                    } elseif ($a==81) {
                        return 'quatre-vingt-un';
                    } elseif ($a==91) {
                        return 'quatre-vingt-onze';
                    }
                } elseif ($a<70){
                    return CustomFunctionsTrait::int2str($a-$a%10).'-'.CustomFunctionsTrait::int2str($a%10);
                } elseif ($a<80){
                    return CustomFunctionsTrait::int2str(60).'-'.CustomFunctionsTrait::int2str($a%20);
                } else{
                    return CustomFunctionsTrait::int2str(80).'-'.CustomFunctionsTrait::int2str($a%20);
                }
            } else if ($a==100){
                return 'cent';
            } else if ($a<200){
                return CustomFunctionsTrait::int2str(100).' '.CustomFunctionsTrait::int2str($a%100);
            } else if ($a<1000){
                if($a%100==0)
                    return CustomFunctionsTrait::int2str((int)($a/100)).' '.CustomFunctionsTrait::int2str(100);
                if($a%100!=0)return CustomFunctionsTrait::int2str((int)($a/100)).' '.CustomFunctionsTrait::int2str(100).' '.CustomFunctionsTrait::int2str($a%100);
            } else if ($a==1000){
                return 'mille';
            } else if ($a<2000){
                return CustomFunctionsTrait::int2str(1000).' '.CustomFunctionsTrait::int2str($a%1000).' ';
            } else if ($a<1000000){
                return CustomFunctionsTrait::int2str((int)($a/1000)).' '.CustomFunctionsTrait::int2str(1000).' '.CustomFunctionsTrait::int2str($a%1000);
            }
            
        }
        
        
        public  static function uploadImage($images,$path){
            $filename = $images->hashName();
            $images->move($path, $filename);
            return $filename;
        }
        public  static function deleteImage($file,$path){
            $destination = $path.$file;
            if(File::exists($destination)){
                File::delete($destination);
            }
        }
    }
    
