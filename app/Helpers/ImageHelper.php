<?php
    
    
    namespace App\Helpers;
    
    
    use App\Models\Image;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Storage;
    
    class ImageHelper
    {
        public static function setDefaultImage(Image $image,$type)
        {
            $image->{$type}->images()->update(['is_default' => false]);
            $image->update(['is_default' => true]);
            return true;
        }
        public static function storeFiles($file,$filePath)
        {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $newFilename = str_replace(' ', '',$filename) . '-' . uniqid();
            $fullPath = $filePath . $newFilename . '.' . $extension;
            if ($file->storeAs($filePath, $newFilename . '.' . $extension, 'public')) {
                $path = Storage::url($fullPath);
            }
            return [
                'path' => $path,
                'extension' => $extension
            ];
        }
        public static function deleteFiles($file)
        {
            $fileuri = str_replace('/storage', '', $file->url);
            Storage::disk('public')->delete($fileuri);
//            if ($file->is_default) {
//                $nextImage = $file->{$type}->images->where('is_default', false)->first();
//                if ($nextImage) {
//                    $nextImage->update(['is_default' => true]);
//                }
//            }
            $file->delete();
        
        }
    
        public static function showFiles($files,$deleteAction=false,$width='col-md-3') {
        
        
            echo '<div class="row">';
            foreach ($files as $file){
                echo '<div class="'.$width.'" id="row'.$file->id.'">';
                echo '
                                    
                                        <a type="submit"  class="icon-btn text-danger p-1 delete" resource="'.$file->id.'" onclick="deleteResource(this,\'resources\');">
                                            <span class="badge badge-danger" style="position: absolute" >
                                                <i class="mdi mdi-close"></i>
                                            </span>
                                        </a>
                                    
                ';
                if (in_array($file->type,['xlsx','xls'])){
                    echo '<a href="'.$file->url.'" download><i class="fa fa-file-excel fa-5x text-success"></i></a>';
                }elseif(in_array($file->type,['pdf'])){
                    echo '<a href="'.$file->url.'" download><i class="fa fa-file-pdf fa-5x text-danger"></i></a>';
                    echo '<embed src="https://drive.google.com/viewerng/viewer?embedded=true&url=https://trustpastry.com'.$file->url.'" width="500" height="375">';
                }elseif (in_array($file->type,['doc','docx'])){
                    echo '<a href="'.$file->url.'" download><i class="fa fa-file-word fa-5x text-primary"></i></a>';
                }else{
                    echo '<img src="'.$file->url.'" alt="'.$file->profile.'" class="img-fluid">';
                }
                echo '<p class="text-muted">'.$file->description.'</p>';
                echo '</div>';
            }
            echo '</div>';
        }
        
    }