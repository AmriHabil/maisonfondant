<?php
    
    
    namespace App\Helpers;
    
    use App\Models\Bank;
    use App\Models\Brand;
    use App\Models\Category;
    use App\Models\Product;
    use App\Models\Store;
    use App\Models\Taste;

    class WebHelper{
        public static function allCategories(){
            return Category::all();
        }
        public static function allBrands(){
            return Brand::where('is_visible',1)->get();
        }
        public static function allTaste(){
            return Taste::all();
        }
        public static function allBanks(){
            return Bank::all();
        }
        public static function allStores(){
            return Store::all();
        }
    }