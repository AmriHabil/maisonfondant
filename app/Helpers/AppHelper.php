<?php
    
    
    namespace App\Helpers;
    use App\Http\Traits\CustomFunctionsTrait;
    use Elibyy\TCPDF\Facades\TCPDF as PDF;
    
    class AppHelper
    {
        public static  function id_Barcode($id)
        {
            return (int)$id+config('global.Barcode');
        }
        public static  function Barcode_id($barcode)
        {
            return (int)$barcode-config('global.Barcode');
        }
        public static function FeesCalc($array){
             $Fees=0;
             if($array['Order.Service']=='exchange'){
                 $Fees=(float)$array['Customer.ExchangingFees'];
             }else{
                 if($array['Order.Status']=='delivered'){
                     $Fees=(float)$array['Customer.DeliveringFees'];
                     if($array['Order.Weight']>$array['Customer.WeightLimit']){
                         $weightFees= ((float)$array['Order.Weight']-(float)$array['Customer.WeightLimit'])*(float)$array['Customer.WeightFees'];
                         $Fees+=$weightFees;
                     }
                     if($array['Order.Size']=='M'){
                         $sizeFees= (float)$array['Customer.SizeFees'];
                         $Fees+=$sizeFees;
                     }
                     if($array['Order.Size']=='L'){
                         $sizeFees= (float)$array['Customer.SizeFees']*2;
                         $Fees+=$sizeFees;
                     }
                     if($array['Order.NbrPackages']>1){
                         $packFees= (float)$array['Order.NbrPackages']*(float)$array['Customer.PackageFees'];
                         $Fees+=$packFees;
                     }
                 }else{
                     $Fees=(float)$array['Customer.ReturningFees'];
                 }
             }
             
         return $Fees;
        }
       
        
        public static function priceFormat($price){
            return str_replace(',',' ',(string)number_format($price,3));
        }
    
        public static function printOrderA4($invoice, $type)
        {
            $title = '';
            switch ($type) {
                case 'facture':
                    $title = 'Facture N: ' . $invoice->number;
                    break;
                case 'bl':
                    $title = 'BL N: ' . $invoice->id;
                    break;
                case 'order':
                    $title = 'Commande N: ' . $invoice->id;
                    break;
                case 'devis':
                    $title = 'Devis N: ' . $invoice->id;
                    break;
                case 'achat':
                    $title = 'Achat N: ' . $invoice->id;
                    break;
                default:
                    $title = 'Document';
                    break;
            }
        
            PDF::SetFont('aefurat', '', 10);
            // Custom Header
            PDF::setHeaderCallback(function ($pdf) use ($invoice, $title, $type) {
            
                $pdf->SetFont('Times','B',11);
                // Position at 15 mm from bottom
                $pdf->SetY(5);
                $pdf->Image('assets/images/logo-dark.png', '', '', 30, 30, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
                // Set font
                $pdf->setCellPadding(2);
                PDF::SetFont('aealarabiya', '', 10);
            
                $pdf->SetFont('Times','B',11);
                $pdf->SetY(5);

//            $this->setCellPadding( 2 );
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,config('global.raisonsociale'),'LT',0,'L');
                $pdf->Cell(70,10,config('global.raisonsociale'),'RT',0,'L');
                $pdf->Ln();
            
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,config('global.address'),'L',0,'L');
                $pdf->Cell(70,10,'TEL : '.config('global.phone'),'R',0,'L');
                $pdf->Ln();
            
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,'MF : '.config('global.mf'),'LB',0,'L');
                $pdf->Cell(70,10,'Email : '.config('global.email'),'RB',0,'L');
            
                $pdf->Ln(15);
                if($title!='Document'):
                    $pdf->Cell(55, 5, $title,  1, 0, 'C');
                    $pdf->Cell(5, 5, '',  0, 0, 'C');
//                $pdf->Cell(95, 5, 'Client : ' . ($invoice->client_details['client_name']??''),  'LRT', 0, 'L');
                    $pdf->Cell(75, 5, 'Client : ' . ($invoice->name??''),  'LRT', 0, 'L');
                    $pdf->Cell(60, 5, 'Numéro : ' . ($invoice->phone?? ''),  'LRT', 0, 'L');
                    $pdf->Ln();
                
                
                    $page = $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages();
                    $pdf->Cell(25, 5, 'Page '.$page,  1, 0, 'L');
                    $pdf->Cell(30, 5, 'Date '.$invoice->date,  1, 0, 'C');
                    $pdf->Cell(5, 5, '',  0, 0, 'C');
                    $pdf->Cell(75, 5, 'Adresse : ' . ($invoice->address??''),  'LRBT', 0, 'LB');
                    $pdf->Cell(60, 5, 'Heure de récupération : ' . ($invoice->hour??''),  'LRBT', 0, 'LB');
            
                endif;
            
            
                $pdf->Ln(15);
            
                $pdf->Cell(15, 5, 'Réf',  1, 0, 'L');
                $pdf->Cell(135, 5, 'Désignation',  1, 0, 'L');
                $pdf->Cell(10, 5, 'Qté',  1, 0, 'C');
                $pdf->Cell(15, 5, 'P.U',  1, 0, 'C');
//                $pdf->Cell(15, 5, 'REMISE', 1, 0, 'C');
                // $pdf->Cell(10, 10, 'TVA', 1, 0, 'C');
                // $pdf->Cell(20, 10, 'PUTTC', 1, 0, 'C');
                $pdf->Cell(20, 5, 'Total',  1, 0, 'C');
                $pdf->Ln();
            });
        
            // Custom Footer
            PDF::setFooterCallback(function ($pdf) {
            
                // Position at 15 mm from bottom
                $pdf->SetY(-10);
                // Set font
                $pdf->SetFont('helvetica', 'I', 8);
                // Page number
                $pdf->Cell(0, 5, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
            });
        
            PDF::SetAuthor('ALDI');
            PDF::SetTitle($title);
            PDF::SetSubject($title);
            PDF::SetMargins(7, 73, 7);
            PDF::SetFontSubsetting(false);
        
            // PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            PDF::AddPage('P', 'A4');
            PDF::SetFont('aealarabiya', '', 8);
            $total_TVA = 0;
            $total_Remise = 0;
            $total_TTC = 0;
            foreach ($invoice->details->where('product_quantity','>',0) as $item) {
            
                PDF::Cell(15, 5, $item->product_ref,  'RL', 0, 'L');
                PDF::Cell(135, 5, $item->product_name,  'RL', 0, 'L');
                PDF::Cell(10, 5, $item->product_quantity,  'RL', 0, 'C');
                // PDF::Cell(15, 5, number_format($row["product_price"] / (1 + ($row["product_tva"] / 100)), 3), 'RL', 0, 'C');
            
                PDF::Cell(15, 5, $item->product_price,  'RL', 0, 'C');
//                PDF::Cell(15, 5, number_format($item->product_remise,0), 'RL', 0, 'C');
                // PDF::Cell(10, 5, $row["product_tva"], 'RL', 0, 'C');
                // PDF::Cell(20, 5, number_format((($row["product_price"] / (1 + ($row["product_tva"] / 100))) * (1 - ($row["product_remise"] / 100))) * (1 + ($row["product_tva"] / 100)), 3), 'RL', 0, 'C');
//                $after_discount= ($item->product_price_selling*(100-$item->product_remise))/100;
                PDF::Cell(20, 5, number_format($item->product_price * $item->product_quantity,3),  'RL', 0, 'C');
                PDF::Ln();
            
                $total_TTC += ($item->product_price * $item->product_quantity);
            }
        
        
            PDF::Cell(195, 10, '',  'T', 0, 'C');
            PDF::Ln();
            PDF::Cell(39, 5, '', 0, 0, 'C');
            PDF::Cell(39, 5, '', 0, 0, 'C');
            PDF::Cell(39, 5, 'Total Brut TTC', 0, 0, 'L');
            PDF::Cell(39, 5, number_format($total_TTC, 3), 0, 0, 'L');
            PDF::Cell(39, 5, 'Net A Payer',  1, 0, 'C');
            PDF::Ln();
        
            PDF::Cell(39, 5, '', 0, 0, 'C');
            PDF::Cell(39, 5, '', 0, 0, 'C');
            PDF::Cell(39, 5, 'Timbre fiscal', 0, 0, 'L');
            if ($invoice->timbre == '1') {
                $timbre = 0.6;
                $topay = $total_TTC + 0.6;
            } else {
                $timbre = 0;
                $topay = $total_TTC;
            }
            PDF::Cell(39, 5, number_format($timbre, 3), 0, 0, 'L');
            PDF::Cell(39, 5, number_format(($topay), 3) . ' DT',  1, 0, 'C');
            PDF::Ln(10);
            PDF::Cell(195, 5, 'Commentaire : ' . $invoice->note, 1, 0, 'L');
            //PDF::Cell(195, 0, 'Arrêtée la présente facture à la somme de : ', 0, 0, 'L');
            PDF::Ln(5);
            PDF::SetFont('helvetica', 'B',8 );
            //PDF::Cell(0, 0, CustomFunctionsTrait::int2str(str_replace(",","",$topay)), 0, 0, 'L');
            PDF::SetFont('helvetica', 'I', 8);
            PDF::lastPage();
            return PDF::Output($title.'.pdf', 'I');
            // exit;
        }
        public static function printA4($invoice, $type)
        {
            $title = '';
            $document = '' ;
            switch ($type) {
                case 'facture':
                    $document = 'FACTURE';
                    $title = $invoice->number;
                    break;
                case 'bl':
                    $document = 'BON DE LIVRAISON';
                    $title =  $invoice->id;
                    break;
                case 'order':
                    $document = 'COMMANDE';
                    $title = $invoice->id;
                    break;
                case 'devis':
                    $document = 'DEVIS';
                    $title = $invoice->id;
                    break;
                case 'achat':
                    $document = 'BON D\'ACHAT';
                    $title = $invoice->id;
                    break;
                default:
                    $document = 'Document';
                    $title = 'Document';
                    break;
            }
        
            PDF::SetFont('aefurat', '', 10);
            // Custom Header
            PDF::setHeaderCallback(function ($pdf) use ($invoice, $title, $type,$document) {
                $pdf->setCellPadding(2);
                $pdf->SetY(5);
    
                $pdf->SetFont('Times','B',18);
//            $this->setCellPadding( 2 );
                $pdf->Cell(75, 10, $document ,  '0', 0, 'C');
                $pdf->SetFont('Times','B',11);
                $pdf->Cell(25,5,'',0,0,'L');
                $pdf->Cell(10,5,'','LT',0,'C');
                $pdf->Cell(75,5,'Adressé à : ','',0,'C');
                $pdf->Cell(10,5,'','TR',0,'C');
                $pdf->Ln();
                $pdf->Cell(35, 5,'Numéro : ',  'LT', 0, 'L');
                $pdf->Cell(40, 5,$title,  'TR', 0, 'L');
                $pdf->Cell(25,5,'',0,0,'L');
                $pdf->Cell(95,5,(strtoupper($invoice->client_details['client_name']??'')??''),'0',0,'C');
                $pdf->Ln();
                $pdf->Cell(35, 5, 'Date : ',  'L', 0, 'L');
                $pdf->Cell(40, 5, $invoice->date,  'R', 0, 'L');
                $pdf->Cell(25,5,'',0,0,'L');
                $pdf->Cell(95,5,($invoice->client_details['client_address']??''),'0',0,'C');
                $pdf->Ln();
                $pdf->Cell(35, 5, 'N° BC Client : ',  'BL', 0, 'L');
                $pdf->Cell(40, 5, '',  'BR', 0, 'L');
                $pdf->Cell(25,5,'',0,0,'BL');
                $pdf->Cell(10,5,'','BL',0,'C');
                $pdf->Cell(75,5,'MF : '.($invoice->client_details['client_mf']??''),'0',0,'C');
                $pdf->Cell(10,5,'','BR',0,'C');
                $pdf->Ln();
                $page = $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages();
                $pdf->Cell(180,5,'',0,0,'L');
                $pdf->Cell(15, 5, 'Page '.$page,  0, 0, 'L');
                $pdf->Ln();
                $pdf->Cell(25, 5, 'Réf',  1, 0, 'L');
                $pdf->Cell(80, 5, 'Désignation',  1, 0, 'L');
                $pdf->Cell(15, 5, 'Qté',  1, 0, 'C');
                $pdf->Cell(15, 5, 'P.U.HT',  1, 0, 'C');
                $pdf->Cell(15, 5, 'TVA', 1, 0, 'C');
                $pdf->Cell(20, 5, 'REMISE', 1, 0, 'C');
                $pdf->Cell(25, 5, 'Montant HT',  1, 0, 'C');
                
                $pdf->Ln();
            });
        
            // Custom Footer
           
        
            PDF::SetAuthor('ALDI');
            PDF::SetTitle($type.' : '.$title);
            PDF::SetSubject($title);
            PDF::SetMargins(7, 58, 7);
            PDF::SetFontSubsetting(false);
            PDF::AddPage('P', 'A4');
            PDF::SetFont('aealarabiya', '', 8);
            
            $total_Remise = 0;
            $total_TTC = 0;
            $total_HT_Brut = 0;
            $total_HT_Discount = 0;
            $total_TVA = [];
            $total_TVA['19']=0;
            foreach ($invoice->details->where('product_quantity','>',0) as $item) {
                
                PDF::Cell(25, 5, $item->product_ref,  'LR', 0, 'L');
                PDF::Cell(80, 5, $item->product_name,  'LR', 0, 'L');
                PDF::Cell(15, 5, $item->product_quantity,  'LR', 0, 'C');
                PDF::Cell(15, 5, number_format($item->product_price_selling / (1 + ($item->product_tva / 100)), 3),  'LR', 0, 'C');
                $total_HT_Brut+=$item->product_price_selling / (1 + ($item->product_tva / 100));
                $total_HT_Discount+=(($item->product_price_selling / (1 + ($item->product_tva / 100))) * (1 - ($item->product_remise / 100)));
                PDF::Cell(15, 5, $item->product_tva, 'LR', 0, 'C');
                PDF::Cell(20, 5, $item->product_remise, 'LR', 0, 'C');
                PDF::Cell(25, 5, number_format((($item->product_price_selling / (1 + ($item->product_tva / 100))) * (1 - ($item->product_remise / 100))), 3),  'LR', 0, 'C');
                PDF::Ln();
//             $after_discount= ($item->product_price_selling*(100-$item->product_remise))/100;
                $total_TTC += ($item->product_price_selling * $item->product_quantity);
            }
            PDF::Cell(195, 10, '',  'T', 0, 'C');
            PDF::Ln();
           
            if ($invoice->timbre == '1') {
                $timbre = 1;
            }
            else {
                $timbre = 0;
            }
          
            PDF::Ln(5);
            PDF::SetFont('helvetica', 'B',8 );
            //PDF::Cell(0, 0, CustomFunctionsTrait::int2str(str_replace(",","",$topay)), 0, 0, 'L');
            PDF::SetFont('helvetica', 'I', 8);
            PDF::lastPage();
            PDF::setFooterCallback(function ($pdf)  use($type,$total_HT_Discount,$total_HT_Brut,$timbre){
                $pdf->SetFont('helvetica', 'B',9 );
                $pdf->SetY(-60);
                $pdf->Cell(40, 5, '  Nom de la Taxe',  'LBT', 0, 'L');
                $pdf->Cell(35, 5, 'Base',  'BT', 0, 'L');
                $pdf->Cell(15, 5, 'Taux',  'BT', 0, 'L');
                $pdf->Cell(30, 5, '  Montant',  1, 0, 'L');
                $pdf->Cell(15, 5, '',  0, 0, 'L');
                $pdf->Cell(30, 5, ' Total HT brut',  'LTR', 0, 'L');
                $pdf->SetFont('helvetica', '',9 );
                $pdf->Cell(30, 5, number_format($total_HT_Brut,3),  'LTR', 0, 'R');
                $pdf->Ln();
                $pdf->Cell(40, 5, '  TVA COLLECTEE 19%',  'L', 0, 'L');
                $pdf->Cell(35, 5, number_format(($total_HT_Discount), 3),  '', 0, 'L');
                $pdf->Cell(15, 5, '%19.00',  'T', 0, 'R');
                $pdf->Cell(30, 5, number_format(($total_HT_Discount*0.19), 3),  'LR', 0, 'R');
                $pdf->Cell(15, 5, '',  0, 0, 'L');
                $pdf->SetFont('helvetica', 'B',9 );
                $pdf->Cell(30, 5, ' Remise',  'LTR', 0, 'L');
                $pdf->SetFont('helvetica', '',9 );
                $pdf->Cell(30, 5, number_format(($total_HT_Brut-$total_HT_Discount),3),  'LTR', 0, 'R');
                $pdf->Ln();
                $pdf->Cell(40, 5, '  ',  'L', 0, 'L');
                $pdf->Cell(35, 5, '',  '', 0, 'L');
                $pdf->Cell(15, 5, '',  '', 0, 'L');
                $pdf->Cell(30, 5, '  ',  'LR', 0, 'L');
                $pdf->Cell(15, 5, '',  0, 0, 'L');
                $pdf->SetFont('helvetica', 'B',9 );
                $pdf->Cell(30, 5, ' Total HT',  'LTR', 0, 'L');
                $pdf->SetFont('helvetica', '',9 );
                $pdf->Cell(30, 5, number_format(($total_HT_Discount), 3),  'LTR', 0, 'R');
                $pdf->Ln();
                $pdf->Cell(40, 5, '  TIMBRE FISCAL',  'L', 0, 'L');
                $pdf->Cell(35, 5, '',  '', 0, 'L');
                $pdf->Cell(15, 5, '',  '', 0, 'L');
                $pdf->Cell(30, 5, number_format($timbre, 3),  'LR', 0, 'R');
                $pdf->Cell(15, 5, '',  0, 0, 'L');
                $pdf->SetFont('helvetica', 'B',9 );
                $pdf->Cell(30, 5, ' Total Taxe',  'LTR', 0, 'L');
                $pdf->SetFont('helvetica', '',9 );
                $pdf->Cell(30, 5, number_format(($total_HT_Discount*0.19)+$timbre, 3),  'LTR', 0, 'R');
                $pdf->Ln();
                $pdf->SetFont('helvetica', 'B',9 );
                $pdf->Cell(40, 5, '  Total',  'BLT', 0, 'C');
                $pdf->SetFont('helvetica', '',9 );
                $pdf->Cell(35, 5, number_format($total_HT_Discount,3),  'BT', 0, 'L');
                $pdf->Cell(15, 5, '',  'BT', 0, 'L');
                $pdf->Cell(30, 5, number_format(($total_HT_Discount*0.19)+$timbre, 3),  'BLTR', 0, 'R');
                $pdf->Cell(15, 5, '',  0, 0, 'L');
                $pdf->SetFont('helvetica', 'B',9 );
                $pdf->Cell(30, 5, ' NET A PAYER',  '1', 0, 'L');
                $pdf->Cell(30, 5, number_format(($total_HT_Discount*1.19)+$timbre,3),  '1', 0, 'R');
                $pdf->Ln();
                if($type=='facture')
                    PDF::Cell(195, 0, 'Arrêtée la présente facture à la somme de : ', 0, 0, 'L');
                PDF::Ln(5);
                $string=CustomFunctionsTrait::int2str((float)str_replace(",","",number_format(($total_HT_Discount*1.19)+$timbre,3)));
                if (stripos($string, 'dinar') === false) {
                    $string .= ' dinar et zero Millimes';
                }
                
                PDF::Cell(100, 5, strtoupper($string), 0, 0, 'L');
                PDF::Cell(30, 0, '', 0, 0, 'L');
                PDF::Cell(60, 0, 'Cachet et signature', 0, 0, 'C');
                $pdf->Ln(20);
                $pdf->SetFont('helvetica', '',9 );
                $pdf->Cell(195, 0, 'TRUST PASTRY - Matricule Fiscal : 1676258M A M 000', 'T', 0, 'C');
                $pdf->Ln();
                $pdf->Cell(195, 0, '28, Rue Sarajevo Ennasr1, Ariana', '0', 0, 'C');
                // Position at 15 mm from bottom
               
            });
            return PDF::Output($title.'.pdf', 'I');
            // exit;
        }
    
    
        public static function printAchat($invoice, $type='achat')
        {
            $title = '';
            switch ($type) {
                case 'achat':
                    $title = 'Achat N: ' . $invoice->number;
                    break;
                default:
                    $title = 'Document';
                    break;
            }
        
            PDF::SetFont('aefurat', '', 10);
            // Custom Header
            PDF::setHeaderCallback(function ($pdf) use ($invoice, $title, $type) {
            
                $pdf->SetFont('Times','B',11);
                // Position at 15 mm from bottom
                $pdf->SetY(5);
                $pdf->Image('assets/images/logo-dark.png', '', '', 30, 30, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
                // Set font
                $pdf->setCellPadding(2);
                PDF::SetFont('aealarabiya', '', 10);
            
                $pdf->SetFont('Times','B',11);
                $pdf->SetY(5);

//            $this->setCellPadding( 2 );
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,config('global.raisonsociale'),'LT',0,'L');
                $pdf->Cell(70,10,config('global.raisonsociale'),'RT',0,'L');
                $pdf->Ln();
            
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,config('global.address'),'L',0,'L');
                $pdf->Cell(70,10,'TEL : '.config('global.phone'),'R',0,'L');
                $pdf->Ln();
            
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,'MF : '.config('global.mf'),'LB',0,'L');
                $pdf->Cell(70,10,'Email : '.config('global.email'),'RB',0,'L');
                $pdf->Ln(15);
            
            
                $pdf->Cell(55, 5, $title,  1, 0, 'C');
                $pdf->Cell(5, 5, '',  0, 0, 'C');
                $pdf->Cell(95, 5, 'Fournisseur : ' . $invoice->provider_details['provider_name'],  'LRT', 0, 'L');
                $pdf->Cell(40, 5, 'MF : ' . $invoice->provider_details['provider_mf'],  'LRT', 0, 'L');
                $pdf->Ln();
            
            
                $page = $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages();
                $pdf->Cell(25, 5, 'Page '.$page,  1, 0, 'L');
                $pdf->Cell(30, 5, 'Date '.$invoice->date,  1, 0, 'C');
                $pdf->Cell(5, 5, '',  0, 0, 'C');
                $pdf->Cell(95, 5, 'Adresse : ' . $invoice->provider_details['provider_address'],  'LRBT', 0, 'LB');
                $pdf->Cell(40, 5, 'Numéro : ' . $invoice->provider_details['provider_phone'],  'LRBT', 0, 'LB');
            
            
                // $pdf->Cell(105, 5, 'Vehicule : ' . $invoice->CustomerCar, 'LRB', 0, 'L');
                $pdf->Ln(15);
                //$pdf->Cell(20, 10, 'Réf',  1, 0, 'L');
                $pdf->Cell(135, 5, 'Désignation',  1, 0, 'L');
                $pdf->Cell(10, 5, 'Qté',  1, 0, 'C');
                $pdf->Cell(15, 5, 'P.U',  1, 0, 'C');
                $pdf->Cell(15, 5, 'REMISE', 1, 0, 'C');
                // $pdf->Cell(10, 10, 'TVA', 1, 0, 'C');
                // $pdf->Cell(20, 10, 'PUTTC', 1, 0, 'C');
                $pdf->Cell(20, 5, 'Total',  1, 0, 'C');
                $pdf->Ln();
            });
        
            // Custom Footer
            PDF::setFooterCallback(function ($pdf) {
            
                // Position at 15 mm from bottom
                $pdf->SetY(-10);
                // Set font
                $pdf->SetFont('helvetica', 'I', 8);
                // Page number
                $pdf->Cell(0, 5, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
            });
        
            PDF::SetAuthor('ALDI');
            PDF::SetTitle($title);
            PDF::SetSubject($title);
            PDF::SetMargins(7, 73, 7);
            PDF::SetFontSubsetting(false);
        
            // PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            PDF::AddPage('P', 'A4');
            PDF::SetFont('aealarabiya', '', 8);
            $total_TVA = 0;
            $total_Remise = 0;
            $total_TTC = 0;
            foreach ($invoice->details as $item) {

//                PDF::Cell(20, 5, $item->product->reference,  'RL', 0, 'L');
                PDF::Cell(135, 5, $item->product_name,  'RL', 0, 'L');
                PDF::Cell(10, 5, $item->product_quantity,  'RL', 0, 'C');
                // PDF::Cell(15, 5, number_format($row["product_price"] / (1 + ($row["product_tva"] / 100)), 3), 'RL', 0, 'C');
            
                PDF::Cell(15, 5, $item->product_price_buying,  'RL', 0, 'C');
                PDF::Cell(15, 5, number_format($item->product_remise,3), 'RL', 0, 'C');
                // PDF::Cell(10, 5, $row["product_tva"], 'RL', 0, 'C');
                // PDF::Cell(20, 5, number_format((($row["product_price"] / (1 + ($row["product_tva"] / 100))) * (1 - ($row["product_remise"] / 100))) * (1 + ($row["product_tva"] / 100)), 3), 'RL', 0, 'C');
//                $after_discount= ($item->product_price_selling*(100-$item->product_remise))/100;
                $after_discount= $item->product_price_buying-$item->product_remise;
                PDF::Cell(20, 5, number_format($after_discount * $item->product_quantity,3),  'RL', 0, 'C');
                PDF::Ln();
                //$total_Remise += (($item->product_price_selling - $after_discount) ) * $item->product_quantity;
                $total_Remise += $item->product_remise * $item->product_quantity;
                $total_TTC += ($after_discount * $item->product_quantity);
                $total_TVA +=  $item->product_quantity*$after_discount*(1-(100/(100+$item->product_tva)));
            
                // $total = $total + ($row["product_price"] / (1 + ($row["product_tva"] / 100)) * (1 - ($row["product_remise"] / 100)) * (1 + ($row["product_tva"] / 100)) * $row["product_quantity"]);
                // $total_HT = $total_HT + ($row["product_price"] / (1 + ($row["product_tva"] / 100)) * (1 - ($row["product_remise"] / 100)) * $row["product_quantity"]);
            }
        
        
           
            if ($invoice->timbre == '1') {
                $timbre = 0.6;
                $topay = $total_TTC + 0.6;
            } else {
                $timbre = 0;
                $topay = $total_TTC;
            }
            PDF::Cell(39, 5, number_format($timbre, 3), 0, 0, 'L');
            PDF::Cell(39, 5, number_format(($topay), 3) . ' DT',  1, 0, 'C');
            PDF::Ln(10);
        
            //PDF::Cell(195, 0, 'Arrêtée la présente facture à la somme de : ', 0, 0, 'L');
            PDF::Ln(5);
            PDF::SetFont('helvetica', 'B',8 );
            //PDF::Cell(0, 0, CustomFunctionsTrait::int2str(str_replace(",","",$topay)), 0, 0, 'L');
            PDF::SetFont('helvetica', 'I', 8);
            PDF::lastPage();
            return PDF::Output($title.'.pdf', 'I');
            // exit;
        }
        public static function printInventory($invoice, $type)
        {
           
            $title = 'BS';
            $title = '';
            switch ($type) {
                case 'bs':
                    $title = 'BS N:' . $invoice->id;
                    break;
                case 'inventory':
                    $title = 'Inventaire N: ' . $invoice->id;
                    break;
                default:
                    $title = 'Document';
                    break;
            }
            PDF::SetFont('aefurat', '', 10);
            // Custom Header
            PDF::setHeaderCallback(function ($pdf) use ($invoice, $title, $type) {
        
                $pdf->SetFont('Times','B',11);
                // Position at 15 mm from bottom
                $pdf->SetY(5);
                $pdf->Image('assets/images/logo-dark.png', '', '', 30, 30, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
                // Set font
                $pdf->setCellPadding(2);
                PDF::SetFont('aealarabiya', '', 10);
        
                $pdf->SetFont('Times','B',11);
                $pdf->SetY(5);

//            $this->setCellPadding( 2 );
                $page = $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages();
    
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,config('global.raisonsociale'),'LT',0,'L');
                $pdf->Cell(70,10,$title,'RT',0,'L');
                $pdf->Ln();
        
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,config('global.address'),'L',0,'L');
                $pdf->Cell(70,10,'Date : '.$invoice->date,'R',0,'L');
                $pdf->Ln();
        
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,'MF : '.config('global.mf'),'LB',0,'L');
                $pdf->Cell(70,10,'Page : '.$page,'RB',0,'L');
                $pdf->Ln(15);
                $pdf->Cell(150, 5, 'Désignation',  1, 0, 'L');
                $pdf->Cell(15, 5, 'Supposé',  1, 0, 'C');
                $pdf->Cell(15, 5, 'Réelle', 1, 0, 'C');
                $pdf->Cell(15, 5, 'Ecart',  1, 0, 'C');
                $pdf->Ln();
            });
    
            // Custom Footer
            PDF::setFooterCallback(function ($pdf) {
            
                // Position at 15 mm from bottom
                $pdf->SetY(-15);
                // Set font
                $pdf->SetFont('helvetica', 'I', 8);
                // Page number
                $pdf->Cell(0, 5, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
            });
        
            PDF::SetAuthor('Habil Amri');
            PDF::SetTitle($title);
            PDF::SetSubject($title);
            PDF::SetMargins(7, 48, 7);
            PDF::SetFontSubsetting(false);
        
            PDF::AddPage('P', 'A4');
            PDF::SetFont('aealarabiya', '', 8);
            
            foreach ($invoice->details as $item) {
                PDF::Cell(150, 5, $item->product_name,  'RL', 0, 'L');
                PDF::Cell(15, 5,  $type == 'inventory' ?$item->product_quantity_supposed :$item->product_quantity,  'RL', 0, 'C');
                PDF::Cell(15, 5,  $type == 'inventory' ?$item->product_quantity_real :0,  'RL', 0, 'C');
                $type == 'inventory' ?PDF::Cell(15, 5,  $type == 'inventory' ?$item->product_quantity_supposed-$item->product_quantity_real:0,  'RL', 0, 'C'):'';
                PDF::Ln();
            }
        
        
            PDF::Cell(195, 30, '', 'T', 0, 'C');
            PDF::Ln();
        
            PDF::lastPage();
            return PDF::Output($title.'.pdf', 'I');
            // exit;
        }
        public static function printBs($invoice)
        {
            
                    $title = 'BS N:' . $invoice->id;
                   
            
            PDF::SetFont('aefurat', '', 10);
            // Custom Header
            PDF::setHeaderCallback(function ($pdf) use ($invoice, $title) {
            
                $pdf->SetFont('Times','B',11);
                // Position at 15 mm from bottom
                $pdf->SetY(5);
                $pdf->Image('assets/images/logo-dark.png', '', '', 30, 30, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
                // Set font
                $pdf->setCellPadding(2);
                PDF::SetFont('aealarabiya', '', 10);
            
                $pdf->SetFont('Times','B',11);
                $pdf->SetY(5);

//            $this->setCellPadding( 2 );
                $page = $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages();
            
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,'De : '.$invoice->source->name.'-'.$invoice->source->address,'LT',0,'L');
                $pdf->Cell(70,10,$title,'RT',0,'L');
                $pdf->Ln();
            
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,'A : '.$invoice->destination->name.'-'.$invoice->destination->address,'L',0,'L');
                $pdf->Cell(70,10,'Date : '.$invoice->created_at,'R',0,'L');
                $pdf->Ln();
            
                $pdf->Cell(35,5,'',0,0,'L');
                $pdf->Cell(90,10,'Pages : '.$page,'LB',0,'L');
                $pdf->Cell(70,10,'','RB',0,'L');
                $pdf->Ln(15);
                
            });
            
            // Custom Footer
            PDF::setFooterCallback(function ($pdf) {
            
                // Position at 15 mm from bottom
                $pdf->SetY(-15);
                // Set font
                $pdf->SetFont('helvetica', 'I', 8);
                // Page number
                $pdf->Cell(0, 5, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
            });
        
            PDF::SetAuthor('Habil Amri');
            PDF::SetTitle($title);
            PDF::SetSubject($title);
            PDF::SetMargins(7, 30, 7);
            PDF::SetFontSubsetting(false);
        
            PDF::AddPage('P', 'A4');
            PDF::SetFont('aealarabiya', '', 14);
    
            PDF::Cell(195, 20, 'Résumé ', 0, 0, 'C');
            PDF::Ln();
            PDF::SetFont('aealarabiya', '', 8);
            PDF::Cell(48, 5, 'Fondant',  1, 0, 'L');
            PDF::Cell(49, 5,  $invoice->sender_report['Fondant']??0,  1, 0, 'C');
            PDF::Cell(48, 5, 'Polystérene',  1, 0, 'L');
            PDF::Cell(49, 5,  $invoice->sender_report['Polysterene']??0,  1, 0, 'C');
            PDF::Ln();
            PDF::Cell(48, 5, 'Fondant Géant',  1, 0, 'L');
            PDF::Cell(49, 5,  $invoice->sender_report['Fondant Geant']??0,  1, 0, 'C');
            PDF::Cell(48, 5,  'Mini Fondant',  1, 0, 'L');
            PDF::Cell(49, 5,  $invoice->sender_report['Mini Fondant']??0,  1, 0, 'C');
            PDF::Ln();
            PDF::Cell(48, 5, 'Fondor',  1, 0, 'L');
            PDF::Cell(49, 5,  $invoice->sender_report['Fondor']??0,  1, 0, 'C');
            PDF::Cell(48, 5,  'Commandes',  1, 0, 'L');
            PDF::Cell(49, 5,  $invoice->sender_report['Commandes']??0,  1, 0, 'C');
            PDF::Ln();
            PDF::Cell(48, 5, 'Consommables',  1, 0, 'L');
            PDF::Cell(49, 5,  $invoice->sender_report['Consommables']??0,  1, 0, 'C');
            PDF::Cell(48, 5, 'Ratés',  1, 0, 'L');
            PDF::Cell(49, 5,  $invoice->sender_report['Rates']??0,  1, 0, 'C');
            PDF::Ln(5);
            PDF::SetFont('aealarabiya', '', 14);
            PDF::Cell(195, 20, 'Détails ', 0, 0, 'C');
            PDF::Ln();
            PDF::SetFont('aealarabiya', '', 8);
            PDF::Cell(30, 5, 'REF',  1, 0, 'L');
            PDF::Cell(150, 5, 'Désignation',  1, 0, 'L');
            PDF::Cell(15, 5, 'Quantité',  1, 0, 'C');
            PDF::Ln();
            foreach ($invoice->details as $item) {
                PDF::Cell(30, 5, $item->product_ref,  'RL', 0, 'L');
                PDF::Cell(150, 5, $item->product_name,  'RL', 0, 'L');
                PDF::Cell(15, 5,  $item->product_quantity_sent,  'RL', 0, 'C');
                PDF::Ln();
            }
        
        
            PDF::Cell(195, 30, '', 'T', 0, 'C');
            PDF::Ln();
        
            PDF::lastPage();
            return PDF::Output($title.'.pdf', 'I');
            // exit;
        }
    
        public static function printTicket($cart, $type = null)
        {
            PDF::SetFont('aefurat', '', 10);
        
            PDF::SetAuthor('AMRI HABIL');
            PDF::SetTitle('Ticket n° ' . $cart->id);
            PDF::SetSubject('Ticket');
            PDF::SetMargins(1, 10, 1);
            PDF::SetFontSubsetting(false);
        
            $page_format = array(
                'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 80, 'ury' => 150),
                'Dur' => 3,
                'trans' => array(
                    'D' => 1.5,
                    'S' => 'Split',
                    'Dm' => 'V',
                    'M' => 'O'
                ),
                // 'Rotate' => 90,
                'PZ' => 1,
            );
        
            // add first page ---
            PDF::AddPage('P', $page_format, false, false);
        
            // PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            // PDF::AddPage('P', 'A7');
            PDF::SetFont('aealarabiya', '', 8);
            PDF::SetY(1);
            PDF::Image('assets/images/logo-dark.png', '', '', 10, 10, '', '', 'T', false, 10, '', false, false, 1, false, false, false);
            // Set font
            PDF::SetY(1);
            // Set font
            PDF::setCellPadding(0);
            PDF::SetFont('aealarabiya', '', 12);
            PDF::Cell(10, 5);
            PDF::Cell(30, 5, 'MAISON', 0, 0, 'L');
            PDF::SetFont('aealarabiya', '', 10);
            PDF::Cell(32.5, 5, 'Ticket N°: ' . str_pad($cart->id, 4, 0, STR_PAD_LEFT), 0, 0, 'C');
            PDF::Ln();
            PDF::Cell(10, 5);
            PDF::SetFont('aealarabiya', '', 12);
            PDF::Cell(30, 5, 'FONDANT', 0, 0, 'l');
            PDF::SetFont('aealarabiya', '', 10);
            PDF::Cell(32.5, 5, $cart->date, 0, 0, 'C');
            PDF::Ln();
            PDF::Cell(80, 1, '________________________________________', 0, 0, 'L');
            PDF::Ln(6);
            $total = 0;
            $total_brut = 0;
            PDF::SetFont('aealarabiya', '', 8);
            foreach ($cart->details as $item) {
        
                $total_brut += ((float)$item->product_price_selling * (float)$item->product_quantity);
                PDF::Cell(47.5, 5, $item->product_quantity . ' x ' . substr($item->product_name,0,25), 0, 0, 'L');
                //$after_discount= ($item->product_price_selling*(100-$item->product_remise))/100;
                $after_discount= $item->product_price_selling-$item->product_remise;
                PDF::Cell(5, 5, '-'.number_format($item->product_remise* $item->product_quantity,3) , 0, 0, 'L');
                PDF::Cell(20, 5, number_format($after_discount * $item->product_quantity,3), 0, 0, 'R');
        
                $total += $after_discount * $item->product_quantity;
                PDF::Ln();
            }
            /*Add Remise*/
            PDF::SetFont('aealarabiya', '', 10);
            PDF::Cell(80, 1, '________________________________________', 0, 0, 'L');
            PDF::Ln(5);
            PDF::Cell(20, 5, 'Avant remise', 0, 0, 'L');
            if ($cart->timbre == '1') {
                $timbre = 0.6;
                $topay = $total_brut + 0.6;
            } else {
                $timbre = 0;
                $topay = $total_brut;
            }
            PDF::Cell(52.5, 5, number_format(($total_brut), 3) . ' DT', 0, 0, 'R');
            PDF::Ln(5);
            PDF::Cell(20, 5, 'Apres remise', 0, 0, 'L');
            if ($cart->timbre == '1') {
                $timbre = 0.6;
                $topay = $cart->total + 0.6;
            } else {
                $timbre = 0;
                $topay = $cart->total;
            }
            PDF::Cell(52.5, 5, number_format(($total), 3) . ' DT', 0, 0, 'R');
            PDF::Ln(10);
            PDF::Cell(72.5,3,config('global.address'),0,0,'C');
            PDF::Ln();
            PDF::Cell(72.5,3,'TEL : '.config('global.phone'),0,0,'C');
            PDF::Ln();
            PDF::SetFont('aealarabiya', '', 8);
           
            PDF::lastPage();
            // dd(PDF::GetY());
            return PDF::Output('Ticket n° ' . $cart->id.'.pdf', 'I');
            // exit;
        }
    
        public static function printCaisse($caisse, $payments ,$decaissements,$date)
        {
            PDF::SetFont('aefurat', '', 10);
    
            PDF::SetAuthor('AMRI HABIL');
            PDF::SetTitle('Rapport de caisse ');
            PDF::SetSubject('Ticket');
            PDF::SetMargins(1, 10, 1);
            PDF::SetFontSubsetting(false);
    
            $page_format = array(
                'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 80, 'ury' => 800),
                'Dur' => 3,
                'trans' => array(
                    'D' => 1.5,
                    'S' => 'Split',
                    'Dm' => 'V',
                    'M' => 'O'
                ),
                // 'Rotate' => 90,
                'PZ' => 1,
            );
    
            // add first page ---
            PDF::AddPage('P', $page_format, false, false);
    
            // PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            // PDF::AddPage('P', 'A7');
            PDF::SetFont('aealarabiya', '', 8);
            PDF::SetY(1);
            PDF::Image('assets/images/logo-dark.png', '', '', 10, 10, '', '', 'T', false, 10, '', false, false, 1, false, false, false);
            // Set font
            PDF::SetY(1);
            // Set font
            PDF::setCellPadding(0);
            PDF::SetFont('aealarabiya', '', 12);
            PDF::Cell(10, 5);
            PDF::Cell(30, 5, 'MAISON', 0, 0, 'L');
            PDF::SetFont('aealarabiya', '', 10);
            PDF::Cell(32.5, 5, 'Rapport de caisse  '  , 0, 0, 'C');
            PDF::Ln();
            PDF::Cell(10, 5);
            PDF::SetFont('aealarabiya', '', 12);
            PDF::Cell(30, 5, 'FONDANT', 0, 0, 'l');
            PDF::SetFont('aealarabiya', '', 10);
            PDF::Cell(32.5, 5, $date, 0, 0, 'C');
            PDF::Ln();
            PDF::Cell(80, 1, '_____________Etat de caisse__________________', 0, 0, 'L');
            PDF::Ln();
            PDF::Cell(20,5, 'CASH', 0, 0, 'L');
            PDF::Cell(50, 5, $caisse['cash'], 0, 0, 'R');
            PDF::Ln();
    
            PDF::Cell(20,5, 'CHEQUE', 0, 0, 'L');
            PDF::Cell(50, 5, $caisse['check'], 0, 0, 'R');
            PDF::Ln();
    
            PDF::Cell(20,5, 'SODEXO', 0, 0, 'L');
            PDF::Cell(50, 5, $caisse['sodexo'], 0, 0, 'R');
            PDF::Ln();
    
            PDF::Cell(20,5, 'TPE', 0, 0, 'L');
            PDF::Cell(50, 5, $caisse['transfer'], 0, 0, 'R');
            PDF::Ln();
    
            PDF::Cell(40,5, 'RESTE (ESPECE)', 1, 0, 'L');
            PDF::Cell(30, 5, $caisse['fond'], 1, 0, 'R');
            PDF::Ln();
            
            
            PDF::Cell(80, 1, '_____________Encaissement___________________', 0, 0, 'L');
            PDF::Ln(6);
            $total = 0;
            $total_brut = 0;
            foreach ($payments as $payment) {
        
        
                PDF::Cell(20,5, $payment->amount, 0, 0, 'R');
    
                PDF::Cell(30,5, '', 0, 0, 'L');
                PDF::Cell(20, 5, __($payment->type), 0, 0, 'R');
        
        
                PDF::Ln();
            }
            PDF::Cell(40,5, 'Total', 1, 0, 'L');
            PDF::Cell(30, 5, $payments->sum('amount'), 1, 0, 'R');
            PDF::Ln();
            /*Add Remise*/
            PDF::Cell(80, 1, '_____________Décaissement___________________', 0, 0, 'L');
            PDF::Ln();
            foreach ($decaissements as $decaissement) {
        
        
                PDF::Cell(20,5, $decaissement->amount, 0, 0, 'R');
        
        
                PDF::Cell(50, 5, $decaissement->details, 0, 0, 'L');
        
        
                PDF::Ln();
            }
            PDF::Cell(40,5, 'Total', 1, 0, 'L');
            PDF::Cell(30, 5, $decaissements->sum('amount'), 1, 0, 'R');
            PDF::Ln();
            PDF::Ln(5);
            PDF::Cell(80, 1, 'Imprimé par '.auth()->user()->name, 0, 0, 'C');
            PDF::Ln();
            PDF::Cell(80, 1, date('Y-m-d H:i:s'), 0, 0, 'C');
            return PDF::Output('Rapport de caisse  '.$date.'.pdf', 'I');
           
        }
        public static function getNotificationMessage($notification)
        {
            $message = '';
            switch ($notification->type) {
                case 'App\Notifications\MinQtyNotification':
                    
                    $message = $notification->data['min_qte']??'';
                    break;
                case 'App\Notifications\NewOrderNotification':
                    $message = $notification->data['client_name'] . ' a passé une nouvelle commande n° ' . $notification->data['codification'];
                    break;
            
                default:
                    $message = '';
                    break;
            }
            return $message;
        }
    
        public static function borderColer($category){
            
            switch ($category){
                case '1' : $color="#1abc9c"; break;
                case '2' : $color="#4fc6e1"; break;
                case '3' : $color="#f7b84b"; break;
                case '4' : $color="#6658dd"; break;
                case '5' : $color="#4a81d4"; break;
                case '6' : $color="#f672a7"; break;
                default : $color="#6c757d"; break;
            }
            return 'style="border-left:2px solid '.$color.'"';
        }
    
    }