<?php
    
    
    namespace App\Helpers;
    
    
    use App\Models\Product;
    use App\Models\Quantity;
    use App\Models\StockHistory;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    
    class QuantityHelper
    {
          public static function getQuantityOrCreate($productId,$storeId){
              $quantity=Quantity::firstOrCreate(
                  [
                      'store_id'=>$storeId,
                      'product_id'=>$productId
                  ]
              );
              return $quantity;
          }
          
          public static function updateQuantity($quantity, $record, $action='out',$relatedModel = null,$event=null){
            
                if ($action == 'out') {
                    $updatedQty = (int)($record->quantity - $quantity);
                } else {
                    $action == 'in';
                    $updatedQty = (int)($record->quantity + $quantity);
                }
              $p = Product::find($record->product_id);
              if ($p->components()->exists()) {
                  // Loop through each component associated with the main product
                  foreach ($p->components as $component) {
                      $componentProduct = $component->id;
                      $componentQuantity = QuantityHelper::getQuantityOrCreate($componentProduct,$record->store_id);
                      if ($action == 'out') {
                          $updatedComponentQty = (int)($componentQuantity->quantity - ($quantity * $component->pivot->quantity));
                      } else {
                          $updatedComponentQty = (int)($componentQuantity->quantity + ($quantity * $component->pivot->quantity));
                      }
                      $previousComponentQty = $componentQuantity->quantity;
                      $stockComponentHistory = new StockHistory([
                          'product_id'      => $componentProduct,
                          'store_id'        => $record->store_id,
                          'quantity_before' => $previousComponentQty,
                          'quantity_after'  => $updatedComponentQty,
                          'action'          => $action,
                          'event'          => $event,
                          'type'          => 'component',
                      ]);
    
                      if ($relatedModel) {
                          $relatedModel->stockHistories()->save($stockComponentHistory);
                      } else {
                          $stockComponentHistory->save();
                      }
                      
                      $componentQuantity->quantity=$updatedComponentQty;
                      $componentQuantity->save();
                  }
              }
                  $previousQty = $record->quantity??0;
                  $stockHistory = new StockHistory([
                      'product_id'      => $record->product_id,
                      'store_id'        => $record->store_id,
                      'quantity_before' => $previousQty,
                      'quantity_after'  => $updatedQty,
                      'action'          => $action,
                      'event'          => $event,
                  ]);
        
                  if ($relatedModel) {
                      $relatedModel->stockHistories()->save($stockHistory);
                  } else {
                      $stockHistory->save();
                  }
                $record->quantity=$updatedQty;
                $record->save();
                
                
                return true;
          }
          
          
    }