<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facture extends Model
{
    use HasFactory;
    protected $fillable=[
        'client_id',
        'store_id',
        'invoice_id',
        'admin_id',
        'status',
        'payment_status',
        'date',
        'type',
        'client_details',
        'number',
        'total',
        'timbre',
        'remise'
    ];
    protected $casts = [
        'client_details' => 'array',
    ];
    public function details() {
        return $this->hasMany(FactureDetail::class);
    }
    public function client() {
        return $this->belongsTo(Client::class,'client_id');
    }
    public function admin() {
        return $this->belongsTo(Admin::class,'admin_id');
    }
    
}
