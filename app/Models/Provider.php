<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use HasFactory;
    
    protected $fillable=[
        
        'name',
        'manager',
        'email',
        'address',
        'mf',
        'rib',
        'phone',
        'website',
        'facebook',
        'price',
        'plafond',
        'remise',
        'status',
    
    ];
}
