<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    
    protected $fillable=[
        'name',
        
        'category_id',
        'brand_id',
        'taste_id',
        'status',
        'value',
        'ref',
        'price',
        'coefficient',
        'unity',
        'image',
        'tva',
        'buying',
       
    ];
    protected $appends = [
        'default_image_url',
        'category_name',
        'brand_name',
        'taste_name',
        
    ];
    
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    public function category() {
        return $this->belongsTo(Category::class);
    }
    public function brand() {
        return $this->belongsTo(Brand::class);
    }
    public function taste() {
        return $this->belongsTo(Taste::class);
    }
    public function quantities() {
        return $this->hasMany(Quantity::class);
    }
    public function quantity() {
        return $this->quantities()->where('store_id',auth()->user()->store_id);
    }
    
    
    
    public function getDefaultImageUrlAttribute()
    {
        $image = $this->images()->where('is_default', true)->first();
        if($image){
            return $image->url;
        }
        return config('global.image');
    }
    public function getCategoryNameAttribute()
    {
        $category_name = $this->category()->first();
        if($category_name){
            return $category_name->name;
        }
        return '';
    }
    public function getBrandNameAttribute()
    {
        $brand_name = $this->brand()->first();
        if($brand_name){
            return $brand_name->name;
        }
        return '';
    }
    public function getTasteNameAttribute()
    {
        $brand_name = $this->brand()->first();
        if($brand_name){
            return $brand_name->name;
        }
        return '';
    }
    
    // Relationship to get child components of a composed product
    public function components()
    {
        return $this->belongsToMany(
            Product::class,
            'product_components',
            'parent_product_id',
            'child_product_id'
        )->withPivot('quantity');
    }
    
    // Relationship to get parent products that use this product as a component
    public function usedIn()
    {
        return $this->belongsToMany(
            Product::class,
            'product_components',
            'child_product_id',
            'parent_product_id'
        )->withPivot('quantity');
    }
    
}
