<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Achat extends Model
{
    use HasFactory;
    protected $fillable=[
        'provider_id',
        'store_id',
        'admin_id',
        'status',
        'date',
        'provider_details',
        'number',
        'total',
        'timbre',
        'payment_status',
        'declared'
    ];
    protected $casts = [
        'provider_details' => 'array',
    ];
    public function details() {
        return $this->hasMany(AchatDetail::class);
    }
    public function provider() {
        return $this->belongsTo(Provider::class,'provider_id');
    }
    public function admin() {
        return $this->belongsTo(Admin::class,'admin_id');
    }
    public function commitments() {
        return $this->hasMany(AchatFinancialCommitment::class);
    }
    public function payments() {
        return $this->hasManyThrough(AchatPayment::class,AchatFinancialCommitment::class);
    }
    public function stockHistories()
    {
        return $this->morphMany(StockHistory::class, 'stockable');
    }
}
