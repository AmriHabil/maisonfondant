<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AchatPayment extends Model
{
    use HasFactory;
    protected $fillable = [
        'achat_financial_commitment_id',
        'provider_id',
        'store_id',
        'type',
        'details',
        'received_at',
        'due_at',
        'status',
        'amount',
        'number',
        'bank',
    ];
    protected $casts = [
        'details' => 'array',
    ];
    
    public function commitment() {
        return $this->belongsTo(AchatFinancialCommitment::class, 'achat_financial_commitment_id');
    }
}
