<?php
    
    namespace App\Models;
    
    use Illuminate\Contracts\Auth\MustVerifyEmail;
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Notifications\Notifiable;
    use Laravel\Sanctum\HasApiTokens;
    use Spatie\Permission\Traits\HasRoles;

    class User extends Authenticatable implements MustVerifyEmail
    {
        use HasApiTokens, HasFactory, Notifiable,HasRoles;
        
        
        protected $guard = 'web';
        /**
         * The attributes that are mass assignable.
         *
         * @var array<int, string>
         */
        protected $fillable = [
           'name', 'email', 'email_verified_at', 'password', 'remember_token', 'BName', 'FullName', 'Address', 'Number', 'Contract_Copy','PatenteStatus', 'Patente','MF','RIB', 'RIB_Copy', 'CIN', 'CIN_Copy', 'Status', 'HUB_id', 'Region', 'Image',
            'DeliveringFees','ReturningFees','ExchangingFees','CollectingFees','WeightFees','SizeFees','PackageFees','CoverageFees','WeightLimit'
        ];
        
        /**
         * The attributes that should be hidden for serialization.
         *
         * @var array<int, string>
         */
        protected $hidden = [
            'Password',
            //'remember_token',
        ];
        
        /**
         * The attributes that should be cast.
         *
         * @var array<string, string>
         */
        /*protected $casts = [
            'email_verified_at' => 'datetime',
        ];*/
        public  function Orders (){
            return $this->hasMany('App\Models\Orders\Orders','admin_id','id');
        }
        public  function Tickets (){
            return $this->hasMany('App\Models\Orders\Tickets','user_id','id');
        }
        public  function Manifests (){
            return $this->hasMany('App\Models\Orders\Manifest','user_id','id');
        }
        
    }
