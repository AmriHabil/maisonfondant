<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlFinancialCommitment extends Model
{
    use HasFactory;
    protected $fillable = [
        'bl_id',
        'amount',
        'date',
        'payment_status',
    ];
    protected $appends= [
        'client_id','client_name'
    ];
    public function payments()
    {
        return $this->hasMany(BlPayment::class);
    }
    public function bl()
    {
        return $this->belongsTo(Bl::class);
    }
    public function getClientIdAttribute(){
        $fc = $this->bl()->first();
        if($fc){
            return $fc->client_id;
        }
        return '';
    }
    public function getClientNameAttribute(){
        $fc = $this->bl()->first();
        if($fc){
            return $fc->client_details['client_name'];
        }
        return '';
    }
    
}
