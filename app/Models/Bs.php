<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bs extends Model
{
    use HasFactory;
    protected $fillable=[
        'from_store_id',
        'to_store_id',
        'sender_report',
        'receiver_report',
        'driver_report',
        'sender_id',
        'receiver_id',
        'driver_id',
        'status',
        'date',
        'total',
        'recette',
        'bl_id',
    ];
    
    protected $casts = [
        'sender_report' => 'array',
        'receiver_report' => 'array',
        'driver_report' => 'array',
    ];
    public function details() {
        return $this->hasMany(BsDetail::class);
    }
   
    public function admin() {
        return $this->belongsTo(Admin::class,'admin_id');
    }
    public function source() {
        return $this->belongsTo(Store::class,'from_store_id');
    }
    public function destination() {
        return $this->belongsTo(Store::class,'to_store_id');
    }
    public function stockHistories()
    {
        return $this->morphMany(StockHistory::class, 'stockable');
    }
}
