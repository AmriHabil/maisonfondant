<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    protected $fillable = [
        'url',
        'is_default',
        'type',
        'imageable',
        'description',
    ];
    
    /**
     * Get the parent imageable model
     */
    public function imageable()
    {
        return $this->morphTo();
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class,'imageable_id');
    }
    
    
}
