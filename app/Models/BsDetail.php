<?php
    
    namespace App\Models;
    
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;
    
    class BsDetail extends Model
    {
        use HasFactory;
        protected $fillable=[
            'bs_id',
            'product_id',
            'product_ref',
            'product_category',
            'product_name',
            'product_quantity_sent',
            'product_quantity_received',
            'product_quantity_delivered',
            'product_price',
            'product_price_buying',
        ];
        protected $appends = [
            'product_coefficient'
        ];
        public function bs()
        {
            return $this->belongsTo(Bs::class);
        }
        public function product()
        {
            return $this->belongsTo(Product::class);
        }
        public function getProductCoefficientAttribute(){
            $product = $this->product;
            if($product){
                return $product->coefficient;
            }
            return "1";
        }
    }
