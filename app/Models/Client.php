<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class Client extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,HasRoles;
    
    
    protected $guard = 'web';
    protected $fillable=[
        
        'name',
        'manager',
        'email',
        'address',
        'mf',
        'rib',
        'phone',
        'status',
        'store_id',
        
    ];
    
    
    public function orders(){
        return $this->hasMany(Order::class);
    }
    public function pos(){
        return $this->hasMany(Pos::class);
    }
    public function bls(){
        return $this->hasMany(Bl::class);
    }
    public function factures(){
        return $this->hasMany(Facture::class);
    }
}
