<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialCommitment extends Model
{
    use HasFactory;
    protected $fillable = [
        'pos_id',
        'amount',
        'date',
        'payment_status',
    ];
    protected $appends= [
        'client_id','client_name'
    ];
    public function payments()
    {
        return $this->hasMany(PosPayment::class);
    }
    public function pos()
    {
        return $this->belongsTo(Pos::class);
    }
    public function getClientIdAttribute(){
        $fc = $this->pos()->first();
        if($fc){
            return $fc->client_id;
        }
        return '';
    }
    public function getClientNameAttribute(){
        $fc = $this->pos()->first();
        if($fc){
            return $fc->client_details['client_name'];
        }
        return '';
    }
    
}
