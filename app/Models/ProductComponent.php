<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductComponent extends Model
{
    use HasFactory;
    protected $fillable = ['parent_product_id', 'child_product_id', 'quantity'];
    
    // Relationship to the parent product (e.g., "Coffee Milk")
    public function parentProduct()
    {
        return $this->belongsTo(Product::class, 'parent_product_id');
    }
    
    // Relationship to the child product (e.g., "Milk" or "Coffee")
    public function childProduct()
    {
        return $this->belongsTo(Product::class, 'child_product_id');
    }
}
