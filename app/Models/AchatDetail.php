<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AchatDetail extends Model
{
    use HasFactory;
    protected $fillable=[
        'achat_id',
        'product_id',
        'product_name',
        'product_quantity',
        'product_tva',
        'product_unity',
        'product_remise',
        'product_price_buying',
        'product_price_selling',
        'options',
    ];
    
    public function bl()
    {
        return $this->belongsTo(Bl::class);
    }
}
