<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class  BlPayment extends Model
{
    use HasFactory;
    protected $fillable = [
        'bl_financial_commitment_id',
        'client_id',
        'store_id',
        'type',
        'details',
        'received_at',
        'due_at',
        'status',
        'amount',
        'number',
        'bank',
    ];
    protected $casts = [
        'details' => 'array',
    ];
    
    public function commitment() {
        return $this->belongsTo(FinancialCommitment::class, 'financial_commitment_id');
    }
    
    
    
}
