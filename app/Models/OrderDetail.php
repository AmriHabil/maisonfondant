<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;
    protected $fillable=[
        'bl_id',
        'product_id',
        'product_ref',
        'product_name',
        'product_quantity',
        'product_price',
    ];
    
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
