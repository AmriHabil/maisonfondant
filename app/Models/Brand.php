<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;
    protected $fillable=[
        'name'
    ];
    protected $appends = [
        'default_image_url',
    ];
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    public function getDefaultImageUrlAttribute()
    {
        $image = $this->images()->where('is_default', true)->first();
        if($image){
            return $image->url;
        }
        return config('global.image.default.product');
    }
}
