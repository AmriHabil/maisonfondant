<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    use HasFactory;
    protected $fillable=[
        'invoice_id',
        'bl_id',
        'product_id',
        'product_ref',
        'product_category',
        'product_name',
        'product_quantity',
        'product_tva',
        'product_unity',
        'product_remise',
        'product_price_buying',
        'product_price_selling',
        'options',
    ];
    
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
