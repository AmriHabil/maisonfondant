<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;
    protected $fillable=[
        'store_id',
        'admin_id',
        'date',
        'number',
    ];
    protected $casts = [
        'client_details' => 'array',
    ];
    public function details() {
        return $this->hasMany(InventoryDetail::class);
    }
    public function admin() {
        return $this->belongsTo(Admin::class,'admin_id');
    }
}
