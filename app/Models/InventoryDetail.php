<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InventoryDetail extends Model
{
    use HasFactory;
    protected $fillable=[
        'inventory_id',
        'product_id',
        'product_name',
        'product_quantity_real',
        'product_quantity_supposed',
    ];
    
    public function inventory()
    {
        return $this->belongsTo(Inventory::class);
    }
}
