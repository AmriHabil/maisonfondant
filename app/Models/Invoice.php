<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;
    protected $fillable=[
        'codification',
        'store_id',
        'timbre',
        'ht',
        'ttc',
        'date',
        'type',
        'created_by',
        'updated_by'
    ];
    
    protected $appends = [
        'url',
    ];
    
    public function store() {
        return $this->belongsTo(Store::class,'store_id');
    }
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    public function getUrlAttribute()
    {
        $image = $this->images()->first();
        if($image){
            return $image->url;
        }
        return "#";
    }
}
