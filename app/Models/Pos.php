<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pos extends Model
{
    use HasFactory;
    protected $fillable=[
        'client_id',
        'store_id',
        'invoice_id',
        'admin_id',
        'status',
        'payment_status',
        'date',
        'type',
        'client_details',
        'number',
        'total',
        'timbre'
    ];
    protected $casts = [
        'client_details' => 'array',
    ];
    protected $appends = [
        'admin_name'
    ];
    public function details() {
        return $this->hasMany(PosDetail::class);
    }
    public function stockHistories()
    {
        return $this->morphMany(StockHistory::class, 'stockable');
    }
    public function commitments() {
        return $this->hasMany(FinancialCommitment::class);
    }
    public function payments() {
        return $this->hasManyThrough(PosPayment::class,FinancialCommitment::class);
    }
    public function client() {
        return $this->belongsTo(Client::class,'client_id');
    }
    public function getAdminNameAttribute()
    {
        $admin = $this->admin()->first();
        if($admin){
            return $admin->name;
        }
        return '';
    }
    public function admin() {
        return $this->belongsTo(Admin::class,'admin_id');
    }
    
    public function store() {
        return $this->belongsTo(Store::class);
    }

}
