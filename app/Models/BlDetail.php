<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlDetail extends Model
{
    use HasFactory;
    protected $fillable=[
        'bl_id',
        'product_id',
        'product_ref',
        'product_category',
        'product_name',
        'product_quantity',
        'product_tva',
        'product_unity',
        'product_remise',
        'product_bonus',
        'product_price_buying',
        'product_price_selling',
        'product_bonus',
//        'product_facturation',
        'options',
    ];
    
    public function bl()
    {
        return $this->belongsTo(Bl::class);
    }
    public function quantity_left()
    {
        return $this->hasMany(Quantity::class,'product_id','product_id')->where('store_id','=', auth()->user()->store_id);
    }
}
