<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AchatFinancialCommitment extends Model
{
    use HasFactory;
    protected $fillable = [
        'achat_id',
        'amount',
        'date',
        'payment_status',
    ];
    protected $appends= [
        'provider_id','provider_name'
    ];
    public function payments()
    {
        return $this->hasMany(AchatPayment::class);
    }
    public function achat()
    {
        return $this->belongsTo(Achat::class);
    }
    public function getProviderIdAttribute(){
        $fc = $this->achat()->first();
        if($fc){
            return $fc->provider_id;
        }
        return '';
    }
    public function getProviderNameAttribute(){
        $fc = $this->achat()->first();
        if($fc){
            return $fc->provider_details['provider_name'];
        }
        return '';
    }
}
