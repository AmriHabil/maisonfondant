<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockHistory extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'product_id',
        'store_id',
        'quantity_before',
        'quantity_after',
        'action',
        'reference',
        'stockable_id',
        'stockable_type',
        'event',
        'type'
    ];
    
    public function stockable()
    {
        return $this->morphTo();
    }
}
