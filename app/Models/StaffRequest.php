<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaffRequest extends Model
{
    use HasFactory;
    
    protected $fillable=[
        'admin_id',
        'store_id',
        'type',
        'description',
        'status',
        'accepted_by',
    ];
    
    protected $appends = [
        'admin_name',
        'supervisor_name'
    ];
    
    
    public function getAdminNameAttribute()
    {
        $admin = $this->admin()->first();
        if($admin){
            return $admin->name;
        }
        return '';
    }
    public function getSupervisorNameAttribute()
    {
        $admin = $this->supervisor()->first();
        if($admin){
            return $admin->name;
        }
        return '';
    }
    public function admin() {
        return $this->belongsTo(Admin::class,'admin_id');
    }
    public function supervisor() {
        return $this->belongsTo(Admin::class,'treated_by');
    }
}
