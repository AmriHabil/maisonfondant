<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Decaissement extends Model
{
    use HasFactory;
    protected $fillable = [
        'admin_id',
        'store_id',
        'details',
        'amount',
    ];
    protected $appends = [
        'admin_name','store_name'
    ];
    public function store(){
        return $this->belongsTo(Store::class);
    }
    public function admin(){
        return $this->belongsTo(Admin::class);
    }
    public function getStoreNameAttribute(){
        $store_name = $this->store()->first();
        if($store_name){
            return $store_name->name;
        }
        return '';
    }
    public function getAdminNameAttribute(){
        $user_name = $this->admin()->first();
        if($user_name){
            return $user_name->name;
        }
        return '';
    }
}
