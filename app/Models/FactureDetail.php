<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FactureDetail extends Model
{
    use HasFactory;
    protected $fillable=[
        'facture_id',
        'bl_id',
        'product_id',
        'product_ref',
        'product_name',
        'product_quantity',
        'product_tva',
        'product_unity',
        'product_remise',
        'product_price_buying',
        'product_price_selling',
        'options',
    ];
    
    public function facture()
    {
        return $this->belongsTo(Facture::class);
    }
}
