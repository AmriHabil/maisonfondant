<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PosDetail extends Model
{
    use HasFactory;
    protected $fillable=[
        'pos_id',
        'product_id',
        'product_name',
        'product_quantity',
        'product_tva',
        'product_unity',
        'product_remise',
        'product_bonus',
        'product_price_buying',
        'product_price_selling',
        'product_bonus',
//        'product_facturation',
        'options',
    ];
    
    public function bl()
    {
        return $this->belongsTo(Pos::class);
    }
    public function quantity_left()
    {
        return $this->hasMany(Quantity::class,'product_id','product_id')->where('store_id','=', auth()->user()->store_id);
    }
}
