<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable=[
        'admin_id',
        'store_id',
        'receiver_id',
        'status',
        'date',
        'hour',
        'address',
        'note',
        'name',
        'phone',
    ];
    
    
    public function details() {
        return $this->hasMany(OrderDetail::class);
    }
    
    public function admin() {
        return $this->belongsTo(Admin::class,'admin_id');
    }
    public function store() {
        return $this->belongsTo(Store::class,'store_id');
    }
}
