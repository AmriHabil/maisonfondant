<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img  loading="lazy" src="{{asset('assets/images/logo-d.png')}}" class="logo" alt="Logo" width="250" height="75" style="width: 250px;height: 75px">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
