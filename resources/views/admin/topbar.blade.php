@section('topbar')
</head>
<body>
<!-- Pre-loader -->
{{--<div id="preloader">--}}
{{--    <div id="status">--}}
{{--        <div class="spinner">Loading...</div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- End Preloader-->
<div id="wrapper">

    <!-- Topbar Start -->
    <div class="navbar-custom">
        <ul class="list-unstyled topnav-menu float-right mb-0">

{{--            <li class="">--}}
{{--                <form class="app-search topsearch" method="GET" action=""  autocomplete="off">--}}
{{--                    @csrf--}}
{{--                    <div class="app-search-box">--}}
{{--                        <div class="input-group">--}}
{{--                            <input type="text" class="form-control " placeholder="Numéro / Nom / Tracking ..." name="topsearch"  autocomplete="off">--}}
{{--                            <div class="input-group-append">--}}
{{--                                <button class="btn topsearch_btn" type="submit">--}}
{{--                                    <i class="fe-search"></i>--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </li>--}}
            @if(auth()->user()->role=="SuperAdmin")
                <li class="dropdown notification-list">
                    <form class="app-search " method="POST" action="{{route('admin.admins.updateRole',auth()->user()->id)}}"  autocomplete="off">
                        @csrf
                        <div class="app-search-box">
                            <div class="input-group">
                                <select name="store_id" class="form-control text-muted">

                                    @foreach(\App\Helpers\WebHelper::allStores() as $store)
                                        <option value="{{$store->id}}" @if(auth()->user()->store_id==$store->id) selected @endif>{{$store->name}}</option>
                                    @endforeach

                                </select>
                                <div class="input-group-append">
                                    <button class="btn topsearch_btn" type="submit">
                                        <i class="fe-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </li>
            @endif
            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="fe-bell noti-icon"></i>

                    @if (auth()->user()->type == 'admin' )
                        @php $orders=\App\Models\Order::where('date',date('Y-m-d', strtotime(date('Y-m-d'). ' + 1 days'))); @endphp
                     @elseif(auth()->user()->type == 'franchise' )
                        @php $orders=\App\Models\Order::where('store_id',auth()->user()->store_id)->where('date',date('Y-m-d')); @endphp
                      @elseif(auth()->user()->type == 'livreur' )
                        @php $orders=\App\Models\Order::where('date',date('Y-m-d')); @endphp
                    @endif
                    <span class="badge badge-danger rounded-circle noti-icon-badge">@if(isset($orders)) {{ $orders->count()??0 }} @else 0 @endif</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                    <!-- item-->
                    <div class="dropdown-item noti-title">
                        <h5 class="m-0">
                                    <span class="float-right">
                                        @if (auth()->user()->unreadNotifications->count() > 0)
                                            <a href="" class="text-dark">
                                                    <small>Clear All</small>
                                            </a>
                                        @endif


                                    </span>Notification
                        </h5>
                    </div>

                    <div class="slimscroll noti-scroll">

                        @if (!isset($orders) || $orders->count()==0 )
                            <a class="dropdown-item">
                                <div class="text-center">Aucune notification disponible</div>
                            </a>
                        @endif

                    <!-- item-->

                            @foreach (auth()->user()->unreadNotifications as $notification)
                                <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                    <div class="notify-icon bg-primary">
                                        <i class="fas fa-boxes"></i>
                                    </div>
                                    <p class="notify-details">{{ \App\Helpers\AppHelper::getNotificationMessage($notification) }}</p>
                                    <p class="text-muted mb-0 user-msg">
                                        <small> {{ \Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</small>
                                    </p>
                                </a>

                            @endforeach<!-- item-->
                            @if(isset($orders))
                            @foreach ($orders->get() as $order)
                                <a href="{{route('admin.orders')}}" class="dropdown-item notify-item active">
                                    <div class="notify-icon bg-primary">
                                        <i class="fas fa-boxes"></i>
                                    </div>
                                    <p class="notify-details">{{ $order->name }}</p>
                                    <p class="notify-details"><small>{{ $order->address }} - {{ $order->date }} - {{ $order->hour }}</small></p>
                                    <p class="text-muted mb-0 user-msg">
                                        <small> Crée le {{ \Carbon\Carbon::parse($order->created_at)->diffForHumans() }}</small>
                                    </p>
                                </a>

                            @endforeach<!-- item-->
                                @endif



                    </div>

                    <!-- All-->
                    <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">
                        View all
                        <i class="fi-arrow-right"></i>
                    </a>

                </div>
            </li>

            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <img src="{{URL::asset('assets/images/logo-dark.png')}}" alt="user-image" class="rounded-circle">
                    <span class="pro-user-name ml-1">
                                {{Auth::user()->name}} <i class="mdi mdi-chevron-down"></i>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                    <!-- item-->
                    <div class="dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{auth()->user()->name}}!</h6>
                    </div>
                    <!-- item-->
                    <a href="{{route('admin.profile.edit')}}" class="dropdown-item notify-item">
                        <i class="fe-user"></i>
                        <span>Mon compte</span>
                    </a>


                    <div class="dropdown-divider"></div>
                    <!-- item-->
{{--                    <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
{{--                        <i class="fe-user"></i>--}}
{{--                        <span>My Account</span>--}}
{{--                    </a>--}}

{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
{{--                        <i class="fe-settings"></i>--}}
{{--                        <span>Settings</span>--}}
{{--                    </a>--}}

{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
{{--                        <i class="fe-lock"></i>--}}
{{--                        <span>Lock Screen</span>--}}
{{--                    </a>--}}

{{--                    <div class="dropdown-divider"></div>--}}

                    <!-- item-->
                    <a href="{{route('admin.logout')}}" class="dropdown-item notify-item">
                        <i class="fe-log-out"></i>
                        <span>Logout</span>
                    </a>

                </div>
            </li>




        </ul>

        <!-- LOGO -->
        <div class="logo-box">
            <a href="/admin/" class="logo text-center">
                        <span class="logo-lg">
                            <h3 class="text-white mt-3">Maison Fondant</h3>
                            <!-- <span class="logo-lg-text-light">UBold</span> -->
                        </span>
                <span class="logo-sm">
                            <!-- <span class="logo-sm-text-dark">U</span> -->
                            <img src="{{asset('assets/images/logo-sm.png')}}" alt="" height="24" style="border-radius: 50%">
                        </span>
            </a>
        </div>

        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
            <li>
                <button class="button-menu-mobile waves-effect waves-light">
                    <i class="fe-menu"></i>
                </button>
            </li>
{{--            <li class="">--}}
{{--            <form class="app-search topsearch" method="POST" action="{{route('admin.admins.updateRole',auth()->user()->id)}}"  autocomplete="off">--}}
{{--                @csrf--}}
{{--                <div class="app-search-box">--}}
{{--                    <div class="input-group">--}}
{{--                        <select name="store_id" class="form-control text-muted">--}}
{{--                            @foreach(\App\Helpers\WebHelper::allStores() as $store)--}}
{{--                                <option value="{{$store->id}}" @if(auth()->user()->store_id==$store->id) selected @endif>{{$store->name}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                        <div class="input-group-append">--}}
{{--                            <button class="btn topsearch_btn" type="submit">--}}
{{--                                <i class="fe-arrow-right"></i>--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--            </li>--}}
{{--            <li class="dropdown d-none d-lg-block">--}}
{{--                <a class="nav-link dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">--}}
{{--                    Create New--}}
{{--                    <i class="mdi mdi-chevron-down"></i>--}}
{{--                </a>--}}
{{--                <div class="dropdown-menu">--}}
{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item">--}}
{{--                        <i class="fe-briefcase mr-1"></i>--}}
{{--                        <span>New Projects</span>--}}
{{--                    </a>--}}

{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item">--}}
{{--                        <i class="fe-user mr-1"></i>--}}
{{--                        <span>Create Users</span>--}}
{{--                    </a>--}}

{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item">--}}
{{--                        <i class="fe-bar-chart-line- mr-1"></i>--}}
{{--                        <span>Revenue Report</span>--}}
{{--                    </a>--}}

{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item">--}}
{{--                        <i class="fe-settings mr-1"></i>--}}
{{--                        <span>Settings</span>--}}
{{--                    </a>--}}

{{--                    <div class="dropdown-divider"></div>--}}

{{--                    <!-- item-->--}}
{{--                    <a href="javascript:void(0);" class="dropdown-item">--}}
{{--                        <i class="fe-headphones mr-1"></i>--}}
{{--                        <span>Help & Support</span>--}}
{{--                    </a>--}}

{{--                </div>--}}
{{--            </li>--}}

{{--            <li class="dropdown dropdown-mega d-none d-lg-block">--}}
{{--                <a class="nav-link dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">--}}
{{--                    Mega Menu--}}
{{--                    <i class="mdi mdi-chevron-down"></i>--}}
{{--                </a>--}}
{{--                <div class="dropdown-menu dropdown-megamenu">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-sm-8">--}}

{{--                            <div class="row">--}}
{{--                                <div class="col-md-4">--}}
{{--                                    <h5 class="text-dark mt-0">UI Components</h5>--}}
{{--                                    <ul class="list-unstyled megamenu-list">--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Widgets</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Nestable List</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Range Sliders</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Masonry Items</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Sweet Alerts</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Treeview Page</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Tour Page</a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}

{{--                                <div class="col-md-4">--}}
{{--                                    <h5 class="text-dark mt-0">Applications</h5>--}}
{{--                                    <ul class="list-unstyled megamenu-list">--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">eCommerce Pages</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">CRM Pages</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Email</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Calendar</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Team Contacts</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Task Board</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Email Templates</a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}

{{--                                <div class="col-md-4">--}}
{{--                                    <h5 class="text-dark mt-0">Extra Pages</h5>--}}
{{--                                    <ul class="list-unstyled megamenu-list">--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Left Sidebar with User</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Menu Collapsed</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Small Left Sidebar</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">New Header Style</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Search Result</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Gallery Pages</a>--}}
{{--                                        </li>--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">Maintenance & Coming Soon</a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-sm-4">--}}
{{--                            <div class="text-center mt-3">--}}
{{--                                <h3 class="text-dark">Special Discount Sale!</h3>--}}
{{--                                <h4>Save up to 70% off.</h4>--}}
{{--                                <button class="btn btn-primary btn-rounded mt-3">Download Now</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </li>--}}
        </ul>
    </div>
    <!-- end Topbar -->
@stop