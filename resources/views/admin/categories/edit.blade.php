@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.categories')}}">Catégorie</a></li>
                        <li class="breadcrumb-item active">Modification</li>
                    </ol>
                </div>
                <h4 class="page-title">Modifier une catégorie</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <form action="{{route('admin.categories.update',$category->id)}}" method="POST" id="category_update"  enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-6">

                <div class="card-box">
                    <h5 class="text-uppercase bg-light p-2 mt-0 mb-3">Général</h5>

                    <div class="form-group mb-3">
                        <label for="name">Nom <span class="text-danger">*</span></label>
                        <input type="text" id="name" class="form-control" placeholder="e.g : Catégorie 1" name="name" value="{{$category->name}}">
                        <ul class="parsley-errors-list filled" id="name_errors">

                        </ul>
                    </div>





{{--                    <div class="form-group mb-3">--}}
{{--                        <label for="product-category">Catégorie <span class="text-danger">*</span></label>--}}
{{--                        <select class="form-control select2" id="product_category" name="parent_id">--}}
{{--                            <option value="">Choisir un parent</option>--}}

{{--                            @foreach($categories as $categorie)--}}
{{--                                @if($category->id!=$categorie->id)--}}
{{--                                <option value="{{$categorie->id}}" @if($category->parent_id==$categorie->id) selected  @endif>{{$categorie->name}}</option>--}}
{{--                                @endif--}}
{{--                            @endforeach--}}

{{--                        </select>--}}
{{--                    </div>--}}
                    <div class="form-group mb-3">
                        <label class="mb-2">Statut <span class="text-danger">*</span></label>
                        <br/>
                        <div class="radio form-check-inline radio-success">
                            <input type="radio" id="inlineRadio1" value="1" name="status"  @if( $category->status == 1) checked @endif>
                            <label for="inlineRadio1"> Online </label>
                        </div>
                        <div class="radio form-check-inline radio-danger">
                            <input type="radio" id="inlineRadio2" value="0" name="status"  @if( $category->status == 0) checked @endif>
                            <label for="inlineRadio2"> Offline </label>
                        </div>

                    </div>
                    <div class="form-group mb-3">
                        <label for="product-category">Image </label>
                        <input type="file" class="form-control" name="images[]" multiple>
                    </div>




                </div> <!-- end card-box -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class=" mb-3">



                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light" id="sa-success">{{__('Enregistrer')}}</button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>






@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<link href="{{URL::asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('assets/css/general.css')}}" rel="stylesheet" type="text/css" />
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}




<!-- Loading buttons js -->
<script src="{{URL::asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{URL::asset('assets/libs/ladda/ladda.js')}}"></script>

<!-- Buttons init js-->
<script src="{{URL::asset('assets/js/pages/loading-btn.init.js')}}"></script>
<script>
    $("#sa-success").click(function (e) {
        e.preventDefault();
        $('.parsley-errors-list').html('');
        var Form=$(this).parents('form:first')[0];
        var formData= new FormData(Form);
        $.ajax({
            type: 'POST',
            url:"{{route('admin.categories.update',$category->id)}}",
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success:function(data)
            {
                $('.is-invalid').removeClass('is-invalid');

                $.toast({
                    heading: 'Succès',
                    text: 'Catégorie modifiée avec succés',
                    icon: 'success',
                    loader: true,
                    position:'top-right',// Change it to false to disable loader
                    loaderBg: '#5ba035',  // To change the background
                    bgColor: '#1abc9c',  // To change the background
                });

            }, error: function (reject) {
                $('.is-invalid').removeClass('is-invalid');
                swal({
                    title: "Fail!",
                    text: "Please follow the instructions",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });
                var response=$.parseJSON(reject.responseText);
                $.each(response.errors,function(key,val){
                    $("#" + key ).addClass('is-invalid');
                    $("#" + key + "_errors").html('<li class="parsley-required">'+val+'</li>');

                });
            }
        });

    });

</script>


{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
