@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{config('global.raisonsociale')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Clients</a></li>
                        <li class="breadcrumb-item active">Details</li>
                    </ol>
                </div>
                <h4 class="page-title">Client Details</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-xl-3">
            <div class="card-box text-center">
                <img src="{{asset('assets/images/logo-dark.png')}}" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">

                <h4 class="mb-0">{{$client->name}}</h4>


                <div class="text-left mt-3">
                    <h4 class="font-13 text-uppercase">Détails :</h4>

                    <p class="text-muted mb-2 font-13"><strong>Nom :</strong> <span class="ml-2">{{$client->name}}</span></p>

                    <p class="text-muted mb-2 font-13"><strong>Tel :</strong><span class="ml-2">{{$client->phone}}</span></p>

                    <p class="text-muted mb-2 font-13"><strong>MF :</strong><span class="ml-2">{{$client->mf}}</span></p>

                    <p class="text-muted mb-2 font-13"><strong>Email :</strong> <span class="ml-2 ">{{$client->email}}</span></p>

                    <p class="text-muted mb-1 font-13"><strong>Adresse :</strong> <span class="ml-2">{{$client->address}}</span></p>
                </div>


            </div>
        </div>
        <div class="col-xl-3">
            <div class="card-box">
                <h4 class="header-title mb-3">Chiffres D'affaires {{$year}}</h4>

                <div class="widget-chart text-center">
                    <input data-plugin="knob" data-width="160" data-height="160" data-linecap=round data-fgColor="#f1556c" value="@if($bls->sum('total')!=0){{number_format(100-($payments->sum('total')/$bls->sum('total'))*100,1)}} @else 0 @endif" data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".12"/>
                    <h5 class="text-muted mt-3">Total ce mois</h5>
                    <h2>{{$bls->where('month',date('m'))->first()->total??0}}</h2>

                    <p class="text-muted w-75 mx-auto sp-line-2">Cette interface vous aide à calculer le crédit actuel des clients.</p>

                    <div class="row mt-3">
                        <div class="col-4">
                            <p class="text-muted font-15 mb-1 text-truncate">Total</p>
                            <h4><i class="fe-arrow-down text-danger mr-1"></i>{{number_format($bls->sum('total'))}}</h4>
                        </div>
                        <div class="col-4">
                            <p class="text-muted font-15 mb-1 text-truncate">Paiements</p>
                            <h4><i class="fe-arrow-up text-success mr-1"></i>{{number_format($payments->sum('total'))}}</h4>
                        </div>
                        <div class="col-4">
                            <p class="text-muted font-15 mb-1 text-truncate">Reste</p>
                            <h4><i class="fe-arrow-down text-danger mr-1"></i>{{number_format($bls->sum('total') - $payments->sum('total'))}}</h4>
                        </div>
                    </div>

                </div>
            </div> <!-- end card-box -->
        </div> <!-- end col-->
        <div class="col-xl-6">
            <div class="card-box">
                <h4 class="header-title mb-3">Evolution Mensuelle</h4>

                <div id="sales-analytics" class="flot-chart mt-4 pt-1" style="height: 375px;"></div>
            </div> <!-- end card-box -->

        </div> <!-- end col-->


    </div>
    <!-- end row-->




    <!-- end row-->


    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-widgets">
                                    <a href="javascript: void(0);" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                                    <a href="javascript: void(0);" data-toggle="remove"><i class="mdi mdi-close"></i></a>
                                </div>
                                <h4 class="header-title mb-0">Top 15 produits plus commandés </h4>

                                <div id="cardCollpase5" class="collapse pt-3 show">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-centered mb-0">
                                            <thead>
                                            <tr>
                                                <th>Produit</th>
                                                <th>Prix</th>
                                                <th>Quantité</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($topproducts as $product)
                                                <tr>
                                                    <td>{{$product->name}}</td>
                                                    <td>{{$product->price}}</td>
                                                    <td>{{$product->total}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div> <!-- end table responsive-->
                                </div> <!-- collapsed end -->
                            </div> <!-- end card-body -->
                        </div> <!-- end card-->
                    </div> <!-- end col -->
                    <div class="col-xl-8">
                        <div class="row">
                            <h4 class="header-title mb-0">Liste des BL </h4>
                            <div class="table-responsive">
                                <table id="bls" class="table table-striped dt-responsive nowrap">
                                    <thead>
                                    <tr>

                                        <th>Numéro</th>
                                        <th>Boutique</th>
                                        <th>MF</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Créé à</th>
                                        <th width="100px">Action </th>

                                    </tr>
                                    </thead>
                                    <tfoot  style="display: table-row-group;">
                                    <tr>

                                        <th class="searchable">Numéro</th>
                                        <th class="searchable">Boutique</th>
                                        <th class="searchable">MF</th>
                                        <th class="searchable">Total</th>
                                        <th class="payment_status">Status</th>
                                        <th class="date">Date</th>
                                        <th class="date">Créé à</th>
                                        <th> </th>

                                    </tr>
                                    </tfoot>
                                    <tbody>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="header-title mb-0">Liste des Factures </h4>
                            <div class="table-responsive">
                                <table id="facture" class="table table-striped dt-responsive nowrap">
                                    <thead>
                                    <tr>

                                        <th>Numéro</th>
                                        <th>Boutique</th>
                                        <th>MF</th>
                                        <th>Total</th>

                                        <th>Date</th>
                                        <th>Créé à</th>
                                        <th width="100px">Action </th>

                                    </tr>
                                    </thead>
                                    <tfoot  style="display: table-row-group;">
                                    <tr>

                                        <th class="searchable">Numéro</th>
                                        <th class="searchable">Boutique</th>
                                        <th class="searchable">MF</th>
                                        <th class="searchable">Total</th>

                                        <th class="date">Date</th>
                                        <th class="date">Créé à</th>
                                        <th> </th>

                                    </tr>
                                    </tfoot>
                                    <tbody>

                                    </tbody>

                                </table>
                            </div>
                        </div>


                    </div><!-- end col-->
                </div>

            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>



    <!-- end row-->




@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/jquery-knob/jquery.knob.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.time.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.selection.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.crosshair.js')}}"></script>
<script src="{{URL::asset('assets/libs/raphael/raphael.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/morris-js/morris.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js" type="text/javascript"></script>


<script>
    !function (o) {
        "use strict";
        var t = function () {
            this.$body = o("body")
        };
        t.prototype.createCombineGraph = function (t, a, e, i) {
            var r = [
                {   label: e[0],
                    data: i[0],
                    lines: {show: !0},
                    points: {show: !0}
                },
                {
                    label: e[1],
                    data: i[1],
                    lines: {show: !0},
                    points: {show: !0}
                }, {
                    label: e[2],
                    data: i[2],
                    lines: {show: !0},
                    points: {show: !0}
                }], l = {
                series: {shadowSize: 0},
                grid: {hoverable: !0, clickable: !0, tickColor: "#f9f9f9", borderWidth: 1, borderColor: "#eeeeee"},
                colors: ["#1abc9c", "#4a81d4", "#1abc9c"],
                tooltip: !0,
                tooltipOpts: {defaultTheme: !1},
                legend: {
                    position: "ne",
                    margin: [0, -32],
                    noColumns: 0,
                    labelBoxBorderColor: null,
                    labelFormatter: function (o, t) {
                        return o + "&nbsp;&nbsp;"
                    },
                    width: 30,
                    height: 2
                },
                yaxis: {axisLabel: "Point Value (1000)", tickColor: "#f5f5f5", font: {color: "#bdbdbd"}},
                xaxis: {axisLabel: "Daily Hours", ticks: a, tickColor: "#f5f5f5", font: {color: "#bdbdbd"}}
            };
            o.plot(o(t), r,l)
        }, t.prototype.init = function () {
            var o = [

                [   @for($month = 1; $month <= 12; $month++)
                    [{{$month}}, {{$bls->where('month',$month)->first()->total??0}}],
                    @endfor
                ],
                [   @for($month = 1; $month <= 12; $month++)
                    [{{$month}}, {{$payments->where('month',$month)->first()->total??0}}],
                    @endfor
                ],

            ];
            this.createCombineGraph("#sales-analytics", [ [1, "Janvier"], [2, "Fevrier"], [3, "Mars"], [4, "Avril"], [5, "Mai"], [6, "Juin"], [7, "Juillet"], [8, "Aout"], [9, "Septembre"], [10, "Octobre"], [11, "Novembre"], [12, "Decembre"]],
                ["ACHATS","PAIEMENTS"],
                o)
        }, o.Dashboard1 = new t, o.Dashboard1.Constructor = t
    }(window.jQuery), function (o) {
        "use strict";
        o.Dashboard1.init()
    }(window.jQuery), $(".dash-daterange").flatpickr({
        altInput: !0,
        altFormat: "F j, Y",
        dateFormat: "Y-m-d",
        defaultDate: "today"
    });


</script>
<script>
    $('#bls').DataTable({

        processing: true,
        serverSide: true,

        ajax: "{{ route('admin.bl.getData',$client->id) }}",
        columns: [

            { data: 'id', name: 'id' },
            { data: 'store', name: 'store' },
            { data: 'date', name: 'date' },
            { data: 'total', name: 'total' },
            { data: 'payment_status', name: 'payment_status' },

            { data: 'date', name: 'date' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action' },

        ],
        order: [[1, 'desc']],



        aLengthMenu: [
            [10, 25, 50, 100, 200, -1],
            [10, 25, 50, 100, 200, "All"]
        ],

        dom: 'lBfrtip',
        responsive:false,
        initComplete: function () {
            // Apply the search
            this.api()
                .columns()
                .every(function () {
                    var that = this;
                    $('input,select', this.footer()).on('keyup change clear', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
        },
    });
    $('#facture').DataTable({

        processing: true,
        serverSide: true,

        ajax: "{{ route('admin.facture.getData',$client->id) }}",
        columns: [

            { data: 'number', name: 'number' },
            { data: 'store', name: 'store' },
            { data: 'date', name: 'date' },
            { data: 'total', name: 'total' },


            { data: 'date', name: 'date' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action' },

        ],
        order: [[1, 'desc']],



        aLengthMenu: [
            [10, 25, 50, 100, 200, -1],
            [10, 25, 50, 100, 200, "All"]
        ],

        dom: 'lBfrtip',
        responsive:false,
        initComplete: function () {
            // Apply the search
            this.api()
                .columns()
                .every(function () {
                    var that = this;
                    $('input,select', this.footer()).on('keyup change clear', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
        },
    });

</script>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

