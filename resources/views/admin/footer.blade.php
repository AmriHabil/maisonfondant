@section('footer')
</div> <!-- container -->

</div> <!-- content -->

<!-- Footer Start -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                2021 - 2022 &copy; Maison Fondant By <a href="https://badhrah.com" target="_blank">Amri Habil</a>
            </div>
            <div class="col-md-6">
                <div class="text-md-right footer-links d-none d-sm-block">
                    <a href="javascript:void(0);">About Us</a>
                    <a href="javascript:void(0);">Help</a>
                    <a href="javascript:void(0);">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end Footer -->

</div>

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->


</div>
<!-- END wrapper -->
<!-- Right Sidebar -->
<div class="right-bar">
    <div class="rightbar-title">
        <a href="javascript:void(0);" class="right-bar-toggle float-right">
            <i class="dripicons-cross noti-icon"></i>
        </a>
        <h5 class="m-0 text-white">Settings</h5>
    </div>
    <div class="slimscroll-menu">
        <!-- User box -->
        <div class="user-box">
            <div class="user-img">
                <img  loading="lazy" src="{{URL::asset('assets/images/users/user-1.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                <a href="javascript:void(0);" class="user-edit"><i class="mdi mdi-pencil"></i></a>
            </div>

            <h5><a href="javascript: void(0);">Geneva Kennedy</a> </h5>
            <p class="text-muted mb-0"><small>Admin Head</small></p>
        </div>

        <!-- Settings -->
        <hr class="mt-0" />
        <h5 class="pl-3">Basic Settings</h5>
        <hr class="mb-0" />

        <div class="p-3">
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox1" type="checkbox" checked>
                <label for="Rcheckbox1">
                    Notifications
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox2" type="checkbox" checked>
                <label for="Rcheckbox2">
                    API Access
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox3" type="checkbox">
                <label for="Rcheckbox3">
                    Auto Updates
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox4" type="checkbox" checked>
                <label for="Rcheckbox4">
                    Online Status
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-0">
                <input id="Rcheckbox5" type="checkbox" checked>
                <label for="Rcheckbox5">
                    Auto Payout
                </label>
            </div>
        </div>

        <!-- Timeline -->
        <hr class="mt-0" />
        <h5 class="pl-3 pr-3">Messages <span class="float-right badge badge-pill badge-danger">25</span></h5>
        <hr class="mb-0" />
        <div class="p-3">
            <div class="inbox-widget">
                <div class="inbox-item">
                    <div class="inbox-item-img"><img  loading="lazy" src="{{URL::asset('assets/images/users/user-2.jpg')}}" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Tomaslau</a></p>
                    <p class="inbox-item-text">I've finished it! See you so...</p>
                </div>
                <div class="inbox-item">
                    <div class="inbox-item-img"><img  loading="lazy" src="{{URL::asset('assets/images/users/user-3.jpg')}}" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Stillnotdavid</a></p>
                    <p class="inbox-item-text">This theme is awesome!</p>
                </div>
                <div class="inbox-item">
                    <div class="inbox-item-img"><img  loading="lazy" src="{{URL::asset('assets/images/users/user-4.jpg')}}" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Kurafire</a></p>
                    <p class="inbox-item-text">Nice to meet you</p>
                </div>

                <div class="inbox-item">
                    <div class="inbox-item-img"><img  loading="lazy" src="{{URL::asset('assets/images/users/user-5.jpg')}}" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Shahedk</a></p>
                    <p class="inbox-item-text">Hey! there I'm available...</p>
                </div>

            </div> <!-- end inbox-widget -->
        </div> <!-- end .p-3-->

    </div> <!-- end slimscroll-menu-->
</div>
<!-- /Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
{{--<div id="ajaxstart">--}}
{{--    <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>--}}
{{--</div>--}}
<style>
    #ajaxstart {
        display: block;
        position:   fixed;
        z-index:    1000;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;
        background: rgba( 255, 255, 255, .8 )
        url('http://sampsonresume.com/labs/pIkfp.gif')
        50% 50%
        no-repeat;
    }

    .lds-ellipsis {
        display: block;
        position: relative;
        width: 80px;
        height: 80px;
        margin: auto;
    }
    .lds-ellipsis div {
        position: absolute;
        top: 33px;
        width: 13px;
        height: 13px;
        border-radius: 50%;
        background: rgba(82, 82, 82, 0.56);
        animation-timing-function: cubic-bezier(0, 1, 1, 0);
    }
    .lds-ellipsis div:nth-child(1) {
        left: 8px;
        animation: lds-ellipsis1 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(2) {
        left: 8px;
        animation: lds-ellipsis2 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(3) {
        left: 32px;
        animation: lds-ellipsis2 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(4) {
        left: 56px;
        animation: lds-ellipsis3 0.6s infinite;
    }
    @keyframes lds-ellipsis1 {
        0% {
            transform: scale(0);
        }
        100% {
            transform: scale(1);
        }
    }
    @keyframes lds-ellipsis3 {
        0% {
            transform: scale(1);
        }
        100% {
            transform: scale(0);
        }
    }
    @keyframes lds-ellipsis2 {
        0% {
            transform: translate(0, 0);
        }
        100% {
            transform: translate(24px, 0);
        }
    }
</style>

<!-- Vendor js -->
<script src="{{URL::asset('assets/js/vendor.min.js')}}"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.2.4/pace.min.js" integrity="sha512-2cbsQGdowNDPcKuoBd2bCcsJky87Mv0LEtD/nunJUgk6MOYTgVMGihS/xCEghNf04DPhNiJ4DZw5BxDd1uyOdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>--}}
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="https://alditunisie.com/assets/plugins/arrow-table/arrow-table.min.js"></script>
<script>
    $(document).ready(function () {
        $('#details_table').arrowTable({
            enabledKeys: ['left', 'right', 'up', 'down'],
            listenTarget:'.quantity',
            focusTarget:'.quantity',
        });
        $('.numeric').keyup(function(e){

            if (/\D/g.test(this.value))
            {
                // Filter non-digits from input value.
                this.value = this.value.replace(/\D/g, '');
            }
            if (e.which < 0x20) {
                // e.which < 0x20, then it's not a printable character
                // e.which === 0 - Not a character
                return; // Do nothing
            }


        });

    });

</script>

{{--<script>--}}
{{--    $(document).ready(function () {--}}
{{--        $(document).ajaxStart(function() { $("#ajaxstart").fadeIn(); });--}}
{{--        $(document).ajaxStop(function(){   $("#ajaxstart").fadeOut(); });--}}
{{--    })--}}

{{--</script>--}}
<script>

    function deleteRecord(element,link){
        let row_id=element.getAttribute('deleteid');
        let Form = element.parentNode;
        console.log(Form);
        let formData = new FormData(Form);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $.ajax({
                    type: 'delete',
                    url: "/admin/"+link+"/delete/" + row_id,
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (data) {
                        if (data == 'success') {
                            $.toast({
                                heading: 'Supprimé',
                                text: 'Record  supprimé avec succés',
                                icon: 'success',
                                loader: true,
                                position:'top-right',// Change it to false to disable loader
                                loaderBg: '#5ba035',  // To change the background
                                bgColor: '#1abc9c',  // To change the background
                            });
                            $('#row' + row_id).remove();
                        } else {
                            swal({
                                    title: 'Not Deleted !',
                                    text: "Record non supprimé",
                                    type: 'info',
                                    confirmButtonClass: 'btn btn-confirm mt-2'
                                }
                            )
                        }
                    }, error: function (reject) {
                        swal({
                                title: 'Not Deleted !',
                                text: "Record non supprimé",
                                type: 'info',
                                confirmButtonClass: 'btn btn-confirm mt-2'
                            }
                        )
                    }
                });
            }
        })

    }
    function updateStatus(element){
        let row_id=element.getAttribute('order_id');
        let Form = element.parentNode;
        console.log(Form);
        let formData = new FormData(Form);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $.ajax({
                    type: 'post',
                    url: "/admin/orders/update/" + row_id,
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (data) {

                        $.toast({
                            heading: 'Modifié',
                            text: 'Record  supprimé avec succés',
                            icon: 'success',
                            loader: true,
                            position:'top-right',// Change it to false to disable loader
                            loaderBg: '#5ba035',  // To change the background
                            bgColor: '#1abc9c',  // To change the background
                        });
                        $('#status_id_' + row_id).html(data);

                    }, error: function (reject) {
                        swal({
                                title: 'Not Deleted !',
                                text: "Record non supprimé",
                                type: 'info',
                                confirmButtonClass: 'btn btn-confirm mt-2'
                            }
                        )
                    }
                });
            }
        })

    }
</script>
<script>
    function deleteResource(element,link){
        let row_id=element.getAttribute('resource');

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $.ajax({
                    type: 'delete',
                    url: "/admin/"+link+"/delete/" + row_id,
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    data: {},
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (data) {
                        if (data == 'success') {
                            $.toast({
                                heading: 'Supprimé',
                                text: 'Record  supprimé avec succés',
                                icon: 'success',
                                loader: true,
                                position:'top-right',// Change it to false to disable loader
                                loaderBg: '#5ba035',  // To change the background
                                bgColor: '#1abc9c',  // To change the background
                            });
                            $('#row' + row_id).remove();
                            $('#'+link + row_id).remove();
                        } else {
                            swal({
                                    title: 'Not Deleted !',
                                    text: "Record non supprimé",
                                    type: 'info',
                                    confirmButtonClass: 'btn btn-confirm mt-2'
                                }
                            )
                        }
                    }, error: function (reject) {
                        swal({
                                title: 'Not Deleted !',
                                text: "Record non supprimé",
                                type: 'info',
                                confirmButtonClass: 'btn btn-confirm mt-2'
                            }
                        )
                    }
                });
            }
        })

    }
</script>
<script>
    $(document).on('submit','#bulk_submit',function(e){
        $('#bulk_submit_details').html($('.order_check:checked').get());

    });

</script>
@if (Session::has('global_error'))
    <script>
        $(function() {
            swal({
                    title: 'Echec !',
                    text: "{{ Session::get('global_error') }}",
                    type: 'info',
                    confirmButtonClass: 'btn btn-confirm mt-2'
                }
            )
        })
    </script>
@endif
@if (Session::has('global_success'))
    <script>
        $(function() {
            swal({
                    title: 'Bravo !',
                    text: "{{ Session::get('global_success') }}",
                    type: 'info',
                    confirmButtonClass: 'btn btn-confirm mt-2'
                }
            )
        })
    </script>
@endif


@stop