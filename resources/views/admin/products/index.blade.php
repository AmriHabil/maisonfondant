@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Maison Fondant</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Produits</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
                <h4 class="page-title"> Liste Des Produits</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- end row-->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                        <!-- end row-->
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                        <table id="key-datatable" class="table table-striped dt-responsive nowrap">
                                            <thead>
                                            <tr>


                                                <th>Ordre</th>
                                                <th>Ref</th>
                                                <th>Produit</th>
                                                <th>Goût</th>
                                                <th>Prix</th>
                                                <th>Categorie</th>
                                                
                                                <th width="100px">Action </th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            </tbody>
                                            <tfoot>
{{--                                            <tr>--}}


{{--                                                <th class="searchable">Produit</th>--}}
{{--                                                <th class="tastes">Goût</th>--}}
{{--                                                <th class="searchable">Brand</th>--}}
{{--                                                <th class="searchable">Categorie</th>--}}
{{--                                               --}}
{{--                                                <th ></th>--}}
{{--                                            </tr>--}}
                                            </tfoot>
                                        </table>
                                </div>

                            </div><!-- end col-->
                        </div>

                        <!-- end row-->



                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->





@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>

<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.2/jquery-ui.min.js" integrity="sha512-57oZ/vW8ANMjR/KQ6Be9v/+/h6bq9/l3f0Oc7vn6qMqyhvPd1cvKBRWWpzu0QoneImqr2SkmO4MSqU+RpHom3Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!-- third party js ends -->
<!-- Datatables init -->
<script>

    table=$('#key-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.products.getData',$type) }}",
        columns: [
            { data: 'value', name: 'value'},
            { data: 'ref', name: 'ref'},
            { data: 'name', name: 'name' },
            { data: 'taste_id', name: 'taste_id' },
            { data: 'price', name: 'price' },
            { data: 'category', name: 'categories.name' },
            { data: 'action', name: 'action' },
        ],
        order: [[0, 'asc']],
        aLengthMenu: [
            [-1, 10, 25, 50, 100, 200 ,-1],
            ["All", 10, 25, 50, 100, 200,"All"]
        ],
        dom: 'Blfrtip',
        responsive:false,


    });
    $('#key-datatable tfoot .searchable').each(function () {
        var title = $(this).text();
        $(this).html('<input class="form-control" type="text" placeholder="Search ' + title + '" />');
    });
    $('#key-datatable tfoot .tastes').each(function () {
        var title = $(this).text();
        $(this).html('<select class="select2 form-control"><option>Choisir un goût !</option>@foreach($tastes as $taste) <option value="{{$taste->id}}">{{$taste->name}}</option> @endforeach</select>');
    });
    $('#key-datatable tfoot .categories').each(function () {
        var title = $(this).text();
        $(this).html('<select class="select2 form-control"><option>Choisir une catégorie !</option>@foreach($categories as $category) <option value="{{$category->id}}">{{$category->name}}</option> @endforeach</select>');
    });
    $('#key-datatable tfoot .brands').each(function () {
        var title = $(this).text();
        $(this).html('<select class="select2 form-control"><option>Choisir une catégorie !</option>@foreach($brands as $brand) <option value="{{$brand->id}}">{{$brand->name}}</option> @endforeach</select>');
    });

    $('tbody').sortable({
        placeholder : "ui-state-highlight",
        update : function(event, ui)
        {
            var page_id_array = new Array();
            $('tbody tr').each(function(){
                page_id_array.push($(this).attr('id'));
            });
            var tab='publication';
            $.ajax({
                url:"{{route('admin.products.reOrdering')}}",
                method:"POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data:{page_id_array:page_id_array,tab:tab, action:'update'},
                success:function()
                {
                    table.draw( 'page' );
                }
            })
        }
    });
    $(document).ajaxStart(function() { $("#ajaxstart").fadeIn(); });
    $(document).ajaxStop(function(){   $("#ajaxstart").fadeOut(); });
</script>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

