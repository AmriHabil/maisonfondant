@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.products')}}">Produits</a></li>
                        <li class="breadcrumb-item active">Création</li>
                    </ol>
                </div>
                <h4 class="page-title">Créer un produit</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <form action="{{route('admin.products.update',$product->id)}}" method="POST" id="product_store" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-6">


                <div class="card-box">
                    <h5 class="text-uppercase bg-light p-2 mt-0 mb-3">Général</h5>

                    <div class="form-group mb-3">
                        <label for="name">Nom <span class="text-danger">*</span></label>
                        <input type="text" id="name" class="form-control" placeholder="e.g : Apple iMac" name="name"  value="{{$product->name}}">
                        <ul class="parsley-errors-list filled" id="name_errors">

                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group mb-3">
                                <label for="coefficient">Coefficient <span class="text-danger">*</span></label>
                                <input type="number" id="coefficient" class="form-control" placeholder="e.g : 1"
                                       name="coefficient" step="0.001"  value="{{$product->coefficient}}">
                                <ul class="parsley-errors-list filled" id="coefficient_errors">

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-3">
                                <label for="price">Prix <span class="text-danger">*</span></label>
                                <input type="number" id="price" class="form-control" placeholder="e.g : 5.300"
                                       name="price" step="0.001"  value="{{$product->price}}">
                                <ul class="parsley-errors-list filled" id="price_errors">

                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group mb-3">
                                <label for="buying">Coût <span class="text-danger">*</span></label>
                                <input type="number" id="buying" class="form-control" placeholder="e.g : 5.300"
                                       name="buying" step="0.001" required   value="{{$product->buying}}">
                                <ul class="parsley-errors-list filled" id="buying_errors">

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-3">
                                <label for="tva">TVA <span class="text-danger">*</span></label>
                                <input type="number" id="tva" class="form-control" placeholder="19"
                                       name="tva" step="0.001" required   value="{{$product->tva}}">
                                <ul class="parsley-errors-list filled" id="tva_errors">

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-3">
                                <label for="ref">Réf <span class="text-danger">*</span></label>
                                <input type="text" id="ref" class="form-control" placeholder="XYZ"   value="{{$product->ref}}"
                                       name="ref" >
                                <ul class="parsley-errors-list filled" id="ref_errors">

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-3">
                                <label for="value">Order <span class="text-danger">*</span></label>
                                <input type="text" id="value" class="form-control" placeholder="N:X"   value="{{$product->value}}"
                                       name="value" >
                                <ul class="parsley-errors-list filled" id="ref_errors">

                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group mb-3">
                                <label for="category_id">Catégorie <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="category_id" name="category_id">
                                    <option value=""></option>
                                    @foreach($categories as $categorie)
                                        <option value="{{$categorie->id}}" @if($product->category_id==$categorie->id) selected @endif>{{$categorie->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group mb-3">
                                <label for="brand_id">Marque <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="brand_id" name="brand_id">
                                    <option value=""></option>
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->id}}"  @if($product->brand_id==$brand->id) selected @endif>{{$brand->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-3">
                                <label for="taste_id">Goûts <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="taste_id" name="taste_id">
                                    <option value=""></option>
                                    @foreach($tastes as $taste)
                                        <option value="{{$taste->id}}" @if($product->taste_id==$taste->id) selected @endif>{{$taste->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                </div> <!-- end card-box -->
            </div> <!-- end col -->
            <div class="col-lg-6">


                <div class="card-box">
                    <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">POS</h5>
                    <div class="form-group mb-3">
                        <label class="mb-2">Affichage dans le POS <span class="text-danger">*</span></label>
                        <br/>
                        <div class="radio form-check-inline radio-success">
                            <input type="radio" id="inlineRadio1" value="1" name="status"  @if( $product->status == 1) checked @endif>
                            <label for="inlineRadio1"> Online </label>
                        </div>
                        <div class="radio form-check-inline radio-danger">
                            <input type="radio" id="inlineRadio2" value="0" name="status"  @if( $product->status == 0) checked @endif>
                            <label for="inlineRadio2"> Offline </label>
                        </div>

                    </div>
                    <!-- Components Section -->
                    <div id="componentsSection">
                        @foreach ($product->components as $index => $component)
                            <div class="row componentRow mb-2">
                                <div class="col-md-5">
                                    <select name="components[{{ $index }}][child_product_id]" class="form-control childProduct" required>
                                        <option value="">-- Select Component --</option>
                                        @foreach($products as $childProduct)
                                            <option value="{{ $childProduct->id }}"
                                                    {{ $childProduct->id == $component->id ? 'selected' : '' }}>
                                                {{ $childProduct->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <input type="number" step="0.001" name="components[{{ $index }}][quantity]"
                                           class="form-control quantity" placeholder="Quantity"
                                           value="{{ $component->pivot->quantity }}" required>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-danger removeComponent">Supprimer</button>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!-- Add Component Button -->
                    <button type="button" id="addComponent" class="btn btn-primary mb-3">Ajouter composants</button>
                    <div class="form-group mb-3">
                        <label for="product-image">Image</label>
                        <input type="file" class="form-control" id="product-image" name="image" >
                    </div>
                    <img src="{{$product->image}}"  class="img-fluid" alt="">

                </div> <!-- end card-box -->

            </div> <!-- end col-->


        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="text-center mb-3">


                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light"
                            id="sa-success">{{__('Enregistrer')}}</button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>





@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/css/general.css')}}" rel="stylesheet" type="text/css"/>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<!-- Loading buttons js -->
<script src="{{asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{asset('assets/libs/ladda/ladda.js')}}"></script>
<!-- Buttons init js-->
<script src="{{asset('assets/js/pages/loading-btn.init.js')}}"></script>
<!-- Init js -->
<script src="{{asset('assets/js/pages/add-product.init.js')}}"></script>
{{--<script src="{{asset('assets/js/validation.js')}}"></script>--}}
<script>
    $(document).ready(function () {
        let componentIndex = {{ count($product->components) }};

        // Add new component row
        $('#addComponent').click(function () {
            const newComponentRow = `
                <div class="row componentRow mb-2">
                    <div class="col-md-5">
                        <select name="components[${componentIndex}][child_product_id]" class="form-control childProduct" required>
                            <option value="">-- Select Component --</option>
                            @foreach($products as $childProduct)
                <option value="{{ $childProduct->id }}">{{ $childProduct->name }}</option>
                            @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <input type="number" step="0.001" name="components[${componentIndex}][quantity]" class="form-control quantity" placeholder="Quantity" required>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-danger removeComponent">Supprimer</button>
                    </div>
                </div>
            `;
            $('#componentsSection').append(newComponentRow);
            componentIndex++;
        });

        // Remove a component row
        $(document).on('click', '.removeComponent', function () {
            $(this).closest('.componentRow').remove();
        });
    });
</script>
<script>

    $("#product_store").submit(function (e) {
        e.preventDefault();
        var Form = $("#product_store")[0];
        var formData = new FormData(Form);
        $.ajax({
            type: 'POST',
            url: "{{route('admin.products.update',$product->id)}}",
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                $('.is-invalid').removeClass('is-invalid');
                $('#preloader').show();
                $.toast({
                    heading: 'Succès',
                    text: 'Produits créé avec succés',
                    icon: 'success',
                    loader: true,
                    position: 'top-right',// Change it to false to disable loader
                    loaderBg: '#5ba035',  // To change the background
                    bgColor: '#1abc9c',  // To change the background
                });

            }, error: function (reject) {
                $('.is-invalid').removeClass('is-invalid');
                swal({
                    title: "Erreur!",
                    text: "Merci de suivre les indications",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });
                var response = $.parseJSON(reject.responseText);
                $.each(response.errors, function (key, val) {
                    $("#" + key).addClass('is-invalid');
                    $("#" + key + "_errors").html('<li class="parsley-required">' + val + '</li>');

                });
            }
        });

    });

</script>
<style>
    input.chk-btn {
        display: none;
    }

    .label-chk-btn {
        width: 60px;
    }

    .label-chk-btn img {
        filter: grayscale(100%);
        height: 60px;
        transition: ease .3s;
        width: 45px;
        margin: auto;
        display: block;
    }

    input.chk-btn:checked + label img {
        transform: scale(1.1);
        filter: grayscale(0);
    }

    .is-invalid {
        border-color: #dc3545;
        padding-right: calc(1.5em + .75rem);
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-position: right calc(.375em + .1875rem) center;
        background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
