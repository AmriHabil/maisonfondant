@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a
                                    href="{{route('admin.dashboard')}}">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.products')}}">Produits</a></li>
                        <li class="breadcrumb-item active">Details</li>
                    </ol>
                </div>
                <h4 class="page-title">Detail des produits</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-xl-5">

                        <div class="tab-content pt-0">
                            <div class="tab-pane active show" id="product-1-item">
                                <img src="{{asset($product->default_image_url)}}" alt=""
                                     class="img-fluid mx-auto d-block rounded">
                            </div>
                        </div>
                    </div> <!-- end col -->
                    <div class="col-xl-7">
                        <div class="pl-xl-3 mt-3 mt-xl-0">
                            <a href="#" class="text-primary"> {{$product->barcode}}</a>
                            <h4 class="mb-3">{{$product->name}}</h4>
                            <h4 class="mb-4">Prix : <b>{{$product->price}} TND</b></h4>
                            <h4><span class="badge bg-soft-success text-success mb-4">Stock</span></h4>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Store</th>
                                    <th width="200px">Quantité</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($stores as $store)
                                    <tr>
                                        <td>
                                            {{$store->name}}
                                        </td>
                                        <td>
                                            {{$stores_details->where('id',$store->id)->first()->quantity??0}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div>
                                        <p class="text-muted"><i
                                                    class="mdi mdi-checkbox-marked-circle-outline h6 text-primary mr-2"></i>
                                            Marque : {{$product->brand_name}}</p>
                                        <p class="text-muted"><i
                                                    class="mdi mdi-checkbox-marked-circle-outline h6 text-primary mr-2"></i>
                                            Catégorie : {{$product->category_name}}</p>
                                        <p class="text-muted"><i
                                                    class="mdi mdi-checkbox-marked-circle-outline h6 text-primary mr-2"></i>
                                            Goût : {{$product->taste_name}}</p>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
                <div class="card">
                    <div class="card-body w-100">
                        <ul class="nav nav-tabs nav-bordered nav-justified w-100" id="myTab" role="tablist">

                            <li class="nav-item" role="presentation">
                                <a class="nav-link " id="logs-tab" data-toggle="tab" data-target="#logs" type="button"
                                   role="tab" aria-controls="home" aria-selected="true">Logs</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link " id="achats-tab" data-toggle="tab" data-target="#achats" type="button"
                                   role="tab" aria-controls="home" aria-selected="true">Achats</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link " id="POS-tab" data-toggle="tab" data-target="#POS" type="button"
                                   role="tab" aria-controls="home" aria-selected="true">Vente POS</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link " id="bs-tab" data-toggle="tab" data-target="#bs" type="button"
                                   role="tab" aria-controls="home" aria-selected="true">BS</a>
                            </li>


                        </ul>
                        <div class="tab-content w-100" id="myTabContent">
                                <div class="tab-pane fade  " id="logs" role="tabpanel"
                                     aria-labelledby="logs-tab">
                                    <div class="row">
                                        <div class="container">
                                            <h2 class="mb-4">Historique du Stock</h2>
                                            <table id="stockHistoryTable"  class="table table-striped nowrap">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Produit</th>
                                                    <th>Magasin</th>
                                                    <th>Opération</th>
                                                    <th>Référence</th>
                                                    <th>Quantité Avant</th>
                                                    <th>Changement</th>
                                                    <th>Quantité Après</th>
                                                    <th>Action</th>
                                                    <th>Type</th>
                                                    <th>Date</th>
                                                </tr>
                                                </thead>
                                                <tfoot style="display: table-row-group;" class="d-none">
                                                <tr>
                                                    <th>ID</th>
                                                    <th class="searchable">Produit</th>
                                                    <th class="searchable">Magasin</th>
                                                    <th class="searchable">Opération</th>
                                                    <th class="searchable">Référence</th>
                                                    <th class="searchable">Quantité Avant</th>
                                                    <th class="searchable">Changement</th>
                                                    <th class="searchable">Quantité Après</th>
                                                    <th class="searchable">Action</th>
                                                    <th class="searchable">Type</th>
                                                    <th class="searchable">Date</th>

                                                </tr>
                                                </tfoot>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade  " id="achats" role="tabpanel"
                                     aria-labelledby="achats-tab">
                                    <div class="row">
                                    </div>
                                </div>
                            <div class="tab-pane fade  " id="POS" role="tabpanel"
                                 aria-labelledby="POS-tab">
                                <div class="row">
                                </div>
                            </div>
                            <div class="tab-pane fade  " id="bs" role="tabpanel"
                                 aria-labelledby="bs-tab">
                                <div class="row">
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


            </div> <!-- end card-->
        </div> <!-- end col-->

    </div>
    <!-- end row-->




@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- third party css end -->
<!-- Plugins css -->
<!-- Plugins css -->

<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>


<!-- third party js ends -->
<!-- Datatables init -->
<script>
    $(document).ready(function() {
        $('#stockHistoryTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route("admin.products.getDataLog",$product->id) }}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'product_id', name: 'product_id' },
                { data: 'store', name: 'stores.name' },
                { data: 'operation', name: 'operation' },
                { data: 'reference', name: 'reference' },
                { data: 'quantity_before', name: 'quantity_before' },
                { data: 'change', name: 'change' },
                { data: 'quantity_after', name: 'quantity_after' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
                { data: 'type', name: 'type' },
                { data: 'created_at', name: 'created_at' }
            ],
            order: [[0, 'desc']], // Trier par date descendante
            initComplete: function () {
                // Apply the search
                this.api()
                    .columns()
                    .every(function () {
                        var that = this;
                        $('input,select', this.footer()).on('keyup change clear', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });
            }
        });
        $('#stockHistoryTable tfoot .searchable').each(function () {
            var title = $(this).text();
            $(this).html('<input class="form-control" type="text" placeholder="Search ' + title + '" />');
        });
    });
</script>


{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

