@section('content')




    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.products')}}">Produits</a></li>
                        <li class="breadcrumb-item active">Création</li>
                    </ol>
                </div>
                <h4 class="page-title">PARAMÈTRES DES PRODUITS</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    @canany(['Forcer Les Quantités'])
    <form action="{{route('admin.products.updateSettings',$product->id)}}" method="POST" id="product_store">
        @csrf
    @endcanany
        <div class="row">
            <div class="col-lg-12">

                <div class="card-box">
                    <h5 class="text-uppercase bg-light p-2 mt-0 mb-3">Paramètres de produits par point de vente</h5>

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Store</th>
                                <th width="200px">Quantité</th>
{{--                                <th>P.Détail</th>--}}
{{--                                <th>P.Semi-Gros</th>--}}
{{--                                <th>P.Gros</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stores as $store)
                                @if(!auth()->user()->can('Qté Dépôt') && $store->id==1)

                                @else
                                <tr>
                                    <td>
                                        {{$store->name}}
                                        <input type="hidden" name="store_id[]" value="{{$store->id}}">
                                    </td>
                                    <td>
                                        <input type="hidden" name="quantity_id[]" value="{{$stores_details->where('id',$store->id)->first()->quantity_id??0}}">
                                        <input type="number" step="0.001" class="form-control" value="{{$stores_details->where('id',$store->id)->first()->quantity??0}}" name="quantity[]">
                                    </td>
{{--                                    <td>--}}
{{--                                        <input type="hidden" name="price_id[]" value="{{$stores_details->where('id',$store->id)->first()->price_id??0}}">--}}
{{--                                        <input type="number" step="0.001" class="form-control" value="{{$stores_details->where('id',$store->id)->first()->selling1??0}}" name="selling1[]">--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        <input type="number" step="0.001" class="form-control" value="{{$stores_details->where('id',$store->id)->first()->selling2??0}}" name="selling2[]">--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        <input type="number" step="0.001" class="form-control" value="{{$stores_details->where('id',$store->id)->first()->selling3??0}}" name="selling3[]">--}}
{{--                                    </td>--}}
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>




                </div> <!-- end card-box -->

            </div> <!-- end col -->


        </div>
        <!-- end row -->
    @canany(['Forcer Les Quantités'])
        <div class="row">
            <div class="col-12">
                <div class="text-center mb-3">



                    <button type="submit" class="ladda-button  btn w-sm btn-success waves-effect waves-light"  data-style="expand-left"  id="sa-success">{{__('Enregistrer')}}</button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>
    @endcanany





@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/summernote/summernote-bs4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/general.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<!-- Loading buttons js -->
<script src="{{asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{asset('assets/libs/ladda/ladda.js')}}"></script>
<script src="{{asset('assets/libs/dropzone/dropzone.min.js')}}"></script>
<script src="{{asset('assets/libs/summernote/summernote-bs4.min.js')}}"></script>
<!-- Buttons init js-->
<script src="{{asset('assets/js/pages/loading-btn.init.js')}}"></script>
<!-- Init js -->
<script src="{{asset('assets/js/pages/add-product.init.js')}}"></script>
{{--<script src="{{asset('assets/js/validation.js')}}"></script>--}}

<script>

    $("#product_store").submit(function (e) {
        e.preventDefault();
        var Form=$("#product_store")[0];
        var formData= new FormData(Form);
        $.ajax({
            type: 'POST',
            url:"{{route('admin.products.updateSettings',$product->id)}}",
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success:function(data)
            {
                $('.is-invalid').removeClass('is-invalid');

                $('#preloader').show();
                $.toast({
                    heading: 'Succès',
                    text: 'Produits mis à jours avec succés',
                    icon: 'success',
                    loader: true,
                    position:'top-right',// Change it to false to disable loader
                    loaderBg: '#5ba035',  // To change the background
                    bgColor: '#1abc9c',  // To change the background
                });

            }, error: function (reject) {
                $('.is-invalid').removeClass('is-invalid');
                swal({
                    title: "Erreur!",
                    text: "Merci de suivre les indications",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });
                var response=$.parseJSON(reject.responseText);
                $.each(response.errors,function(key,val){
                    $("#" + key ).addClass('is-invalid');
                    $("#" + key + "_errors").html('<li class="parsley-required">'+val+'</li>');

                });
            }
        });

    });
    $(".select2").select2();
    $("#product-description").summernote({height:180,minHeight:null,maxHeight:null,focus:!1});
</script>
<style>
    input.chk-btn {
        display: none;
    }
    .label-chk-btn{
        width: 60px;
    }
    .label-chk-btn img{
        filter: grayscale(100%);
        height: 60px;
        transition: ease .3s;
        width: 45px;
        margin: auto;
        display: block;
    }

    input.chk-btn:checked + label img{
        transform: scale(1.1);
        filter: grayscale(0);
    }
    .is-invalid{
        border-color: #dc3545;
        padding-right: calc(1.5em + .75rem);
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-position: right calc(.375em + .1875rem) center;
        background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
