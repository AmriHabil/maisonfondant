@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{config('global.raisonsociale')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Echéances</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
                <h4 class="page-title">Echéances d'achats</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- end row-->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- end row-->
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="key-datatable" class="table table-striped dt-responsive nowrap">
                                    <thead>
                                    <tr>

                                        <th>Ref</th>
                                        <th>Fournisseur</th>
                                        <th>Achat</th>
                                        <th>Montant</th>
                                        <th>Date</th>
                                        <th>Store</th>
                                        <th>Montant</th>
                                        <th width="100px">Action </th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>

                                        <th class="searchable">Ref</th>
                                        <th class="searchable">Fournisseur</th>
                                        <th class="searchable">Achat</th>
                                        <th class="searchable">Montant</th>
                                        <th class="searchable">Date</th>
                                        <th class="stores">Store</th>
                                        <th class="payment">Etat</th>
                                        <th ></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div><!-- end col-->
                    </div>

                    <!-- end row-->



                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->





@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>

<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js" type="text/javascript"></script>



<!-- third party js ends -->
<!-- Datatables init -->
<script>

    $('#key-datatable').DataTable({

        processing: true,
        serverSide: true,

        ajax: "{{ route('admin.achatfinancialcommitment.getData') }}",
        columns: [
            { data: 'id', name: 'id' },
            { data: 'provider', name: 'achats.provider_details' },
            { data: 'achat_id', name: 'achat_id' },
            { data: 'amount', name: 'amount' },
            { data: 'date', name: 'date' },
            { data: 'store', name: 'achats.store_id' },
            { data: 'payment_status', name: 'payment_status' },

            { data: 'action', name: 'action' },

        ],
        order: [[0, 'desc']],



        aLengthMenu: [
            [10, 25, 50, 100, 200, -1],
            [10, 25, 50, 100, 200, "All"]
        ],

        dom: 'Blfrtip',
        responsive:false,
        initComplete: function () {
            // Apply the search
            this.api()
                .columns()
                .every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change clear', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                    $('select', this.footer()).on('keyup change clear', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
        },
    });
    $('#key-datatable tfoot .searchable').each(function () {
        var title = $(this).text();
        $(this).html('<input class="form-control" type="text" placeholder="Search ' + title + '" />');
    });
    $('#key-datatable tfoot .stores').each(function () {
        var title = $(this).text();
        $(this).html('<select class="form-control"> @foreach(\App\Helpers\WebHelper::allStores() as $store) <option value="{{$store->id}}">{{$store->name}}</option> @endforeach <select/>');
    });
    $('#key-datatable tfoot .payment').each(function () {
        var title = $(this).text();
        $(this).html('<select class="form-control">  <option></option> <option value="0">Non Payé</option><option value="1">Partiellement Payé</option> <option value="2">Payé</option> <select/>');
    });

</script>


{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

