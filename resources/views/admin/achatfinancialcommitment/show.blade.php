
@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Encaissement</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="bx bx-home-alt"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.financialcommitment.index')}}">Liste des paiements</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Détails échéance</li>
                    </ol>
                </nav>
            </div>
            {{-- <div class="ms-auto">
                <div class="btn-group">
                    <button type="button" class="btn btn-primary">Settings</button>
                    <button type="button" class="btn btn-primary split-bg-primary dropdown-toggle dropdown-toggle-split"
                        data-bs-toggle="dropdown"> <span class="visually-hidden">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-end"> <a class="dropdown-item"
                            href="javascript:;">Action</a>
                        <a class="dropdown-item" href="javascript:;">Another action</a>
                        <a class="dropdown-item" href="javascript:;">Something else here</a>
                        <div class="dropdown-divider"></div> <a class="dropdown-item" href="javascript:;">Separated link</a>
                    </div>
                </div>
            </div> --}}
        </div>
        <!--end breadcrumb-->

        <div class="card">
            <div class="card-header py-3">
                <div class="row g-3 align-items-center">
                    <div class="col-12 col-lg-4 col-md-6 me-auto">
                        <h1 class="mb-0">BL: {{ $cart->id }}</h1>
                        <p class="mb-1">{{ $cart->date }}</p>
                    </div>
                    <div class="col">
                        <div class="widget-rounded-circle card-box">
                            <a href="{{route('admin.orders','inhouse')}}">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="avatar-lg rounded-circle shadow-lg bg-info border-info border">
                                            <i class="fa fa-dollar-sign font-22 avatar-title text-white"></i>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="text-right">
                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $cart->total }}</span>
                                            </h3>
                                            <p class="text-muted mb-1 text-truncate">Total</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                            </a>

                        </div> <!-- end widget-rounded-circle-->

                    </div>

                    @php
                        $total = $cart
                            ->commitments()
                            ->with('payements')
                            ->get()
                            ->pluck('payements')
                            ->collapse()
                            ->sum('amount');
                    @endphp
                    <div class="col">
                        <div class="widget-rounded-circle card-box">
                            <a href="{{route('admin.orders','inhouse')}}">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="avatar-lg rounded-circle shadow-lg bg-success border-success border">
                                            <i class="fa fa-check font-22 avatar-title text-white"></i>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="text-right">
                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ number_format($total, 3) }}</span>
                                            </h3>
                                            <p class="text-muted mb-1 text-truncate">Montant payé</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                            </a>

                        </div> <!-- end widget-rounded-circle-->

                    </div>

                </div>
            </div>
            <div class="card-body">
                <div class="row row-cols-1 row-cols-xl-2 row-cols-xxl-3">
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-primary bpayment-0">
                                        <i class="bi bi-person text-primary"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Client</h6>
                                        <p class="mb-1">{{ $cart->client_details['client_name'] }}</p>
                                        <p class="mb-1">{{ $cart->client_details['client_address'] }}</p>
                                        <p class="mb-1">{{ $cart->client_details['client_phone'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-success bpayment-0">
                                        <i class="bi bi-truck text-success"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Info de Paiement </h6>
                                        <p class="mb-1"><strong>Expédition</strong> : ALDI</p>
                                        <p class="mb-1"><strong>Statut</strong>
                                            @php
                                                if ($cart->paiment_status == 0) {
                                                    $status = '<td><span class="badge rounded-pill badge-danger">Non Payé</span></td>';
                                                } else {
                                                    $status = '<td><span class="badge rounded-pill badge-success">Payé</span></td>';
                                                }
                                                echo $status;
                                            @endphp
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center gap-3">
                                    <div class="icon-box bg-light-danger bpayment-0">
                                        <i class="bi bi-geo-alt text-danger"></i>
                                    </div>
                                    <div class="info">
                                        <h6 class="mb-2">Livrer à</h6>
                                        <p class="mb-1"><strong>Address</strong> :
                                            {{ $cart->client_details['client_address'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->

                <div class="row">
                    <div class="col-12 col-lg-12">
                        <div class="card bpayment shadow-none radius-10">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table align-middle mb-0">
                                        <thead class="table-light">
                                            <tr>
                                                {{-- <th>ID de Paiement</th> --}}
                                                <th>Montant échéance</th>
                                                <th>Date</th>
                                                <th>Montant payé</th>
                                                <th>Etat</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($cart->commitments as $commitment)
                                                <tr>
                                                    {{-- <td>#{{ $paiement->id }}</td> --}}
                                                    <td>{{ $commitment->amount }}</td>
                                                    <td class="{{ !$commitment->payment_status && $commitment->date <= date('Y-m-d') ?'text-danger':''}}">{{ $commitment->date }}</td>
                                                    <td>{{ $commitment->payements()->sum('amount') }}</td>
                                                    <td>
                                                        <span class="badge rounded-pill badge-{{$commitment->payment_status ? 'success' : 'danger'}}">{{$commitment->payment_status ? '' : 'Non '}}Payé</span>
                                                    <td>

                                                        <a href="{{ route('admin.financialcommitment.showcommitment', ['commitment' => $commitment->id, 'type' => strtolower(class_basename($commitment->parent))]) }}"
                                                            class="text-info">
                                                            <i class="fas fa-eye"></i>
                                                        </a>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--end row-->

            </div>
        </div>

    </main>
    <!--end page main-->





    <script src="{{ asset('assets/plugins/sweetalert2/js/sweetalert2.min.js') }}"></script>

    @if (Session::has('success'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'success',
                    title: "{{ Session::get('success') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>

        {{ Session::forget('success') }}
        {{ Session::save() }}
    @endif

    @if (Session::has('error'))
        <script>
            $(function() {
                Swal.fire({
                    icon: 'error',
                    title: "{{ Session::get('error') }}",
                    showConfirmButton: false,
                    timer: 500
                })
            })
        </script>
    @endif
@endsection

@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>

<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js" type="text/javascript"></script>



<!-- third party js ends -->
<!-- Datatables init -->
<script>
