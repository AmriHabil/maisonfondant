@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Vente</a></li>
                        <li class="breadcrumb-item active">Performance</li>
                    </ol>
                </div>
                <h4 class="page-title">Performance des Boutiques</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->





    <!-- end row-->


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3">
                        <form action="{{route('admin.pos.performance.search')}}" method="GET" id="facture">

                            <div class="row">
                                <div class="col-lg-12">

                                    <!-- Portlet card -->
                                    <div class="card">
                                        <div class="card-header bg-info py-3 text-white">
                                            <div class="card-widgets">
                                                <a data-toggle="collapse" href="#cardCollpase7" role="button"
                                                   aria-expanded="false" aria-controls="cardCollpase2"
                                                   class="collapsed"><i class="mdi mdi-minus"></i></a>
                                            </div>
                                            <h5 class="card-title mb-0 text-white">Recherche Avancé</h5>
                                        </div>
                                        <div id="cardCollpase7" class="collapse ">
                                            <div class="card-body">

                                                <div class="row justify-content-center">
                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label>De  </label>
                                                            <input type="date" name="from"
                                                                   class="form-control datepicker"
                                                                   placeholder="D-M-Y "
                                                                   value="{{$request->from??''}}">
                                                        </div>

                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label>A  </label>
                                                            <input type="date" name="to"
                                                                   class="form-control datepicker"
                                                                   placeholder="D-M-Y "
                                                                   value="{{$request->to??''}}">
                                                        </div>

                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group mb-3">
                                                            <label for="Hubs">HUBS</label>
                                                            <select class="select2 form-control" multiple id="Hubs"
                                                                    name="Hubs[]">
                                                                @foreach($Hubs as $Hub)
                                                                    <option value="{{$Hub->id}}">{{$Hub->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <ul class="parsley-errors-list filled" id="Hubs_errors">
                                                            </ul>
                                                        </div>
                                                    </div>


                                                </div>


                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="text-center mb-3">

                                                        <button type="submit"
                                                                class="btn w-sm btn-success waves-effect waves-light"
                                                                id="sa-success">Chercher
                                                        </button>
                                                    </div>
                                                </div> <!-- end col -->

                                            </div>
                                        </div>
                                        <!-- end row -->
                                    </div> <!-- end card-->
                                </div> <!-- end col -->
                            </div>
                            <!-- end row -->
                        </form>
                    </div>
                    <h3>Résumé</h3>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                        <tr>
                            <td >Point de vente</td>
                            <td >Nbr BL</td>
                            <td >Total Vente</td>
                            <td >Total Achat</td>
                            <td >Marge</td>
                            <td >Performance</td>
                        </tr>
                        @foreach($Hubs as $hub)
                        <tr>

                            <td >{{$hub->name}}</td>
                            <td >{{$mus->where('store_id',$hub->id)->count()}}</td>
                            <td >{{\App\Helpers\AppHelper::priceFormat((float)$mus->where('store_id',$hub->id)->sum('total')) }}</td>
                            <td >{{\App\Helpers\AppHelper::priceFormat((float)$mus->where('store_id',$hub->id)->sum('sumBuying')) }}</td>
                            <td >{{\App\Helpers\AppHelper::priceFormat((float)$mus->where('store_id',$hub->id)->sum('total')-(float)$mus->where('store_id',$hub->id)->sum('sumBuying')) }}</td>
                            <td >
                                @if($mus->sum('total')!=0)
                                    <div class="progress mb-2" style="height: 1rem">
                                        <div class="progress-bar progress-bar-striped
                                                @if(  ($mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total')) *100 <64 )
                                                bg-danger
                                                @elseif(($mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total'))*100 >=65 && ($mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total'))*100<80)
                                                bg-info
                                                @else
                                                bg-success
                                                @endif
                                                " role="progressbar"
                                             style="width:{{((float)$mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total'))*100  ?? ''}}%"
                                             aria-valuenow="{{((float)$mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total'))*100  ?? ''}}"
                                             aria-valuemin="0"
                                             aria-valuemax="100">{{ number_format(((float)$mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total'))*100)  ?? ''}}
                                            %
                                        </div>
                                    </div>
                                @endif


                            </td>

                        </tr>
                        @endforeach
                    </table>
                    </div>
                    <div class="table-responsive">
                        <table id="key-datatable" class="  table table-striped dt-responsive nowrap orders_tbl">
                            <thead>
                            <tr>
                                <th>Vente</th>
                                <th>Vendeur</th>
                                <th>Total</th>
                                <th>Achat</th>
                                <th>Marge</th>


                                <th>Date</th>

                            </tr>
                            </thead>

                            <tbody>
                            @foreach($mus as $MU)
                                <tr class="check" id="row{{$MU->id}}">
                                    <td>{{$MU->id}}</td>

                                    <td>{{$MU->admin_name ?? ''}}</td>
                                    <td>{{$MU->total ?? ''}}</td>
                                    <td>{{$MU->sumBuying  ?? ''}}</td>


                                    <td>{{$MU->total - $MU->sumBuying ?? ''}}</td>

                                    <td>
                                        {{$MU->date}}
                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <style>
                        .status .badge {
                            padding: 5px;
                            font-size: 12px;
                            width: 70px;
                        }

                        .check .checkbox label {

                            margin-bottom: 0 !important;
                        }
                    </style>
                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->




@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- third party css end -->
<!-- Plugins css -->
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>

<!-- third party js ends -->
<!-- Datatables init -->
<script>
    $('.orders_tbl').DataTable({
        aLengthMenu: [
            [-1, 10, 25, 50, 100, 200, -1],
            ["All", 10, 25, 50, 100, 200, "All"]
        ],
        dom: 'Blfrtip',
        responsive: false,
        select: true
    });
    $(".datepicker").flatpickr();
    $('.select2').select2();


</script>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

