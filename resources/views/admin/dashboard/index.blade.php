@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Maison Fondant</a></li>
                        <li class="breadcrumb-item active">Accueil</li>
                    </ol>
                </div>
                <h4 class="page-title">Accueil</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div>
                        <img src="{{asset('')}}" alt="" style="display: block;margin-right: auto;margin-left: auto">
                    </div>
                    <div class="row justify-content-center">
                        @can('Liste personnel')
                            <div class="col-md-6 col-xl-3 ">
                                <a href="{{route('admin.staffRequests')}}" class="btn btn-outline-primary waves-effect waves-light w-100 mb-2">
                                    <i class="fa fa-users"></i> <br>
                                    Gestion personnel
                                </a>
                            </div> <!-- end col-->
                        @endcan
                        @can('Ajouter personnel')
                            <div class="col-md-6 col-xl-3">
                                <a href="{{route('admin.staffRequests.create')}}" class="btn btn-outline-blue waves-effect waves-light w-100 mb-2">
                                    <i class="fa fa-users"></i> <br>
                                    Créer une demande
                                </a>
                            </div> <!-- end col-->
                        @endcan
                        @can('Liste commande')
                            <div class="col-md-6 col-xl-3">
                                <a href="{{route('admin.orders')}}" class="btn btn-outline-info waves-effect waves-light w-100 mb-2">
                                    <i class="fas fa-cart-plus"></i> <br>
                                    Suivi des Commandes
                                </a>
                            </div> <!-- end col-->
                        @endcan
                        @can('Ajouter commande')
                                <div class="col-md-6 col-xl-3">
                                <a href="{{route('admin.orders.create')}}" class="btn btn-outline-success waves-effect waves-light w-100 mb-2">
                                    <i class="fas fa-cart-plus"></i> <br>
                                    Créer une Commande
                                </a>
                            </div> <!-- end col-->
                        @endcan
                        @can('Ajouter bon de sortie')
                                <div class="col-md-6 col-xl-3">
                                <a href="{{route('admin.bs.create')}}" class="btn btn-outline-danger waves-effect waves-light w-100 mb-2">
                                    <i class="fa fa-truck-moving"></i> <br>
                                    Créer une Livraison
                                </a>
                            </div> <!-- end col-->
                        @endcan
                        @can('Liste bon de sortie')
                                <div class="col-md-6 col-xl-3">
                                <a href="{{route('admin.bs')}}" class="btn btn-outline-pink waves-effect waves-light w-100 mb-2">
                                    <i class="fa fa-truck-moving"></i> <br>
                                    Suivi des Livraisons
                                </a>
                            </div> <!-- end col-->
                        @endcan
                        @can('Journal bon de sortie')
                            <div class="col-md-6 col-xl-3">
                                <a href="{{route('admin.bs.daily')}}" class="btn btn-outline-secondary waves-effect waves-light w-100 mb-2">
                                    <i class="fa fa-calendar"></i> <br>
                                    Journal
                                </a>
                            </div> <!-- end col-->
                        @endcan
                        @can('Recap bon de sortie')
                                <div class="col-md-6 col-xl-3">
                                <a href="{{route('admin.bs.getRecap')}}" class="btn btn-outline-dark waves-effect waves-light w-100 mb-2">
                                    <i class="fa fa-dollar-sign"></i> <br>
                                    Suivi des Ventes
                                </a>
                                </div> <!-- end col-->
                        @endcan
                    </div>
                    <div class="row justify-content-center">


                        <div class="col-md-6 col-xl-3">
                            <a href="{{route('admin.products',1)}}">
                                <div class="widget-rounded-circle card-box">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="avatar-lg rounded-circle shadow-lg bg-success border-success border">
                                                <i class="mdi mdi-cupcake font-22 avatar-title text-white"></i>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-right">
                                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{\App\Models\Product::where('category_id',1)->count()}}</span></h3>
                                                <p class="text-muted mb-1 text-truncate">Fondants</p>
                                            </div>
                                        </div>
                                    </div> <!-- end row-->
                                </div> <!-- end widget-rounded-circle-->
                            </a>
                        </div> <!-- end col-->



                        <div class="col-md-6 col-xl-3">
                            <a href="{{route('admin.products',2)}}">
                                <div class="widget-rounded-circle card-box">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="avatar-lg rounded-circle shadow-lg bg-blue border-blue border">
                                                <i class="mdi mdi-cupcake font-22 avatar-title text-white"></i>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-right">
                                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{\App\Models\Product::where('category_id',2)->count()}}</span></h3>
                                                <p class="text-muted mb-1 text-truncate">Mini Fondant</p>
                                            </div>
                                        </div>
                                    </div> <!-- end row-->
                                </div> <!-- end widget-rounded-circle-->
                            </a>
                        </div> <!-- end col-->

                        <div class="col-md-6 col-xl-3">
                            <a href="{{route('admin.products',3)}}">
                                <div class="widget-rounded-circle card-box">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="avatar-lg rounded-circle shadow-lg bg-danger border-danger border">
                                                <i class="mdi mdi-cupcake font-22 avatar-title text-white"></i>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-right">
                                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{\App\Models\Product::where('category_id',3)->count()}}</span></h3>
                                                <p class="text-muted mb-1 text-truncate">Fondant Géant</p>
                                            </div>
                                        </div>
                                    </div> <!-- end row-->
                                </div> <!-- end widget-rounded-circle-->
                            </a>
                        </div> <!-- end col-->

                        <div class="col-md-6 col-xl-3">
                            <a href="{{route('admin.products',6)}}">
                                <div class="widget-rounded-circle card-box">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="avatar-lg rounded-circle shadow-lg bg-primary border-primary border">
                                                <i class="mdi mdi-cupcake font-22 avatar-title text-white"></i>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-right">
                                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{\App\Models\Product::where('category_id',6)->count()}}</span></h3>
                                                <p class="text-muted mb-1 text-truncate">Fondor</p>
                                            </div>
                                        </div>
                                    </div> <!-- end row-->
                                </div> <!-- end widget-rounded-circle-->
                            </a>
                        </div> <!-- end col-->


                        <div class="col-md-6 col-xl-3">
                            <a href="{{route('admin.products',4)}}">
                                <div class="widget-rounded-circle card-box">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="avatar-lg rounded-circle shadow-lg bg-secondary border-secondary border">
                                                <i class="mdi mdi-cube font-22 avatar-title text-white"></i>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-right">
                                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{\App\Models\Product::where('category_id',4)->count()}}</span></h3>
                                                <p class="text-muted mb-1 text-truncate">Consommables</p>
                                            </div>
                                        </div>
                                    </div> <!-- end row-->
                                </div> <!-- end widget-rounded-circle-->
                            </a>
                        </div> <!-- end col-->
                        <div class="col-md-6 col-xl-3">
                            <a href="{{route('admin.admins')}}">
                                <div class="widget-rounded-circle card-box">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="avatar-lg rounded-circle shadow-lg bg-warning border-warning border">
                                                <i class="mdi mdi-account-multiple font-22 avatar-title text-white"></i>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-right">
                                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{\App\Models\Admin::count()}}</span></h3>
                                                <p class="text-muted mb-1 text-truncate">Membres</p>
                                            </div>
                                        </div>
                                    </div> <!-- end row-->
                                </div> <!-- end widget-rounded-circle-->
                            </a>
                        </div> <!-- end col-->
                        <div class="col-md-6 col-xl-3">
                            <a href="{{route('admin.stores')}}">
                                <div class="widget-rounded-circle card-box">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="avatar-lg rounded-circle shadow-lg bg-info border-info border">
                                                <i class="mdi mdi-store font-22 avatar-title text-white"></i>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-right">
                                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{\App\Models\Store::count()}}</span></h3>
                                                <p class="text-muted mb-1 text-truncate">Boutiques </p>
                                            </div>
                                        </div>
                                    </div> <!-- end row-->
                                </div> <!-- end widget-rounded-circle-->
                            </a>
                        </div> <!-- end col-->

                    </div>
                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->



@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- Plugins css -->
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/jquery-knob/jquery.knob.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.time.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.selection.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.crosshair.js')}}"></script>

<!-- Dashboar 1 init js-->
<script src="{{URL::asset('assets/js/pages/dashboard-1.init.js')}}"></script>

<script>
    !function (o) {
        "use strict";
        var t = function () {
            this.$body = o("body")
        };
        t.prototype.createCombineGraph = function (t, a, e, i) {
            var r = [{label: e[0], data: i[0], lines: {show: !0, fill: !0}, points: {show: !0}},
                /*{
                label: e[1],
                data: i[1],
                lines: {show: !0},
                points: {show: !0}
                }, {label: e[2], data: i[2], bars: {show: !0, barWidth: .7}}*/
            ], l = {
                series: {shadowSize: 0},
                grid: {hoverable: !0, clickable: !0, tickColor: "#f1556c", borderWidth: 1, borderColor: "#eeeeee"},
                colors: ["#4fc6e1", "#4a81d4", "#1abc9c"],
                tooltip: !0,
                tooltipOpts: {defaultTheme: !1},
                legend: {
                    position: "ne",
                    margin: [0, -32],
                    noColumns: 0,
                    labelBoxBorderColor: null,
                    labelFormatter: function (o, t) {
                        return o + "&nbsp;&nbsp;"
                    },
                    width: 30,
                    height: 2
                },
                yaxis: {axisLabel: "Orders", tickColor: "#f5f5f5", font: {color: "#bdbdbd"}},
                xaxis: {axisLabel: "Daily", ticks: a, tickColor: "#f5f5f5", font: {color: "#bdbdbd"}}
            };
            o.plot(o(t), r, l)
        }, t.prototype.init = function () {
            var o = [
                [

                    [ 1, 10 ],
                ],


            ];
            this.createCombineGraph("#sales-analytics", [

                [ 3, "10-12-2022" ],

            ], ["Pickup", "Email Marketing", "Marketplaces"], o)
        }, o.Dashboard1 = new t, o.Dashboard1.Constructor = t
    }(window.jQuery), function (o) {
        "use strict";
        o.Dashboard1.init()
    }(window.jQuery);
    $(".datepicker").flatpickr({mode: "range"});
</script>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')