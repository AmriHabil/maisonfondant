@section('content')




    <!-- end row-->
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="form-inline">
                        <div class="form-group">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control border-white dash-daterange">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-blue border-blue text-white">
                                        <i class="mdi mdi-calendar-range font-13"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control border-white dash-daterange">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-blue border-blue text-white">
                                        <i class="mdi mdi-calendar-range font-13"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-2">
                            <i class="mdi mdi-autorenew"></i>
                        </a>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-1">
                            <i class="mdi mdi-filter-variant"></i>
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">


        <div class="col-md-6 col-xl-3">
            <a href="{{route('admin.bl')}}">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle shadow-lg bg-success border-success border">
                                <i class="fe-truck font-22 avatar-title text-white"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$bl_number}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Bon de livraison</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </a>
        </div> <!-- end col-->



        <div class="col-md-6 col-xl-3">
            <a href="{{route('admin.bl')}}">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle shadow-lg bg-blue border-blue border">
                                <i class="fe-tag font-22 avatar-title text-white"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$product_number}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Produits</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </a>
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <a href="{{route('admin.orders')}}">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle shadow-lg bg-danger border-danger border">
                                <i class="fe-book-open font-22 avatar-title text-white"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$online_number}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Vente en ligne</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </a>
        </div> <!-- end col-->


        <div class="col-md-6 col-xl-3">
            <a href="{{route('admin.clients')}}">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle shadow-lg bg-secondary bordbg-secondary border">
                                <i class="fe-users font-22 avatar-title text-white"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$clients_number}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Clients</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </a>
        </div> <!-- end col-->
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="card-box">
                <h4 class="header-title mb-3">Evolution Mensuelle</h4>

                <div id="sales-analytics" class="flot-chart mt-4 pt-1" style="height: 280px;"></div>
            </div> <!-- end card-box -->
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="header-title mb-3">Evolution Hebdomadaire</h4>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <p class="text-muted font-15 font-family-secondary mb-0">
                                        @foreach($stores as $store)
                                            @switch ($store->id)
                                             @case(1)
                                            @endswitch
                                        <span class="mx-2"><i class="mdi mdi-checkbox-blank-circle
                                        @switch ($store->id)
                                            @case(1) text-secondary @break
                                            @case(2) text-danger @break
                                            @case(3) text-blue @break
                                            @case(4) text-primary @break
                                        @endswitch
                                                    "></i> {{$store->name}}</span>
                                        @endforeach
                                    </p>
                                </div>
                                <div id="morris-area-with-dotted" style="height: 320px;" class="morris-chart my-3 mb-lg-0"></div>
                            </div> <!-- end col -->

{{--                            <div class="col-lg-4">--}}

{{--                                <div class="row text-center justify-content-center">--}}
{{--                                    @foreach($stores as $store)--}}
{{--                                    <div class="col-6 mt-3">--}}
{{--                                        <h3 class="font-weight-light"> <i class="mdi mdi-cloud-download text-info"></i> {{1- number_format((float)$monthly->where('store_id',$store->id)->sum('total') /(float)$bls->where('store_id',$store->id)->where('month',$now->month-1)->sum('total'),3)}}</h3>--}}
{{--                                        <p class="text-muted text-overflow">Per min user</p>--}}
{{--                                    </div> <!-- end col -->--}}
{{--                                    @endforeach--}}
{{--                                    <div class="col-6 mt-3">--}}
{{--                                        <h3 class="font-weight-light"> <i class="mdi mdi-cloud-upload text-danger"></i> 23%</h3>--}}
{{--                                        <p class="text-muted text-overflow">Bounce Rate</p>--}}
{{--                                    </div> <!-- end col -->--}}
{{--                                </div>--}}
{{--                            </div> <!-- end col -->--}}
                        </div> <!-- end row-->
                    </div>  <!-- end card-box-->
                </div> <!-- end col -->
            </div>
        </div> <!-- end col-->
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                        <a href="javascript: void(0);" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                        <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase5"><i class="mdi mdi-minus"></i></a>
                        <a href="javascript: void(0);" data-toggle="remove"><i class="mdi mdi-close"></i></a>
                    </div>
                    <h4 class="header-title mb-0">Top 25 </h4>

                    <div id="cardCollpase5" class="collapse pt-3 show">
                        <div class="table-responsive">
                            <table class="table table-hover table-centered mb-0">
                                <thead>
                                <tr>
                                    <th>Produit</th>
                                    <th>Prix</th>
                                    <th>Quantité</th>
                                </tr>
                                </thead>
                                <tbody>
                               @foreach($topproducts as $product)
                                <tr>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->detail}}</td>
                                    <td>{{$product->total}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div> <!-- end table responsive-->
                    </div> <!-- collapsed end -->
                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
    <!-- end row-->


@stop


@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- Plugins css -->
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/jquery-knob/jquery.knob.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.time.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.selection.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.crosshair.js')}}"></script>
<script src="{{URL::asset('assets/libs/raphael/raphael.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/morris-js/morris.min.js')}}"></script>


<script>
    !function (o) {
        "use strict";
        var t = function () {
            this.$body = o("body")
        };
        t.prototype.createCombineGraph = function (t, a, e, i) {
            var r = [
                {   label: e[0],
                    data: i[0],
                    lines: {show: !0},
                    points: {show: !0}
                },
                {
                label: e[1],
                data: i[1],
                lines: {show: !0},
                points: {show: !0}
                }, {
                label: e[2],
                data: i[2],
                lines: {show: !0},
                points: {show: !0}
            }], l = {
                series: {shadowSize: 0},
                grid: {hoverable: !0, clickable: !0, tickColor: "#f9f9f9", borderWidth: 1, borderColor: "#eeeeee"},
                colors: ["#e3eaef", "#4a81d4", "#1abc9c"],
                tooltip: !0,
                tooltipOpts: {defaultTheme: !1},
                legend: {
                    position: "ne",
                    margin: [0, -32],
                    noColumns: 0,
                    labelBoxBorderColor: null,
                    labelFormatter: function (o, t) {
                        return o + "&nbsp;&nbsp;"
                    },
                    width: 30,
                    height: 2
                },
                yaxis: {axisLabel: "Point Value (1000)", tickColor: "#f5f5f5", font: {color: "#bdbdbd"}},
                xaxis: {axisLabel: "Daily Hours", ticks: a, tickColor: "#f5f5f5", font: {color: "#bdbdbd"}}
            };
            o.plot(o(t), r,l)
        }, t.prototype.init = function () {
            var o = [
                @foreach ($stores as $store)
                [
                    @for($month = 1; $month <= 12; $month++)
                    [{{$month}}, {{$bls->where('store_id',$store->id)->where('month',$month)->first()->total??0}}],
                    @endfor
                ],
                @endforeach
            ];
            this.createCombineGraph("#sales-analytics", [ [1, "Janvier"], [2, "Fevrier"], [3, "Mars"], [4, "Avril"], [5, "Mai"], [6, "Juin"], [7, "Juillet"], [8, "Aout"], [9, "Septembre"], [10, "Octobre"], [11, "Novembre"], [12, "Decembre"]],
                [
                    @foreach ($stores as $store) "{{$store->name}}",@endforeach],
                o)
        }, o.Dashboard1 = new t, o.Dashboard1.Constructor = t
    }(window.jQuery), function (o) {
        "use strict";
        o.Dashboard1.init()
    }(window.jQuery), $(".dash-daterange").flatpickr({
        altInput: !0,
        altFormat: "F j, Y",
        dateFormat: "Y-m-d",
        defaultDate: "today"
    });
</script>

<script>
    !function (e) {
        "use strict";
        var t = function () {
        };
        t.prototype.createAreaChartDotted = function (e, t, r, i, o, a, c, n, s, y) {
            Morris.Area({
                element: e,
                pointSize: 3,
                lineWidth: 2,
                data: i,
                xkey: o,
                ykeys: a,
                labels: c,
                hideHover: "auto",
                pointFillColors: n,
                pointStrokeColors: s,
                resize: !0,
                behaveLikeLine: !0,
                fillOpacity: .4,
                gridLineColor: "#eef0f2",
                lineColors: y
            })
        }, t.prototype.init = function () {
            this.createAreaChartDotted("morris-area-with-dotted", 0, 0, [
                    @for($day = 1; $day <= 31; $day++)
                {
                    y: "{{$day}}",
                    @foreach($stores as $store)
                    _{{$store->id}}: {{$monthly->where('store_id',$store->id)->where('day',$day)->first()->total ??0}},
                    @endforeach
                },
                @endfor
                ], "y", [@foreach($stores as $store)"_{{$store->id}}",@endforeach ], [@foreach($stores as $store)"{{$store->name}}",@endforeach], ["#ffffff"], ["#999999"], ["#ebeff2", "#f1556c", "#4a81d4"])
        }, e.MorrisCharts = new t, e.MorrisCharts.Constructor = t
    }(window.jQuery), function (e) {
        "use strict";
        e.MorrisCharts.init()
    }(window.jQuery);
</script>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')