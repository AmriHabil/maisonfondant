@section('content')




    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Produits</a></li>
                        <li class="breadcrumb-item active">Création</li>
                    </ol>
                </div>
                <h4 class="page-title">Créer une pièce de vente</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <h1 class="text-center">Vendeur Actuel : <b>{{auth()->user()->name}}</b></h1>
    <div class="row">
        <div class="col-md-12">
            <form action="{{route('admin.bl.store')}}" method="POST" id="cartStore" enctype="multipart/form-data"  target="_blank"  onsubmit="setTimeout(function(){location.reload();}, 1000);return true;">
                <div class="form-group d-none">
                    <br>
                    <div class="radio form-check-inline radio-success">
                        <input type="radio" id="bl" value="bl" name="piece_type" checked>
                        <label for="bl"> Bon de livraison </label>
                    </div>
                    <div class="radio form-check-inline radio-info">
                        <input type="radio" id="devis" value="devis" name="piece_type">
                        <label for="devis"> Devis </label>
                    </div>


                </div>
                @csrf
                <div class="row">

                    <div class="col-lg-12">

                        <div class="card-box ribbon-box">
                            <div class="ribbon-two ribbon-two-info"><span><i class="fa fa-user-cog"></i> Client</span></div>
                            <div class="row justify-content-center">



                                <div class="col-lg-2  col-md-3">
                                    <label for="store_id">Boutique <span class="text-danger">*</span></label>
                                    <select type="text" id="store_id" class="form-control select2"  name="store_id" >
                                        <option value="">Slectionner PTV</option>
                                        @foreach($stores as $store)
                                            <option value="{{$store->id}}" >{{$store->name}}</option>
                                        @endforeach
                                    </select>
                                    <ul class="parsley-errors-list filled" id="store_id_errors">

                                    </ul>
                                </div>
                                <div class="col-lg-2  col-md-3">
                                    <div class="form-group">
                                        <label for="client_id">Client <span class="text-danger">*</span></label>
                                        <select class="form-control select2" id="client_id" name="client_id" required>
                                            <option value="0"
                                                    data-name="Passager"
                                                    data-phone="12345678"
                                                    data-mf="1234567890"
                                                    data-address="Adresse Passager" >
                                                PASSAGER
                                            </option>
                                            @foreach($clients as $client)
                                                <option value="{{$client->id}}"
                                                        data-store="{{$client->store_id}}"
                                                        data-name="{{$client->name}}"
                                                        data-phone="{{$client->phone}}"
                                                        data-mf="{{$client->mf}}"
                                                        data-address="{{$client->address}}"
                                                        data-plafond="{{$client->plafond??'Aucun Plafond'}}"
                                                        data-encours="{{$client->encours??0}}"
                                                        data-remise="{{$client->remise??0}}"
                                                        data-price="{{$client->price??0}}"
                                                >
                                                    {{$client->mf}} - {{$client->name}} - {{$client->phone}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2  col-md-3">
                                    <div class="form-group">
                                        <label for="client_name">Nom <span class="text-danger">*</span></label>
                                        <input type="text" id="client_name" class="form-control" placeholder="e.g : Apple iMac"
                                               name="client_details[client_name]" required >
                                        <input type="hidden" id="remise_client" value="0">
                                    </div>
                                </div>
                                <div class="col-lg-2  col-md-3">
                                    <div class="form-group">
                                        <label for="client_mf">MF <span class="text-danger">*</span></label>
                                        <input type="text" id="client_mf" class="form-control" placeholder="e.g : Apple iMac"
                                               name="client_details[client_mf]" value=" ">
                                    </div>
                                </div>
                                <div class="col-lg-2  col-md-3">
                                    <div class="form-group">
                                        <label for="client_phone">Téléphone <span class="text-danger">*</span></label>
                                        <input type="text" id="client_phone" class="form-control" placeholder="e.g : Apple iMac"
                                               name="client_details[client_phone]" >
                                    </div>
                                </div>
                                <div class="col-lg-2  col-md-3">
                                    <div class="form-group">
                                        <label for="client_address">Adresse <span class="text-danger">*</span></label>
                                        <input type="text" id="client_address" class="form-control" placeholder="e.g : Apple iMac"
                                               name="client_details[client_address]" value=" ">
                                    </div>
                                </div>
                                <div class="col-lg-2  col-md-3">
                                    <div class="form-group">
                                        <label for="date">Date <span class="text-danger">*</span></label>
                                        <input type="date" id="date" class="form-control datepicker" placeholder="e.g : Apple iMac"
                                               name="date" value="{{date('Y-m-d')}}">

                                    </div>
                                </div>

                            </div>
                            <div class="row justify-content-center">


                                <div class="col-lg-2 col-md-4 text-center">
                                    <p><b>En Cours:</b> <span id="encours"></span> TND</p>
                                </div>


                            </div>
                        </div> <!-- end card-box -->

                    </div> <!-- end col -->

                    <div class="col-lg-12">

                        <div class="card-box  ribbon-box">
                            <div class="ribbon-two ribbon-two-success"><span><i class="fa fa-list"></i> Détails</span></div>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th width="450px">Article</th>
                                        <th>Qté</th>
                                        <th>Remise</th>
                                        <th>TVA</th>
                                        <th>P.U</th>
                                        <th>Total</th>
                                        <th width="30px">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody  id="cartItems">

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td>
                                            <select class="form-control select2 productList" id="productList" name="">
                                                <option value=""></option>
                                                @foreach($products as $product)

                                                    <option value="{{$product->id}}"
                                                            data-id="{{$product->id}}"
                                                            data-name="{{strtoupper($product->name)??''}}"
                                                            data-barcode="{{$product->barcode??''}}"
                                                            data-ref="{{$product->ref??''}}"
                                                            data-tva="{{$product->tva??'0'}}"
                                                            data-unity="{{$product->unity??'u'}}"
                                                            data-buying="{{$product->buying??'0'}}"
                                                            data-price="{{$product->price??'0'}}"
                                                            data-price-detail="{{$product->price??'0'}}"
                                                            data-price-gros="{{$product->price??'0'}}"
                                                            data-price-sgros="{{$product->price??'0'}}"
                                                            data-min_price="{{$product->min_price??'0'}}"
                                                            data-bonus="{{$product->bonus??'0'}}"
                                                            data-quantity="{{$product->quantity??'0'}}"

                                                    >
                                                        {{$product->ref}} - {{strtoupper($product->name)}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td><span class="withPopover" data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="La quantité actuelle de cette article est " data-original-title="Raccourci"
                                            ><i class="fa fa-info-circle"></i></span> Articles</td>
                                    </tr>

                                    </tfoot>
                                </table>
                            </div>
                            <audio src="{{asset('assets/sound/success.mp3')}}"></audio>
                            <audio src="{{asset('assets/sound/warning.mp3')}}"></audio>

                        </div> <!-- end col-->

                    </div> <!-- end col-->

                    <div class="col-lg-12" id="payment_section">

                        <div class="card-box  ribbon-box">
                            <div class="ribbon-two ribbon-two-warning"><span><i class="fa fa-dollar-sign"></i> Paiement</span></div>
                            <div class="row">

                                <div class="col-md-8" id="comptant">
                                    <div class="row justify-content-center mb-3">
                                        <button type="button" class="btn btn-success btn-rounded waves-effect waves-light ml-1" id="addCash">
                                            <span class="btn-label"><i class="fa fa-dollar-sign"></i></span>Espèce
                                        </button>
                                        <button type="button" class="btn btn-info btn-rounded waves-effect waves-light ml-1" id="addCheck">
                                            <span class="btn-label"><i class="fas fa-money-check-alt"></i></span>Chèque
                                        </button>
                                        <button type="button" class="btn btn-danger btn-rounded waves-effect waves-light ml-1" id="addTransfer">
                                            <span class="btn-label"><i class="fab fa-cc-visa"></i></span>TPE
                                        </button>
                                        <button type="button" class="btn btn-warning btn-rounded waves-effect waves-light ml-1" id="addSodexo">
                                            <span class="btn-label"><i class="fa  fa-tags"></i></span>Sodexo
                                        </button>
                                    </div>
                                    <div class="row justify-content-center" id="paymentMethod">

                                    </div>
                                </div>
                                <div class="col-md-8 " id="commitment">
                                    <div id="echeance">

                                    </div>


                                    <div class="col-12 text-center">
                                        <a class="btn btn-sm btn-info text-white" id="addCommitment">Ajouter une échéance</a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="">
                                        <h5>Résumé</h5>
                                        <p><b>TVA:</b> <span class="float-right" id="totalTva">0.000</span></p>
                                        <p><b>Remise :</b> <span class="float-right" id="totalRemise"> &nbsp;&nbsp;&nbsp; 0.000</span></p>
                                        <hr>
                                        <p class="font-24">A Payer<span class="float-right" id="totalGlobal">0.000</span></p>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group mb-3">
                                        <label class="mb-2">Mode de réglement <span class="text-danger">*</span></label>
                                        <br>
                                        <div class="radio form-check-inline radio-success">
                                            <input type="radio" id="Comptant" value="comptant" name="commitmentType" checked="">
                                            <label for="Comptant"> Comptant </label>
                                        </div>

                                        <div class="radio form-check-inline radio-info">
                                            <input type="radio" id="Echeance" value="echeance" name="commitmentType">
                                            <label for="Echeance"> Echeance </label>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div> <!-- end col-->


                    </div> <!-- end col-->

                </div>
                <!-- end row -->


                <div class="row">
                    <div class="col-12">
                        <div class="text-center mb-3">


                            <button type="submit" class="btn w-sm btn-success waves-effect waves-light submit" id="sa-a4" value="a4" name="submit" >Imprimer A4
                            </button>
                            <button type="submit" class="btn w-sm btn-warning waves-effect waves-light submit" id="sa-ticket" value="ticket" name="submit">Imprimer Ticket
                            </button>
                        </div>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </form>

        </div>

    </div>



    <style>
        .select2 {
            width: 100%!important;
        }
        .popover-title{
            background: #ffff99;
        }
    </style>

@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<link href="{{asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/css/general.css')}}" rel="stylesheet" type="text/css"/>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<!-- Loading buttons js -->
<script src="{{asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{asset('assets/libs/ladda/ladda.js')}}"></script>
<script src="{{asset('assets/js/pages/loading-btn.init.js')}}"></script>

<script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('assets/js/pages/add-product.init.js')}}"></script>

{{--<script src="{{asset('assets/js/validation.js')}}"></script>--}}

<script>





    $('.withPopover').popover({offset: 10});
    var productListAfterSelect2 = $("#productList").select2();
    $(window).on('load', function() {
        productListAfterSelect2.select2("open");
    });
    $(".datepicker").flatpickr();

    $(document).on('change', '#productList', function () {
        let priceType=$('#client_price').text();
        let product = {

            id:this.options[this.selectedIndex].getAttribute('data-id'),
            name:this.options[this.selectedIndex].getAttribute('data-name'),
            ref:this.options[this.selectedIndex].getAttribute('data-ref'),
            unity:this.options[this.selectedIndex].getAttribute('data-unity'),
            tva:this.options[this.selectedIndex].getAttribute('data-tva'),
            buying:this.options[this.selectedIndex].getAttribute('data-buying'),
            price:this.options[this.selectedIndex].getAttribute('data-price'),
            facturation:this.options[this.selectedIndex].getAttribute('data-price-facturation'),
            min_price:this.options[this.selectedIndex].getAttribute('data-min_price'),
            bonus:this.options[this.selectedIndex].getAttribute('data-bonus'),
            quantity:this.options[this.selectedIndex].getAttribute('data-quantity'),

        };
        addProduct(product);
    });
    $(document).on('click', '.removeRow', function () {
        let tr = $(this).attr("data-tr");
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $('#'+tr).remove();
                calculate_gtotal();
            }
        });

    });






    $(document).on('change', '.quantity,.remise,.price_u', function() {
        calculate_gtotal();
    });
    $(document).on('change', '.quantity', function() {
        checkQuantity(this);

    });

    $(document).on('change', '.price_u', function() {
        let min=parseFloat($(this).attr('data-min'));
        let id=parseFloat($(this).attr('data-product_id'));
        if(parseFloat($(this).val())<min){
            $.toast({
                heading: 'Dépassage du remise maximale',
                text: 'Le prix d\'achat actuel est '+min,
                icon: 'error',
                loader: true,
                position:'top-right',// Change it to false to disable loader
                loaderBg: '#f1556c',  // To change the background
                bgColor: '#f1556c',  // To change the background
            });
            //$(this).val(max);
            $(this).addClass('alert-danger');
        }else{
            $(this).removeClass('alert-danger');
        }
        $(this).val(parseFloat($(this).val()).toFixed(3));
    });

    function addProduct(product) {
        let priceType=$('#client_price').text();

        let client_remise=$('#remise_client').val();
        if($('#quantity_id_'+product.id).length){
            $('#quantity_id_'+product.id).val(parseFloat($('#quantity_id_'+product.id).val())+1);
            checkQuantity($('#quantity_id_'+product.id));
            playSound();
        }
        else{
            let alert='';
            if (product.quantity==0) alert='alert-danger';
            tr=`<tr id="row${product.id}" class="item_cart"  data-id="${product.id}">
                   <td>
                        ${product.name}
                        <input type="hidden" name="details[${product.id}][product_id]" value="${product.id}">
                        <input type="hidden" name="details[${product.id}][product_ref]" value="${product.ref}">
                        <input type="hidden" name="details[${product.id}][product_bonus]" value="${product.bonus}">
                        <input type="hidden" name="details[${product.id}][product_facturation]" value="${product.facturation}">
                       <input type="hidden" id="name_id_${product.id}" name="details[${product.id}][product_name]" class="form-control name" readonly value="${product.name}" style="width: 430px" required/>
                    </td>
                    <td>
                       <input type="number" id="quantity_id_${product.id}" name="details[${product.id}][product_quantity]" class="form-control ${alert} quantity withPopover min-w-60" data-product_id="${product.id??0}" value="1" step="1" min="1"
                        oninput="this.value = Math.floor(this.value);" data-max="${product.quantity??0}"
                        data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="La quantité actuelle de cette article est : ${product.quantity}" data-original-title="Information du stock" required
                        >
                    </td>
                    <td>
                       <input type="number" id="remise_id_${product.id}" name="details[${product.id}][product_remise]" class="form-control remise  min-w-60" data-product_id="${product.id}" value="0" min="0"  max="100" step="0.01" required>
                    </td>
                    <td>
                       <input type="text" id="tva_id_${product.id}" name="details[${product.id}][product_tva]" class="form-control tva  min-w-60" data-product_id="${product.id}" value="${product.tva??0}" readonly required>
                    </td>
                   <td>
                       <input type="text" id="price_id_${product.id}" name="details[${product.id}][product_price_selling]" class="form-control price price_u withPopover number_format_3  min-w-60" data-product_id="${product.id}" data-product_grade="" value="${product.price??0}" data-min="${product.min_price}" min="${product.min_price}"
                        data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="Le prix limite de cet article est : ${product.min_price}" data-original-title="Prix de revient" required readonly>
                        <input type="hidden" name="details[${product.id}][product_price_buying]" value="${product.buying}">
                    </td>
                    <td>
                       <input type="text" id="total_id_${product.id}" name="details[${product.id}][product_total]"  class="form-control price  min-w-60"  data-product_id="${product.id}" value="${product.price??0}" readonly required  min="${product.min_price}">
                    </td>
                    <td>
                        <a data-tr="row${product.id}" class="text-danger removeRow"><i class="fa fa-minus-circle"></i></a>
                   </td>
                </tr>`;
            $('#cartItems').append(tr);
            $('.withPopover').popover({offset: 10});
            playSound();
        }
        calculate_gtotal();
        // openAndFocus('productList');

    }
    function playSound() {
        $('audio')[0].load();
        $('audio')[0].play();
    }



    function checkQuantity(element){
        let max=parseFloat($(element).attr('data-max'));
        let id=parseFloat($(element).attr('data-product_id'));
        if(parseFloat($(element).val())>max){
            $.toast({
                heading: 'Dépassage du stock',
                text: 'La quantité actuelle est '+max,
                icon: 'error',
                loader: true,
                position:'top-right',// Change it to false to disable loader
                loaderBg: '#f1556c',  // To change the background
                bgColor: '#f1556c',  // To change the background
            });
            //$(this).val(max);
            $(element).addClass('alert-danger');
        }else{
            $(element).removeClass('alert-danger');
        }
    }

    function calculate_gtotal() {
        $('#cartItems').each(function () {
            let totalTva = 0;
            let totalRemise = 0;
            let totalGlobal = 0;
            let totalPerLine = 0;
            $(this).find('.quantity').each(function () {

                let product_id = $(this).attr('data-product_id');
                let price = parseFloat($('#price_id_' + product_id).val()??0);
                let remise = parseFloat($('#remise_id_' + product_id).val()??0);
                let tva = parseFloat($('#tva_id_' + product_id).val()??0);

                totalRemise += parseFloat((price *  remise) / 100)*parseFloat($(this).val());

                // totalRemise += (remise*parseFloat($(this).val()));

                totalTva += parseFloat($(this).val())*parseFloat(price*(1-(100/(100+tva))));
                totalPerLine = parseFloat($(this).val()) * parseFloat(price - parseFloat((price *  remise) / 100)*parseFloat($(this).val()));
                totalGlobal += totalPerLine;
                $('#total_id_' + product_id).attr('value', totalPerLine.toFixed(3));
            });
            $('#totalGlobal').html((parseFloat(totalGlobal)-parseFloat($('#remiseFact').val()??0)).toFixed(3));
            $('#totalTva').html(totalTva.toFixed(3));
            $('#totalRemise').html(totalRemise.toFixed(3));


        });
    }



    function openAndFocus(element){
        $('#'+element).val('');
        $('#'+element).select2().select2('open');
        $('.select2-search__field').last().focus();
    }

    /*Adding addCheck*/
    $(document).on('click', '#addCheck', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addCheck').length+1;
        let listOfBanks = `
        @foreach(\App\Helpers\WebHelper::allBanks() as $bank)
            <option value="{{$bank->name}}">{{$bank->name}}</option>
         @endforeach
        `;
        let add=`
                                <div class="card addCheck col-12">
                                    <div class="card-header bg-info  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addCheck${count}" role="button" aria-expanded="false" aria-controls="addCheck${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCheck  removePayment" data-type="addCheck"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- Chèque</h5>
                                    </div>
                                    <div id="addCheck${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="number" id="name" class="form-control amount" placeholder="e.g :  1 350 TND"
                                                               name="payments[check][${count}][amount]" required step="0.001">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="date" class="form-control" placeholder="e.g : Apple iMac"
                                                               name="payments[check][${count}][date]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Numéro <span class="text-danger">*</span></label>
                                                        <input type="text" id="name" class="form-control" placeholder="e.g : 1234567"
                                                               name="payments[check][${count}][number]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Banque <span class="text-danger">*</span></label>
                                                        <select class="form-control select2" id="select_bank${count}" name="payments[check][${count}][bank]">
                                                            ${listOfBanks}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


        `;
        $('#paymentMethod').append(add);
        $('#select_bank'+count).select2();
    });

    /*Adding addCheck*/

    /*Adding addCash*/
    $(document).on('click', '#addCash', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addCash').length+1;
        let date = '{{date('Y-m-d')}}';
        let add=`<div class="card addCash  col-12">
                                    <div class="card-header bg-success  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addCash${count}" role="button" aria-expanded="false" aria-controls="addCash${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCash removePayment" data-type="addCash"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- Espèce</h5>
                                    </div>
                                    <div id="addCash${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="number"  class="form-control amount" placeholder="e.g : 1 350 TND"
                                                               name="payments[cash][${count}][amount]" value="${total.toFixed(3)}" step="0.001">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="name" class="form-control"
                                                               name="payments[cash][${count}][date]" value="${date}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

        `;
        $('#paymentMethod').append(add);
    });

    /*Adding addCash*/

    /*Adding addTransfer*/
    $(document).on('click', '#addTransfer', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addTransfer').length+1;
        let date = '{{date('Y-m-d')}}';
        let add=`<div class="card addTransfer  col-12">
                                    <div class="card-header bg-danger  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addTransfer${count}" role="button" aria-expanded="false" aria-controls="addTransfer${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCash removePayment" data-type="addTransfer"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- TPE</h5>
                                    </div>
                                    <div id="addTransfer${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="number"  class="form-control amount" placeholder="e.g : 1 350 TND"
                                                               name="payments[transfer][${count}][amount]" value="${total.toFixed(3)}" step="0.001">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="number">Numéro <span class="text-danger">*</span></label>
                                                        <input type="text" id="number" class="form-control" placeholder="e.g : 123467"
                                                               name="payments[transfer][${count}][number]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="name" class="form-control"
                                                               name="payments[transfer][${count}][date]" value="${date}" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

        `;
        $('#paymentMethod').append(add);
    });

    /*Adding addTransfer*/

    /*Adding addTransfer*/
    $(document).on('click', '#addSodexo', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addSodexo').length+1;
        let date = '{{date('Y-m-d')}}';
        let add=`<div class="card addSodexo  col-12">
                                    <div class="card-header bg-warning  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addSodexo${count}" role="button" aria-expanded="false" aria-controls="addSodexo${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCash removePayment" data-type="addSodexo"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- Sodexo</h5>
                                    </div>
                                    <div id="addSodexo${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="number"  class="form-control amount" placeholder="e.g : 1 350 TND"
                                                               name="payments[sodexo][${count}][amount]" value="${total.toFixed(3)}" onblur="this.value=(this.value*0.9).toFixed(3);" step="0.001">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Numéro <span class="text-danger">*</span></label>
                                                        <input type="text" id="name" class="form-control" placeholder="e.g : Apple iMac"
                                                               name="payments[sodexo][${count}][number]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="name" class="form-control"
                                                               name="payments[sodexo][${count}][date]" value="${date}" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

        `;
        $('#paymentMethod').append(add);
    });

    /*Adding Sodexo*/
    /*removePayment*/
    $(document).on('click','.removePayment',function () {
        let type=$(this).attr('data-type');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $('.'+type).last().remove();
            }
        });
    });
    /*removePayment*/


    /*FetchClientInformation*/
    $(document).on('change', '#client_id', function(){

        $('#client_name').val(this.options[this.selectedIndex].getAttribute('data-name'));
        $('#client_price').text(this.options[this.selectedIndex].getAttribute('data-price'));
        $('#client_mf').val(this.options[this.selectedIndex].getAttribute('data-mf'));
        $('#client_address').val(this.options[this.selectedIndex].getAttribute('data-address'));
        $('#client_phone').val(this.options[this.selectedIndex].getAttribute('data-phone'));
        $('#encours').text(this.options[this.selectedIndex].getAttribute('data-encours'));
        $('#plafond').text(this.options[this.selectedIndex].getAttribute('data-plafond'));
        //$('.remise').val(this.options[this.selectedIndex].getAttribute('data-remise'));
        $('#remise_client').val(this.options[this.selectedIndex].getAttribute('data-remise'));
        calculate_gtotal();
    });
    $(document).on('click', '.addClient', function(){

        $('#client_id').val(this.getAttribute('data-id'));
        $('#client_name').val(this.getAttribute('data-name'));
        $('#client_price').text(this.getAttribute('data-price'));
        $('#client_mf').val(this.getAttribute('data-mf'));
        $('#client_address').val(this.getAttribute('data-address'));
        $('#client_phone').val(this.getAttribute('data-phone'));
        // $('#encours').text(this.getAttribute('data-encours'));
        // $('#plafond').text(this.getAttribute('data-plafond'));
        // $('#remise_client').val(this.getAttribute('data-remise'));
        $('#remise_client').val(0);

    });
    /*FetchClientInformation*/

    /*Add Commitment*/
    $(document).on('click', '#addCommitment', function(){
        let count=$('.echeance').length+1;
        let add=` <div class="card echeance col-12">
                                <div class="card-header bg-info  text-white">
                                    <div class="card-widgets">
                                        <span data-toggle="collapse" href="#echeance${count}" role="button" aria-expanded="false" aria-controls="echeance${count}"><i class="mdi mdi-minus"></i></span>
                                        <span  class="removeCheck  removePayment" data-type="echeance"><i class="mdi mdi-close"></i></span>
                                    </div>
                                    <h5 class="card-title mb-0 text-white">${count} - Echéance</h5>
                                </div>
                                <div class="collapse show">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Montant <span class="text-danger">*</span></label>
                                                    <input type="text" id="commitmentAmount${count}" class="form-control commitmentAmount amount" placeholder="e.g : Apple iMac"
                                                           name="echeance[${count}][amount]" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Date <span class="text-danger">*</span></label>
                                                    <input type="date" id="commitmentDate${count}" class="form-control commitmentDate"  placeholder="e.g : Apple iMac"
                                                           name="echeance[${count}][date]" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
        $('#echeance').append(add);
        setCommitment();


    });
    function setCommitment(){
        let count=$('.echeance').length;
        let total=parseFloat($('#totalGlobal').text());
        let rest=total%count;
        let echeance=Math.floor(total / count);
        let element;
        for(let i=0;i<count;i++){
            if(i==0) document.getElementsByClassName('commitmentAmount')[i].value=((rest+echeance).toFixed(3));
            else document.getElementsByClassName('commitmentAmount')[i].value=echeance.toFixed(3);
            let now = new Date();
            let echance = new Date(now.setMonth(now.getMonth() + i));
            element=document.getElementsByClassName('commitmentDate')[i];
            var day = ("0" + echance.getDate()).slice(-2);
            var month = ("0" + (echance.getMonth() + 1)).slice(-2);
            var today = echance.getFullYear()+"-"+(month)+"-"+(day) ;
            //element.flatpickr();
            element.value=today;

        }
    }
    /*Add Commitment*/
    $('#commitment').hide();
    $('#comptant').show();
    $(document).on('change', 'input[type=radio][name=commitmentType]', function(){
        if (this.value == 'echeance') {
            $('#commitment').show();
            $('#comptant').hide();
            $('#paymentMethod').html('');
        }
        else if (this.value == 'comptant') {
            $('#commitment').hide();
            $('#comptant').show();
            $('#echeance').html('');
        }
    });
    $(document).on('change', 'input[type=radio][name=piece_type]', function(){
        if (this.value == 'bl') {
            $('#payment_section').show();
        }
        else if (this.value == 'devis') {
            $('#payment_section').hide();
        }
    });
    var $currentFocus=$(':focus');

    $(document).on('click','.submit',function (e) {
        if ($('input[type=radio][name=piece_type]:checked').val()=='bl'){

            let total_amounts=0;
            $('.amount').each(function(i, obj) {
                total_amounts+=parseFloat($(obj).val());
            });
            // if(total_amounts>parseFloat($('#totalGlobal').html()) || total_amounts!=parseFloat($('#totalGlobal').html())){
            if(total_amounts!=parseFloat($('#totalGlobal').html())){
                e.preventDefault();
                swal({
                    title: 'Total incorrect!',
                    text: "Veuillez corriger les montant saisies!",
                    type: 'error'
                });
                $('.amount').addClass('is-invalid');
            }
        }
    });
</script>
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js" type="text/javascript"></script>
<script>

    $('#key-datatable').DataTable({

        processing: true,
        serverSide: true,

        ajax: "{{ route('admin.products.getData','bl') }}",
        columns: [

            { data: 'checkbox', name: 'checkbox' },
            { data: 'name', name: 'name' },
            { data: 'barcode', name: 'barcode' },
            { data: 'detail', name: 'detail' },
            { data: 'action', name: 'action' },
        ],
        order: [[0, 'asc']],



        aLengthMenu: [
            [10, 25, 50, 100, 200 ,-1],
            [10, 25, 50, 100, 200,"All"]
        ],

        dom: 'lfrtip',
        responsive:false,
        "search": {
            "regex": true
        },

    });


    $(document).on('click','#addThis', function() {
        $("input:checkbox[name=product_id]:checked").each(function(){
            $('#addToCart'+$(this).val()).click();
        });
    });

    $(document).on('click','.addToCart',function() {
        let priceType=$('#client_price').text();
        let product = {

            id:this.getAttribute('data-id'),
            name:this.getAttribute('data-name'),
            unity:this.getAttribute('data-unity'),
            tva:this.getAttribute('data-tva'),
            buying:this.getAttribute('data-buying'),
            price:this.getAttribute('data-price-'+priceType),
            min_price:this.getAttribute('data-min_price'),
            bonus:this.getAttribute('data-bonus'),
            quantity:this.getAttribute('data-quantity'),

        };
        addProduct(product);
    });

    $(document).ready(function() {
        // Initialize select2 if not already initialized
        $('#client_id').select2();

        // Function to update client_id based on store_id
        $('#store_id').on('change', function() {
            var storeId = $(this).val(); // Get the selected store_id

            // Find the first client option that matches the selected store_id
            var matchingClientOption = $('#client_id option').filter(function() {
                return $(this).data('store') == storeId;
            }).first(); // Get the first matching option

            // If no matching client found, select "Passager"
            if (matchingClientOption.length > 0) {
                $('#client_id').val(matchingClientOption.val()).trigger('change.select2');
            } else {
                // If no match, select "Passager"
                $('#client_id').val(0).trigger('change.select2');
            }
            $('#client_id').trigger('change');
        });
    });
</script>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
