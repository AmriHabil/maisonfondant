@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">MU</a></li>
                        <li class="breadcrumb-item active">Journalier</li>
                    </ol>
                </div>
                <h4 class="page-title">Journalier</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->





    <!-- end row-->


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
{{--                    <h3>Résumé</h3>--}}
{{--                    <div class="table-responsive">--}}
{{--                        <table class="table table-striped table-bordered">--}}
{{--                        <tr>--}}
{{--                            <td >Point de vente</td>--}}
{{--                            <td >Nbr BL</td>--}}
{{--                            <td >Total Vente</td>--}}
{{--                            <td >Total Achat</td>--}}
{{--                            <td >Marge</td>--}}
{{--                            <td >Performance</td>--}}
{{--                        </tr>--}}
{{--                        @foreach($Hubs as $hub)--}}
{{--                        <tr>--}}

{{--                            <td >{{$hub->name}}</td>--}}
{{--                            <td >{{$mus->where('store_id',$hub->id)->count()}}</td>--}}
{{--                            <td >{{\App\Helpers\AppHelper::priceFormat((float)$mus->where('store_id',$hub->id)->sum('total')) }}</td>--}}
{{--                            <td >{{\App\Helpers\AppHelper::priceFormat((float)$mus->where('store_id',$hub->id)->sum('sumBuying')) }}</td>--}}
{{--                            <td >{{\App\Helpers\AppHelper::priceFormat((float)$mus->where('store_id',$hub->id)->sum('total')-(float)$mus->where('store_id',$hub->id)->sum('sumBuying')) }}</td>--}}
{{--                            <td >--}}
{{--                                @if($mus->sum('total')!=0)--}}
{{--                                    <div class="progress mb-2" style="height: 1rem">--}}
{{--                                        <div class="progress-bar progress-bar-striped--}}
{{--                                                @if(  ($mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total')) *100 <64 )--}}
{{--                                                bg-danger--}}
{{--                                                @elseif(($mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total'))*100 >=65 && ($mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total'))*100<80)--}}
{{--                                                bg-info--}}
{{--                                                @else--}}
{{--                                                bg-success--}}
{{--                                                @endif--}}
{{--                                                " role="progressbar"--}}
{{--                                             style="width:{{((float)$mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total'))*100  ?? ''}}%"--}}
{{--                                             aria-valuenow="{{((float)$mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total'))*100  ?? ''}}"--}}
{{--                                             aria-valuemin="0"--}}
{{--                                             aria-valuemax="100">{{ number_format(((float)$mus->where('store_id',$hub->id)->sum('total')/(float)$mus->sum('total'))*100)  ?? ''}}--}}
{{--                                            %--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endif--}}


{{--                            </td>--}}

{{--                        </tr>--}}
{{--                        @endforeach--}}
{{--                    </table>--}}
{{--                    </div>--}}
                    <div id="example_wrapper mb-4" class="dataTables_wrapper">
                        <div class="row ">
                            <div class="col-md-3">
                                <label>Du</label>
                                <input autocomplete="off"  required type="date" class="form-control  filter-field"
                                       name="from" placeholder="De" id="from" >
                            </div>
                            <div class="col-md-3">
                                <label>Au</label>
                                <input autocomplete="off"  required type="date" class="form-control  filter-field"
                                       name="to" placeholder="Au" id="to">
                            </div>

                            <div class="col-md-3 d-none">
                                <label>Boutique</label>
                                <select class="form-control select2 filter-field" name="store_id" id="store_id">
                                    @foreach($stores as $store)
                                        <option value="{{$store->id}}">{{$store->name}}</option>
                                    @endforeach
                                </select>

                            </div>

                            <div class="col-md-3 d-none">
                                <label>Catégorie</label>
                                <select class="form-control select2 filter-field" name="category[]" id="category" multiple>
                                    <option value="">Tous</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="col-md-3 ">
                                <label>Brands</label>
                                <select class="form-control select2 filter-field" name="brand_id" id="brand_id" >
                                    <option value="">Tous</option>
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="col-md-3 ">
                                <label>Boutiques</label>
                                <select class="form-control select2 filter-field" name="store_id" id="store_id" >
                                    <option value="">Tous</option>
                                    @foreach($stores as $store)
                                        <option value="{{$store->id}}">{{$store->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="col-md-3 ">
                                <label>Produit</label>
                                <input type="text" class="form-control filter-field" name="group_name" id="group_name">

                            </div>

                        </div>
                        @can('Total bon de sortie')<h1 class="text-center text-success">Total : <span  id="gTotal"></span></h1>@endcan
                    </div>
                    <div class="table-responsive">
                        <table id="key-datatable" class="  table table-striped dt-responsive nowrap orders_tbl">
                            <thead>
                            <tr>
                                <th>Produit</th>
                                <th>Quantity</th>
                                <th>BL</th>
                                <th>Online</th>




                            </tr>
                            </thead>

                            <tbody>
                            </tbody>
                            @can('Total bon de sortie')<tfoot class="show-footer-above">
                            <tr>
                                <th colspan="6" style="text-align:right">Total:</th>
                                <th></th>
                            </tr>
                            </tfoot>@endcan
                        </table>
                    </div>
                    <style>
                        .status .badge {
                            padding: 5px;
                            font-size: 12px;
                            width: 70px;
                        }

                        .check .checkbox label {

                            margin-bottom: 0 !important;
                        }
                    </style>
                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->




@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- third party css end -->
<!-- Plugins css -->
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
    .show-footer-above{
        display: table-row-group;
    }


</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/plug-ins/1.13.4/api/sum().js"></script>

<!-- third party js ends -->
<!-- Datatables init -->
<script>

    var table = $('#key-datatable').DataTable({

        processing: true,
        serverSide: true,

        ajax: "{{ route('admin.bl.getRoulement') }}",
        columns: [
            { data: 'product_name', name: 'products.group_name' , orderable: false,
                paging: false,
                searchable: false,
                bSearchable: false
                },
            { data: 'total', name: 'total', orderable: false,
                paging: false,
                searchable: false,
                bSearchable: false },
            { data: 'total_bl_pieces_sold', name: 'total_bl_pieces_sold', orderable: false,
                paging: false,
                searchable: false,
                bSearchable: false },
            { data: 'total_order_pieces_sold', name: 'total_order_pieces_sold', orderable: false,
                paging: false,
                searchable: false,
                bSearchable: false },



        ],
        order: [[1, 'asc']],



        aLengthMenu: [
            [10, 25, 50, 100, 200, -1],
            [10, 25, 50, 100, 200, "All"]
        ],

        dom: 'Blrtip',
        responsive:false,
        initComplete: function () {
            // Apply the search
            this.api()
                .columns()
                .every(function () {
                    var that = this;
                    $('input,select', this.footer()).on('keyup change clear', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
        },

    });
    function filter() {
        var queryString = '?' + new URLSearchParams(filter).toString();
        var url = @json(route('admin.bl.getRoulement'));
        table.ajax.url(url + queryString).load();
        table.draw();
    }

    $(document).on('change', '.filter-field', function(e) {
        var key = $(this).attr('id');
        var value = $(this).val();
        filter[key] = value;
        filter();
    });





</script>
<script>
    $(".datepicker").flatpickr();
    $('.select2').select2();
</script>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

