@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.roles')}}">Role</a></li>
                        <li class="breadcrumb-item active">Création</li>
                    </ol>
                </div>
                <h4 class="page-title">Créer un role</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <form action="{{route('admin.roles.store')}}" method="POST" id="product_store">
        @csrf
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <h5 class="text-uppercase bg-light p-2 mt-0 mb-3">Général</h5>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group mb-3">
                                <label for="name">Nom <span class="text-danger">*</span></label>
                                <input type="text" id="name" class="form-control" placeholder="e.g : Nom " name="name">
                                <ul class="parsley-errors-list filled" id="name_errors">
                                </ul>
                            </div>
                        </div>
                    </div>
                    @foreach ($permissions as $group_name => $groups)

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-check form-switch form-switch-md mb-3">

                                    <h5 >{{ ucfirst($group_name) }}</h5>
                                </div>
                                <div class="row">
                                    @foreach ($groups as $key => $permission)
                                    <div class="form-check form-switch mb-3 col-lg-2" >
                                        <input type="checkbox" class="js-switch" id="permissions-{{ $group_name.$loop->index . $key}}"
                                               name="permissions[{{$group_name.$loop->index . $key }}]"
                                               value="{{ $permission->id }}">
                                        <label class="form-check-label" for="permissions-{{ $group_name.$loop->index . $key}}">  {{ str_replace($group_name, '', $permission->name) }}</label>
                                    </div>
                                    @endforeach
                                </div>
                                <hr>
                            </div>

                        </div>
                        <!-- end row -->
                    @endforeach
                </div> <!-- end card-box -->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-12">
                <div class=" mb-3">
                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light" id="sa-success">{{__('Enregistrer')}}</button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>
@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')
@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<link href="{{asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/css/general.css')}}" rel="stylesheet" type="text/css" />
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- Loading buttons js -->
<script src="{{asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{asset('assets/libs/ladda/ladda.js')}}"></script>
<script src="{{asset('assets/js/pages/loading-btn.init.js')}}"></script>

<script src="{{asset('assets/libs/switchery/switchery.min.js')}}"></script>
<script>
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });
    $("#product_store").submit(function (e) {
        e.preventDefault();
        var Form=$("#product_store")[0];
        var formData= new FormData(Form);
        $.ajax({
            type: 'POST',
            url:"{{route('admin.roles.store')}}",
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success:function(data)
            {
                $('.is-invalid').removeClass('is-invalid');
                Form.reset();
                $('#preloader').show();
                $.toast({
                    heading: 'Succès',
                    text: 'Rôle créé avec succés',
                    icon: 'success',
                    loader: true,
                    position:'top-right',// Change it to false to disable loader
                    loaderBg: '#5ba035',  // To change the background
                    bgColor: '#1abc9c',  // To change the background
                });

            }, error: function (reject) {
                $('.is-invalid').removeClass('is-invalid');
                swal({
                    title: "Erreur!",
                    text: "Merci de suivre les indications",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });
                var response=$.parseJSON(reject.responseText);
                $.each(response.errors,function(key,val){
                    $("#" + key ).addClass('is-invalid');
                    $("#" + key + "_errors").html('<li class="parsley-required">'+val+'</li>');

                });
            }
        });

    });
</script>
<style>
    input.chk-btn {
        display: none;
    }
    .label-chk-btn{
        width: 60px;
    }
    .label-chk-btn img{
        filter: grayscale(100%);
        height: 60px;
        transition: ease .3s;
        width: 45px;
        margin: auto;
        display: block;
    }
    input.chk-btn:checked + label img{
        transform: scale(1.1);
        filter: grayscale(0);
    }
    .is-invalid{
        border-color: #dc3545;
        padding-right: calc(1.5em + .75rem);
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-position: right calc(.375em + .1875rem) center;
        background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
