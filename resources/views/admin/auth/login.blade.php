<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Maison Fondant</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Bienvenus chez MAISON FONDANT" name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{URL::asset('assets/images/favicon.ico')}}">

    <!-- App css -->
    <link href="{{URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

</head>

<body class="authentication-bg">

<div class="account-pages mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card bg-pattern">

                    <div class="card-body p-4">

                        <div class="text-center w-75 m-auto">
                            <a href="{{url(('/'))}}">
                                <span><img src="{{asset('assets/images/logo-dark.png')}}" class="w-100"></span>
                            </a>
                            <h5>Managers Portal</h5>
                            <p class="text-muted mb-4 mt-3">Enter your email address and password to access admin panel.</p>
                        </div>
                        @if(Auth::check())
                            enter code here
                        @endif
                        <form method="POST" action="{{ route('admin_login') }}">
                            @csrf

                            <div class="form-group mb-3">
                                <label for="emailaddress">Email ou Numéro</label>
                                <input class="form-control" type="text" id="emailaddress" required="" placeholder="Enter your email" name="email" autocomplete="off">
                            </div>

                            <div class="form-group mb-3">
                                <label for="password">Password</label>
                                <input class="form-control" type="Password" required="" id="password" placeholder="Enter your password" name="password">
                            </div>

                            <div class="form-group mb-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="checkbox-signin" checked>
                                    <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                                </div>
                            </div>

                            <div class="form-group mb-0 text-center">
                                <button class="btn btn-primary btn-block" type="submit"> Log In </button>
                            </div>

                        </form>



                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->


                <!-- end row -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end page -->


<footer class="footer footer-alt">
    2021 - 2022 &copy; Celerity by <a href="https://badhrah.com" target="_blank">Badhrah</a>
</footer>

<!-- Vendor js -->
<script src="{{URL::asset('assets/js/vendor.min.js')}}"></script>


<!-- App js -->
<script src="{{URL::asset('assets/js/app.min.js')}}"></script>

</body>
</html>