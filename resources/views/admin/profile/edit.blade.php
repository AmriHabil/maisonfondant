@section('content')



    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.products')}}">Profile</a></li>
                        <li class="breadcrumb-item active">Modification</li>
                    </ol>
                </div>
                <h4 class="page-title">Modifier ses informations</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-4 col-xl-4">
            <div class="card-box text-center">
                <img src="{{URL::asset('assets/images/logo-dark.png')}}" class="rounded-circle avatar-lg img-thumbnail"
                     alt="profile-image">

                <h4 class="mb-0">{{auth()->user()->name}}</h4>

                <div class="text-center mt-3">

                    <p class="text-muted mb-2 font-13"><strong>Numéro :</strong> <span class="ml-2">{{auth()->user()->phone}}</span></p>


                </div>


            </div> <!-- end card-box -->



        </div> <!-- end col-->

        <div class="col-lg-8 col-xl-8">
            <div class="card-box">

                <div class="tab-content">


                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible text-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif


                        <form action="{{route('admin.profile.update')}}" method="POST">
                            @csrf

                            <h5 class="text-uppercase"><i class="mdi mdi-account-circle mr-1"></i> Modifier le mot de passe
                            </h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row p-2">


                                        <label  for="password">Password<span class="text-danger">*</span></label>
                                        <input type="password" id="password" name="password"  autocomplete="off"
                                               class="form-control" required>
                                        @error('Password')
                                        <ul class="parsley-errors-list filled">
                                            <li class="parsley-required">{{ $message }}</li>
                                        </ul>
                                        @enderror

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row p-2">


                                        <label  for="c_password">Confirm
                                            Password<span class="text-danger">*</span></label>
                                        <input type="password" id="c_password" name="c_password"
                                               class="form-control" required  autocomplete="off">
                                        @error('c_password')
                                        <ul class="parsley-errors-list filled">
                                            <li class="parsley-required">{{ $message }}</li>
                                        </ul>
                                        @enderror

                                    </div>
                                </div> <!-- end col -->
                            </div> <!-- end row -->



                            <div class="text-right">
                                <button type="submit" class="btn btn-success waves-effect waves-light mt-2"><i
                                            class="mdi mdi-content-save"></i> Enregistrer
                                </button>
                            </div>
                        </form>

                    <!-- end settings content-->

                </div> <!-- end tab-content -->
            </div> <!-- end card-box-->

        </div> <!-- end col -->
    </div>
    <!-- end row-->


@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}



<link href="{{URL::asset('assets/css/general.css')}}" rel="stylesheet" type="text/css"/>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}



<!-- Select2 js-->
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>


{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
