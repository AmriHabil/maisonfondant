@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="form-inline" action="{{route('admin.caisse.status')}}" method="GET">
                        @csrf
                        <div class="form-group">
                            <div class="input-group input-group-sm">
                                <input type="date" name="date" class="form-control border-white dash-daterange">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-blue border-blue text-white">
                                        <i class="mdi mdi-calendar-range font-13"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <button href="javascript: void(0);" class="btn btn-blue btn-sm ml-2">
                            <i class="mdi mdi-arrow-right-thick"></i>
                        </button>

                    </form>
                </div>
                <h4 class="page-title">Caisse</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">

        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle shadow-lg bg-success border-success border">
                            <i class="fa fa-dollar-sign font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{number_format($caisse['cash'],3)}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Espèces</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle shadow-lg bg-info border-info border">
                            <i class="fa fa-money-check-alt  font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$caisse['check']}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Chèque</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle shadow-lg bg-warning border-warning border">
                            <i class="fa fa-tags font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$caisse['sodexo']}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Sodexo</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle shadow-lg bg-danger border-danger border">
                            <i class="fab fa-cc-visa font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$caisse['transfer']}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">TPE</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
    </div>
    <!-- end row-->


    <!-- end row -->

    <div class="row">
        <div class="col-xl-6">
            <div class="card-box">
                <h4 class="header-title mb-3">Encaissement
                    <button type="button" class="btn btn-danger btn-rounded waves-effect waves-light ml-1" data-toggle="modal" data-target="#con-close-modal">
                        <span class="btn-label"><i class="fa fa-arrow-alt-circle-down"></i></span> Décaissement
                    </button>
                </h4>

                <div class="table-responsive">
                    <table class="table table-borderless table-hover table-centered m-0 dataTable">

                        <thead class="thead-light">
                        <tr>
                            <th></th>
                            <th >Motif</th>
                            <th>Date</th>
                            <th>Montant</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($payments->where('type','cash') as $payment)
                            <tr>
                                <td>
                                    @if($payment->amount>0)
                                        <i class="fa fa-arrow-alt-circle-up fa-2x text-success"></i>
                                    @else
                                        <i class="fa fa-arrow-alt-circle-down fa-2x text-danger"></i>
                                    @endif
                                </td>

                                <td>
                                    <h5 class="m-0 font-weight-normal"><a href="{{route('admin.pos.printA4',$payment->pos_number)}}">VENTE N:°{{str_pad($payment->pos_number,3,0,STR_PAD_LEFT)}}</a></h5>
                                </td>
                                <td>
                                    <h5 class="m-0 font-weight-normal">{{$payment->created_at}}</h5>
                                </td>
                                <td>
                                    {{\App\Helpers\AppHelper::priceFormat($payment->amount)}} TND
                                </td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <hr>
                <h4 class="header-title mb-3">Decaissement
                    <button type="button" class="btn btn-danger btn-rounded waves-effect waves-light ml-1" data-toggle="modal" data-target="#con-close-modal">
                        <span class="btn-label"><i class="fa fa-arrow-alt-circle-down"></i></span> Décaissement
                    </button>
                </h4>
                <div class="table-responsive">
                    <table class="table table-borderless table-hover table-centered m-0 dataTable">

                        <thead class="thead-light">
                        <tr>
                            <th></th>
                            <th >Motif</th>
                            <th>Date</th>
                            <th>Montant</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($decaissements as $decaissement)
                            <tr>
                                <td>
                                    <i class="fa fa-arrow-alt-circle-down fa-2x text-danger"></i>
                                </td>

                                <td>
                                    <h5 class="m-0 font-weight-normal">{{$decaissement->details.'('.$decaissement->admin_name.')'}}</h5>
                                </td>
                                <td>
                                    <h5 class="m-0 font-weight-normal">{{$decaissement->created_at}}</h5>
                                </td>
                                <td>
                                    {{\App\Helpers\AppHelper::priceFormat($decaissement->amount)}} TND
                                </td>


                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
        <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <form action="{{route('admin.decaissement.store')}}" id="form_decaissement" method="POST">
                    @csrf
                    <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Décaissement</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body p-4">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="amount" class="control-label">Montatnt</label>
                                    <input type="number" class="form-control" id="amount" placeholder="e.g : 1 350 TND" name="amount"  step="0.001" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="details" class="control-label">Description</label>
                                    <input type="text" class="form-control" id="details" placeholder="Description" name="details">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary btn-rounded waves-effect waves-light ml-1" data-dismiss="modal" >
                            <span class="btn-label"><i class="fa fa-redo"></i></span> Annuler
                        </button>
                        <button type="submit" class="btn btn-danger btn-rounded waves-effect waves-light ml-1" >
                            <span class="btn-label"><i class="fa fa-arrow-alt-circle-down"></i></span> Décaissement
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div><!-- /.modal -->


        <div class="col-xl-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="header-title mb-3">Liste des chèques</h4>

                        <div class="table-responsive">
                            <table class="table table-borderless table-hover table-centered m-0 dataTable">

                                <thead class="thead-light">
                                <tr>
                                    <th>Numéro</th>
                                    <th>Montant</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments->where('type','check') as $payment)
                                <tr>
                                    <td>
                                        <h5 class="m-0 font-weight-normal">{{$payment->number}}</h5>
                                    </td>

                                    <td>
                                        {{\App\Helpers\AppHelper::priceFormat($payment->amount)}} TND
                                    </td>
                                    <td>
                                        {{$payment->created_at}}
                                    </td>
                                    <td>
                                        @if($payment->status==0)
                                            <span class="badge bg-soft-warning text-warning shadow-none">Caisse</span>
                                        @else
                                            <span class="badge bg-soft-success text-success shadow-none">Reçu</span>
                                        @endif
                                    </td>

                                    <td>
                                        @if($payment->status==0 && auth()->user()->phone=='98611913' || auth()->user()->phone=='94896642')
                                            <form action="{{route('admin.payments.updatestatuts',$payment->id)}}" method="post">
                                                @csrf
                                                <button type="submit" class="btn btn-xs btn-success"><i class="mdi mdi-check"></i></button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach




                                </tbody>
                            </table>
                        </div> <!-- end .table-responsive-->
                    </div> <!-- end card-box-->
                </div>
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="header-title mb-3">Liste des tickets</h4>

                        <div class="table-responsive">
                            <table class="table table-borderless table-hover table-centered m-0 dataTable">

                                <thead class="thead-light">
                                <tr>
                                    <th>Numéro</th>
                                    <th>Montant</th>
                                    <th>Date</th>
                                    <th>Status</th>

                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments->where('type','sodexo') as $payment)
                                    <tr>
                                        <td>
                                            <h5 class="m-0 font-weight-normal">{{$payment->number}}</h5>
                                        </td>

                                        <td>
                                            {{\App\Helpers\AppHelper::priceFormat($payment->amount)}} TND
                                        </td>
                                        <td>
                                            <h5 class="m-0 font-weight-normal">{{$payment->created_at}}</h5>
                                        </td>
                                        <td>
                                            @if($payment->status==0)
                                                <span class="badge bg-soft-warning text-warning shadow-none">Caisse</span>
                                            @else
                                                <span class="badge bg-soft-success text-success shadow-none">Reçu</span>
                                            @endif
                                        </td>

                                        <td>
                                            @if($payment->status==0 && auth()->user()->phone=='98611913' || auth()->user()->phone=='94896642')
                                                <form action="{{route('admin.payments.updatestatuts',$payment->id)}}" method="post">
                                                    @csrf
                                                    <button type="submit" class="btn btn-xs btn-success"><i class="mdi mdi-check"></i></button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach



                                </tbody>
                            </table>
                        </div> <!-- end .table-responsive-->
                    </div> <!-- end card-box-->
                </div>
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="header-title mb-3">TPE</h4>

                        <div class="table-responsive">
                            <table class="table table-borderless table-hover table-centered m-0 dataTable">

                                <thead class="thead-light">
                                <tr>
                                    <th>Numéro</th>
                                    <th>Montant</th>
                                    <th>Date</th>
                                    <th>Status</th>

                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments->where('type','transfer') as $payment)
                                    <tr>
                                        <td>
                                            <h5 class="m-0 font-weight-normal">{{$payment->number}}</h5>
                                        </td>

                                        <td>
                                            {{\App\Helpers\AppHelper::priceFormat($payment->amount)}} TND
                                        </td>
                                        <td>
                                            <h5 class="m-0 font-weight-normal">{{$payment->created_at}}</h5>
                                        </td>
                                        <td>
                                            @if($payment->status==0)
                                                <span class="badge bg-soft-warning text-warning shadow-none">Non Déclaré</span>
                                            @else
                                                <span class="badge bg-soft-success text-success shadow-none">Déclaré</span>
                                            @endif
                                        </td>

                                         <td>
                                            @if($payment->status==0 && auth()->user()->phone=='98611913' || auth()->user()->phone=='94896642')
                                                <form action="{{route('admin.payments.updatestatuts',$payment->id)}}" method="post">
                                                    @csrf
                                                    <button type="submit" class="btn btn-xs btn-success"><i class="mdi mdi-check"></i></button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach



                                </tbody>
                            </table>
                        </div> <!-- end .table-responsive-->
                    </div> <!-- end card-box-->
                </div>
            </div>

        </div> <!-- end col -->
    </div>
    <!-- end row -->

    <!-- Plugins js-->


@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .dt-buttons {
        float: right;
    }
    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<!-- Dashboar 1 init js-->
<script>
    $(".dataTable").dataTable({
        order:[2,'desc']
    });
</script>
<script src="{{URL::asset('assets/js/pages/dashboard-1.init.js')}}"></script>
<script>

</script>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')