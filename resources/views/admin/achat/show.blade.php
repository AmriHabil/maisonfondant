@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Celerity</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                        <li class="breadcrumb-item active">Details</li>
                    </ol>
                </div>
                <h4 class="page-title">Orders Details</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->





    <!-- end row-->


    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="float-right">
                    <a href="{{route('admin.orders.edit',$orders->id)}}" class="btn btn-success"><i class="mdi mdi-pencil "></i> Edit</a>
                    @if(\Illuminate\Support\Facades\Auth::user()->Role=='superadmin')
                        <form id="{{$orders->id}}" method="POST" action="" style="display:inline">
                            @csrf
                            <a class="btn btn-danger delete" href="#" deleteid="{{$orders->id}}"><i class="mdi mdi-trash-can "></i> Supprimer</a>
                        </form>

                    @endif
                    <form method="POST" action="{{route('print.orders')}}" target="_blank"  style="display:inline">
                        @csrf
                        <input type="hidden" name="print_data" id="print_data" value="{{$orders->id}}">
                        <input type="hidden" name="save_print">
                        <button type="submit" class="btn btn-info"  ><i class="mdi mdi-printer "></i> Imprimer</button>

                    </form>
                </div>
                <br>
                <br>
                <div class="row">
                    <div class="col-xl-5">
                        <div class="text-center">

                            <h3>{!!  config('global.'.$orders->Status) !!}</h3>
                            @if($orders->PaymentType=='1' || $orders->PaymentType=='2')
                                <h5><span class="badge badge-secondary">Cheque</span></h5>
                                <h5>{{$orders->CheckDetails}}</h5><br>

                            @endif
                            {!! $generator->getBarcode(\App\Helpers\AppHelper::id_Barcode($orders->id), $generator::TYPE_CODE_128,2,60) !!}
                            <h1 class="text-center">{{\App\Helpers\AppHelper::id_Barcode($orders->id)}}</h1>
                            <p class="mb-4"><a href="" class="text-muted">( {{$orders->Attempt }} Attempts )</a></p>

                            <div style="display: flex;
                        flex-direction: row;
                        flex-wrap: wrap;
                        align-content: stretch;
                        justify-content: space-evenly;
                        align-items: center;
                        width: 100%">


                                <?php
                                if (str_contains($orders->Instruction, 'fragile')) {
                                    echo '<img  loading="lazy" src="'.asset('assets/images/orders/fragile.png').'" alt="" style="width: 26%">';
                                }
                                if (str_contains($orders->Instruction, 'keepdry')) {
                                    echo '<img  loading="lazy" src="'.asset('assets/images/orders/keepdry.png').'" alt="" style="width: 26%">';
                                }
                                if (str_contains($orders->Instruction, 'care')) {
                                    echo '<img  loading="lazy" src="'.asset('assets/images/orders/care.png').'" alt="" style="width: 26%">';
                                }
                                if (str_contains($orders->Instruction, 'trample')) {
                                    echo '<img  loading="lazy" src="'.asset('assets/images/orders/trample.png').'" alt="" style="width: 26%">';
                                }
                                if (str_contains($orders->Instruction, 'cutter')) {
                                    echo '<img  loading="lazy" src="'.asset('assets/images/orders/cutter.png').'" alt="" style="width: 26%">';
                                }
                                if (str_contains($orders->Instruction, 'upward')) {
                                    echo '<img  loading="lazy" src="'.asset('assets/images/orders/upward.png').'" alt="" style="width: 26%">';
                                }
                                ?>
                            </div>
                        </div>
                    </div> <!-- end col -->
                    <div class="col-xl-7">
                        <div class="pl-xl-3 mt-3 mt-xl-0">



                            <h4 class="mb-1">Prix :  <b>{{number_format($orders->Price,3)}} TND</b></h4>
                            <h4 class="mb-1">Hub Actuel :  <b>{{$Hubs->firstWhere('id', $orders->HUB_current)->Name ?? ''}}</b></h4>
                            <div class="row">
                                <div class="col">
                                    <h4 class="mb-1">Expediteur</h4>
                                    <ul>
                                        <li>{{$orders->SName}}</li>
                                        <li>{{$orders->SNumber}}</li>
                                        <li>{{$orders->SEmail}}</li>
                                        <li>{{$orders->SAddress}}</li>
                                    </ul>
                                </div>
                                <div class="col">
                                    <h4 class="mb-1">Destinataire</h4>
                                    <ul>
                                        <li>{{$orders->RName}}</li>
                                        <li>{{$orders->RNumber1}} - {{$orders->RNumber2}}</li>
                                        <li>{{$orders->RAddress}}</li>
                                        <li>{{$orders->Governorate}} - {{$orders->Delegation}} - {{$orders->Locality}}</li>
                                    </ul>
                                </div>
                            </div>


                            Description :
                            <p class="text-muted mb-4"> {{$orders->Description}}</p>
                            Détails :
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="text-muted"><i class="mdi mdi-checkbox-marked-circle-outline h6 text-primary mr-2"></i> <b>Service :</b> {{strtoupper($orders->Service)}}</p>
                                    <p class="text-muted"><i class="mdi mdi-checkbox-marked-circle-outline h6 text-primary mr-2"></i> <b>Weight :</b> {{$orders->Weight}}KG</p>
                                </div>
                                <div class="col-md-6">
                                    <p class="text-muted"><i class="mdi mdi-checkbox-marked-circle-outline h6 text-primary mr-2"></i> <b>Size :</b> {{$orders->Size}}</p>
                                    <p class="text-muted"><i class="mdi mdi-checkbox-marked-circle-outline h6 text-primary mr-2"></i> <b>Packages :</b> {{$orders->NbrPackages}}</p>
                                </div>
                            </div>

                            @if($orders->isPaid!=1)
                                <h4>{!!  config('global.isnotpaid') !!}</h4>
                            @else
                                <h4>{!!  config('global.ispaid') !!} <a href="{{route('admin.payments.show',$orders->OP)}}"  target="_blank">Ordre de paiement Ref°: {{$orders->OP}}</a></h4>
                            @endif
                        </div>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->


                <div class="table-responsive mt-4">
                    <h3>Tracking</h3>
                    <table class="table table-bordered table-centered mb-0 table-striped">
                        <thead >
                        <tr>
                            <th>Status</th>
                            <th width="100px">Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Trackings as $Tracking)
                            <tr>
                                <td>{{$Tracking->Status ?? ''}}</td>
                                <td>{{$Tracking->created_at->toDateString() ?? ''}}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive mt-4">
                    <h3>Tickets</h3>
                    <table class="table table-bordered table-centered mb-0  table-striped">
                        <thead>
                        <tr>
                            <th>Subject</th>
                            <th>Status</th>
                            <th width="100px">Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Tickets as $Ticket)
                            <tr>
                                <td>{{$Ticket->Status ?? ''}}</td>
                                <td>@if($Ticket->Status=='0')

                                        <span class="badge badge-primary">
                                                  En Attente
                                        </span>
                                    @elseif($Ticket->Status=='1')
                                        <span class="badge badge-success">
                                                Résolu
                                        </span>
                                    @elseif($Ticket->Status=='2')
                                        <span class="badge badge-danger">
                                                Refusé
                                        </span>


                                    @endif</td>
                                <td>{{$Ticket->created_at->toDateString() ?? ''}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>



    <!-- end row-->




@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- third party css end -->
<!-- Plugins css -->
<!-- Plugins css -->

<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>



<!-- third party js ends -->
<!-- Datatables init -->
<script>
    $('.orders_tbl').DataTable({
        aLengthMenu: [
            [-1, 10, 25, 50, 100, 200, -1],
            ["All", 10, 25, 50, 100, 200, "All"]
        ],
        dom: 'Blfrtip',
        responsive: false,
        paging: false,
        "bInfo" : false
    });
    $('body').on('click', '.main_select', function (e) {
        var check = $('.orders_tbl').find('tbody > tr > td:first-child .order_check');
        if ($('.main_select').prop("checked") == true) {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', true);
        } else {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', false);
        }

        $('.orders_tbl').find('tbody > tr > td:first-child .order_check').val();
    });
    $('.check').click(function () {

        var firstInput = $(this).find('input')[0];
        firstInput.checked = !firstInput.checked;
    });


    $(document).on("click", ".delete", function (e) {
        e.preventDefault();
        var Form = $(this).parents('form:first')[0];
        var formData = new FormData(Form);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $.ajax({
                    type: 'post',
                    url: "/admin/orders/delete/" + $('.delete').attr('deleteid'),
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (data) {
                        if (data == 'success') {
                            swal({
                                    title: 'Success !',
                                    text: "Order created successfully !",
                                    type: 'success',
                                    confirmButtonClass: 'btn btn-confirm mt-2'
                                }
                            );
                            window.location.replace('{{route('admin.orders')}}');
                        } else {
                            swal({
                                    title: 'Not Deleted !',
                                    text: "Order has not been deleted",
                                    type: 'info',
                                    confirmButtonClass: 'btn btn-confirm mt-2'
                                }
                            )
                        }
                    }, error: function (reject) {
                        swal({
                                title: 'Not Deleted !',
                                text: "Order has not been deleted",
                                type: 'info',
                                confirmButtonClass: 'btn btn-confirm mt-2'
                            }
                        )
                    }
                });
            }
        })
    });
</script>


{{--ticket--}}
<script>

    $('input[type=radio][name=Type]').change(function () {
        if (this.value == 'price') {
            $('.ticket').hide();
            $('.ticket input').prop('required', false);
            $('.price').show();
            $('.price input').prop('required', true);
        } else if (this.value == 'receiver') {
            $('.ticket').hide();
            $('.ticket input').prop('required', false);
            $('.receiver').show();
            $('.receiver input').prop('required', true);
        } else if (this.value == 'description') {
            $('.ticket').hide();
            $('.ticket input').prop('required', false);
            $('.description').show();
            $('.description input').prop('required', true);
        } else if (this.value == 'cancel') {
            $('.ticket').hide();
            $('.ticket input').prop('required', false);
        }
    });
    $('.ticket_btn').click(function () {
        $('.ticket').hide();
        $('#ticket_form')[0].reset();
        $('#order_id').val($(this).attr('order_id'));
    });




    $('.tracking_btn').click(function () {
        $('#tracking_timeline').html('');
        var order_id = $(this).attr('order_id');

        $.ajax({
            type: 'POST',
            url: "trackings/show/"+order_id ,
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: {order_id:order_id},
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                var html='';
                $('#SName').html(data.SName);
                $('#SNumber').html(data.SNumber);
                $('#SAddress').html(data.SAddress);
                $('#RName').html(data.RName);
                $('#RNumber').html(data.RNumber1);
                $('#RAddress').html(data.RAddress);
                $('.tracking_barcode').html(data.tracking_barcode);
                if(data.tracking.length != 0){
                    for(var key in data.tracking) {
                        var value = data.tracking[key];
                        //html+='<div>'+key+':'+value+'</div>'
                        if(key%2==0){
                            html+='<article class="timeline-item">';
                        }else{
                            html+='<article class="timeline-item timeline-item-left">';
                        }
                        html+='    <div class="timeline-desk">\n' +
                            '          <div class="timeline-box">';
                        if(key%2==0){
                            html+='         <span class="arrow"></span>\n' ;
                        }else{
                            html+='          <span class="arrow-alt"></span>\n' ;
                        }
                        html+='              <span class="timeline-icon"><i class="mdi mdi-adjust"></i></span>\n' +
                            '                <h4 class="mt-0 font-16">'+value['created_at']+'</h4>\n' +
                            '\n' +
                            '                <p class="mb-0">'+value['Status']+' </p>\n' +
                            '             </div>\n' +
                            '          </div>\n' +
                            '      </article>';
                    }
                }
                $('#tracking_timeline').html(html);
              
            }, error: function (reject) {
                $('.is-invalid').removeClass('is-invalid');
                swal({
                    title: "Fail!",
                    text: "Please follow the instructions",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });

            }
        });
    });
</script>
{{--ticket--}}
<script>
    var mydata = [];
    $('body').on('click', '.print_invoice', function (e) {
        var mydata = [];
        e.preventDefault();
        $('.orders_tbl > tbody  > tr').each(function () {
            var checkbox = $(this).find('td:first-child .order_check');
            if (checkbox.prop("checked") == true) {
                var order_id = $(checkbox).data('id');
                mydata.push(order_id);
            }
        });
        var order_data = mydata.join(',');

        $('#print_data').val(order_data);
        $('#bulk_submit').submit();
    })
</script>
<script>
    $(".datepicker").flatpickr({mode: "range"});
    $(".numeric").maxlength({

        warningClass: "badge badge-success",
        limitReachedClass: "badge badge-danger"
    })
</script>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

