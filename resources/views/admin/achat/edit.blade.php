@section('content')




    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Achat</a></li>
                        <li class="breadcrumb-item active">Modification</li>
                    </ol>
                </div>
                <h4 class="page-title">Modifier une piece de vente</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <form action="{{route('admin.achat.update',$achat->id)}}" method="POST" id="cartStore" enctype="multipart/form-data"  target="_blank"  onsubmit="setTimeout(function(){location.reload();}, 1000);return true;">

        @csrf
        <div class="row">
            <div class="col-lg-12">

                <div class="card-box ribbon-box">
                    <div class="ribbon-two ribbon-two-info"><span><i class="fa fa-user-cog"></i> Client</span></div>
                    <div class="row justify-content-center">
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="provider_id">Client <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="provider_id" name="provider_id" required>
                                    <option value="0"
                                            data-name="Passager"
                                            data-phone="12345678"
                                            data-mf="1234567890"
                                            data-address="Adresse Passager" >
                                        Client Passager
                                    </option>
                                    @foreach($providers as $provider)
                                        <option value="{{$provider->id}}"
                                                data-name="{{$provider->name}}"
                                                data-phone="{{$provider->phone}}"
                                                data-mf="{{$provider->mf}}"
                                                data-address="{{$provider->address}}"
                                                data-plafond="{{$provider->plafond??'Aucun Plafond'}}"
                                                data-encours="{{$provider->encours??0}}"
                                                data-remise="{{$provider->remise??0}}"
                                                @if($achat->client_id==$provider->id) selected @endif
                                        >
                                            {{$provider->mf}} - {{$provider->name}} - {{$provider->phone}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="provider_name">Nom <span class="text-danger">*</span></label>
                                <input type="text" id="provider_name" class="form-control" placeholder="e.g : Apple iMac"
                                       name="provider_details[provider_name]" required  value="{{$achat->provider_details['provider_name']}}">
                                <input type="hidden" id="remise_provider" value="0">
                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="provider_mf">MF <span class="text-danger">*</span></label>
                                <input type="text" id="provider_mf" class="form-control" placeholder="e.g : Apple iMac"
                                       name="provider_details[provider_mf]" value="{{$achat->provider_details['provider_mf']}}">
                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="provider_phone">Téléphone <span class="text-danger">*</span></label>
                                <input type="text" id="provider_phone" class="form-control" placeholder="e.g : Apple iMac"
                                       name="provider_details[provider_phone]"  value="{{$achat->provider_details['provider_phone']}}">
                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="provider_address">Adresse <span class="text-danger">*</span></label>
                                <input type="text" id="provider_address" class="form-control" placeholder="e.g : Apple iMac"
                                       name="provider_details[provider_address]"  value="{{$achat->provider_details['provider_address']}}">
                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="date">Date <span class="text-danger">*</span></label>
                                <input type="date" id="date" class="form-control datepicker" placeholder="e.g : Apple iMac"
                                       name="date" value="{{$achat->date}}">

                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="declared">Déclaration <span class="text-danger">*</span></label>
                                <select name="declared" id="" class="form-control">
                                    <option value="0" @if($achat->declared==0) selected @endif>Non Déclaré</option>
                                    <option value="1" @if($achat->declared==1) selected @endif>Déclaré</option>
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">

                        <div class="col-lg-2 col-md-4 text-center">
                            <p><b>En Cours:</b> <span id="encours"></span> TND</p>
                        </div>


                    </div>
                </div> <!-- end card-box -->

            </div> <!-- end col -->

            <div class="col-lg-12">

                <div class="card-box  ribbon-box">
                    <div class="ribbon-two ribbon-two-success"><span><i class="fa fa-list"></i> Détails</span></div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th style="width: 350px">Article </th>
                                <th>Qté</th>
                                <th>Remise</th>

                                <th>P.U</th>
                                <th>Total</th>
                                <th>MAJ</th>
                                <th width="30px">Action</th>
                            </tr>
                            </thead>
                            <tbody  id="cartItems">
                                @foreach($achat->details as $detail)
                                    <tr id="row{{$detail['product_id']}}" class="item_cart"  data-id="{{$detail['product_id']}}">
                                        <td>
                                            <input type="hidden" name="details[{{$detail['product_id']}}][product_id]" value="{{$detail['product_id']}}">
                                            <input type="text" id="name_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_name]" class="form-control name" readonly value="{{$detail['product_name']}}" style="width: 300px" required/>
                                        </td>
                                        <td>
                                            <input type="number" id="quantity_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_quantity]" class="form-control quantity withPopover " data-product_id="{{$detail['product_id']}}" value="{{$detail['product_quantity']}}" step="1" min="1"
                                                   oninput="this.value = Math.floor(this.value);" required
                                            >
                                        </td>
                                        <td>
                                            <input type="number" id="remise_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_remise]" class="form-control remise" data-product_id="{{$detail['product_id']}}" value="{{number_format($detail['product_remise'],2)}}" min="0" max="100" step="0.01" required>
                                        </td>

                                        <td>
                                            <input type="hidden"  name="details[{{$detail['product_id']}}][product_price_selling]" class="form-control price price_u withPopover number_format_3" data-product_id="{{$detail['product_id']}}" data-product_grade="" value="{{$detail['product_price_selling']}}"
                                                    required>
                                            <input type="text" id="price_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_price_buying]" value="{{$detail['product_price_buying']}}" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" id="total_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_total]"  class="form-control price number_format_3" data-product_id="{{$detail['product_id']}}" value="{{number_format(($detail['product_price_buying']*(1-($detail['product_remise']/100)))*$detail['product_quantity'],3)}}" readonly required>
                                        </td>
                                        <td>
                                            <input type="checkbox" name="details[{{$detail['product_id']}}][product_tva]" >
                                            <input type="hidden" id="tva_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_tva]" class="form-control tva" data-product_id="{{$detail['product_id']}}" value="{{number_format($detail['product_tva'],2)}}" readonly required>
                                        </td>
                                        <td>
                                            <a data-tr="row{{$detail['product_id']}}" class="text-danger removeRow"><i class="fa fa-minus-circle"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <select class="form-control select2 productList" id="productList" name="">
                                        <option value=""></option>
                                        @foreach($products as $product)

                                            <option value="{{$product->id}}"
                                                    data-name="{{$product->name??''}}"
                                                    data-barcode="{{$product->barcode??''}}"
                                                    data-ref="{{$product->ref??''}}"
                                                    data-tva="{{$product->tva??'0'}}"
                                                    data-unity="{{$product->unity??'u'}}"
                                                    data-buying="{{$product->buying??'0'}}"
                                                    data-price="{{$product->selling1??'0'}}"
                                                    data-quantity="{{$product->quantity??'0'}}"

                                            >
                                                {{$product->name}} <span class="d-none">{{$product->barcode}}</span>
                                            </option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><span class="withPopover" data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="La quantité actuelle de cette article est " data-original-title="Raccourci"
                                    ><i class="fa fa-info-circle"></i></span> Recherche</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <audio src="{{asset('assets/sound/success.mp3')}}"></audio>
                    <audio src="{{asset('assets/sound/warning.mp3')}}"></audio>

                </div> <!-- end col-->

            </div> <!-- end col-->
            <div class="col-lg-12" id="payment_section">

                <div class="card-box  ribbon-box">
                    <div class="ribbon-two ribbon-two-warning"><span><i class="fa fa-dollar-sign"></i> Paiement</span></div>
                    <div class="row">

                        <div class="col-md-8" id="comptant">
                            <div class="row justify-content-center mb-3">
                                <button type="button" class="btn btn-success btn-rounded waves-effect waves-light ml-1" id="addCash">
                                    <span class="btn-label"><i class="fa fa-dollar-sign"></i></span>Espèce
                                </button>
                                <button type="button" class="btn btn-info btn-rounded waves-effect waves-light ml-1" id="addCheck">
                                    <span class="btn-label"><i class="fas fa-money-check-alt"></i></span>Chèque
                                </button>
                                <button type="button" class="btn btn-danger btn-rounded waves-effect waves-light ml-1" id="addTransfer">
                                    <span class="btn-label"><i class="fab fa-cc-visa"></i></span>TPE
                                </button>
                                <button type="button" class="btn btn-warning btn-rounded waves-effect waves-light ml-1" id="addSodexo">
                                    <span class="btn-label"><i class="fa  fa-tags"></i></span>Sodexo
                                </button>
                            </div>
                            <div class="row justify-content-center" id="paymentMethod">

                            </div>
                        </div>
                        <div class="col-md-8 " id="commitment">
                            <div id="echeance">

                            </div>


                            <div class="col-12 text-center">
                                <a class="btn btn-sm btn-info text-white" id="addCommitment">Ajouter une échéance</a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="">
                                <h5>Résumé</h5>
                                <p><b>TVA:</b> <span class="float-right" id="totalTva">0.000</span></p>
                                <p><b>Remise :</b> <span class="float-right" id="totalRemise"> &nbsp;&nbsp;&nbsp; 0.000</span></p>
                                <hr>
                                <p class="font-24">A Payer<span class="float-right" id="totalGlobal">0.000</span></p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group mb-3">
                                <label class="mb-2">Mode de réglement <span class="text-danger">*</span></label>
                                <br>
                                <div class="radio form-check-inline radio-success">
                                    <input type="radio" id="Comptant" value="comptant" name="commitmentType" checked="">
                                    <label for="Comptant"> Comptant </label>
                                </div>
                                @can('Supprimer encaissement')
                                    <div class="radio form-check-inline radio-info">
                                        <input type="radio" id="Echeance" value="echeance" name="commitmentType">
                                        <label for="Echeance"> Echeance </label>
                                    </div>
                                @endcan
                            </div>
                        </div>

                    </div>

                </div> <!-- end col-->


            </div> <!-- end col-->

        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-12">
                <div class="text-center mb-3">


                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light submit" id="sa-a4" value="a4" name="submit" >Imprimer A4
                    </button>
                    <button type="submit" class="btn w-sm btn-warning waves-effect waves-light submit" id="sa-ticket" value="ticket" name="submit">Imprimer Ticket
                    </button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>



    <style>
        .select2 {
            width: 100%!important;
        }
        .popover-title{
            background: #ffff99;
        }
    </style>

@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/css/general.css')}}" rel="stylesheet" type="text/css"/>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<!-- Loading buttons js -->
<script src="{{asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{asset('assets/libs/ladda/ladda.js')}}"></script>
<script src="{{asset('assets/js/pages/loading-btn.init.js')}}"></script>

<script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('assets/js/pages/add-product.init.js')}}"></script>
{{--<script src="{{asset('assets/js/validation.js')}}"></script>--}}
<script>
    /*Adding addCheck*/
    $(document).on('click', '#addCheck', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addCheck').length+1;
        let listOfBanks = `
        @foreach(\App\Helpers\WebHelper::allBanks() as $bank)
            <option value="{{$bank->name}}">{{$bank->name}}</option>
         @endforeach
            `;
        let add=`
                                <div class="card addCheck col-12">
                                    <div class="card-header bg-info  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addCheck${count}" role="button" aria-expanded="false" aria-controls="addCheck${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCheck  removePayment" data-type="addCheck"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- Chèque</h5>
                                    </div>
                                    <div id="addCheck${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="number" id="name" class="form-control amount" placeholder="e.g :  1 350 TND"
                                                               name="payments[check][${count}][amount]" required step="0.001">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="date" class="form-control" placeholder="e.g : Apple iMac"
                                                               name="payments[check][${count}][date]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Numéro <span class="text-danger">*</span></label>
                                                        <input type="text" id="name" class="form-control" placeholder="e.g : 1234567"
                                                               name="payments[check][${count}][number]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Banque <span class="text-danger">*</span></label>
                                                        <select class="form-control select2" id="select_bank${count}" name="payments[check][${count}][bank]">
                                                            ${listOfBanks}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


        `;
        $('#paymentMethod').append(add);
        $('#select_bank'+count).select2();
    });

    /*Adding addCheck*/

    /*Adding addCash*/
    $(document).on('click', '#addCash', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addCash').length+1;
        let date = '{{date('Y-m-d')}}';
        let add=`<div class="card addCash  col-12">
                                    <div class="card-header bg-success  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addCash${count}" role="button" aria-expanded="false" aria-controls="addCash${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCash removePayment" data-type="addCash"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- Espèce</h5>
                                    </div>
                                    <div id="addCash${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="number"  class="form-control amount" placeholder="e.g : 1 350 TND"
                                                               name="payments[cash][${count}][amount]" value="${total.toFixed(3)}" step="0.001">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="name" class="form-control"
                                                               name="payments[cash][${count}][date]" value="${date}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

        `;
        $('#paymentMethod').append(add);
    });

    /*Adding addCash*/

    /*Adding addTransfer*/
    $(document).on('click', '#addTransfer', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addTransfer').length+1;
        let date = '{{date('Y-m-d')}}';
        let add=`<div class="card addTransfer  col-12">
                                    <div class="card-header bg-danger  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addTransfer${count}" role="button" aria-expanded="false" aria-controls="addTransfer${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCash removePayment" data-type="addTransfer"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- TPE</h5>
                                    </div>
                                    <div id="addTransfer${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="number"  class="form-control amount" placeholder="e.g : 1 350 TND"
                                                               name="payments[transfer][${count}][amount]" value="${total.toFixed(3)}" step="0.001">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="number">Numéro <span class="text-danger">*</span></label>
                                                        <input type="text" id="number" class="form-control" placeholder="e.g : 123467"
                                                               name="payments[transfer][${count}][number]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="name" class="form-control"
                                                               name="payments[transfer][${count}][date]" value="${date}" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

        `;
        $('#paymentMethod').append(add);
    });

    /*Adding addTransfer*/

    /*Adding addTransfer*/
    $(document).on('click', '#addSodexo', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addCash').length+1;
        let date = '{{date('Y-m-d')}}';
        let add=`<div class="card addSodexo  col-12">
                                    <div class="card-header bg-warning  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addSodexo${count}" role="button" aria-expanded="false" aria-controls="addSodexo${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCash removePayment" data-type="addSodexo"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- Sodexo</h5>
                                    </div>
                                    <div id="addSodexo${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="number"  class="form-control amount" placeholder="e.g : 1 350 TND"
                                                               name="payments[sodexo][${count}][amount]" value="${total.toFixed(3)}" onblur="this.value=(this.value*0.9).toFixed(3);" step="0.001">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Numéro <span class="text-danger">*</span></label>
                                                        <input type="text" id="name" class="form-control" placeholder="e.g : Apple iMac"
                                                               name="payments[sodexo][${count}][number]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="name" class="form-control"
                                                               name="payments[sodexo][${count}][date]" value="${date}" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

        `;
        $('#paymentMethod').append(add);
    });

    /*Adding Sodexo*/
    /*removePayment*/
    $(document).on('click','.removePayment',function () {
        let type=$(this).attr('data-type');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $('.'+type).last().remove();
            }
        });
    });
    /*removePayment*/
    /*Add Commitment*/
    $(document).on('click', '#addCommitment', function(){
        let count=$('.echeance').length+1;
        let add=` <div class="card echeance col-12">
                                <div class="card-header bg-info  text-white">
                                    <div class="card-widgets">
                                        <span data-toggle="collapse" href="#echeance${count}" role="button" aria-expanded="false" aria-controls="echeance${count}"><i class="mdi mdi-minus"></i></span>
                                        <span  class="removeCheck  removePayment" data-type="echeance"><i class="mdi mdi-close"></i></span>
                                    </div>
                                    <h5 class="card-title mb-0 text-white">${count} - Echéance</h5>
                                </div>
                                <div class="collapse show">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Montant <span class="text-danger">*</span></label>
                                                    <input type="text" id="commitmentAmount${count}" class="form-control commitmentAmount amount" placeholder="e.g : Apple iMac"
                                                           name="echeance[${count}][amount]" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Date <span class="text-danger">*</span></label>
                                                    <input type="date" id="commitmentDate${count}" class="form-control commitmentDate"  placeholder="e.g : Apple iMac"
                                                           name="echeance[${count}][date]" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
        $('#echeance').append(add);
        setCommitment();


    });
    function setCommitment(){
        let count=$('.echeance').length;
        let total=parseFloat($('#totalGlobal').text());
        let rest=total%count;
        let echeance=Math.floor(total / count);
        let element;
        for(let i=0;i<count;i++){
            if(i==0) document.getElementsByClassName('commitmentAmount')[i].value=((rest+echeance).toFixed(3));
            else document.getElementsByClassName('commitmentAmount')[i].value=echeance.toFixed(3);
            let now = new Date();
            let echance = new Date(now.setMonth(now.getMonth() + i));
            element=document.getElementsByClassName('commitmentDate')[i];
            var day = ("0" + echance.getDate()).slice(-2);
            var month = ("0" + (echance.getMonth() + 1)).slice(-2);
            var today = echance.getFullYear()+"-"+(month)+"-"+(day) ;
            //element.flatpickr();
            element.value=today;

        }
    }
    /*Add Commitment*/
    $('#commitment').hide();
    $('#comptant').show();
    $(document).on('change', 'input[type=radio][name=commitmentType]', function(){
        if (this.value == 'echeance') {
            $('#commitment').show();
            $('#comptant').hide();
            $('#paymentMethod').html('');
        }
        else if (this.value == 'comptant') {
            $('#commitment').hide();
            $('#comptant').show();
            $('#echeance').html('');
        }
    });
</script>
<script>
    calculate_gtotal();
    $('.withPopover').popover({offset: 10});
    var productListAfterSelect2 = $("#productList").select2();
    $(window).on('load', function() {
        productListAfterSelect2.select2("open");
    });
    $(".datepicker").flatpickr();

    $(document).on('change', '#productList', function () {
        addProduct(this);
    });
    $(document).on('click', '.removeRow', function () {
        let tr = $(this).attr("data-tr");
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $('#'+tr).remove();
                calculate_gtotal();
            }
        });

    });





    $(document).on('change', '.quantity,.remise,.price_u', function() {
        calculate_gtotal();
    });
    $(document).on('change', '.quantity', function() {
        checkQuantity(this);

    });

    $(document).on('change', '.price_u', function() {
        let min=parseFloat($(this).attr('data-min'));
        let id=parseFloat($(this).attr('data-product_id'));
        if(parseFloat($(this).val())<min){
            $.toast({
                heading: 'Dépassage du remise maximale',
                text: 'Le prix d\'achat actuel est '+min,
                icon: 'error',
                loader: true,
                position:'top-right',// Change it to false to disable loader
                loaderBg: '#f1556c',  // To change the background
                bgColor: '#f1556c',  // To change the background
            });
            //$(this).val(max);
            $(this).addClass('alert-danger');
        }else{
            $(this).removeClass('alert-danger');
        }
        $(this).val(parseFloat($(this).val()).toFixed(3));
    });

    function addProduct(select) {
        let product = {
            id:select.value,
            name:select.options[select.selectedIndex].getAttribute('data-name'),
            unity:select.options[select.selectedIndex].getAttribute('data-unity'),
            tva:select.options[select.selectedIndex].getAttribute('data-tva'),
            buying:select.options[select.selectedIndex].getAttribute('data-buying'),
            price:select.options[select.selectedIndex].getAttribute('data-price'),
            quantity:select.options[select.selectedIndex].getAttribute('data-quantity'),

        };
        let provider_remise=$('#remise_provider').val();
        if($('#quantity_id_'+product.id).length){
            $('#quantity_id_'+product.id).val(parseFloat($('#quantity_id_'+product.id).val())+1);

            playSound();
        }
        else{
            let alert='';
            if (product.quantity==0) alert='alert-danger';
            tr=`<tr id="row${product.id}" class="item_cart"  data-id="${product.id}">
                   <td>
${product.name}
                        <input type="hidden" name="details[${product.id}][product_id]" value="${product.id}">
                       <input type="hidden" id="name_id_${product.id}" name="details[${product.id}][product_name]" class="form-control name" readonly value="${product.name}" style="width: 300px" required/>
                    </td>
                    <td>
                       <input type="number" id="quantity_id_${product.id}" name="details[${product.id}][product_quantity]" class="form-control ${alert} quantity withPopover " data-product_id="${product.id??0}" value="1" step="1" min="1"
                        oninput="this.value = Math.floor(this.value);" data-max="${product.quantity??0}"
                        data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="La quantité actuelle de cette article est : ${product.quantity}" data-original-title="Information du stock" required
                        >
                    </td>
                    <td>
                       <input type="number" id="remise_id_${product.id}" name="details[${product.id}][product_remise]" class="form-control remise" data-product_id="${product.id}" value="${product.remise??provider_remise}" min="0" max="100" step="0.01" required>
                    </td>
                    <td>
                       <input type="text" id="tva_id_${product.id}" name="details[${product.id}][product_tva]" class="form-control tva" data-product_id="${product.id}" value="${product.tva??0}" readonly required>
                    </td>
                   <td>
                       <input type="text" id="price_id_${product.id}" name="details[${product.id}][product_price_buying]" class="form-control price price_u withPopover number_format_3" data-product_id="${product.id}" data-product_grade="" value="${product.buying??0}" data-min="${product.buying}"
                        data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="Le prix d'achat de cet article est : ${product.buying}" data-original-title="Prix de revient" required>
                        <input type="hidden" name="details[${product.id}][product_price_selling]" value="${product.price}">
                    </td>
                    <td>
                       <input type="text" id="total_id_${product.id}" name="details[${product.id}][product_total]"  class="form-control price" data-product_id="${product.id}" value="${product.price??0}" readonly required>
                    </td>
                    <td>
                        <a data-tr="row${product.id}" class="text-danger removeRow"><i class="fa fa-minus-circle"></i></a>
                   </td>
                </tr>`;
            $('#cartItems').append(tr);
            $('.withPopover').popover({offset: 10});
            playSound();
        }
        calculate_gtotal();
        openAndFocus('productList');

    }
    function playSound() {
        $('audio')[0].load();
        $('audio')[0].play();
    }



    function checkQuantity(element){
        let max=parseFloat($(element).attr('data-max'));
        let id=parseFloat($(element).attr('data-product_id'));
        if(parseFloat($(element).val())>max){
            $.toast({
                heading: 'Dépassage du stock',
                text: 'La quantité actuelle est '+max,
                icon: 'error',
                loader: true,
                position:'top-right',// Change it to false to disable loader
                loaderBg: '#f1556c',  // To change the background
                bgColor: '#f1556c',  // To change the background
            });
            //$(this).val(max);
            $(element).addClass('alert-danger');
        }else{
            $(element).removeClass('alert-danger');
        }
    }

    function calculate_gtotal() {
        $('#cartItems').each(function () {
            let totalTva = 0;
            let totalRemise = 0;
            let totalGlobal = 0;
            let totalPerLine = 0;
            $(this).find('.quantity').each(function () {

                let product_id = $(this).attr('data-product_id');
                let price = parseFloat($('#price_id_' + product_id).val()??0);
                let remise = parseFloat($('#remise_id_' + product_id).val()??0);
                let tva = parseFloat($('#tva_id_' + product_id).val()??0);

                totalRemise += parseFloat((price *  remise) / 100)*parseFloat($(this).val());

                totalTva += parseFloat($(this).val())*parseFloat(price*(1-(100/(100+tva))));
                totalPerLine = parseFloat($(this).val()) * parseFloat((price * (100 - remise)) / 100);
                totalGlobal += totalPerLine;
                $('#total_id_' + product_id).attr('value', totalPerLine.toFixed(3));
            });
            $('#totalGlobal').html(totalGlobal.toFixed(3));
            $('#totalTva').html(totalTva.toFixed(3));
            $('#totalRemise').html(totalRemise.toFixed(3));


        });
    }



    function openAndFocus(element){
        $('#'+element).val('');
        $('#'+element).select2().select2('open');
        $('.select2-search__field').last().focus();
    }



    /*FetchClientInformation*/
    $(document).on('change', '#client_id', function(){

        $('#client_name').val(this.options[this.selectedIndex].getAttribute('data-name'));
        $('#client_mf').val(this.options[this.selectedIndex].getAttribute('data-mf'));
        $('#client_address').val(this.options[this.selectedIndex].getAttribute('data-address'));
        $('#client_phone').val(this.options[this.selectedIndex].getAttribute('data-phone'));
        $('#encours').text(this.options[this.selectedIndex].getAttribute('data-encours'));
        $('#plafond').text(this.options[this.selectedIndex].getAttribute('data-plafond'));
        //$('.remise').val(this.options[this.selectedIndex].getAttribute('data-remise'));
        $('#remise_client').val(this.options[this.selectedIndex].getAttribute('data-remise'));
        calculate_gtotal();
    });
    /*FetchClientInformation*/

    $('#commitment').hide();
    $('#comptant').show();
    $(document).on('change', 'input[type=radio][name=commitmentType]', function(){
        if (this.value == 'echeance') {
            $('#commitment').show();
            $('#comptant').hide();
            $('#paymentMethod').html('');
        }
        else if (this.value == 'comptant') {
            $('#commitment').hide();
            $('#comptant').show();
            $('#echeance').html('');
        }
    });


    $(document).on('click','.submit',function (e) {

            let total_amounts=0;
            $('.amount').each(function(i, obj) {
                total_amounts+=parseFloat($(obj).val());
            });
            if(total_amounts!=parseFloat($('#totalGlobal').html())){
                e.preventDefault();
                swal({
                    title: 'Total incorrect!',
                    text: "Veuillez corriger les montant saisies!",
                    type: 'error'
                });
                $('.amount').addClass('is-invalid');
            }

    });
</script>
<style>
    input.chk-btn {
        display: none;
    }

    .label-chk-btn {
        width: 60px;
    }

    .label-chk-btn img {
        filter: grayscale(100%);
        height: 60px;
        transition: ease .3s;
        width: 45px;
        margin: auto;
        display: block;
    }

    input.chk-btn:checked + label img {
        transform: scale(1.1);
        filter: grayscale(0);
    }

    .is-invalid {
        border-color: #dc3545;
        padding-right: calc(1.5em + .75rem);
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-position: right calc(.375em + .1875rem) center;
        background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }
    #select2-productList-container{
        max-width: 300px;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
