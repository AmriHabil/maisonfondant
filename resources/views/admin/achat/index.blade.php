@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Celerity</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
                <h4 class="page-title">Orders List</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->





    <!-- end row-->


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">





                    <div class="row mt-2">
                        @if(auth()->user()->phone=='98611913' || auth()->user()->phone=='56525990')
                            <div class="col-12">
                                <div class="row justify-content-center mb-1">
                                    <div class="col-md-3">
                                        <label>Du</label>
                                        <input autocomplete="off"  required type="date" class="form-control  filter-field"
                                               name="from" placeholder="De" id="from" >
                                    </div>
                                    <div class="col-md-3">
                                        <label>Au</label>
                                        <input autocomplete="off"  required type="date" class="form-control  filter-field"
                                               name="to" placeholder="Au" id="to">
                                    </div>
                                    <div class="col-md-3">
                                        <label>Fournisseur</label>
                                        <input autocomplete="off"  required type="text" class="form-control  filter-field"
                                               name="provider" placeholder="Fournisseur" id="provider">
                                    </div>


                                </div>

                            </div>
                            <div class="col-12 text-center">

                                <label for="all" class="mb-0 btn btn btn-secondary"> <input type="radio" name="status" class="filter-field" id="all" value="all"> ALL (<span id="allSpan"></span>)</label>
                                <label for="paid" class="mb-0 btn btn badge-success"> <input type="radio" name="status" class="filter-field" id="paid" value="2"> Payé (<span id="paidSpan"></span>)</label>
                                <label for="p_paid" class="mb-0 btn btn badge-warning"> <input type="radio" name="status" class="filter-field" id="p_paid" value="1"> Partiellement Payé (<span id="p_paidSpan"></span>)</label>
                                <label for="unpaid" class="mb-0 btn btn btn-danger">  <input type="radio" name="status" class="filter-field" id="unpaid" value="0"> Non Payé (<span id="unpaidSpan"></span>)</label>



                            </div>
                            <div id="chart" class="col-12">

                            </div>

                        @endif
                        <div class="col-12">

                            <h5 class="text-center @if(auth()->user()->phone!='98611913') d-none @endif">Total : <span id="gTotal"></span></h5>
                            <div class="table-responsive">
                                <table id="key-datatable" class="table table-striped dt-responsive nowrap">
                                    <thead>
                                    <tr>

                                        <th>Numéro</th>
                                        <th>Fournisseur</th>
                                        <th>Boutique</th>
                                        <th>Déclaration</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th width="100px">Action </th>

                                    </tr>
                                    </thead>

                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th class="searchable">Numéro</th>
                                        <th class="searchable">Fournisseur</th>
                                        <th class="searchable">Boutique</th>
                                        <th class="searchable">Déclaration</th>
                                        <th class="searchable">Total</th>
                                        <th class="payment_status">Status</th>
                                        <th class="searchable">Date</th>
                                        <th> </th>

                                    </tr>

                                    </tfoot>
                                </table>

                            </div>

                        </div><!-- end col-->
                    </div>

                    <!-- end row-->



                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->





@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>

<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js" type="text/javascript"></script>

<script src="{{URL::asset('assets/libs/morris-js/morris.min.js')}}"></script>

<script src="{{URL::asset('assets/libs/raphael/raphael.min.js')}}"></script>

<!-- third party js ends -->
<!-- Datatables init -->
<script>

    table=$('#key-datatable').DataTable({
        @if(auth()->user()->phone=='98611913' || auth()->user()->phone=='56525990')
        drawCallback: function(settings) {
            $('#paidSpan').text(settings.json.order.paid+' | '+(((settings.json.order.paid/settings.json.order.all)*100).toFixed(2)) +'%');
            $('#p_paidSpan').text(settings.json.order.p_paid+' | '+(((settings.json.order.p_paid/settings.json.order.all)*100).toFixed(2)) +'%');
            $('#unpaidSpan').text(settings.json.order.unpaid+' | '+(((settings.json.order.unpaid/settings.json.order.all)*100).toFixed(2)) +'%');


            $('#allSpan').text(settings.json.order.all);
            $('#chart').html(`<div id="morris-donut-example" style="display: block;margin-left: auto;margin-right: auto;width: 300px" class="morris-chart"></div>`);
            var MorrisCharts = {
                createDonutChart: function (elementId, data, colors) {
                    Morris.Donut({
                        element: elementId,
                        data: data,
                        resize: true,
                        colors: colors
                    });
                },

                init: function () {
                    var data = [];
                    data.push({
                        label: "Payé",
                        value: settings.json.order.paid
                    });
                    data.push({
                        label: "P Payé",
                        value: settings.json.order.p_paid
                    });
                    data.push({
                        label: "Non Payé",
                        value: settings.json.order.unpaid
                    });




                    this.createDonutChart("morris-donut-example", data,
                        ["#1abc9c", "#f7b84b", "#f1556c"]);
                }
            };

            MorrisCharts.init();
        },
        @endif
        processing: true,
        serverSide: true,

        ajax: "{{ route('admin.achat.getData') }}",
        columns: [
            { data: 'id', name: 'id' },
            { data: 'provider', name: 'provider_details' },
            { data: 'store', name: 'store' },
            { data: 'declared', name: 'declared' },
            { data: 'total', name: 'total' },
            { data: 'payment_status', name: 'payment_status' },
            { data: 'date', name: 'date' },
            { data: 'action', name: 'action' },

        ],
        order: [[0, 'desc']],



        aLengthMenu: [
            [10, 25, 50, 100, 200, -1],
            [10, 25, 50, 100, 200, "All"]
        ],

        dom: 'Blfrtip',
        responsive:false,
        initComplete: function () {
            // Apply the search
            this.api()
                .columns()
                .every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change clear', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
        },
        footerCallback: function (row, data, start, end, display) {
            var api = this.api();

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
            };

            // Total over all pages
            total = api
                .column(3)
                .data()
                .reduce(function (a, b) {
                    return parseFloat(a) + parseFloat(b);
                }, 0);

            // Total over this page
            pageTotal = api
                .column(3, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return parseFloat(a) + parseFloat(b);
                }, 0);

            // Update footer

            $('#gTotal').text(total);
        },
    });
    $('#key-datatable tfoot .searchable').each(function () {
        var title = $(this).text();
        $(this).html('<input class="form-control" type="text" placeholder="Search ' + title + '" />');
    });
    $('body').on('click', '.main_select', function (e) {
        var check = $('.orders_tbl').find('tbody > tr > td:first-child .order_check');
        if ($('.main_select').prop("checked") == true) {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', true);
        } else {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', false);
        }

        $('.orders_tbl').find('tbody > tr > td:first-child .order_check').val();
    });
    $('.check').click(function () {

        var firstInput = $(this).find('input')[0];
        firstInput.checked = !firstInput.checked;
    });
    $('#key-datatable tfoot .payment_status').each(function () {
        var title = $(this).text();
        let select=`<select class="form-control">
                        <option value=""></option>
                        <option value="0">Non Payé</option>
                        <option value="1">Partiellement Payé</option>
                        <option value="2">Payé</option>
                    </select>`;
        $(this).html(select);
    });





    function filter() {
        var queryString = '?' + new URLSearchParams(filter).toString();
        var url = @json(route('admin.achat.getData'));
        table.ajax.url(url + queryString).load();
        table.draw();
    }

    $(document).on('change', '.filter-field', function(e) {
        var key = $(this).attr('id');
        var value = $(this).val();
        filter[key] = value;
        filter();
    });
    $(document).on('change', 'input[name=status]', function(e) {
        var value = $(this).val();
        filter['status'] = value;
        if(value=='all') $('input[name=type]').prop('checked',false);
        filter();
    });
</script>



{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

