@section('content')




    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Commandes</a></li>
                        <li class="breadcrumb-item active">Modification</li>
                    </ol>
                </div>
                <h4 class="page-title">Modification d'une commande</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <form action="{{route('admin.orders.update',$order->id)}}" method="POST" id="cartStore" enctype="multipart/form-data"  target="_blank"  onsubmit="setTimeout(function(){location.reload();}, 1000);return true;">

        @csrf
        <div class="row">


            <div class="col-lg-12">
                <div class="card-box ribbon-box">
                    <div class="ribbon-two ribbon-two-info"><span><i class="fa fa-store"></i> Coordonnées</span></div>
                    <div class="row justify-content-center">

                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="name">Nom & Prénom <span class="text-danger">*</span></label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Nom & Prénom" value="{{$order->name}}">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="phone">Numéro <span class="text-danger">*</span></label>
                                <input type="text" name="phone" class="form-control" id="phone" placeholder="Numéro" value="{{$order->phone}}">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="date">Date de récupération<span class="text-danger">*</span></label>
                                <input type="date" name="date" class="form-control" id="date"  value="{{$order->date}}">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="time">Heure de récupération<span class="text-danger">*</span></label>
                                <input type="time" name="hour" class="form-control" id="time"  value="{{$order->hour}}">
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="address">Adresse<span class="text-danger">*</span></label>
                                <input type="text" name="address" class="form-control" id="address" placeholder="Adresse"  value="{{$order->address}}">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="note">Commentaire<span class="text-danger">*</span></label>
                                <input type="text" name="note" class="form-control" id="note" placeholder="Commentaire"  value="{{$order->note}}">
                            </div>
                        </div>


                    </div> <!-- end card-box -->


                </div> <!-- end col -->
                <div class="card-box  ribbon-box">
                    <div class="ribbon-two ribbon-two-success"><span><i class="fa fa-list"></i> Détails</span></div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th width="300px">Article</th>
                                <th width="100px">Qté</th>

                                <th width="30px">Action</th>
                            </tr>
                            </thead>
                            <tbody  id="cartItems">
                            @foreach($order->details as $detail)

                                    <tr id="row{{$detail->product_id}}" class="item_cart" data-id="{{$detail->product_id}}">
                                        <td>
                                            <input type="hidden" name="details[{{$detail->product_id}}][product_ref]" value="{{$detail->product_ref}}" readonly="" class="form-control">


                                            <input type="hidden" name="details[{{$detail->product_id}}][product_id]" value="{{$detail->product_id}}">
                                            <input type="hidden" name="details[{{$detail->product_id}}][product_coefficient]" value="{{$detail->product_coefficient}}" id="coef_id_{{$detail->product_id}}">
                                            <input type="hidden" name="details[{{$detail->product_id}}][product_category]" value="{{$detail->product_category}}" id="category_id_{{$detail->product_id}}">
                                            <input type="hidden" name="details[{{$detail->product_id}}][product_price]" value="{{$detail->product_price}}" id="price_id_{{$detail->product_id}}">
                                            <input type="hidden" id="name_id_{{$detail->product_id}}" name="details[{{$detail->product_id}}][product_name]" class="form-control name" readonly="" value="{{$detail->product_name}}"  required="">
                                            {{$detail->product_ref}} - {{$detail->product_name}}
                                        </td>
                                        <td>
                                            <input type="number" id="quantity_id_{{$detail->product_id}}" name="details[{{$detail->product_id}}][product_quantity]" class="form-control  quantity withPopover numeric" data-product_id="{{$detail->product_id}}" value="{{$detail->product_quantity}}" step="1" min="0" data-original-title="" title="" onFocus="this.select();">
                                        </td>

                                        <td>
                                            <a data-tr="row{{$detail->product_id}}" class="text-danger removeRow"><i class="fa fa-minus-circle"></i></a>
                                        </td>
                                    </tr>

                            @endforeach

                            </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td>
                                                                <select class="form-control select2 productList" id="productList" name=""  style="width: 150px">
                                                                    <option value=""></option>
                                                                    @foreach($products as $product)

                                                                        <option value="{{$product->id}}"
                                                                                data-name="{{$product->name??''}}"
                                                                                data-ref="{{$product->ref??''}}"
                                                                                data-category="{{$product->category_id??''}}"
                                                                                data-price="{{$product->price??''}}"
                                                                                data-coefficient="{{$product->coefficient??''}}"
                                                                        >
                                                                            {{$product->ref}} - {{$product->name}}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td><span class="withPopover" data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="La quantité actuelle de cette article est " data-original-title="Raccourci"
                                                                ><i class="fa fa-info-circle"></i></span> Recherche</td>
                                                        </tr>
                                                        </tfoot>
                        </table>
                    </div>
                    <audio src="{{asset('assets/sound/success.mp3')}}"></audio>
                    <audio src="{{asset('assets/sound/warning.mp3')}}"></audio>

                </div> <!-- end col-->

            </div> <!-- end col-->


        </div>
        <!-- end row -->


        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-center mb-3">


                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light submit" id="sa-a4" value="a4" name="submit" >Enregistrer
                    </button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>



    <style>
        .select2 {
            width: 100%!important;
        }
        .popover-title{
            background: #ffff99;
        }
    </style>

@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/css/general.css')}}" rel="stylesheet" type="text/css"/>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<!-- Loading buttons js -->
<script src="{{asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{asset('assets/libs/ladda/ladda.js')}}"></script>
<script src="{{asset('assets/js/pages/loading-btn.init.js')}}"></script>

<script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('assets/js/pages/add-product.init.js')}}"></script>

{{--<script src="{{asset('assets/js/validation.js')}}"></script>--}}

<script>

    $('.withPopover').popover({offset: 10});
    var productListAfterSelect2 = $("#productList").select2();
    $(window).on('load', function() {
        productListAfterSelect2.select2("open");
    });
    $(".datepicker").flatpickr();

    $(document).on('change', '#productList', function () {
        addProduct(this);
    });
    $(document).on('click', '.removeRow', function () {
        let tr = $(this).attr("data-tr");
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $('#'+tr).remove();
                calculate_gtotal();
            }
        });

    });








    function addProduct(select) {
        let product = {
            id:select.value,
            name:select.options[select.selectedIndex].getAttribute('data-name'),
            ref:select.options[select.selectedIndex].getAttribute('data-ref'),
            category:select.options[select.selectedIndex].getAttribute('data-category'),
            coefficient:select.options[select.selectedIndex].getAttribute('data-coefficient'),
            // unity:select.options[select.selectedIndex].getAttribute('data-unity'),
            // tva:select.options[select.selectedIndex].getAttribute('data-tva'),
            // buying:select.options[select.selectedIndex].getAttribute('data-buying'),
            price:select.options[select.selectedIndex].getAttribute('data-price'),
            // quantity:select.options[select.selectedIndex].getAttribute('data-quantity'),

        };

        if($('#quantity_id_'+product.id).length){
            $('#quantity_id_'+product.id).val(parseFloat($('#quantity_id_'+product.id).val())+1);
            checkQuantity($('#quantity_id_'+product.id));
            playSound();
        }
        else{
            let alert='';

            tr=`<tr id="row${product.id}" class="item_cart"  data-id="${product.id}">
                   <td>
                        <input type="hidden" name="details[${product.id}][product_id]" value="${product.id}">
                        <input type="hidden" name="details[${product.id}][product_coefficient]" value="${product.coefficient}" id="coef_id_${product.id}">
                        <input type="hidden" name="details[${product.id}][product_category]" value="${product.category}" id="category_id_${product.id}">
                        <input type="hidden" name="details[${product.id}][product_ref]" value="${product.ref}" id="ref_id_${product.id}">
                        <input type="hidden" name="details[${product.id}][product_price]" value="${product.price}" id="ref_id_${product.id}">
                        <input type="hidden" id="name_id_${product.id}" name="details[${product.id}][product_name]" class="form-control name" readonly value="${product.name}" style="width: 150px" required/>
                        ${product.ref} - ${product.name}
                    </td>
                    <td>
                       <input type="number" id="quantity_id_${product.id}" name="details[${product.id}][product_quantity]" class="form-control  quantity withPopover " data-product_id="${product.id??0}" value="1" step="1" min="1">
                    </td>

                    <td>
                        <a data-tr="row${product.id}" class="text-danger removeRow"><i class="fa fa-minus-circle"></i></a>
                   </td>
                </tr>`;
            $('#cartItems').append(tr);
            $('.withPopover').popover({offset: 10});
            playSound();
        }
        openAndFocus('productList');
        // calculate_gtotal();
    }
    function playSound() {
        $('audio')[0].load();
        $('audio')[0].play();
    }

    /*<td>
                           <input type="number" id="polysterene_id_${product.id}" name="details[${product.id}][product_quantity_sent]" class="form-control  withPopover " value="${1/product.coefficient}" step="0.001">
                        </td>*/

    function checkQuantity(element){
        // let max=parseFloat($(element).attr('data-max'));
        // let id=parseFloat($(element).attr('data-product_id'));
        // if(parseFloat($(element).val())>max){
        //     $.toast({
        //         heading: 'Dépassage du stock',
        //         text: 'La quantité actuelle est '+max,
        //         icon: 'error',
        //         loader: true,
        //         position:'top-right',// Change it to false to disable loader
        //         loaderBg: '#f1556c',  // To change the background
        //         bgColor: '#f1556c',  // To change the background
        //     });
        //     //$(this).val(max);
        //     $(element).addClass('alert-danger');
        // }else{
        //     $(element).removeClass('alert-danger');
        // }
    }





    function openAndFocus(element){
        $('#'+element).val('');
        $('#'+element).select2().select2('open');
        $('.select2-search__field').last().focus();
    }


    function calculate_gtotal() {
        $('#cartItems').each(function () {
            let polysterene = 0;
            let mf = 0;
            let fd = 0;
            let fg = 0;
            let cmd = 0;
            let cbl = 0;
            $(this).find('.quantity').each(function () {

                let product_id = $(this).attr('data-product_id');
                let coef = parseFloat($('#coef_id_' + product_id).val()??1);
                let ref = parseFloat($('#ref_id_' + product_id).val());
                // $('#polysterene_id_' + product_id).attr('value', (parseFloat($(this).val())/coef).toFixed(3));
                if(ref==1){
                    polysterene += (parseFloat($(this).val())/coef);
                    fd += (parseFloat($(this).val()));
                }else if(ref==2){
                    mf += (parseFloat($(this).val()));
                }else if(ref==3){
                    fg += (parseFloat($(this).val()));
                }else if(ref==4){
                    cbl += (parseFloat($(this).val()));
                }else if(ref==5){
                    cmd += (parseFloat($(this).val()));
                }
            });
            $('#fd').val(fd.toFixed(0));
            $('#polysterene').val(polysterene.toFixed(2));
            $('#mf').val(mf.toFixed(0));
            $('#cmd').val(cmd.toFixed(0));
            $('#cbl').val(cbl.toFixed(0));
            $('#fg').val(fg.toFixed(0));
        });
    }
    $(document).on('change', '.quantity', function() {
        // calculate_gtotal();
    });
</script>
<style>
    input.chk-btn {
        display: none;
    }

    .label-chk-btn {
        width: 60px;
    }

    .label-chk-btn img {
        filter: grayscale(100%);
        height: 60px;
        transition: ease .3s;
        width: 45px;
        margin: auto;
        display: block;
    }

    input.chk-btn:checked + label img {
        transform: scale(1.1);
        filter: grayscale(0);
    }

    .is-invalid {
        border-color: #dc3545;
        padding-right: calc(1.5em + .75rem);
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-position: right calc(.375em + .1875rem) center;
        background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }
    #select2-productList-container{
        max-width: 300px;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
