@section('sidebar')
    <!-- ========== Left Sidebar Start ========== -->
    <div class="left-side-menu">

        <div class="slimscroll-menu">

            <!--- Sidemenu -->
            <div id="sidebar-menu">

                <ul class="metismenu" id="side-menu">

                    <li class="menu-title">Navigation</li>
                    <li>
                        <a href="{{route('admin.dashboard')}}"><i class="fas fa-home"></i><span>  Accueil </span></a>
                    </li>


                    @canany([ 'Liste inventaire','Liste bon de sortie'])
                        <li class="menu-title">Stock</li>


                        <li>
                            <a href="javascript: void(0);">
                                <i class="fa fa-arrow-right"></i>
                                <span> Bon de sortie </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                @canany([ 'Liste bon de sortie'])
                                    <li>
                                        <a href="{{route('admin.bs')}}">Livraisons </a>
                                    </li>
                                @endcanany
                                @canany([ 'Ajouter bon de sortie'])
                                    <li>
                                        <a href="{{route('admin.bs.create')}}">Ajouter</a>
                                    </li>
                                @endcanany

                            </ul>
                        </li>
                        <li>
                            @canany([ 'Liste bon de réception'])
                                <a href="{{route('admin.bs','br')}}">
                                    <i class="fa fa-arrow-left"></i>
                                    <span> Bon de Réception </span>
                                    @php
                                        $count_br=\App\Models\Bs::where('receiver_id', NULL)->where('to_store_id',auth()->user()->store_id)->count();

                                        if($count_br>0) echo '<span class="badge badge-success badge-pill float-right">'.$count_br.'</span>';

                                    @endphp
                                </a>
                            @endcanany
                        </li>
                    @endcanany
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fa fa-arrow-right"></i>
                            <span> Commandes </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            @canany([ 'Liste bon de sortie'])
                                <li>
                                    <a href="{{route('admin.orders')}}">Historique </a>
                                </li>
                            @endcanany
                            @canany([ 'Ajouter bon de sortie'])
                                <li>
                                    <a href="{{route('admin.orders.create')}}">Ajouter</a>
                                </li>
                            @endcanany

                        </ul>
                    </li>
                    @can('Comparatif bon de sortie')
                        <li>

                            <a href="{{route('admin.bs.journal')}}">
                                <i class="fa fa-file"></i>
                                <span> Tableau Comparatif </span>
                            </a>
                        </li>
                    @endcan
                    @can('Journal bon de sortie')
                        <li>

                            <a href="{{route('admin.bs.daily')}}">
                                <i class="fa fa-file"></i>
                                <span> Journal </span>
                            </a>
                        </li>
                    @endcan
                    @can('Recap bon de sortie')
                        <li>

                            <a href="{{route('admin.bs.getRecap')}}">
                                <i class="fa fa-chart-line"></i>
                                <span> Suivi des ventes </span>
                            </a>
                        </li>
                    @endcan
                    @can('Dashboard bon de sortie')
                        <li>
                            <a href="{{route('admin.bs.charts')}}">
                                <i class="fa fa-chart-line"></i>
                                <span> Dashboard </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.bs.reports')}}">
                                <i class="fa fa-chart-line"></i>
                                <span> Rapports </span>
                            </a>
                        </li>
                    @endcan
                    @canany([ 'Liste facture','Ajouter facture'])
                        <li>
                            <a href="javascript: void(0);">
                                <i class="fa fa-file-excel"></i>
                                <span> Facturation </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                @canany([ 'Liste facture'])
                                    <li>
                                        <a href="{{route('admin.invoices')}}">Historique </a>
                                    </li>
                                @endcanany
                                @canany([ 'Ajouter facture'])
                                    <li>
                                        <a href="{{route('admin.invoices.create')}}">Ajouter</a>
                                    </li>
                                @endcanany

                            </ul>

                        </li>
                    @endcan

                    <li class="menu-title">Vente</li>
                    @canany([ 'Ajouter bon de livraison'])
                        <li>
                            <a href="{{route('admin.bl.create')}}">
                                <i class="fa fa-plus"></i>
                                <span> Ajouter </span>
                            </a>
                        </li>
                    @endcanany
                    @canany([ 'Liste bon de livraison'])
                        <li>
                            <a href="{{route('admin.bl')}}">
                                <i class="fa fa-cart-plus"></i>
                                <span> Bon De livraison </span>
                            </a>
                        </li>
                    @endcanany
                    @canany([ 'Liste facture'])
                    <li>
                        <a href="{{route('admin.facture')}}">
                            <i class="fa fa-cart-plus"></i>
                            <span> Factures </span>
                        </a>
                    </li>
                    @endcanany
                    @canany([ 'Liste clients'])
                    <li>
                        <a href="javascript: void(0);">
                            <i class="fas fa-users"></i>
                            <span> Clients </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="{{route('admin.clients')}}">Clients</a>
                            </li>
                            <li>
                                <a href="{{route('admin.clients.create')}}">Ajouter Clients</a>
                            </li>

                        </ul>

                    </li>
                    @endcanany


                    <li class="menu-title">POS & Encaissement</li>


                    <li>
                        <a href="{{route('admin.pos.create')}}">
                            <i class="fa fa-plus"></i>

                            <span> Ajouter Vente </span>
                        </a>

                    </li>
                    <li>
                        <a href="{{route('admin.pos')}}">
                            <i class="fa fa-list"></i>

                            <span> Liste des ventes </span>
                        </a>

                    </li>
                    <li>
                        <a href="{{route('admin.pos.daily')}}">
                            <i class="fas fa-list"></i>
                            <span>V.Détaillés</span>
                        </a>
                    </li>


                    <li>
                        <a href="{{route('admin.caisse.index')}}">
                            <i class="fa fa-cart-plus"></i>
                            <span> Caisse </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin.financialcommitment.index')}}">
                            <i class="fa fa-calendar"></i>
                            <span>Encaissement</span>
                            @php
                                $count_ech_v=\App\Models\FinancialCommitment::whereNot('payment_status',2)->count();
                                if($count_ech_v>0) echo '<span class="badge badge-success badge-pill float-right">'.$count_ech_v.'</span>';
                            @endphp
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin.pos.roulement')}}">
                            <i class="fas fa-redo"></i>
                            <span>Roulement</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin.pos.performance')}}"><i class="fas fa-chart-bar"></i>
                            <span>Performance</span> </a>
                    </li>
                    <li>
                        <a href="{{route('admin.pos.statistics')}}"><i class="fas fa-chart-line"></i>
                            <span>Statistiques</span> </a>
                    </li>

                        <li class="menu-title">Achat</li>
                        <li>
                            <a href="{{route('admin.achat.create')}}">
                                <i class="fa fa-plus"></i>

                                <span> Ajouter </span>
                            </a>

                        </li>
                        <li>
                            <a href="{{route('admin.achat')}}">
                                <i class="fa fa-cart-plus"></i>

                                <span> Bon D'Achat </span>
                            </a>

                        </li>
                        <li>
                            <a href="javascript: void(0);">
                                <i class="fas fa-users"></i>
                                <span> Fournisseurs </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="{{route('admin.providers')}}">Fournisseurs</a>
                                </li>
                                <li>
                                    <a href="{{route('admin.providers.create')}}">Ajouter Fournisseurs</a>
                                </li>

                            </ul>

                        </li>
                        <li>
                            <a href="{{route('admin.achatfinancialcommitment.index')}}">
                                <i class="fa fa-calendar"></i>
                                <span>Décaissement</span>
                                @php
                                    $count_ech_a=\App\Models\AchatFinancialCommitment::whereNot('payment_status',2)->count();
                                    if($count_ech_a>0) echo '<span class="badge badge-success badge-pill float-right">'.$count_ech_a.'</span>';
                                @endphp
                            </a>
                        </li>

                    <li class="menu-title">Produits</li>
                    @canany(['Liste produits', 'Ajouter produits'])
                        <li>
                            <a href="javascript: void(0);">
                                <i class="fas fa-boxes"></i>
                                <span> Produits </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                @canany(['Liste produits'])
                                    <li>
                                        <a href="{{route('admin.products')}}">Produits</a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.products',1)}}">Fondant</a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.products',2)}}">Mini Fondant</a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.products',3)}}">Fondant Géant</a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.products',4)}}">Consommable</a>
                                    </li>
                                @endcanany
                                @canany(['Ajouter produits'])
                                    <li>
                                        <a href="{{route('admin.products.create')}}">Ajouter Produits</a>
                                    </li>
                                @endcanany

                            </ul>
                        </li>
                    @endcanany
                    @canany(['Liste catégorie', 'Liste goûts', 'Liste brands'])
                        <li>
                            <a href="javascript: void(0);">
                                <i class="fa fa-share-alt"></i>
                                <span> Catégories </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">

                                <li>
                                    <a href="{{route('admin.categories')}}">Liste</a>
                                </li>

                                <li>
                                    <a href="{{route('admin.categories.create')}}">Ajouter Catégorie</a>
                                </li>

                            </ul>
                        </li>


                        <li>
                            <a href="javascript: void(0);">
                                <i class="fa fa-tags"></i>
                                <span> Brands </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                @canany(['Liste brands'])
                                    <li>
                                        <a href="{{route('admin.brands')}}">Liste</a>
                                    </li>
                                @endcanany
                                @canany(['Ajouter brands'])
                                    <li>
                                        <a href="{{route('admin.brands.create')}}">Ajouter un Brand</a>
                                    </li>
                                @endcanany
                            </ul>
                        </li>
                        <li>
                            <a href="javascript: void(0);">
                                <i class="fa fa-prescription-bottle"></i>
                                <span> Goûts </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                @canany(['Liste goûts'])
                                    <li>
                                        <a href="{{route('admin.tastes')}}">Liste</a>
                                    </li>
                                @endcanany
                                @canany(['Liste goûts'])
                                    <li>
                                        <a href="{{route('admin.tastes.create')}}">Ajouter un Goût</a>
                                    </li>
                                @endcanany
                            </ul>
                        </li>
                    @endcanany
                    @canany(['Liste boutiques' , 'Liste admins' , 'Liste roles' , 'Liste banque'])
                        <li class="menu-title">Général</li>
                        @can('Liste boutiques')
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fa fa-store"></i>
                                    <span> Boutiques </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">

                                    <li>
                                        <a href="{{route('admin.stores')}}">Liste</a>
                                    </li>
                                    @can('Ajouter boutiques')
                                        <li>
                                            <a href="{{route('admin.stores.create')}}">Ajouter Boutique</a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endcan
                        @can('Liste admins')
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fa fa-users"></i>
                                    <span> Membres </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">

                                    <li>
                                        <a href="{{route('admin.admins')}}">Liste</a>
                                    </li>
                                    @can('Ajouter admins')
                                        <li>
                                            <a href="{{route('admin.admins.create')}}">Ajouter un Membre</a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endcan
                        @can('Liste roles')
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fa fa-users-cog"></i>
                                    <span> Rôles & Permissions </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">

                                    <li>
                                        <a href="{{route('admin.roles')}}">Liste</a>
                                    </li>
                                    @can('Ajouter roles')
                                        <li>
                                            <a href="{{route('admin.roles.create')}}">Ajouter Rôle</a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endcan
                        @can('Liste banque')
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fa fa-building"></i>
                                    <span> Banque </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">

                                    <li>
                                        <a href="{{route('admin.banks')}}">Liste</a>
                                    </li>
                                    @can('Ajouter boutiques')
                                        <li>
                                            <a href="{{route('admin.banks.create')}}">Ajouter Banque</a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endcan
                    @endcan
                    @canany(['Liste personnel' , 'Liste personnel' , 'Liste personnel' , 'Liste personnel'])
                        <li>
                            <a href="javascript: void(0);">
                                <i class="fa fa-id-card"></i>
                                <span> Personnel</span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                @can('Liste personnel')
                                    <li>
                                        <a href="{{route('admin.staffRequests')}}">Historique</a>
                                    </li>
                                @endcan
                                @can('Ajouter personnel')
                                    <li>
                                        <a href="{{route('admin.staffRequests.create')}}">Demander </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcan
                </ul>

            </div>
            <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
@stop