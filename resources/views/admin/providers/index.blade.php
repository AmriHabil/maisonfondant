@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Matjar</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Clients</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
                <h4 class="page-title">Clients List</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->





    <!-- end row-->


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">





                        <!-- end row-->


                        <div class="row">
                            <div class="col-12">


                                <div class="table-responsive">
                                        <table id="key-datatable" class="table table-striped dt-responsive nowrap">
                                            <thead>
                                            <tr>
                                                <th>Client</th>
                                                <th>MF</th>
                                                <th>Numéro</th>
                                                <th>Remise</th>
                                                <th>Plafond</th>
                                                <th width="100px">Action </th>

                                            </tr>
                                            </thead>

                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th class="searchable">Client</th>
                                                <th class="searchable">MF</th>
                                                <th class="searchable">Numéro</th>
                                                <th class="searchable">Remise</th>
                                                <th class="searchable">Plafond</th>
                                                <th></th>

                                            </tr>
                                            </tfoot>
                                        </table>
                                </div>

                            </div><!-- end col-->
                        </div>

                        <!-- end row-->



                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->





@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>

<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js" type="text/javascript"></script>



<!-- third party js ends -->
<!-- Datatables init -->
<script>
    $('#key-datatable tfoot .searchable').each(function () {
        var title = $(this).text();
        $(this).html('<input class="form-control" type="text" placeholder="Search ' + title + '" />');
    });
    $('#key-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.providers.getData') }}",
        columns: [
            { data: 'name', name: 'name' },
            { data: 'mf', name: 'mf' },
            { data: 'phone', name: 'phone' },
            { data: 'remise', name: 'remise' },
            { data: 'plafond', name: 'plafond' },
            { data: 'action', name: 'action' },
        ],
        order: [[0, 'desc']],
        aLengthMenu: [
            [10, 25, 50, 100, 200, -1],
            [10, 25, 50, 100, 200, "All"]
        ],
        dom: 'Blfrtip',
        responsive:false,
        initComplete: function () {
            // Apply the search
            this.api()
                .columns()
                .every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change clear', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
        },
    });
    $('body').on('click', '.main_select', function (e) {
        var check = $('.orders_tbl').find('tbody > tr > td:first-child .order_check');
        if ($('.main_select').prop("checked") == true) {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', true);
        } else {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', false);
        }

        $('.orders_tbl').find('tbody > tr > td:first-child .order_check').val();
    });
    $('.check').click(function () {

        var firstInput = $(this).find('input')[0];
        firstInput.checked = !firstInput.checked;
    });



    $('#sa-reset').click(function () {
        $("#StatusFilter,#customer_id").val("");
        $("#StatusFilter,#customer_id").select2({});
        $("#my_multi_select2").val('');
        $("#my_multi_select2").multiSelect('refresh');
    });



</script>


{{--ticket--}}
<script>

    $('input[type=radio][name=Type]').change(function () {
        if (this.value == 'price') {
            $('.ticket').hide();
            $('.ticket input').prop('required', false);
            $('.price').show();
            $('.price input').prop('required', true);
        } else if (this.value == 'receiver') {
            $('.ticket').hide();
            $('.ticket input').prop('required', false);
            $('.receiver').show();
            $('.receiver input').prop('required', true);
        } else if (this.value == 'description') {
            $('.ticket').hide();
            $('.ticket input').prop('required', false);
            $('.description').show();
            $('.description input').prop('required', true);
        } else if (this.value == 'cancel') {
            $('.ticket').hide();
            $('.ticket input').prop('required', false);
        }
    });
    $('.ticket_btn').click(function () {
        $('.ticket').hide();
        $('#ticket_form')[0].reset();
        $('#order_id').val($(this).attr('order_id'));
    });




    $('.tracking_btn').click(function () {
        $('#tracking_timeline').html('');
        var order_id = $(this).attr('order_id');

        $.ajax({
            type: 'POST',
            url: "/admin/orders/trackings/show/"+order_id ,
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: {order_id:order_id},
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                var html='';
                $('#SName').html(data.SName);
                $('#SNumber').html(data.SNumber);
                $('#SAddress').html(data.SAddress);
                $('#RName').html(data.RName);
                $('#RNumber').html(data.RNumber1);
                $('#RAddress').html(data.RAddress);
                $('.tracking_barcode').html(data.tracking_barcode);
                $('#details_order').html('<a href="/admin/orders/show/'+order_id+'" class="btn btn-info"><i class="fa fa-plus-circle"></i> Action</a>');
                if(data.tracking.length != 0){
                    for(var key in data.tracking) {
                        var value = data.tracking[key];
                        //html+='<div>'+key+':'+value+'</div>'
                        if(key%2==0){
                            html+='<article class="timeline-item timeline-item-left">';

                        }else{
                            html+='<article class="timeline-item">';
                        }
                        html+='    <div class="timeline-desk">\n' +
                            '          <div class="timeline-box">';
                        if(key%2==0){
                            html+='          <span class="arrow-alt"></span>\n' ;

                        }else{
                            html+='         <span class="arrow"></span>\n' ;
                        }
                        html+='              <span class="timeline-icon"><i class="mdi mdi-adjust"></i></span>\n' +
                            '                <h4 class="mt-0 font-16">'+value['created_at'].toLocaleString()+'</h4>\n' +
                            '\n' +
                            '                <p class="mb-0">'+value['Status']+' </p>\n' +
                            '             </div>\n' +
                            '          </div>\n' +
                            '      </article>';
                    }
                }
                $('#tracking_timeline').html(html);

            }, error: function (reject) {
                $('.is-invalid').removeClass('is-invalid');
                swal({
                    title: "Fail!",
                    text: "Please follow the instructions",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });

            }
        });
    });
</script>
{{--ticket--}}
<script>

    $('body').on('click', '.print_invoice', function (e) {
        var mydata = [];
        e.preventDefault();
        $('.orders_tbl > tbody  > tr').each(function () {
            var checkbox = $(this).find('td:first-child .order_check');
            if (checkbox.prop("checked") == true) {
                var order_id = $(checkbox).data('id');
                mydata.push(order_id);
            }
        });
        var order_data = mydata.join(',');

        $('#print_data').val(order_data);
        $('#bulk_submit').submit();
    })
</script>
<script>
    $(".datepicker").flatpickr({mode: "range"});
    $('.select2').select2();
    $('#my_multi_select2').multiSelect();
    $(".numeric").maxlength({

        warningClass: "badge badge-success",
        limitReachedClass: "badge badge-danger"
    })
</script>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

