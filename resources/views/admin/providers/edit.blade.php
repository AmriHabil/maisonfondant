@section('content')




    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Fournisseur</a></li>
                        <li class="breadcrumb-item active">Modification</li>
                    </ol>
                </div>
                <h4 class="page-title">Modifier un fournisseur</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <form action="{{route('admin.providers.update',$provider->id)}}" method="POST" id="product_store">
        @csrf
        <div class="row">


            <div class="col-lg-12">

                <div class="card-box">
                    <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">Details Client</h5>
                    <div class="form-group mb-3">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="name">Nom Commercial <span class="text-danger">*</span></label>
                                <input type="text" id="name" class="form-control" placeholder="Nom Commercial" name="name" value="{{$provider->name}}">
                                <ul class="parsley-errors-list filled" id="name_errors">

                                </ul>
                            </div>
                            <div class="col-md-3">
                                <label for="price">Type de prix <span class="text-danger">*</span></label>
                                <select  id="price" class="form-control" name="price" >
                                    <option value="detail" @if($provider->id=='detail') selected @endif>detail</option>
                                    <option value="gros" @if($provider->id=='gros') selected @endif>gros</option>
                                    <option value="sgros" @if($provider->id=='sgros') selected @endif>sgros</option>
                                </select>
                                <ul class="parsley-errors-list filled" id="name_errors">

                                </ul>
                            </div>
                            <div class="col-md-3">
                                <label for="mf">MF </label>
                                <input type="text" id="mf" class="form-control" placeholder="MF" name="mf" value="{{$provider->mf}}">
                                <ul class="parsley-errors-list filled" id="mf_errors">

                                </ul>
                            </div>
                            <div class="col-md-3">
                                <label for="address">Adresse </label>
                                <input type="text" id="address" class="form-control" placeholder="Adresse" name="address"   value="{{$provider->address}}">
                                <ul class="parsley-errors-list filled" id="address_errors">

                                </ul>
                            </div>
                            <div class="col-md-3">
                                <label for="phone">Numéro</label>
                                <input type="text" id="phone" class="form-control" placeholder="Numéro" name="phone"  value="{{$provider->phone}}">
                                <ul class="parsley-errors-list filled" id="phone_errors">

                                </ul>
                            </div>
                            <div class="col-md-3">
                                <label for="remise">Remise Maximal </label>
                                <input type="number" id="remise" class="form-control" placeholder="remise" name="remise"  value="{{$provider->remise}}">
                                <ul class="parsley-errors-list filled" id="remise_errors">

                                </ul>
                            </div>
                            <div class="col-md-3">
                                <label for="rib">RIB </label>
                                <input type="text" id="rib" class="form-control" placeholder="rib" name="rib"  value="{{$provider->rib}}">
                                <ul class="parsley-errors-list filled" id="rib_errors">

                                </ul>
                            </div>
                            <div class="col-md-3">
                                <label for="email">Email </label>
                                <input type="text" id="email" class="form-control" placeholder="email" name="email"  value="{{$provider->email}}">
                                <ul class="parsley-errors-list filled" id="email_errors">

                                </ul>
                            </div>
                            <div class="col-md-3">
                                <label for="manager">Manager </label>
                                <input type="text" id="manager" class="form-control" placeholder="manager" name="manager"  value="{{$provider->manager}}">
                                <ul class="parsley-errors-list filled" id="manager_errors">

                                </ul>
                            </div>
                            <div class="col-md-3">
                                <label for="tva">Website </label>
                                <input type="text" id="product-reference" class="form-control" placeholder="www.Client.com" name="website"  value="{{$provider->website}}">
                                <ul class="parsley-errors-list filled" id="website_errors">

                                </ul>
                            </div>
                            <div class="col-md-3">
                                <label for="remise">Page facebook></label>
                                <input type="number" id="product-reference" class="form-control" placeholder="Page Facebook" name="facebook" value="{{$provider->facebook}}" >
                                <ul class="parsley-errors-list filled" id="facebook_errors">

                                </ul>
                            </div>
                        </div>
                        {{--                        <h5 class="text-uppercase mt-0 mb-3 mt-3 bg-light p-2">Details Complémentaire</h5>--}}
                        {{--                        <div class="row">--}}
                        {{--                            <div  style="width: 80px">--}}
                        {{--                                <label for="res_quantity">Statut <span class="text-danger">*</span></label>--}}
                        {{--                                <div class="switchery-demo">--}}
                        {{--                                    <input type="checkbox"  data-plugin="switchery" data-color="#039cfd" class="js-switch" name="status" value="1" checked>--}}
                        {{--                                    <ul class="parsley-errors-list filled" id="Timbre_errors">--}}

                        {{--                                    </ul>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                            <div style="width: 80px">--}}
                        {{--                                <label for="res_quantity">Plafond </label>--}}
                        {{--                                <div class="switchery-demo">--}}
                        {{--                                    <input type="checkbox"  data-plugin="switchery" data-color="#039cfd" class="js-switch" name="switchplafond" value="1" id="switchplafond">--}}
                        {{--                                    <ul class="parsley-errors-list filled" id="Timbre_errors">--}}

                        {{--                                    </ul>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                            <div class="col-md-4 " id="div_plafond" style="display: none">--}}
                        {{--                                <label for="plafond">Plafond de l'encours <span class="text-danger">*</span></label>--}}
                        {{--                                <input type="number" id="plafond" class="form-control" placeholder="Plafond" name="plafond" >--}}
                        {{--                                <ul class="parsley-errors-list filled" id="plafond_errors">--}}

                        {{--                                </ul>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                </div> <!-- end col-->
            </div>
            <!-- end col-->
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="text-center mb-3">
                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light" id="sa-success">Ajouter un fournisseur</button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>





@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<link href="{{asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/general.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css"/>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<!-- Loading buttons js -->
<script src="{{asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{asset('assets/libs/ladda/ladda.js')}}"></script>
<script src="{{asset('assets/js/pages/loading-btn.init.js')}}"></script>
<script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('assets/libs/switchery/switchery.min.js')}}"></script>

<script>
    $( "#switchplafond" ).change(function() {

        $( "#div_plafond" ).toggle();
    });
    $("#product_store").submit(function (e) {
        e.preventDefault();
        var Form=$("#product_store")[0];
        var formData= new FormData(Form);
        $.ajax({
            type: 'POST',
            url:"{{route('admin.providers.update',$provider->id)}}",
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success:function(data)
            {
                $('.is-invalid').removeClass('is-invalid');
                $('.parsley-required').remove();
                $('#preloader').show();
                $.toast({
                    heading: 'Succès',
                    text: 'Fournisseur créé avec succés',
                    icon: 'success',
                    loader: true,
                    position:'top-right',// Change it to false to disable loader
                    loaderBg: '#5ba035',  // To change the background
                    bgColor: '#1abc9c',  // To change the background
                });

            }, error: function (reject) {
                $('.is-invalid').removeClass('is-invalid');
                swal({
                    title: "Erreur!",
                    text: "Merci de suivre les indications",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });
                var response=$.parseJSON(reject.responseText);
                $.each(response.errors,function(key,val){
                    $("#" + key ).addClass('is-invalid');
                    $("#" + key + "_errors").html('<li class="parsley-required">'+val+'</li>');

                });
            }
        });

    });
    $(".select2").select2();
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });
</script>
<style>
    input.chk-btn {
        display: none;
    }
    .label-chk-btn{
        width: 60px;
    }
    .label-chk-btn img{
        filter: grayscale(100%);
        height: 60px;
        transition: ease .3s;
        width: 45px;
        margin: auto;
        display: block;
    }

    input.chk-btn:checked + label img{
        transform: scale(1.1);
        filter: grayscale(0);
    }
    .is-invalid{
        border-color: #dc3545;
        padding-right: calc(1.5em + .75rem);
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-position: right calc(.375em + .1875rem) center;
        background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
