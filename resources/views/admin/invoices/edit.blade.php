@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.invoices')}}">Facture</a></li>
                        <li class="breadcrumb-item active">Modification</li>
                    </ol>
                </div>
                <h4 class="page-title">Modifier une facture</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <form action="{{route('admin.invoices.update',$invoice->id)}}" method="POST" id="category_update"  enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <h5 class="text-uppercase bg-light p-2 mt-0 mb-3">Général</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group mb-3">
                                <label for="codification">Référence <span class="text-danger">*</span></label>
                                <input type="text" id="codification" class="form-control" placeholder="e.g : 2024-0001 " name="codification" required value="{{$invoice->codification}}">
                                <ul class="parsley-errors-list filled" id="name_errors">
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group mb-3">
                                <label for="ht">HT <span class="text-danger">*</span></label>
                                <input type="number" id="ht" class="form-control" placeholder="e.g : HT " name="ht" required value="{{$invoice->ht}}" step="0.001">
                                <ul class="parsley-errors-list filled" id="name_errors">
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group mb-3">
                                <label for="ttc">TTC <span class="text-danger">*</span></label>
                                <input type="number" id="ttc" class="form-control" placeholder="e.g : TTC " name="ttc" required value="{{$invoice->ttc}}" step="0.001">
                                <ul class="parsley-errors-list filled" id="name_errors">
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group mb-3">
                                <label for="timbre">Timbre <span class="text-danger">*</span></label>
                                <input type="number" id="timbre" class="form-control" placeholder="e.g : Timbre" name="timbre" required value="{{$invoice->timbre}}" step="0.001">
                                <ul class="parsley-errors-list filled" id="name_errors">
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group mb-3">
                                <label for="date">Date  <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" name="date"  id="date" required value="{{$invoice->date}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group mb-3">
                                <label for="store_id">Boutique <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="store_id" name="store_id">
                                    <option value="">Choisir PTV</option>
                                    @foreach($stores as $store)
                                        <option value="{{$store->id}}" @if( $invoice->store_id==$store->id) selected @endif>{{$store->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-1">
                            <input type="file"  class="form-control" name="images[]"   >
                        </div>
                        <div class="col-md-6 mb-1">
                            <input type="text"  class="form-control" name="description[]" placeholder="Description"  >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="button" class="btn btn-info btn-rounded waves-effect waves-light ml-1 addResources">
                                    <span class="btn-label"><i class="fe-link"></i></span> Ressources
                                </button>
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-box -->
            </div> <!-- end col -->
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class=" mb-3">



                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light" id="sa-success">{{__('Enregistrer')}}</button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>






@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<link href="{{URL::asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('assets/css/general.css')}}" rel="stylesheet" type="text/css" />
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}




<!-- Loading buttons js -->
<script src="{{URL::asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{URL::asset('assets/libs/ladda/ladda.js')}}"></script>

<!-- Buttons init js-->
<script src="{{URL::asset('assets/js/pages/loading-btn.init.js')}}"></script>
<script>
    $("#sa-success").click(function (e) {
        e.preventDefault();
        $('.parsley-errors-list').html('');
        var Form=$(this).parents('form:first')[0];
        var formData= new FormData(Form);
        $.ajax({
            type: 'POST',
            url:"{{route('admin.invoices.update',$invoice->id)}}",
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success:function(data)
            {
                $('.is-invalid').removeClass('is-invalid');

                $.toast({
                    heading: 'Succès',
                    text: 'Catégorie modifiée avec succés',
                    icon: 'success',
                    loader: true,
                    position:'top-right',// Change it to false to disable loader
                    loaderBg: '#5ba035',  // To change the background
                    bgColor: '#1abc9c',  // To change the background
                });

            }, error: function (reject) {
                $('.is-invalid').removeClass('is-invalid');
                swal({
                    title: "Fail!",
                    text: "Please follow the instructions",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });
                var response=$.parseJSON(reject.responseText);
                $.each(response.errors,function(key,val){
                    $("#" + key ).addClass('is-invalid');
                    $("#" + key + "_errors").html('<li class="parsley-required">'+val+'</li>');

                });
            }
        });

    });

</script>
<script>
    /*Adding addSection*/
    $(document).on('click', '.addResources', function () {

        let add = `
                        <div class="row">
                             <div class="col-md-6 mb-1">
                                 <input type="file"  class="form-control" name="images[]"   >
                             </div>
                             <div class="col-md-6 mb-1">
                                  <input type="text"  class="form-control" name="description[]" placeholder="Description"  >
                              </div>
                        </div>
        `;
        $(this).parent().prepend(add);
    });

    /*Adding addSection*/
</script>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
