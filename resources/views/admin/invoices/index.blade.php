@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.categories')}}">Facturation</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
                <h4 class="page-title">Liste des factures</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- end row-->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <div class="table-responsive">
                            <table id="key-datatable" class="table table-striped dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>Ref</th>
                                    <th>Type</th>
                                    <th>Boutique</th>
                                    <th>Facture</th>
                                    <th>Date</th>
                                    <th>Timbre</th>
                                    <th>HT</th>
                                    <th>TTC</th>
                                    <th>Action </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- end col-->
                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')
@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .dt-buttons {
        float: right;
    }
    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<!-- third party js ends -->
<!-- Datatables init -->
<script>
    let table=$('#key-datatable').DataTable({
        processing: true,
        serverSide: true,
        responsive:false,
        type:'POST',
        ajax: "{{ route('admin.invoices.getData') }}",

        columns: [
            { data: 'id', name: 'id' },
            { data: 'type', name: 'type' },
            { data: 'store', name: 'stores.name' },
            { data: 'codification', name: 'codification' },
            { data: 'date', name: 'date' },
            { data: 'timbre', name: 'timbre' },
            { data: 'ht', name: 'ht' },
            { data: 'ttc', name: 'ttc' },
            { data: 'action', name: 'action' },

        ],
        order: [[0, 'desc']],
        buttons: [
            'copy', 'excel'
        ],

        aLengthMenu: [
            [10, 25, 50, 100, 200, -1],
            [10, 25, 50, 100, 200, "All"]
        ],
        dom: 'Brtip',
    });
    $('body').on('click', '.main_select', function (e) {
        var check = $('.orders_tbl').find('tbody > tr > td:first-child .order_check');
        if ($('.main_select').prop("checked") == true) {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', true);
        } else {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', false);
        }
        $('.orders_tbl').find('tbody > tr > td:first-child .order_check').val();
    });
    $('.check').click(function () {
        var firstInput = $(this).find('input')[0];
        firstInput.checked = !firstInput.checked;
    });
</script>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@include('admin.end')

