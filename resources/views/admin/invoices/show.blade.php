@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.invoices')}}">Factures</a></li>
                        <li class="breadcrumb-item active">Détails</li>
                    </ol>
                </div>
                <h4 class="page-title">Créer une Facture</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <!-- Logo & title -->
                <div class="clearfix">
                    <div class="float-left">
                        <img src="{{asset('assets/images/logo-dark.png')}}" alt="" height="150">
                    </div>
                    <div class="float-right">
                        <h2 class="m-0 d-print-none">{{strtoupper($invoice->type).' - '.strtoupper($invoice->codification)}}</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        {{\App\Helpers\ImageHelper::showFiles($invoice->images)}}

                    </div><!-- end col -->
                    <div class="col-md-4 offset-md-2">
                        <div class="mt-3 float-right">
                            <h4 class="m-b-10"><strong>Date : </strong> <span class="float-right"> {{$invoice->date}}</span></h4>
                            <h4 class="m-b-10"><strong>HT : </strong> <span class="float-right">{{number_format($invoice->ht,3)}}</span></h4>
                            <h4 class="m-b-10"><strong>TTC  : </strong> <span class="float-right">{{number_format($invoice->ttc,3)}} </span></h4>
                            <h4 class="m-b-10"><strong>Timbre  : </strong> <span class="float-right">{{number_format($invoice->timbre,3)}} </span></h4>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->



            </div> <!-- end card-box -->
        </div> <!-- end col -->
    </div>
@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')
@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<link href="{{asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/general.css')}}" rel="stylesheet" type="text/css" />
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- Loading buttons js -->
<link href="{{asset('assets/libs/select2/select2.min.js')}}" rel="stylesheet" type="text/css" />
<script src="{{asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{asset('assets/libs/ladda/ladda.js')}}"></script>
<script src="{{asset('assets/js/pages/loading-btn.init.js')}}"></script>
<script>
    $("#sa-success").click(function (e) {
        e.preventDefault();
        $('.parsley-errors-list').html('');
        var Form=$(this).parents('form:first')[0];
        var formData= new FormData(Form);
        $.ajax({
            type: 'POST',
            url:"{{route('admin.invoices.update',$invoice->id)}}",
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success:function(data)
            {
                $('.is-invalid').removeClass('is-invalid');

                $.toast({
                    heading: 'Succès',
                    text: 'Catégorie modifiée avec succés',
                    icon: 'success',
                    loader: true,
                    position:'top-right',// Change it to false to disable loader
                    loaderBg: '#5ba035',  // To change the background
                    bgColor: '#1abc9c',  // To change the background
                });

            }, error: function (reject) {
                $('.is-invalid').removeClass('is-invalid');
                swal({
                    title: "Fail!",
                    text: "Please follow the instructions",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });
                var response=$.parseJSON(reject.responseText);
                $.each(response.errors,function(key,val){
                    $("#" + key ).addClass('is-invalid');
                    $("#" + key + "_errors").html('<li class="parsley-required">'+val+'</li>');

                });
            }
        });

    });

</script>
<style>
    input.chk-btn {
        display: none;
    }
    .label-chk-btn{
        width: 60px;
    }
    .label-chk-btn img{
        filter: grayscale(100%);
        height: 60px;
        transition: ease .3s;
        width: 45px;
        margin: auto;
        display: block;
    }
    input.chk-btn:checked + label img{
        transform: scale(1.1);
        filter: grayscale(0);
    }
    .is-invalid{
        border-color: #dc3545;
        padding-right: calc(1.5em + .75rem);
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-position: right calc(.375em + .1875rem) center;
        background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
