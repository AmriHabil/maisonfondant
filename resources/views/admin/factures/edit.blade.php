@section('content')




    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Produits</a></li>
                        <li class="breadcrumb-item active">Création</li>
                    </ol>
                </div>
                <h4 class="page-title">Créer une pièce de vente</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <form action="{{route('admin.facture.update',$facture->id)}}" method="POST" id="cartStore" enctype="multipart/form-data"  target="_blank"  onsubmit="setTimeout(function(){location.reload();}, 1000);return true;">

        @csrf
        <div class="row">
            <div class="col-lg-12">

                <div class="card-box ribbon-box">
                    <div class="ribbon-two ribbon-two-info"><span><i class="fa fa-user-cog"></i> Client</span></div>
                    <div class="row justify-content-center">
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="client_id">Client <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="client_id" name="client_id" required>
                                    <option value="0"
                                            data-name="Passager"
                                            data-phone="12345678"
                                            data-mf="1234567890"
                                            data-address="Adresse Passager" >
                                        Client Passager
                                    </option>
                                    @foreach($clients as $client)
                                        <option value="{{$client->id}}"
                                                data-name="{{$client->name}}"
                                                data-phone="{{$client->phone}}"
                                                data-mf="{{$client->mf}}"
                                                data-address="{{$client->address}}"
                                                data-plafond="{{$client->plafond??'Aucun Plafond'}}"
                                                data-encours="{{$client->encours??0}}"
                                                data-remise="{{$client->remise??0}}"
                                                data-price="{{$client->price??0}}"
                                                @if($facture->client_id==$client->id) selected @endif
                                        >
                                            {{$client->mf}} - {{$client->name}} - {{$client->phone}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="client_name">Nom <span class="text-danger">*</span></label>
                                <input type="text" id="client_name" class="form-control" placeholder="e.g : Apple iMac"
                                       name="client_details[client_name]" required  value="{{$facture->client_details['client_name']}}">
                                <input type="hidden" id="remise_client" value="0">
                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="client_mf">MF <span class="text-danger">*</span></label>
                                <input type="text" id="client_mf" class="form-control" placeholder="e.g : Apple iMac"
                                       name="client_details[client_mf]" value="{{$facture->client_details['client_mf']}}">
                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="client_phone">Téléphone <span class="text-danger">*</span></label>
                                <input type="text" id="client_phone" class="form-control" placeholder="e.g : Apple iMac"
                                       name="client_details[client_phone]"  value="{{$facture->client_details['client_phone']}}">
                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="client_address">Adresse <span class="text-danger">*</span></label>
                                <input type="text" id="client_address" class="form-control" placeholder="e.g : Apple iMac"
                                       name="client_details[client_address]"  value="{{$facture->client_details['client_address']}}">
                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="date">Date <span class="text-danger">*</span></label>
                                <input type="date" id="date" class="form-control datepicker" placeholder="e.g : Apple iMac"
                                       name="date" value="{{$facture->date}}">

                            </div>
                        </div>
                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="number">Numéro <span class="text-danger">*</span></label>
                                <input type="text" id="number" class="form-control " placeholder="e.g : Apple iMac"
                                       name="number" value="{{$facture->number}}">

                            </div>
                        </div>

                        <div class="col-lg-2  col-md-3">
                            <div class="form-group">
                                <label for="store_id">Boutique <span class="text-danger">*</span></label>
                                <select name="store_id" id="" class="form-control">
                                    @foreach(\App\Helpers\WebHelper::allStores() as $store)
                                        @if($store->id!=1)
                                        <option value="{{$store->id}}" @if($facture->store_id==$store->id) selected @endif>{{$store->name}}</option>
                                        @endif
                                    @endforeach
                                </select>

                            </div>
                        </div>


                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-2 col-md-4 text-center">
                            <p><b>Type de prix:</b> <span id="client_price">detail</span></p>
                        </div>
                        <div class="col-lg-2 col-md-4 text-center">
                            <p><b>En Cours:</b> <span id="encours"></span> TND</p>
                        </div>

                    </div>
                </div> <!-- end card-box -->

            </div> <!-- end col -->

            <div class="col-lg-12">

                <div class="card-box  ribbon-box">
                    <div class="ribbon-two ribbon-two-success"><span><i class="fa fa-list"></i> Détails</span></div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th width="300px">Article</th>
                                <th>Qté</th>
                                <th>Remise</th>
                                <th>TVA</th>
                                <th>P.U</th>
                                <th>Total</th>
                                <th width="30px">Action</th>
                            </tr>
                            </thead>
                            <tbody  id="cartItems">
                                @foreach($facture->details as $detail)
                                    <tr id="row{{$detail['product_id']}}" class="item_cart"  data-id="{{$detail['product_id']}}">
                                        <td>
                                            <input type="hidden" name="details[{{$detail['product_id']}}][product_id]" value="{{$detail['product_id']}}">
                                            <input type="hidden" name="details[{{$detail['product_ref']}}][product_id]" value="{{$detail['product_ref']}}">
                                            <input type="hidden" name="details[{{$detail['product_id']}}][product_bonus]" value="{{$detail['product_bonus']}}">
                                            <input type="text" id="name_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_name]" class="form-control name"  value="{{$detail['product_name']}}" style="width: 300px" required/>
                                        </td>
                                        <td>
                                            <input type="number" id="quantity_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_quantity]" class="form-control quantity withPopover " data-product_id="{{$detail['product_id']}}" value="{{$detail['product_quantity']}}" step="1" min="1"
                                                   oninput="this.value = Math.floor(this.value);" required
                                            >
                                        </td>
                                        <td>
                                            <input type="number" id="remise_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_remise]" class="form-control remise" data-product_id="{{$detail['product_id']}}" value="{{number_format($detail['product_remise'],2)}}" min="0" max="100" step="0.01" required>
                                        </td>
                                        <td>
                                            <input type="text" id="tva_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_tva]" class="form-control tva" data-product_id="{{$detail['product_id']}}" value="{{number_format($detail['product_tva'],2)}}" readonly required>
                                        </td>
                                        <td>
                                            <input type="text" id="price_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_price_selling]" class="form-control price price_u withPopover number_format_3" data-product_id="{{$detail['product_id']}}" data-product_grade="" value="{{$detail['product_price_selling']}}"
                                                    required>
                                            <input type="hidden" name="details[{{$detail['product_id']}}][product_price_buying]" value="{{$detail['product_price_buying']}}">
                                        </td>
                                        <td>
                                            <input type="text" id="total_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_total]"  class="form-control price number_format_3" data-product_id="{{$detail['product_id']}}" value="{{number_format(($detail['product_price_selling']-$detail['product_remise'])*$detail['product_quantity'],3)}}" readonly required>
                                        </td>
                                        <td>
                                            <a data-tr="row{{$detail['product_id']}}" class="text-danger removeRow"><i class="fa fa-minus-circle"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <select class="form-control select2 productList" id="productList" name="">
                                        <option value=""></option>
                                        @foreach($products as $product)
                                            <option value="{{$product->id}}"
                                                    data-name="{{$product->name??''}}"
                                                    data-barcode="{{$product->barcode??''}}"
                                                    data-ref="{{$product->ref??''}}"
                                                    data-tva="{{$product->tva??'0'}}"
                                                    data-unity="{{$product->unity??'u'}}"
                                                    data-buying="{{$product->buying??'0'}}"
                                                    data-price="{{$product->detail??'0'}}"
                                                    data-price-detail="{{$product->detail??'0'}}"
                                                    data-price-gros="{{$product->gros??'0'}}"
                                                    data-price-sgros="{{$product->sgros??'0'}}"
                                                    data-bonus="{{$product->bonus??'0'}}"
                                                    data-min_price="{{$product->min_price??'0'}}"
                                                    data-quantity="{{$product->quantity??'0'}}">
                                                {{$product->name}} - {{$product->barcode}}
                                            </option>
                                        @endforeach
                                    </select> 
                                </td>
                                <td><span class="withPopover" data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="La quantité actuelle de cette article est " data-original-title="Raccourci"
                                    ><i class="fa fa-info-circle"></i></span> Recherche</td>
                            </tr>

                            </tfoot>
                        </table>
                    </div>
                    <audio src="{{asset('assets/sound/success.mp3')}}"></audio>
                    <audio src="{{asset('assets/sound/warning.mp3')}}"></audio>

                </div> <!-- end col-->

            </div> <!-- end col-->
            <div class="col-lg-12" id="payment_section">

                <div class="card-box  ribbon-box">
                    <div class="ribbon-two ribbon-two-warning"><span><i class="fa fa-dollar-sign"></i> Paiement</span></div>
                    <div class="row">

                        <div class="col-md-8" id="comptant">

                        </div>
                        <div class="col-md-8 " id="commitment">
                            <div id="echeance">

                            </div>


                            <div class="col-12 text-center">
                                <a class="btn btn-sm btn-info text-white" id="addCommitment">Ajouter une échéance</a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="">
                                <h5>Résumé</h5>
                                <p><b>TVA:</b> <span class="float-right" id="totalTva">0.000</span></p>
                                <p><b>Remise :</b> <span class="float-right" id="totalRemise"> &nbsp;&nbsp;&nbsp; 0.000</span></p>
                                <hr>
                                <p class="font-24">A Payer<span class="float-right" id="totalGlobal">0.000</span></p>
                            </div>
                            <div class="clearfix"></div>

                        </div>

                    </div>

                </div> <!-- end col-->


            </div> <!-- end col-->

        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-12">
                <div class="text-center mb-3">


                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light submit" id="sa-a4" value="a4" name="submit" >Imprimer A4
                    </button>
                    <button type="submit" class="btn w-sm btn-warning waves-effect waves-light submit" id="sa-ticket" value="ticket" name="submit">Imprimer Ticket
                    </button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>



    <style>
        .select2 {
            width: 100%!important;
        }
        .popover-title{
            background: #ffff99;
        }
    </style>

@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/css/general.css')}}" rel="stylesheet" type="text/css"/>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<!-- Loading buttons js -->
<script src="{{asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{asset('assets/libs/ladda/ladda.js')}}"></script>
<script src="{{asset('assets/js/pages/loading-btn.init.js')}}"></script>

<script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('assets/js/pages/add-product.init.js')}}"></script>
{{--<script src="{{asset('assets/js/validation.js')}}"></script>--}}

<script>
    $(document).ready(function(){
        calculate_gtotal();
    });
    $(document).on('change', '#packList', function () {
        $.ajax({
            type: 'POST',
            url: "/admin/pack/details/"+$(this).val(),
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },

            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                $.each(data, function(k, v) {
                    let product = {
                        id:v.product_id,
                        name:v.product_name,
                        unity:v.product_unity,
                        tva:v.product_tva,
                        remise:v.product_remise,
                        buying:v.product_price_buying,
                        price:v.product_price_selling,
                        quantity:v.product_quantity,
                    };
                    let alert='';
                    if (product.quantity==0) alert='alert-danger';
                    tr=`<tr  class="item_cart pack${v.pack_id}"  data-id="${product.id}">
                   <td>
                        <input type="hidden" name="details[${product.id}][product_id]" value="${product.id}">
                       <input type="text" id="name_id_${product.id}" name="details[${product.id}][product_name]" class="form-control name"  value="${product.name}" style="width: 300px" required/>
                    </td>
                    <td>
                       <input type="number" id="quantity_id_${product.id}" name="details[${product.id}][product_quantity]" class="form-control ${alert} quantity withPopover " data-product_id="${product.id??0}" value="${product.quantity}" step="1" min="1"
                        oninput="this.value = Math.floor(this.value);" data-max="${product.quantity??0}" readonly
                        data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="La quantité actuelle de cette article est : ${product.quantity}" data-original-title="Information du stock" required
                        >
                    </td>
                    <td>
                       <input type="number" id="remise_id_${product.id}" name="details[${product.id}][product_remise]" class="form-control remise" data-product_id="${product.id}" value="${product.remise??client_remise}" min="0" max="100" step="0.01" required readonly>
                    </td>
                    <td>
                       <input type="text" id="tva_id_${product.id}" name="details[${product.id}][product_tva]" class="form-control tva" data-product_id="${product.id}" value="${product.tva??0}"  required>
                    </td>
                   <td>
                       <input type="text" id="price_id_${product.id}" name="details[${product.id}][product_price_selling]" class="form-control price price_u withPopover number_format_3" data-product_id="${product.id}" data-product_grade="" value="${product.price??0}" data-min="${product.min_price}" min="${product.min_price}"
                        data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="Le prix limite de cet article est : ${product.min_price}" data-original-title="Prix de revient" required >
                        <input type="hidden" name="details[${product.id}][product_price_buying]" value="${product.buying}">
                    </td>
                    <td>
                       <input type="text" id="total_id_${product.id}" name="details[${product.id}][product_total]"  class="form-control price" data-product_id="${product.id}" value="${product.price??0}" readonly required  min="${product.min_price}">
                    </td>
                    <td>
                        <a data-tr="pack${v.pack_id}" class="text-danger removePack"><i class="fa fa-minus-circle"></i></a>
                   </td>
                </tr>`;
                    $('#cartItems').append(tr);
                    $('.withPopover').popover({offset: 10});

                });
                calculate_gtotal();
            }, error: function (reject) {
                $('.is-invalid').removeClass('is-invalid');
                swal({
                    title: "Erreur!",
                    text: "Merci de suivre les indications",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });
                var response = $.parseJSON(reject.responseText);
                $.each(response.errors, function (key, val) {
                    $("#" + key).addClass('is-invalid');
                    $("#" + key + "_errors").html('<li class="parsley-required">' + val + '</li>');

                });
            }
        });
    });

    $('.withPopover').popover({offset: 10});
    var productListAfterSelect2 = $("#productList").select2();
    $(window).on('load', function() {
        productListAfterSelect2.select2("open");
    });
    $(".datepicker").flatpickr();

    $(document).on('change', '#productList', function () {
        addProduct(this);
    });
    $(document).on('click', '.removeRow', function () {
        let tr = $(this).attr("data-tr");
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $('#'+tr).remove();
                calculate_gtotal();
            }
        });

    });





    $(document).on('change', '.quantity,.remise,.price_u,#remiseFact', function() {
        calculate_gtotal();
    });
    $(document).on('change', '.quantity', function() {
        checkQuantity(this);

    });

    $(document).on('change', '.price_u', function() {
        let min=parseFloat($(this).attr('data-min'));
        let id=parseFloat($(this).attr('data-product_id'));
        if(parseFloat($(this).val())<min){
            $.toast({
                heading: 'Dépassage du remise maximale',
                text: 'Le prix d\'achat actuel est '+min,
                icon: 'error',
                loader: true,
                position:'top-right',// Change it to false to disable loader
                loaderBg: '#f1556c',  // To change the background
                bgColor: '#f1556c',  // To change the background
            });
            //$(this).val(max);
            $(this).addClass('alert-danger');
        }else{
            $(this).removeClass('alert-danger');
        }
        $(this).val(parseFloat($(this).val()).toFixed(3));
    });

    function addProduct(select) {
        let priceType=$('#client_price').text().toLowerCase();
        let product = {
            id:select.value,
            name:select.options[select.selectedIndex].getAttribute('data-name'),
            ref:select.options[select.selectedIndex].getAttribute('data-ref'),
            unity:select.options[select.selectedIndex].getAttribute('data-unity'),
            tva:select.options[select.selectedIndex].getAttribute('data-tva'),
            buying:select.options[select.selectedIndex].getAttribute('data-buying'),
            price:select.options[select.selectedIndex].getAttribute('data-price-'+priceType),
            min_price:select.options[select.selectedIndex].getAttribute('data-min_price'),
            bonus:select.options[select.selectedIndex].getAttribute('data-bonus'),
            quantity:select.options[select.selectedIndex].getAttribute('data-quantity'),

        };
        let client_remise=$('#remise_client').val();
        if($('#quantity_id_'+product.id).length){
            $('#quantity_id_'+product.id).val(parseFloat($('#quantity_id_'+product.id).val())+1);
            checkQuantity($('#quantity_id_'+product.id));
            playSound();
        }
        else{
            let alert='';
            if (product.quantity==0) alert='alert-danger';
            tr=`<tr id="row${product.id}" class="item_cart"  data-id="${product.id}">
                   <td>
                        <input type="hidden" name="details[${product.id}][product_id]" value="${product.id}">
                        <input type="hidden" name="details[${product.id}][product_bonus]" value="${product.bonus}">
                        <input type="hidden" name="details[${product.id}][product_ref]" value="${product.ref}">
                       <input type="text" id="name_id_${product.id}" name="details[${product.id}][product_name]" class="form-control name"  value="${product.name}" style="width: 300px" required/>
                    </td>
                    <td>
                       <input type="number" id="quantity_id_${product.id}" name="details[${product.id}][product_quantity]" class="form-control ${alert} quantity withPopover " data-product_id="${product.id??0}" value="1" step="1" min="1"
                        oninput="this.value = Math.floor(this.value);" data-max="${product.quantity??0}"
                        data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="La quantité actuelle de cette article est : ${product.quantity}" data-original-title="Information du stock" required
                        >
                    </td>
                    <td>
                       <input type="number" id="remise_id_${product.id}" name="details[${product.id}][product_remise]" class="form-control remise" data-product_id="${product.id}" value="0" min="0" max="100" step="0.01" required>
                    </td>
                    <td>
                       <input type="text" id="tva_id_${product.id}" name="details[${product.id}][product_tva]" class="form-control tva" data-product_id="${product.id}" value="${product.tva??0}" readonly required>
                    </td>
                   <td>
                       <input type="text" id="price_id_${product.id}" name="details[${product.id}][product_price_selling]" class="form-control price price_u withPopover number_format_3" data-product_id="${product.id}" data-product_grade="" value="${product.price??0}" data-min="${product.min_price}" min="${product.min_price}"
                        data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="Le prix limite de cet article est : ${product.min_price}" data-original-title="Prix de revient" required >
                        <input type="hidden" name="details[${product.id}][product_price_buying]" value="${product.buying}">
                    </td>
                    <td>
                       <input type="text" id="total_id_${product.id}" name="details[${product.id}][product_total]"  class="form-control price" data-product_id="${product.id}" value="${product.price??0}" readonly required  min="${product.min_price}">
                    </td>
                    <td>
                        <a data-tr="row${product.id}" class="text-danger removeRow"><i class="fa fa-minus-circle"></i></a>
                   </td>
                </tr>`;
            $('#cartItems').append(tr);
            $('.withPopover').popover({offset: 10});
            playSound();
        }
        calculate_gtotal();
        openAndFocus('productList');

    }
    function playSound() {
        $('audio')[0].load();
        $('audio')[0].play();
    }



    function checkQuantity(element){
        let max=parseFloat($(element).attr('data-max'));
        let id=parseFloat($(element).attr('data-product_id'));
        if(parseFloat($(element).val())>max){
            $.toast({
                heading: 'Dépassage du stock',
                text: 'La quantité actuelle est '+max,
                icon: 'error',
                loader: true,
                position:'top-right',// Change it to false to disable loader
                loaderBg: '#f1556c',  // To change the background
                bgColor: '#f1556c',  // To change the background
            });
            //$(this).val(max);
            $(element).addClass('alert-danger');
        }else{
            $(element).removeClass('alert-danger');
        }
    }

    function calculate_gtotal() {
        $('#cartItems').each(function () {
            let totalTva = 0;
            let totalRemise = 0;
            let totalGlobal = 0;
            let totalPerLine = 0;
            $(this).find('.quantity').each(function () {

                let product_id = $(this).attr('data-product_id');
                let price = parseFloat($('#price_id_' + product_id).val()??0);
                let remise = parseFloat($('#remise_id_' + product_id).val()??0);
                let tva = parseFloat($('#tva_id_' + product_id).val()??0);

                totalRemise += parseFloat((price *  remise) / 100)*parseFloat($(this).val());

                // totalRemise += (remise*parseFloat($(this).val()));

                totalTva += parseFloat($(this).val())*parseFloat(price*(1-(100/(100+tva))));
                totalPerLine = parseFloat($(this).val()) * parseFloat(price - parseFloat((price *  remise) / 100)*parseFloat($(this).val()));
                totalGlobal += totalPerLine;
                $('#total_id_' + product_id).attr('value', totalPerLine.toFixed(3));
            });
            $('#totalGlobal').html((parseFloat(totalGlobal)+1-parseFloat($('#remiseFact').val()??0)).toFixed(3));
            $('#totalTva').html(totalTva.toFixed(3));
            $('#totalRemise').html(totalRemise.toFixed(3));


        });
    }



    function openAndFocus(element){
        $('#'+element).val('');
        $('#'+element).select2().select2('open');
        $('.select2-search__field').last().focus();
    }

    /*Adding addCheck*/
    $(document).on('click', '#addCheck', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addCheck').length+1;
        let listOfBanks = `
        @foreach(\App\Helpers\WebHelper::allBanks() as $bank)
            <option value="{{$bank->name}}">{{$bank->name}}</option>
         @endforeach
            `;
        let add=`

                                <div class="card addCheck col-12">
                                    <div class="card-header bg-info  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addCheck${count}" role="button" aria-expanded="false" aria-controls="addCheck${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCheck  removePayment" data-type="addCheck"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- Chèque</h5>
                                    </div>
                                    <div id="addCheck${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="text" id="name" class="form-control amount" placeholder="e.g :  1 350 TND"
                                                               name="payments[check][${count}][amount]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="date" class="form-control" placeholder="e.g : Apple iMac"
                                                               name="payments[check][${count}][date]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Numéro <span class="text-danger">*</span></label>
                                                        <input type="text" id="name" class="form-control" placeholder="e.g : 1234567"
                                                               name="payments[check][${count}][number]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Banque <span class="text-danger">*</span></label>
                                                        <select class="form-control select2" id="select_bank${count}" name="payments[check][${count}][bank]">
                                                            ${listOfBanks}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


        `;
        $('#paymentMethod').append(add);
        $('#select_bank'+count).select2();
    });

    /*Adding addCheck*/

    /*Adding addCash*/
    $(document).on('click', '#addCash', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addCash').length+1;
        let date = '{{date('Y-m-d')}}';
        let add=`<div class="card addCash  col-12">
                                    <div class="card-header bg-success  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addCash${count}" role="button" aria-expanded="false" aria-controls="addCash${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCash removePayment" data-type="addCash"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- Espèce</h5>
                                    </div>
                                    <div id="addCash${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="number"  class="form-control amount" placeholder="e.g : 1 350 TND"
                                                               name="payments[cash][${count}][amount]" value="${total.toFixed(3)}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="name" class="form-control"
                                                               name="payments[cash][${count}][date]" value="${date}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

        `;
        $('#paymentMethod').append(add);
    });

    /*Adding addCash*/

    /*Adding addTransfer*/
    $(document).on('click', '#addTransfer', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addTransfer').length+1;
        let date = '{{date('Y-m-d')}}';
        let add=`<div class="card addTransfer  col-12">
                                    <div class="card-header bg-danger  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addTransfer${count}" role="button" aria-expanded="false" aria-controls="addTransfer${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCash removePayment" data-type="addTransfer"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- TPE</h5>
                                    </div>
                                    <div id="addTransfer${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="number"  class="form-control amount" placeholder="e.g : 1 350 TND"
                                                               name="payments[transfer][${count}][amount]" value="${total.toFixed(3)}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Numéro <span class="text-danger">*</span></label>
                                                        <input type="text" id="name" class="form-control" placeholder="e.g : 123467"
                                                               name="payments[transfer][${count}][number]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="name" class="form-control"
                                                               name="payments[transfer][${count}][date]" value="${date}" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

        `;
        $('#paymentMethod').append(add);
    });

    /*Adding addTransfer*/

    /*Adding addTransfer*/
    $(document).on('click', '#addSodexo', function(){
        let total=parseFloat($('#totalGlobal').text());
        let count=$('.addCash').length+1;
        let date = '{{date('Y-m-d')}}';
        let add=`<div class="card addSodexo  col-12">
                                    <div class="card-header bg-warning  text-white">
                                        <div class="card-widgets">
                                            <span data-toggle="collapse" href="#addSodexo${count}" role="button" aria-expanded="false" aria-controls="addSodexo${count}"><i class="mdi mdi-minus"></i></span>
                                            <span  class="removeCash removePayment" data-type="addSodexo"><i class="mdi mdi-close"></i></span>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">${count}- Sodexo</h5>
                                    </div>
                                    <div id="addSodexo${count}" class="collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Montant <span class="text-danger">*</span></label>
                                                        <input type="number"  class="form-control amount" placeholder="e.g : 1 350 TND"
                                                               name="payments[sodexo][${count}][amount]" value="${total.toFixed(3)}" onblur="this.value=this.value*0.9;">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Numéro <span class="text-danger">*</span></label>
                                                        <input type="text" id="name" class="form-control" placeholder="e.g : Apple iMac"
                                                               name="payments[sodexo][${count}][number]" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Date <span class="text-danger">*</span></label>
                                                        <input type="date" id="name" class="form-control"
                                                               name="payments[sodexo][${count}][date]" value="${date}" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

        `;
        $('#paymentMethod').append(add);
    });

    /*Adding Sodexo*/
    /*removePayment*/
    $(document).on('click','.removePayment',function () {
        let type=$(this).attr('data-type');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $('.'+type).last().remove();
            }
        });
    });
    /*removePayment*/


    /*FetchClientInformation*/
    $(document).on('change', '#client_id', function(){

        $('#client_name').val(this.options[this.selectedIndex].getAttribute('data-name'));
        $('#client_price').text(this.options[this.selectedIndex].getAttribute('data-price'));
        $('#client_mf').val(this.options[this.selectedIndex].getAttribute('data-mf'));
        $('#client_address').val(this.options[this.selectedIndex].getAttribute('data-address'));
        $('#client_phone').val(this.options[this.selectedIndex].getAttribute('data-phone'));
        $('#encours').text(this.options[this.selectedIndex].getAttribute('data-encours'));
        $('#plafond').text(this.options[this.selectedIndex].getAttribute('data-plafond'));
        //$('.remise').val(this.options[this.selectedIndex].getAttribute('data-remise'));
        $('#remise_client').val(this.options[this.selectedIndex].getAttribute('data-remise'));
        calculate_gtotal();
    });
    /*FetchClientInformation*/

    /*Add Commitment*/
    $(document).on('click', '#addCommitment', function(){
        let count=$('.echeance').length+1;
        let add=` <div class="card echeance col-12">
                                <div class="card-header bg-info  text-white">
                                    <div class="card-widgets">
                                        <span data-toggle="collapse" href="#echeance${count}" role="button" aria-expanded="false" aria-controls="echeance${count}"><i class="mdi mdi-minus"></i></span>
                                        <span  class="removeCheck  removePayment" data-type="echeance"><i class="mdi mdi-close"></i></span>
                                    </div>
                                    <h5 class="card-title mb-0 text-white">${count} - Echéance</h5>
                                </div>
                                <div class="collapse show">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Montant <span class="text-danger">*</span></label>
                                                    <input type="text" id="commitmentAmount${count}" class="form-control commitmentAmount amount" placeholder="e.g : Apple iMac"
                                                           name="echeance[${count}][amount]" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Date <span class="text-danger">*</span></label>
                                                    <input type="date" id="commitmentDate${count}" class="form-control commitmentDate"  placeholder="e.g : Apple iMac"
                                                           name="echeance[${count}][date]" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
        $('#echeance').append(add);
        setCommitment();


    });
    function setCommitment(){
        let count=$('.echeance').length;
        let total=parseFloat($('#totalGlobal').text());
        let rest=total%count;
        let echeance=Math.floor(total / count);
        let element;
        for(let i=0;i<count;i++){
            if(i==0) document.getElementsByClassName('commitmentAmount')[i].value=((rest+echeance).toFixed(3));
            else document.getElementsByClassName('commitmentAmount')[i].value=echeance.toFixed(3);
            let now = new Date();
            let echance = new Date(now.setMonth(now.getMonth() + i));
            element=document.getElementsByClassName('commitmentDate')[i];
            var day = ("0" + echance.getDate()).slice(-2);
            var month = ("0" + (echance.getMonth() + 1)).slice(-2);
            var today = echance.getFullYear()+"-"+(month)+"-"+(day) ;
            //element.flatpickr();
            element.value=today;

        }
    }
    /*Add Commitment*/
    $('#commitment').hide();
    $('#comptant').show();
    $(document).on('change', 'input[type=radio][name=commitmentType]', function(){
        if (this.value == 'echeance') {
            $('#commitment').show();
            $('#comptant').hide();
            $('#paymentMethod').html('');
        }
        else if (this.value == 'comptant') {
            $('#commitment').hide();
            $('#comptant').show();
            $('#echeance').html('');
        }
    });
    $(document).on('change', 'input[type=radio][name=piece_type]', function(){
        if (this.value == 'bl') {
            $('#payment_section').show();
        }
        else if (this.value == 'devis') {
            $('#payment_section').hide();
        }
    });
    var $currentFocus=$(':focus');
    // $(document).keydown(function(e){
    //
    //     switch (e.which) {
    //         case 106: // *
    //             e.preventDefault();
    //             $('#commitment').hide();
    //             $('#comptant').show();
    //             $('#echeance').html('');
    //             $('#addCash').click();
    //             $('#Comptant').click();
    //             break;
    //         case 111: // *
    //             e.preventDefault();
    //             $('#commitment').hide();
    //             $('#comptant').show();
    //             $('#echeance').html('');
    //             $('#addCheck').click();
    //             $('#Comptant').click();
    //             break;
    //         case 45: // insert
    //             e.preventDefault();
    //             $('#commitment').show();
    //             $('#comptant').hide();
    //             $('#paymentMethod').html('');
    //             $('#addCommitment').click();
    //             $('#Echeance').click();
    //             break;
    //         case 112: // F1
    //             e.preventDefault();
    //             $('#sa-a4').click();
    //             break;
    //         case 113: // F2
    //             e.preventDefault();
    //             $('#sa-ticket').click();
    //             break;
    //
    //         // case 16: // SHIFT
    //         //     e.preventDefault();
    //         //     openAndFocus('client_id');
    //         //     break;
    //         // case 18: // ALT
    //         //     e.preventDefault();
    //         //     openAndFocus('productList');
    //         //     break;
    //         case 39: // arrow right
    //             e.preventDefault();
    //             $('.quantity').first().focus();
    //             break;
    //         case 37: // arrow left
    //             e.preventDefault();
    //             $('.quantity').last().focus();
    //             break;
    //
    //
    //     }
    // });
    $(document).on('click','.submit',function (e) {
        if ($('input[type=radio][name=piece_type]:checked').val()=='bl'){

            let total_amounts=0;
            $('.amount').each(function(i, obj) {
                total_amounts+=parseFloat($(obj).val());
            });
            // if(total_amounts>parseFloat($('#totalGlobal').html()) || total_amounts!=parseFloat($('#totalGlobal').html())){
            if(total_amounts!=parseFloat($('#totalGlobal').html())){
                e.preventDefault();
                swal({
                    title: 'Total incorrect!',
                    text: "Veuillez corriger les montant saisies!",
                    type: 'error'
                });
                $('.amount').addClass('is-invalid');
            }
        }
    });
</script>
<style>
    input.chk-btn {
        display: none;
    }

    .label-chk-btn {
        width: 60px;
    }

    .label-chk-btn img {
        filter: grayscale(100%);
        height: 60px;
        transition: ease .3s;
        width: 45px;
        margin: auto;
        display: block;
    }

    input.chk-btn:checked + label img {
        transform: scale(1.1);
        filter: grayscale(0);
    }

    .is-invalid {
        border-color: #dc3545;
        padding-right: calc(1.5em + .75rem);
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-position: right calc(.375em + .1875rem) center;
        background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }
    #select2-productList-container{
        max-width: 300px;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
