@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">MU</a></li>
                        <li class="breadcrumb-item active">Suivi des ventes</li>
                    </ol>
                </div>
                <h4 class="page-title">Suivi des ventes</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->





    <!-- end row-->


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3">
                        <form action="{{route('admin.bs.charts')}}" method="GET" id="facture">

                            <div class="row">
                                <div class="col-lg-12">

                                    <!-- Portlet card -->
                                    <div class="card">
                                        <div class="card-header bg-info py-3 text-white">
                                            <div class="card-widgets">
                                                <a data-toggle="collapse" href="#cardCollpase7" role="button"
                                                   aria-expanded="false" aria-controls="cardCollpase2"
                                                   class="collapsed"><i class="mdi mdi-minus"></i></a>
                                            </div>
                                            <h5 class="card-title mb-0 text-white">Recherche Avancé</h5>
                                        </div>
                                        <div id="cardCollpase7" class="collapse ">
                                            <div class="card-body">

                                                <div class="row justify-content-center">
                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label>De  </label>
                                                            <input type="date" name="from"
                                                                   class="form-control datepicker"
                                                                   placeholder="D-M-Y "
                                                                   value="{{$request->from??''}}">
                                                        </div>

                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label>A  </label>
                                                            <input type="date" name="to"
                                                                   class="form-control datepicker"
                                                                   placeholder="D-M-Y "
                                                                   value="{{$request->to??''}}">
                                                        </div>

                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label for="Hubs">Boutiques</label>
                                                            <select class="select2 form-control" id="Hubs" name="stores[]">
                                                                @if(auth()->user()->type=='franchise' )
                                                                    @foreach ($stores->whereIn('id',[auth()->user()->store_id]) as $store)

                                                                        <option value="{{$store->id}}" @if(in_array($store->id,$request->stores)) selected @endif >{{$store->name}}</option>

                                                                    @endforeach
                                                                @else
                                                                    @foreach ($stores as $store)

                                                                        <option value="{{$store->id}}" @if(in_array($store->id,$request->stores)) selected @endif >{{$store->name}}</option>

                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                            <ul class="parsley-errors-list filled" id="Hubs_errors">
                                                            </ul>
                                                        </div>
                                                    </div>



                                                </div>


                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="text-center mb-3">

                                                        <button type="submit"
                                                                class="btn w-sm btn-success waves-effect waves-light"
                                                                id="sa-success">
                                                            Chercher
                                                        </button>
                                                    </div>
                                                </div> <!-- end col -->

                                            </div>
                                        </div>
                                        <!-- end row -->
                                    </div> <!-- end card-->
                                </div> <!-- end col -->
                            </div>
                            <!-- end row -->
                        </form>
                    </div>


                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-box">
                                <h4 class="header-title">Envoi & Retour ({{$stores->whereIn('id',$request->stores)->first()->name??''}})</h4>

                                <div id="sales-analytics" style="height: 350px;" class="flot-chart mt-5"></div>
                            </div> <!-- end card-box -->
                        </div> <!-- end col -->
                        <div class="col-lg-12">
                            <div class="card-box">
                                <h4 class="header-title">Taux ({{$stores->whereIn('id',$request->stores)->first()->name??''}})</h4>

                                <div id="rate" style="height: 350px;" class="flot-chart mt-5"></div>
                            </div>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->




                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->




@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->

<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- third party css end -->
<!-- Plugins css -->
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.time.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.resize.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.pie.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.selection.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.stack.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.orderBars.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.crosshair.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.axis.js')}}"></script>
<script src="{{URL::asset('assets/js/pages/flot.init.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>

<!-- third party js ends -->
<!-- Datatables init -->
<script>
    $('.select2').select2();
    $(".datepicker").flatpickr();
  </script>
<script>
    !function (o) {
        "use strict";
        var t = function () {
            this.$body = o("body")
        };
        t.prototype.createCombineGraph = function (t, a, e, i) {
            var r =
                [
                        @foreach ($stores as $store)
                    {   label: e[{{ $loop->index }}],
                        data: i[{{ $loop->index }}],
                        lines: {show: !0},
                        points: {show: !0}
                    },
                        @endforeach
                    {   label: e[5],
                        data: i[5],
                        lines: {show: !0},
                        points: {show: !0}
                    }
                ], l = {
                series: {shadowSize: 0},
                grid: {hoverable: !0, clickable: !0, tickColor: "#f9f9f9", borderWidth: 1, borderColor: "#eeeeee"},
                colors: ["#4fc6e1", "#f44336", "#1abc9c","#f7b84b","#f1556c","#4fc6e1"],
                tooltip: !0,
                tooltipOpts: {defaultTheme: !1},
                legend: {
                    position: "ne",
                    margin: [0, -32],
                    noColumns: 0,
                    labelBoxBorderColor: null,
                    labelFormatter: function (o, t) {
                        return o + "&nbsp;&nbsp;"
                    },
                    width: 30,
                    height: 2
                },
                yaxis: {axisLabel: "Chiffres",axisLabelUseCanvas: true, tickColor: "#f5f5f5", font: {color: "#bdbdbd"}},
                xaxis: {axisLabel: "Date",axisLabelUseCanvas: true, ticks: a, tickColor: "#f5f5f5", font: {color: "#bdbdbd"}}
            };
            o.plot(o(t), r,l)
        }, t.prototype.init = function () {
            var o = [
                    @foreach ($stores->whereIn('id',$request->stores) as $store)
                        [
                            @foreach ($records->where('name', $store->name)->sortBy('date')->groupBy('date') as $groupedRecords)
                            [{{$loop->index}}, @if(isset($request->stores[0]) && $request->stores[0]==1) {{($groupedRecords->first()->total_sent??0) - ($groupedRecords->first()->total_received ?? 0) }} @else {{($groupedRecords->first()->total_received ?? 0) - ($groupedRecords->first()->total_sent??0)  }} @endif ],
                            @endforeach
                        ],
                    @endforeach
                    @foreach ($stores->whereIn('id',$request->stores) as $store)
                        [
                                @foreach ($records->where('name', $store->name)->sortBy('date')->groupBy('date') as $groupedRecords)
                            [{{$loop->index}}, {{$groupedRecords->first()->total_sent??0}}],
                            @endforeach
                        ],
                    @endforeach
                    @foreach ($stores->whereIn('id',$request->stores) as $store)
                        [
                                @foreach ($records->where('name', $store->name)->sortBy('date')->groupBy('date') as $groupedRecords)
                            [{{$loop->index}}, {{$groupedRecords->first()->total_received??0}}],
                            @endforeach
                        ],
                    @endforeach
            ];
            this.createCombineGraph(
                "#sales-analytics",
                [
                    @foreach ($records->where('name', $store->name)->sortBy('date')->groupBy('date') as $groupedRecords)
                    [{{$loop->index}}, "{{$groupedRecords->first()->date}}"],
                    @endforeach
                ],
                [
                    @if(isset($request->stores[0]) && $request->stores[0]==1) "Vente","Envoie","Retour" @else "Vente","Retour","Envoie" @endif


                ],
                o)
        }, o.Dashboard1 = new t, o.Dashboard1.Constructor = t
    }(window.jQuery), function (o) {
        "use strict";
        o.Dashboard1.init()
    }(window.jQuery), $(".dash-daterange").flatpickr({
        altInput: !0,
        altFormat: "F j, Y",
        dateFormat: "Y-m-d",
        defaultDate: "today"
    });
</script>
<script>
    !function (o) {
        "use strict";
        var t = function () {
            this.$body = o("body")
        };
        t.prototype.createCombineGraph = function (t, a, e, r) {
            var i = [{label: a[0], data: e[0]}, {label: a[1], data: e[1]}, {label: a[2], data: e[2]}, {
                label: a[3],
                data: e[3]
            }, {label: a[4], data: e[4]}], l = {
                series: {pie: {show: !0, radius: 1, label: {show: !0, radius: 1, background: {opacity: .2}}}},
                legend: {show: !1},
                grid: {hoverable: !0, clickable: !0},
                colors: r,
                tooltip: !0,
                tooltipOpts: {content: "%s, %p.0%"}
            };
            o.plot(o(t), i, l)
        }, t.prototype.init = function () {
            this.createCombineGraph("#rate",
                ["Retour", "Reception"], [{{$records->where('name', $store->name)->sum('total_sent')}}, {{$records->where('name', $store->name)->sum('total_received')}}], ["#4a81d4", "#1abc9c"],
                o)
        }, o.Dashboard1 = new t, o.Dashboard1.Constructor = t
    }(window.jQuery), function (o) {
        "use strict";
        o.Dashboard1.init()
    }(window.jQuery), $(".dash-daterange").flatpickr({
        altInput: !0,
        altFormat: "F j, Y",
        dateFormat: "Y-m-d",
        defaultDate: "today"
    });
</script>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')


