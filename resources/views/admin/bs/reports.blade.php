@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">MU</a></li>
                        <li class="breadcrumb-item active">Suivi des ventes</li>
                    </ol>
                </div>
                <h4 class="page-title">Suivi des ventes</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->





    <!-- end row-->


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3">
                        <form action="{{route('admin.bs.reports')}}" method="GET" id="facture">

                            <div class="row">
                                <div class="col-lg-12">

                                    <!-- Portlet card -->
                                    <div class="card">
                                        <div class="card-header bg-info py-3 text-white">
                                            <div class="card-widgets">
                                                <a data-toggle="collapse" href="#cardCollpase7" role="button"
                                                   aria-expanded="false" aria-controls="cardCollpase2"
                                                   class="collapsed"><i class="mdi mdi-minus"></i></a>
                                            </div>
                                            <h5 class="card-title mb-0 text-white">Recherche Avancé</h5>
                                        </div>
                                        <div id="cardCollpase7" class="collapse ">
                                            <div class="card-body">

                                                <div class="row justify-content-center">
                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label>De  </label>
                                                            <input type="date" name="from"
                                                                   class="form-control datepicker"
                                                                   placeholder="D-M-Y "
                                                                   value="{{$request->from??''}}">
                                                        </div>

                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label>A  </label>
                                                            <input type="date" name="to"
                                                                   class="form-control datepicker"
                                                                   placeholder="D-M-Y "
                                                                   value="{{$request->to??''}}">
                                                        </div>

                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label for="Hubs">Boutiques</label>
                                                            <select class="select2 form-control" id="Hubs" name="store">
                                                                @if(auth()->user()->type=='franchise')
                                                                    @foreach ($stores->where('id',auth()->user()->store_id) as $store)

                                                                        <option value="{{$store->id}}" @if($store->id==$request->store) selected @endif >{{$store->name}}</option>

                                                                    @endforeach
                                                                @else
                                                                    @foreach ($stores as $store)

                                                                        <option value="{{$store->id}}" @if($store->id==$request->store) selected @endif >{{$store->name}}</option>

                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                            <ul class="parsley-errors-list filled" id="Hubs_errors">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                        <label>Catégorie</label>
                                                        <select class="form-control select2" name="category[]" id="category" multiple>
                                                            <option value="">Tous</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        </div>
                                                    </div>


                                                </div>


                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="text-center mb-3">

                                                        <button type="submit"
                                                                class="btn w-sm btn-success waves-effect waves-light"
                                                                id="sa-success">
                                                            Chercher
                                                        </button>
                                                    </div>
                                                </div> <!-- end col -->

                                            </div>
                                        </div>
                                        <!-- end row -->
                                    </div> <!-- end card-->
                                </div> <!-- end col -->
                            </div>
                            <!-- end row -->
                        </form>
                    </div>


                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-box">
                                <h4 class="header-title">Quantité ({{$stores->where('id',$request->store)->first()->name??''}})</h4>


                                    <div class="card-box">


                                        <div id="qty-bar-stacked" style="height: 350px;" class="morris-chart"></div>
                                    </div> <!-- end card-box-->

                            </div> <!-- end card-box -->
                        </div> <!-- end col -->

                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-box">
                                <h4 class="header-title">Chiffre d'affaires ({{$stores->where('id',$request->store)->first()->name??''}})</h4>


                                <div class="card-box">

                                    <div id="ca-bar-stacked" style="height: 350px;" class="morris-chart"></div>
                                </div> <!-- end card-box-->

                            </div> <!-- end card-box -->
                        </div> <!-- end col -->

                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-box">
                                <h4 class="header-title">Répartition par catégorie ({{$stores->where('id',$request->store)->first()->name??''}})</h4>


                                <div class="card-box">

                                    <div id="morris-donut-example" style="height: 350px;" class="morris-chart"></div>
                                    <div class="text-center">
                                        <p class="text-muted font-15 font-family-secondary mb-0">


                                            @foreach($sortedCategories as $category)

                                                <span class="mx-2"><i class="mdi mdi-checkbox-blank-circle " style="color: {{$colors[$loop->index]}}"></i> {{$category->name}}</span>
                                            @endforeach
                                        </p>
                                    </div>
                                </div> <!-- end card-box-->

                            </div> <!-- end card-box -->
                        </div> <!-- end col -->

                    </div>



                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->




@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->

<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- third party css end -->
<!-- Plugins css -->
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->

<script src="{{URL::asset('assets/libs/morris-js/morris.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/raphael/raphael.min.js')}}"></script>
<script src="{{URL::asset('assets/js/pages/morris.init.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>

<!-- third party js ends -->
<!-- Datatables init -->
<script>
    $('.select2').select2();
    $(".datepicker").flatpickr();
  </script>
<script>
    !function (e) {
        "use strict";
        var a = function () {
        };
        a.prototype.createStackedChart = function (e, a, t, r, i, o) {
            Morris.Bar({
                element: e,
                data: a,
                xkey: t,
                ykeys: r,
                stacked: !0,
                labels: i,
                hideHover: "auto",
                resize: !0,
                gridLineColor: "#eeeeee",
                barColors: o
            })
        }, a.prototype.init = function () {
            this.createStackedChart(
                "qty-bar-stacked",
                [
                        @foreach($records as $record)
                    {y: "{{$record->product_name??''}}", a: {{$record->total_quantity_sent??0}}, b: {{$record->total_quantity_returned??0}}},
                    @endforeach
                ], "y", ["a", "b"], [@if(isset($request->store) && $request->store!=1)"Envoie", "Retour"@else "Retour", "Envoie" @endif], [@if(isset($request->store) && $request->store!=1)"#21d43e", "#e12a3e"@else "#e12a3e", "#21d43e" @endif ]);


        }, e.MorrisCharts = new a, e.MorrisCharts.Constructor = a
    }(window.jQuery), function (e) {
        "use strict";
        e.MorrisCharts.init()
    }(window.jQuery);
</script>
<script>
    !function (e) {
        "use strict";
        var a = function () {
        };
        a.prototype.createStackedChart = function (e, a, t, r, i, o) {
            Morris.Bar({
                element: e,
                data: a,
                xkey: t,
                ykeys: r,
                stacked: !0,
                labels: i,
                hideHover: "auto",
                resize: !0,
                gridLineColor: "#eeeeee",
                barColors: o
            })
        }, a.prototype.init = function () {
            this.createStackedChart(
                "ca-bar-stacked",
                [
                        @foreach($records->sortBy('ca') as $record)
                    {y: "{{$record->product_name??''}}", a: Math.abs({{$record->ca??0}})},
                    @endforeach
                ], "y", ["a"], ["Envoie"], ["#21d43e"]);


        }, e.MorrisCharts = new a, e.MorrisCharts.Constructor = a
    }(window.jQuery), function (e) {
        "use strict";
        e.MorrisCharts.init()
    }(window.jQuery);
</script>
<script>
    !function (e) {
        "use strict";
        var a = function () {
        };
         a.prototype.createDonutChart = function (e, a, t) {
            Morris.Donut({element: e, data: a, barSize: .2, resize: !0, colors: t})
        }, a.prototype.init = function () {
            this.createDonutChart("morris-donut-example", [

                @foreach($sortedCategories as $category)
                {label: "{{$category->name}}", value: Math.abs({{$records->where('product_category',$category->id)->sum('ca')}})},
                @endforeach
                ], ["#1abc9c","#4fc6e1","#4a81d4","#f672a7","#f7b84b","#6658dd","#675aa9"])
        }, e.MorrisCharts = new a, e.MorrisCharts.Constructor = a
    }(window.jQuery), function (e) {
        "use strict";
        e.MorrisCharts.init()
    }(window.jQuery);
</script>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')


