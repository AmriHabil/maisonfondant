@section('content')




    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Produits</a></li>
                        <li class="breadcrumb-item active">Création</li>
                    </ol>
                </div>
                <h4 class="page-title">Création d'envoie</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <form action="{{route('admin.bs.store')}}" method="POST" id="cartStore" enctype="multipart/form-data"
          target="_blank" onsubmit="setTimeout(function(){location.reload();}, 1000);return true;"  autocomplete="off">

        @csrf
        <div class="row">
            <div class="col-lg-12">

                <div class="card-box ribbon-box">
                    <div class="ribbon-two ribbon-two-info"><span><i class="fa fa-store"></i> Boutique</span></div>
                    <div class="row justify-content-center">
                        <input type="hidden" value="{{$order->id??0}}" name="order">
                        @if(auth()->user()->type=='franchise')
                            <input type="hidden" name="from_store_id" value="{{auth()->user()->store_id}}">
                        @else
                            <div class="col-lg-2 col-md-3">
                                <div class="form-group">
                                    <label for="from_store_id">De <span class="text-danger">*</span></label>
                                    <select class="form-control select2" id="from_store_id" name="from_store_id"
                                            required>
                                        <option value=""></option>
                                        @foreach($stores as $store)
                                            <option value="{{$store->id}}"
                                                    data-name="{{$store->name}}"
                                                    data-address="{{$store->address}}"
                                            >
                                                {{$store->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="to_store_id">A <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="to_store_id" name="to_store_id" required>
                                    <option value=""></option>
                                    @foreach($stores as $store)

                                            <option value="{{$store->id}}"
                                                    data-name="{{$store->name}}"
                                                    data-address="{{$store->address}}"
                                            >
                                                {{$store->name}}
                                            </option>

                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- "Create BL" radio buttons -->
                        <div class="form-group mb-3" id="create_bl_radio" style="display: none;">
                            <label class="mb-2">Génération de BL <span class="text-danger">*</span></label>
                            <br/>
                            <div class="radio form-check-inline radio-success">
                                <input type="radio" id="inlineRadio1" value="1" name="create_bl">
                                <label for="inlineRadio1"> Auto </label>
                            </div>
                            <div class="radio form-check-inline radio-danger">
                                <input type="radio" id="inlineRadio2" value="0" name="create_bl">
                                <label for="inlineRadio2"> Manuelle </label>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="date">Date <span class="text-danger">*</span></label>
                                <input type="date" name="date" class="form-control" id="date" value="{{date('Y-m-d')}}">
                            </div>
                        </div>
                        {{--                        <div class="col-lg-2 col-md-3">--}}
                        {{--                            <div class="form-group">--}}
                        {{--                                <label for="to_store_id">Livreur <span class="text-danger">*</span></label>--}}
                        {{--                                <select class="form-control select2" id="to_store_id" name="to_store_id" required>--}}


                        {{--                                </select>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-lg-2 col-md-3">--}}
                        {{--                            <div class="form-group">--}}
                        {{--                                <label for="to_store_id">Destinataire <span class="text-danger">*</span></label>--}}
                        {{--                                <select class="form-control select2" id="to_store_id" name="to_store_id" required>--}}


                        {{--                                </select>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}


                    </div> <!-- end card-box -->
                    <div class="row justify-content-center">
                        <table class="table">
                            <thead>
                            <tr>

                                <th>Type</th>

                                <th>Résumé Expéditeur</th>


                            </tr>
                            </thead>
                            <tbody>
{{--                            <tr>--}}
{{--                                <td>Fondant</td>--}}

{{--                                <td>--}}
{{--                                    <input name="sender_report[Fondant]" class="form-control" value="0" id="fd">--}}
{{--                                    <input type="hidden" name="driver_report[Fondant]" class="form-control" value="0">--}}
{{--                                    <input type="hidden" name="receiver_report[Fondant]" class="form-control" value="0">--}}
{{--                                </td>--}}


{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>Polysterene</td>--}}

{{--                                <td>--}}
{{--                                    <input name="sender_report[Polysterene]" class="form-control" value="0"--}}
{{--                                           id="polysterene">--}}
{{--                                    <input type="hidden" name="driver_report[Polysterene]" class="form-control"--}}
{{--                                           value="0">--}}
{{--                                    <input type="hidden" name="receiver_report[Polysterene]" class="form-control"--}}
{{--                                           value="0">--}}
{{--                                </td>--}}


{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>Mini Fondant</td>--}}

{{--                                <td><input name="sender_report[Mini Fondant]" class="form-control" value="0" id="mf">--}}
{{--                                    <input type="hidden" name="driver_report[Mini Fondant]" class="form-control"--}}
{{--                                           value="0">--}}
{{--                                    <input type="hidden" name="receiver_report[Mini Fondant]" class="form-control"--}}
{{--                                           value="0">--}}
{{--                                </td>--}}


{{--                            </tr>--}}

{{--                            <tr>--}}
{{--                                <td>Fondant Geant</td>--}}

{{--                                <td><input name="sender_report[Fondant Geant]" class="form-control" value="0" id="fg">--}}
{{--                                    <input type="hidden" name="driver_report[Fondant Geant]" class="form-control"--}}
{{--                                           value="0">--}}
{{--                                    <input type="hidden" name="receiver_report[Fondant Geant]" class="form-control"--}}
{{--                                           value="0">--}}
{{--                                </td>--}}

{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>Fondor</td>--}}

{{--                                <td><input name="sender_report[Fondor]" class="form-control" value="0" id="fr">--}}
{{--                                    <input type="hidden" name="driver_report[Fondor]" class="form-control" value="0">--}}
{{--                                    <input type="hidden" name="receiver_report[Fondor]" class="form-control" value="0">--}}
{{--                                </td>--}}

{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>Commandes</td>--}}

{{--                                <td><input name="sender_report[Commandes]" class="form-control" id="cmd" value="0">--}}
{{--                                    <input type="hidden" name="driver_report[Commandes]" class="form-control" value="0">--}}
{{--                                    <input type="hidden" name="receiver_report[Commandes]" class="form-control"--}}
{{--                                           value="0">--}}
{{--                                </td>--}}

{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>Consommables</td>--}}

{{--                                <td><input name="sender_report[Consommables]" class="form-control" id="cbl" value="0">--}}
{{--                                    <input type="hidden" name="driver_report[Consommables]" class="form-control"--}}
{{--                                           value="0">--}}
{{--                                    <input type="hidden" name="receiver_report[Consommables]" class="form-control"--}}
{{--                                           value="0">--}}
{{--                                </td>--}}

{{--                            </tr>--}}
                            <tr>
                                <td>Rates</td>

                                <td><input name="sender_report[Rates]" class="form-control" value="0" id="rt">
                                    <input type="hidden" name="driver_report[Rates]" class="form-control" value="0">
                                    <input type="hidden" name="receiver_report[Rates]" class="form-control" value="0">
                                </td>

                            </tr>

                            </tbody>
                        </table>

                    </div>

                </div> <!-- end col -->

            </div>

            <div class="col-lg-12">

                <div class="card-box  ribbon-box">
                    <div class="ribbon-two ribbon-two-success"><span><i class="fa fa-list"></i> Détails</span></div>
                    <div class="table-responsive">
                        <table class="table table-striped" id="details_table">
                            <thead>
                            <tr>

                                <th width="150px">Article</th>
                                <th width="100px">Qté</th>
                                {{--                                <th width="100px">Polysterene</th>--}}
                                <th width="30px">Action</th>
                            </tr>
                            </thead>
                            <tbody id="cartItems">
                            @if($order)
                                @foreach($order->details as $detail)
                                    <tr id="row{{$detail->id}}" class="item_cart" data-id="{{$detail->id}}">
                                        <td>
                                            <input type="hidden" name="details[{{$detail->product_id}}][product_ref]"
                                                   value="{{$detail->product_ref}}" readonly="" class="form-control">


                                            <input type="hidden" name="details[{{$detail->product_id}}][product_id]"
                                                   value="{{$detail->product_id}}">
                                            <input type="hidden"
                                                   name="details[{{$detail->product_id}}][product_coefficient]"
                                                   value="{{$detail->product_coefficient}}"
                                                   id="coef_id_{{$detail->product_id}}">
                                            <input type="hidden"
                                                   name="details[{{$detail->product_id}}][product_category]"
                                                   value="{{$detail->product_category}}"
                                                   id="category_id_{{$detail->product_id}}">
                                            <input type="hidden" name="details[{{$detail->product_id}}][product_price]"
                                                   value="{{$detail->product_price}}"
                                                   id="price_id_{{$detail->product_id}}">
                                            <input type="hidden" id="name_id_{{$detail->product_id}}"
                                                   name="details[{{$detail->product_id}}][product_name]"
                                                   class="form-control name" readonly=""
                                                   value="{{$detail->product_name}}" required="">
                                            {{$detail->product_ref}} - {{$detail->product_name}}
                                        </td>
                                        <td>
                                            <input type="number" value="{{$detail['product_quantity']}}"
                                                   id="quantity_id_{{$detail->product_id}}"
                                                   name="details[{{$detail->product_id}}][product_quantity_sent]"
                                                   class="form-control  quantity  numeric"
                                                   data-product_id="{{$detail->product_id}}" value="" step="1" min="0"
                                                   data-original-title="" title="" onfocus="this.value='';"  autocomplete="off">
                                        </td>

                                        <td>
                                            <a data-tr="row{{$detail->id}}" class="text-danger removeRow"><i
                                                        class="fa fa-minus-circle"></i></a>
                                        </td>
                                    </tr>

                                @endforeach
                            @else
                                @foreach($products->where('category_id','1')->sortBy('value') as $detail)

                                    <tr id="row{{$detail->id}}" class="item_cart" data-id="{{$detail->id}}">
                                        <td {!!  \App\Helpers\AppHelper::borderColer($detail->category_id)!!}>
                                            <input type="hidden" name="details[{{$detail->id}}][product_ref]"
                                                   value="{{$detail->ref}}" readonly="" class="form-control">
                                            <input type="hidden" name="details[{{$detail->id}}][product_id]"
                                                   value="{{$detail->id}}">
                                            <input type="hidden" name="details[{{$detail->id}}][product_coefficient]"
                                                   value="{{$detail->coefficient}}" id="coef_id_{{$detail->id}}">
                                            <input type="hidden" name="details[{{$detail->id}}][product_category]"
                                                   value="{{$detail->category_id}}" id="category_id_{{$detail->id}}">
                                            <input type="hidden" name="details[{{$detail->id}}][product_price]"
                                                   value="{{$detail->price}}" id="price_id_{{$detail->id}}">
                                            <input type="hidden" id="name_id_{{$detail->id}}"
                                                   name="details[{{$detail->id}}][product_name]"
                                                   class="form-control name" readonly="" value="{{$detail->name}}"
                                                   required="">
                                            {{$detail->ref}} - {{$detail->name}}
                                        </td>
                                        <td>
                                            <input type="number" id="quantity_id_{{$detail->id}}"
                                                   name="details[{{$detail->id}}][product_quantity_sent]"
                                                   class="form-control  quantity  numeric"
                                                   data-product_id="{{$detail->id}}"  step="1" min="0"
                                                  title=""   autocomplete="do-not-autofill">
                                        </td>

                                        <td>
                                            <a data-tr="row{{$detail->id}}" class="text-danger removeRow"><i
                                                        class="fa fa-minus-circle"></i></a>
                                        </td>
                                    </tr>

                                @endforeach
                            @endif
                            </tbody>
                                                        <tfoot>
                                                        <tr>

                                                            <td colspan="2">
                                                                <select class="form-control select2 productList" id="productList" name=""  style="width: 150px">
                                                                    <option value=""></option>
                                                                    @foreach($products as $product)

                                                                        <option value="{{$product->id}}"
                                                                                data-name="{{$product->name??''}}"
                                                                                data-category="{{$product->category_id??''}}"
                                                                                data-ref="{{$product->ref??''}}"
                                                                                data-price="{{$product->price??''}}"
                                                                                data-buying="{{$product->buying??''}}"
                                                                                data-coefficient="{{$product->coefficient??''}}"
                                                                        >
                                                                            {{$product->ref}} - {{$product->name}}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                                <style>
                                                                    .select2-container{
                                                                        max-width: 230px;
                                                                    }
                                                                </style>
                                                            </td>
                                                            <td><span class="" data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="La quantité actuelle de cette article est " data-original-title="Raccourci"
                                                                ><i class="fa fa-info-circle"></i></span></td>
                                                        </tr>
                                                        </tfoot>
                        </table>
                    </div>
                    <audio src="{{asset('assets/sound/success.mp3')}}"></audio>
                    <audio src="{{asset('assets/sound/warning.mp3')}}"></audio>

                </div> <!-- end col-->

            </div> <!-- end col-->


        </div>
        <!-- end row -->

        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-center mb-3">


                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light submit" id="sa-a4"
                            value="a4" name="submit">Enregistrer
                    </button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>



    <style>
        .select2 {
            width: 100% !important;
        }

        .popover-title {
            background: #ffff99;
        }
    </style>

@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/css/general.css')}}" rel="stylesheet" type="text/css"/>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<!-- Loading buttons js -->
<script src="{{asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{asset('assets/libs/ladda/ladda.js')}}"></script>
<script src="{{asset('assets/js/pages/loading-btn.init.js')}}"></script>

<script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('assets/js/pages/add-product.init.js')}}"></script>

{{--<script src="{{asset('assets/js/validation.js')}}"></script>--}}

<script>

    $('.withPopover').popover({offset: 10});
    var productListAfterSelect2 = $("#productList").select2();
    // $(window).on('load', function() {
    //     productListAfterSelect2.select2("open");
    // });
    $(".datepicker").flatpickr();

    $(document).on('change', '#productList', function () {
        addProduct(this);
    });
    $(document).on('click', '.removeRow', function () {
        let tr = $(this).attr("data-tr");
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value == true) {
                $('#' + tr).remove();
            }
        });

    });


    function addProduct(select) {
        let product = {
            id: select.value,
            name: select.options[select.selectedIndex].getAttribute('data-name'),
            ref: select.options[select.selectedIndex].getAttribute('data-ref'),
            category: select.options[select.selectedIndex].getAttribute('data-category'),
            coefficient: select.options[select.selectedIndex].getAttribute('data-coefficient'),
            // unity:select.options[select.selectedIndex].getAttribute('data-unity'),
            // tva:select.options[select.selectedIndex].getAttribute('data-tva'),
            // buying:select.options[select.selectedIndex].getAttribute('data-buying'),
            price: select.options[select.selectedIndex].getAttribute('data-price'),
            buying: select.options[select.selectedIndex].getAttribute('data-buying'),
            // quantity:select.options[select.selectedIndex].getAttribute('data-quantity'),

        };

        if ($('#quantity_id_' + product.id).length) {
            $('#quantity_id_' + product.id).val(parseFloat($('#quantity_id_' + product.id).val()) + 1);
            checkQuantity($('#quantity_id_' + product.id));
            playSound();
        } else {
            let alert = '';

            tr = `<tr id="row${product.id}" class="item_cart"  data-id="${product.id}">
                    <td>
                        <input type="hidden" name="details[${product.id}][product_ref]" value="${product.ref}" readonly class="form-control">
                        <input type="hidden" name="details[${product.id}][product_id]" value="${product.id}">
                        <input type="hidden" name="details[${product.id}][product_coefficient]" value="${product.coefficient}" id="coef_id_${product.id}">
                        <input type="hidden" name="details[${product.id}][product_category]" value="${product.category}" id="category_id_${product.id}">
                        <input type="hidden" name="details[${product.id}][product_price]" value="${product.price}" id="price_id_${product.id}">
                        <input type="hidden" name="details[${product.id}][product_price_buying]" value="${product.buying}" id="price_buying_id_${product.id}">
                        <input type="hidden" id="name_id_${product.id}" name="details[${product.id}][product_name]" class="form-control name" readonly value="${product.name}" style="width: 150px" required/>
                     ${product.ref} - ${product.name}
                    </td>
                    <td>
                       <input type="number" id="quantity_id_${product.id}" name="details[${product.id}][product_quantity_sent]" class="form-control  quantity  " data-product_id="${product.id ?? 0}" step="1" min="0" autocomplete="do-not-autofill">
                    </td>

                    <td>
                        <a data-tr="row${product.id}" class="text-danger removeRow"><i class="fa fa-minus-circle"></i></a>
                   </td>
                </tr>`;
            $('#cartItems').append(tr);

            playSound();
        }
        openAndFocus('productList');

    }




    function playSound() {
        $('audio')[0].load();
        $('audio')[0].play();
    }

    /*<td>
                           <input type="number" id="polysterene_id_${product.id}" name="details[${product.id}][product_quantity_sent]" class="form-control  withPopover " value="${1/product.coefficient}" step="0.001">
                        </td>*/

    function checkQuantity(element) {
        // let max=parseFloat($(element).attr('data-max'));
        // let id=parseFloat($(element).attr('data-product_id'));
        // if(parseFloat($(element).val())>max){
        //     $.toast({
        //         heading: 'Dépassage du stock',
        //         text: 'La quantité actuelle est '+max,
        //         icon: 'error',
        //         loader: true,
        //         position:'top-right',// Change it to false to disable loader
        //         loaderBg: '#f1556c',  // To change the background
        //         bgColor: '#f1556c',  // To change the background
        //     });
        //     //$(this).val(max);
        //     $(element).addClass('alert-danger');
        // }else{
        //     $(element).removeClass('alert-danger');
        // }
    }


    function openAndFocus(element) {
        $('#' + element).val('');
        $('#' + element).select2().select2('open');
        $('.select2-search__field').last().focus();
    }




    /*$(document).on('click', '#sa-a4', function (e) {
        e.preventDefault();
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value == true) {
                calculate_gtotal();
            }else{

            }
        });

    });*/
</script>
<script>
    $(document).ready(function() {
        // Function to check conditions and toggle radio button visibility
        function checkStoreConditions() {
            var fromStoreId = $('#from_store_id').val();
            var toStoreId = $('#to_store_id').val();

            // Check if from_store_id != 1 and to_store_id == 1
            if (fromStoreId != '1' && toStoreId == '1') {
                $('#create_bl_radio').show(); // Show radio buttons
            } else {
                $('#create_bl_radio').hide(); // Hide radio buttons
                $('input[name="create_bl"]').prop('checked', false); // Uncheck the radio buttons
            }
        }

        // Trigger the function when either of the selects change
        $('#from_store_id, #to_store_id').on('change', function() {
            checkStoreConditions();
        });

        // Initial check in case the form is pre-filled
        checkStoreConditions();
    });

</script>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
