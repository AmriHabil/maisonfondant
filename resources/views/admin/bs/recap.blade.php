@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">MU</a></li>
                        <li class="breadcrumb-item active">Suivi des ventes</li>
                    </ol>
                </div>
                <h4 class="page-title">Suivi des ventes</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->





    <!-- end row-->


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3">
                        <form action="{{route('admin.bs.getRecap')}}" method="GET" id="facture">

                            <div class="row">
                                <div class="col-lg-12">

                                    <!-- Portlet card -->
                                    <div class="card">
                                        <div class="card-header bg-info py-3 text-white">
                                            <div class="card-widgets">
                                                <a data-toggle="collapse" href="#cardCollpase7" role="button"
                                                   aria-expanded="false" aria-controls="cardCollpase2"
                                                   class="collapsed"><i class="mdi mdi-minus"></i></a>
                                            </div>
                                            <h5 class="card-title mb-0 text-white">Recherche Avancé</h5>
                                        </div>
                                        <div id="cardCollpase7" class="collapse ">
                                            <div class="card-body">

                                                <div class="row justify-content-center">
                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label>De  </label>
                                                            <input type="date" name="from"
                                                                   class="form-control datepicker"
                                                                   placeholder="D-M-Y "
                                                                   value="{{$request->from??''}}">
                                                        </div>

                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label>A  </label>
                                                            <input type="date" name="to"
                                                                   class="form-control datepicker"
                                                                   placeholder="D-M-Y "
                                                                   value="{{$request->to??''}}">
                                                        </div>

                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="form-group mb-3">
                                                            <label for="Hubs">Boutiques</label>
                                                            <select class="select2 form-control" multiple id="Hubs"
                                                                    name="stores[]">
                                                                @if(auth()->user()->type=='franchise' )
                                                                    @foreach ($stores->whereIn('id',[auth()->user()->store_id]) as $store)

                                                                        <option value="{{$store->id}}" @if(in_array($store->id,$request->stores)) selected @endif >{{$store->name}}</option>

                                                                    @endforeach
                                                                @else
                                                                    @foreach ($stores as $store)

                                                                        <option value="{{$store->id}}" @if(in_array($store->id,$request->stores)) selected @endif >{{$store->name}}</option>

                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                            <ul class="parsley-errors-list filled" id="Hubs_errors">
                                                            </ul>
                                                        </div>
                                                    </div>



                                                </div>
                                                <div class="row justify-content-center">
                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label for="type" class="text-center">
                                                                <input type="checkbox" name="type" id="type" value="1" @if($request->type) checked @endif>
                                                                Affichage des envoies / retours
                                                            </label>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group mb-3">
                                                            <label for="progress" class="text-center">
                                                                <input type="checkbox" name="progress" id="progress" value="1" @if($request->progress) checked @endif>
                                                                Progress Bar
                                                            </label>

                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="text-center mb-3">

                                                        <button type="submit"
                                                                class="btn w-sm btn-success waves-effect waves-light"
                                                                id="sa-success">
                                                            Chercher
                                                        </button>
                                                    </div>
                                                </div> <!-- end col -->

                                            </div>
                                        </div>
                                        <!-- end row -->
                                    </div> <!-- end card-->
                                </div> <!-- end col -->
                            </div>
                            <!-- end row -->
                        </form>
                    </div>




                    <div class="table-responsive">
                        <table id="key-datatable" class="  table table-striped dt-responsive nowrap orders_tbl">

                                <thead>
                                <tr>
                                    <th>Date</th>
                                    @foreach ($stores->whereIn('id',$request->stores) as $store)
                                        <th>{{ $store->name }}</th>
                                    @endforeach
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $total=[]; @endphp
                                @foreach ($records->sortBy('date')->groupBy('date') as $groupedRecords)
                                    @if(isset($day) && $day >date('d',strtotime($groupedRecords->first()->date)))
                                        <tr>
                                            <td   class="text-center bg-soft-success font-17 font-weight-bold">
                                                TOTAL {{$month??''}}
                                            </td>
                                            @php
                                                $total_per_row=0;
                                            @endphp
                                            @foreach ($stores->whereIn('id',$request->stores) as $store)
                                                <td  class="bg-soft-success font-17 font-weight-bold">
                                                    {{$total[$store->id]}}
                                                </td>
                                                @php
                                                    $total_per_row+=$total[$store->id];
                                                @endphp
                                            @endforeach
                                            <td   class="bg-soft-success font-17 font-weight-bold">{{$total_per_row}}</td>
                                        </tr>
                                        @php
                                            $total[$store->id]=0;
                                        @endphp
                                    @endif
                                    <tr>
                                        <td >{{ $groupedRecords->first()->date }}</td>
                                        @php
                                            $total_per_row=0;
                                        @endphp
                                        @foreach ($stores->whereIn('id',$request->stores) as $store)
                                        <td>
                                            <div class="row">
                                                @if(isset($request->type) && $request->type==1)
                                                    <div class="col">
                                                        <p class="text-danger"><i class="fa fa-arrow-circle-up"></i> {{ abs($groupedRecords->where('name', $store->name)->first()->total_sent??00) }}</p>
                                                    </div>
                                                    <div class="col">
                                                        <p class="text-success"><i class="fa fa-arrow-circle-down"></i> {{ abs($groupedRecords->where('name', $store->name)->first()->total_received??00) }}</p>
                                                    </div>
                                                    @php
                                                    if (isset($groupedRecords->where('name', $store->name)->first()->total_received) && $groupedRecords->where('name', $store->name)->first()->total_received !=0 ){
                                                        $total_received=$groupedRecords->where('name', $store->name)->first()->total_received;
                                                    }else{
                                                        $total_received=1;
                                                    }
                                                    $taux_vente=number_format(100-((abs($groupedRecords->where('name', $store->name)->first()->total_sent??00)/abs($total_received))*100),2);
                                                    @endphp
                                                    <div class="col">
                                                        <p class="text-info"><i class="fa fa-chart-pie"></i> {{ $taux_vente }} %</p>
                                                    </div>
                                                @endif
                                                    <div class="col">
                                                        <p class="font-18"><i class="fa fa-dollar-sign"></i> {{ abs($groupedRecords->where('name', $store->name)->first()->total??00) }}</p>
                                                    </div>

                                                    @php
                                                        if(isset($total[$store->id])){
                                                            $total[$store->id]+=abs($groupedRecords->where('name', $store->name)->first()->total??00);
                                                        }else{
                                                            $total[$store->id]=abs($groupedRecords->where('name', $store->name)->first()->total??00);
                                                        }

                                                        $total_per_row+=abs($groupedRecords->where('name', $store->name)->first()->total??00);

                                                        if(isset($day)){
                                                            $month= date('M',strtotime($groupedRecords->first()->date));
                                                            $day= date('d',strtotime($groupedRecords->first()->date));
                                                        }else{
                                                            $day=0;
                                                            $month='';
                                                        }

                                                    @endphp
                                            </div>
                                            @if(isset($request->progress) && $request->progress==1)
                                            <div class="row ml-3 mr-3">
                                                <div class="progress mb-2 w-100" style="height: 2rem">
                                                    <div class="progress-bar progress-bar-striped
                                                    @php
                                                        if($taux_vente < 50 ){
                                                            echo 'bg-warning';
                                                        }else{
                                                            echo 'bg-success';
                                                        }
                                                    @endphp

                                                            " role="progressbar"
                                                         style="width:{{$taux_vente}}%;"
                                                         aria-valuenow="{{$taux_vente}}"
                                                         aria-valuemin="0"
                                                         aria-valuemax="100">{{$taux_vente}}
                                                        %
                                                    </div>
                                                </div>
                                            </div>
                                                @endif
                                        </td>
                                        @endforeach
                                        <td   class="bg-soft-success font-17 font-weight-bold">
                                            {{$total_per_row}}
                                        </td>
                                    </tr>

                                @endforeach
                                <tr>
                                    <td   class="text-center bg-soft-success font-17 font-weight-bold">
                                        TOTAL {{$month??''}}
                                    </td>
                                    @php
                                        $total_per_row=0;
                                    @endphp
                                    @foreach ($stores->whereIn('id',$request->stores) as $store)
                                        <td  class="bg-soft-success font-17 font-weight-bold">
                                            {{$total[$store->id]}}
                                        </td>
                                        @php
                                            $total_per_row+=$total[$store->id];
                                        @endphp
                                    @endforeach
                                    <td  class="bg-soft-success font-17 font-weight-bold">{{$total_per_row}}</td>
                                </tr>
                                @php
                                    $total[$store->id]=0;
                                @endphp
                                </tbody>


                        </table>
                    </div>


                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->




@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- third party css end -->
<!-- Plugins css -->
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>

<!-- third party js ends -->
<!-- Datatables init -->
<script>
    $('.select2').select2();
    $(".datepicker").flatpickr();
    $('.orders_tbl').DataTable({
        aLengthMenu: [
            [-1, 10, 25, 50, 100, 200, -1],
            ["All", 10, 25, 50, 100, 200, "All"]
        ],
        "ordering": false,
        dom: 'Blfrtip',
        responsive: false,
        select: true
    });



</script>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

