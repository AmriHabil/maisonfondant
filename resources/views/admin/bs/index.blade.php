@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Maison Fondant</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Logistiques</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
                <h4 class="page-title">Logistiques</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->





    <!-- end row-->


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">



                        <div class="row mt-2">
                            <div class="col-12">


                                <div class="table-responsive">
                                        <table id="key-datatable" class="table table-striped dt-responsive nowrap">
                                            <thead>
                                            <tr>
                                                <th>Numéro</th>
                                                <th>De</th>
                                                <th>A</th>
                                                <th>Envoyé Par</th>
                                                <th>Reçu Par</th>
                                                <th>Livré Par</th>
                                                <th>Date</th>
                                                <th>Créé à</th>
                                                <th>Modifié à</th>
                                                <th>BL</th>
                                                <th width="100px">Action </th>

                                            </tr>
                                            </thead>
                                            <tfoot style="display: table-row-group;">
                                            <tr>
                                                <th class="searchable">Numéro</th>
                                                <th @if (auth()->user()->type == 'admin') class="store"@endif>De</th>
                                                <th @if (auth()->user()->type == 'admin') class="store"@endif>A</th>

                                                <th class="searchable">Envoyé Par</th>
                                                <th class="searchable">Reçu Par</th>
                                                <th class="searchable">Livré Par</th>

                                                <th class="date">Date</th>
                                                <th class="date">Créé à</th>
                                                <th class="date">Modifié à</th>
                                                <th class="searchable">BL</th>
                                                <th> </th>

                                            </tr>
                                            </tfoot>
                                            <tbody>

                                            </tbody>


                                        </table>
                                </div>

                            </div><!-- end col-->
                        </div>

                        <!-- end row-->



                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->





@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>

<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js" type="text/javascript"></script>



<!-- third party js ends -->
<!-- Datatables init -->
<script>

    $('#key-datatable').DataTable({

        processing: true,
        serverSide: true,

        ajax: "{{ route('admin.bs.getData',$type) }}",
        columns: [
            { data: 'id', name: 'id' },
            { data: 'from', name: 'from.name' },
            { data: 'to', name: 'to.name' },
            { data: 'sender', name: 'sender.name' },
            { data: 'receiver', name: 'receiver.name' },
            { data: 'driver', name: 'driver.name' },
            { data: 'date', name: 'date' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'bl_id', name: 'bl_id' },
            { data: 'action', name: 'action' },

        ],
        order: [[6, 'desc'],[0, 'desc']],



        aLengthMenu: [
            [10, 25, 50, 100, 200, -1],
            [10, 25, 50, 100, 200, "All"]
        ],

        // dom: 'Blfrtip',
        dom: 'Blrtip',
        responsive:false,
        initComplete: function () {
            // Apply the search
            this.api()
                .columns()
                .every(function () {
                    var that = this;
                    $('input,select', this.footer()).on('keyup change clear', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
        },
    });
    $('#key-datatable tfoot .searchable').each(function () {
        var title = $(this).text();
        $(this).html('<input class="form-control" type="text" placeholder=" ' + title + '" />');
    });
    $('#key-datatable tfoot .date').each(function () {
        var title = $(this).text();
        $(this).html('<input class="form-control" type="date" placeholder=" ' + title + '" />');
    });
    $('#key-datatable tfoot .select').each(function () {
        var title = $(this).text();
        $(this).html('<select class="form-control"  ><option>Tous</option><option value="1">Reçu</option><option value="0">Non Reçu</option></select>');
    });
    $('#key-datatable tfoot .store').each(function () {
        var title = $(this).text();
        $(this).html('<select class="form-control"  ><option>Tous</option>@foreach($stores as $store)<option value="{{$store->name}}">{{$store->name}}</option>@endforeach</select>');
    });
    $('body').on('click', '.main_select', function (e) {
        var check = $('.orders_tbl').find('tbody > tr > td:first-child .order_check');
        if ($('.main_select').prop("checked") == true) {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', true);
        } else {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', false);
        }

        $('.orders_tbl').find('tbody > tr > td:first-child .order_check').val();
    });
    $('.check').click(function () {

        var firstInput = $(this).find('input')[0];
        firstInput.checked = !firstInput.checked;
    });



    $('#sa-reset').click(function () {
        $("#StatusFilter,#customer_id").val("");
        $("#StatusFilter,#customer_id").select2({});
        $("#my_multi_select2").val('');
        $("#my_multi_select2").multiSelect('refresh');
    });



</script>



<script>

    $('body').on('click', '.print_invoice', function (e) {
        var mydata = [];
        e.preventDefault();
        $('.orders_tbl > tbody  > tr').each(function () {
            var checkbox = $(this).find('td:first-child .order_check');
            if (checkbox.prop("checked") == true) {
                var order_id = $(checkbox).data('id');
                mydata.push(order_id);
            }
        });
        var order_data = mydata.join(',');

        $('#print_data').val(order_data);
        $('#bulk_submit').submit();
    })
</script>
<script>
    $(".datepicker").flatpickr();
    $('.select2').select2();
    $('#my_multi_select2').multiSelect();
    $(".numeric").maxlength({

        warningClass: "badge badge-success",
        limitReachedClass: "badge badge-danger"
    })
</script>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

