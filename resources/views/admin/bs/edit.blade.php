@section('content')




    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{__('general.store_name')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Produits</a></li>
                        <li class="breadcrumb-item active">Création</li>
                    </ol>
                </div>
                <h4 class="page-title">Créer une pièce de vente</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <form action="{{route('admin.bs.update',$bs->id)}}" method="POST" id="cartStore" enctype="multipart/form-data"  target="_blank"  onsubmit="setTimeout(function(){location.reload();}, 1000);return true;">

        @csrf
        <div class="row">
            <div class="col-lg-12">

                <div class="card-box ribbon-box">
                    <div class="ribbon-two ribbon-two-info"><span><i class="fa fa-store"></i> Boutique</span></div>
                    <div class="row justify-content-center">
                        @if($type=='admin')
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="from_store_id">De <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="from_store_id" name="from_store_id" required>

                                    @foreach($stores as $store)
                                        <option value="{{$store->id}}"
                                                data-name="{{$store->name}}"
                                                data-address="{{$store->address}}"
                                                @if($bs->from_store_id==$store->id) selected @endif
                                        >
                                            {{$store->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="to_store_id">A <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="to_store_id" name="to_store_id" required>

                                    @foreach($stores as $store)
                                        <option value="{{$store->id}}"
                                                data-name="{{$store->name}}"
                                                data-address="{{$store->address}}"
                                                @if($bs->to_store_id==$store->id) selected @endif
                                        >
                                            {{$store->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @else
                            <p class="font-15">Du : <span class="font-20">{{$stores->where('id',$bs->from_store_id)->first()->name}}</span> A : <span class="font-20">{{$stores->where('id',$bs->to_store_id)->first()->name}}</span></p>
                        @endif

                        @if($type=='admin')
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="driver_id">Livreur <span class="text-danger">*</span></label>
{{--                                <select class="form-control select2" id="driver_id" name="driver_id" required>--}}
{{--                                    @foreach($admins as $admin)--}}
{{--                                        <option value="{{$admin->id}}"--}}
{{--                                                @if($bs->driver_id==$admin->id) selected @endif--}}
{{--                                        >--}}
{{--                                            {{$admin->name}}--}}
{{--                                        </option>--}}
{{--                                    @endforeach--}}

{{--                                </select>--}}
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3">
                            <div class="form-group">
                                <label for="receiver_id">Destinataire <span class="text-danger">*</span></label>
{{--                                <select class="form-control select2" id="receiver_id" name="receiver_id" required>--}}
{{--                                    @foreach($admins as $admin)--}}
{{--                                        <option value="{{$admin->id}}"--}}
{{--                                                @if($bs->receiver_id==$admin->id) selected @endif--}}
{{--                                        >--}}
{{--                                            {{$admin->name}}--}}
{{--                                        </option>--}}
{{--                                    @endforeach--}}

{{--                                </select>--}}
                            </div>
                        </div>

                        @endif

                            @can('Date bon de sortie')
                            <div class="col-lg-2 col-md-3">
                                <div class="form-group">
                                    <label for="date">Date <span class="text-danger">*</span></label>
                                    <input type="date" name="date" class="form-control" id="date" value="{{$bs->date}}">
                                </div>
                            </div>
                            @endcan
                    </div> <!-- end card-box -->
                    <div class="row justify-content-center">
                        <table class="table">
                            <thead>
                            <tr>

                                    <th>Type</th>
                                @if($type=='admin')
                                    <th>Résumé Expéditeur</th>
                                @endif
                                @if($type=='admin' || $type=='driver')
                                    <th>Résumé Livreur</th>
                                @endif
                                @if($type=='admin' || $type=='receiver')
                                    <th>Résumé Receveur</th>
                                @endif
                                @if($type=='admin')
                                    <th>Etat</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
{{--                            <tr>--}}
{{--                                <td>Fondant</td>--}}
{{--                                @if($type=='admin')--}}
{{--                                    <td><input name="sender_report[Fondant]" class="form-control" value="{{$bs->sender_report['Fondant']??'0'}}"></td>--}}
{{--                                @endif--}}
{{--                                @if($type=='admin' || $type=='driver')--}}
{{--                                    <td><input name="driver_report[Fondant]" class="form-control" value="{{$bs->driver_report['Fondant']??'0'}}"></td>--}}
{{--                                @endif--}}
{{--                                @if($type=='admin' || $type=='receiver')--}}
{{--                                    <td><input name="receiver_report[Fondant]" class="form-control" value="{{$bs->receiver_report['Fondant']??'0'}}"></td>--}}
{{--                                @endif--}}
{{--                                @if($type=='admin')--}}
{{--                                    <td>--}}
{{--                                        @if($bs->sender_report['Fondant']==$bs->driver_report['Fondant'] && $bs->driver_report['Fondant']==$bs->receiver_report['Fondant'])--}}
{{--                                            <i class="fa fa-check-circle text-success"></i>--}}
{{--                                        @else--}}
{{--                                            <i class="fa fa-minus-circle text-danger"></i>--}}
{{--                                        @endif--}}
{{--                                    </td>--}}
{{--                                @endif--}}
{{--                            </tr>--}}
                            <tr>
                                <td>Fondant</td>
                                @if($type=='admin')
                                <td><input name="sender_report[Fondant]" class="form-control" value="{{$bs->sender_report['Fondant']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='driver')
                                <td><input name="driver_report[Fondant]" class="form-control" value="{{$bs->driver_report['Fondant']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='receiver')
                                <td><input name="receiver_report[Fondant]" class="form-control" value="{{$bs->receiver_report['Fondant']??'0'}}"></td>
                                @endif
                                @if($type=='admin')
                                    <td>
                                        @if($bs->sender_report['Fondant']==$bs->driver_report['Fondant'] && $bs->driver_report['Fondant']==$bs->receiver_report['Fondant'])
                                            <i class="fa fa-check-circle text-success"></i>
                                        @else
                                            <i class="fa fa-minus-circle text-danger"></i>
                                        @endif
                                    </td>
                                @endif
                            </tr>
                            <tr>
                                <td>Polysterene</td>
                                @if($type=='admin')
                                    <td><input name="sender_report[Polysterene]" class="form-control" value="{{$bs->sender_report['Polysterene']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='driver')
                                    <td><input name="driver_report[Polysterene]" class="form-control" value="{{$bs->driver_report['Polysterene']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='receiver')
                                    <td><input name="receiver_report[Polysterene]" class="form-control" value="{{$bs->receiver_report['Polysterene']??'0'}}"></td>
                                @endif
                                @if($type=='admin')
                                    <td>
                                        @if($bs->sender_report['Polysterene']==$bs->driver_report['Polysterene'] && $bs->driver_report['Polysterene']==$bs->receiver_report['Polysterene'])
                                            <i class="fa fa-check-circle text-success"></i>
                                        @else
                                            <i class="fa fa-minus-circle text-danger"></i>
                                        @endif
                                    </td>
                                @endif
                            </tr>
                            <tr>
                                <td>Mini Fondant</td>
                                @if($type=='admin')
                                <td><input name="sender_report[Mini Fondant]" class="form-control" value="{{$bs->sender_report['Mini Fondant']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='driver')
                                <td><input name="driver_report[Mini Fondant]" class="form-control" value="{{$bs->driver_report['Mini Fondant']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='receiver')
                                <td><input name="receiver_report[Mini Fondant]" class="form-control" value="{{$bs->receiver_report['Mini Fondant']??'0'}}"></td>
                                @endif
                                @if($type=='admin')
                                    <td>
                                        @if($bs->sender_report['Mini Fondant']==$bs->driver_report['Mini Fondant'] && $bs->driver_report['Mini Fondant']==$bs->receiver_report['Mini Fondant'])
                                            <i class="fa fa-check-circle text-success"></i>
                                        @else
                                            <i class="fa fa-minus-circle text-danger"></i>
                                        @endif
                                    </td>
                                @endif
                            </tr>

                            <tr>
                                <td>Fondant Geant</td>
                                @if($type=='admin')
                                <td><input name="sender_report[Fondant Geant]" class="form-control" value="{{$bs->sender_report['Fondant Geant']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='driver')
                                <td><input name="driver_report[Fondant Geant]" class="form-control" value="{{$bs->driver_report['Fondant Geant']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='receiver')
                                <td><input name="receiver_report[Fondant Geant]" class="form-control" value="{{$bs->receiver_report['Fondant Geant']??'0'}}"></td>
                                @endif
                                @if($type=='admin')
                                    <td>
                                        @if($bs->sender_report['Fondant Geant']==$bs->driver_report['Fondant Geant'] && $bs->driver_report['Fondant Geant']==$bs->receiver_report['Fondant Geant'])
                                            <i class="fa fa-check-circle text-success"></i>
                                        @else
                                            <i class="fa fa-minus-circle text-danger"></i>
                                        @endif
                                    </td>
                                @endif
                            </tr>
                            <tr>
                                <td>Fondor</td>
                                @if($type=='admin')
                                <td><input name="sender_report[Fondor]" class="form-control" value="{{$bs->sender_report['Fondor']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='driver')
                                <td><input name="driver_report[Fondor]" class="form-control" value="{{$bs->driver_report['Fondor']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='receiver')
                                <td><input name="receiver_report[Fondor]" class="form-control" value="{{$bs->receiver_report['Fondor']??'0'}}"></td>
                                @endif
                                @if($type=='admin')
                                    <td>
                                        @if( isset($bs->sender_report['Fondor']) && $bs->sender_report['Fondor']==$bs->driver_report['Fondor'] && $bs->driver_report['Fondor']==$bs->receiver_report['Fondor'])
                                            <i class="fa fa-check-circle text-success"></i>
                                        @else
                                            <i class="fa fa-minus-circle text-danger"></i>
                                        @endif
                                    </td>
                                @endif
                            </tr>
                            <tr>
                                <td>Rates</td>
                                @if($type=='admin')
                                    <td><input name="sender_report[Rates]" class="form-control" value="{{$bs->sender_report['Rates']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='driver')
                                    <td><input name="driver_report[Rates]" class="form-control" value="{{$bs->driver_report['Rates']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='receiver')
                                    <td><input name="receiver_report[Rates]" class="form-control" value="{{$bs->receiver_report['Rates']??'0'}}"></td>
                                @endif
                                @if($type=='admin')
                                    <td>
                                        @if(isset($bs->sender_report['Rates'])&& $bs->sender_report['Rates']==$bs->driver_report['Rates'] && $bs->driver_report['Rates']==$bs->receiver_report['Rates'])
                                            <i class="fa fa-check-circle text-success"></i>
                                        @else
                                            <i class="fa fa-minus-circle text-danger"></i>
                                        @endif
                                    </td>
                                @endif
                            </tr>
                            <tr>
                                <td>Commandes</td>
                                @if($type=='admin')
                                <td><input name="sender_report[Commandes]" class="form-control" value="{{$bs->sender_report['Commandes']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='driver')
                                <td><input name="driver_report[Commandes]" class="form-control" value="{{$bs->driver_report['Commandes']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='receiver')
                                <td><input name="receiver_report[Commandes]" class="form-control" value="{{$bs->receiver_report['Commandes']??'0'}}"></td>
                                @endif
                                @if($type=='admin')
                                    <td>
                                        @if($bs->sender_report['Commandes']==$bs->driver_report['Commandes'] && $bs->driver_report['Commandes']==$bs->receiver_report['Commandes'])
                                            <i class="fa fa-check-circle text-success"></i>
                                        @else
                                            <i class="fa fa-minus-circle text-danger"></i>
                                        @endif
                                    </td>
                                @endif
                            </tr>
                            <tr>
                                <td>Consommables</td>
                                @if($type=='admin')
                                <td><input name="sender_report[Consommables]" class="form-control" value="{{$bs->sender_report['Consommables']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='driver')
                                <td><input name="driver_report[Consommables]" class="form-control" value="{{$bs->driver_report['Consommables']??'0'}}"></td>
                                @endif
                                @if($type=='admin' || $type=='receiver')
                                <td><input name="receiver_report[Consommables]" class="form-control" value="{{$bs->receiver_report['Consommables']??'0'}}"></td>
                                @endif
                                @if($type=='admin')
                                    <td>
                                        @if($bs->sender_report['Consommables']==$bs->driver_report['Consommables'] && $bs->driver_report['Consommables']==$bs->receiver_report['Consommables'])
                                            <i class="fa fa-check-circle text-success"></i>
                                        @else
                                            <i class="fa fa-minus-circle text-danger"></i>
                                        @endif
                                    </td>
                                @endif
                            </tr>
                            </tbody>
                        </table>

                    </div>

                </div> <!-- end col -->
            </div>

            <div class="col-lg-12">

                <div class="card-box  ribbon-box">
                    <div class="ribbon-two ribbon-two-success"><span><i class="fa fa-list"></i> Détails</span></div>
                    <div class="table-responsive">
                        <table class="table table-striped"  id="details_table">
                            <thead>
                            <tr>

                                <th width="150px">Article</th>
                                @if($type=='admin')
                                <th width="100px">Qté Envoyée</th>
                                @endif
                                @if($type=='admin' || $type=='driver')
                                <th width="100px">Qté Livrée</th>
                                @endif
                                @if($type=='admin' || $type=='receiver')
                                <th width="100px">Qté Reçue</th>
                                @endif
                                @if($type=='admin')
                                <th width="100px">Prix</th>
                                <th width="100px">Total</th>
                                @endif
                                <th width="30px">Action</th>
                            </tr>
                            </thead>
                            <tbody  id="cartItems">
                                @foreach($bs->details as $detail)
                                    <tr id="row{{$detail['product_id']}}" class="item_cart"  data-id="{{$detail['product_id']}}">
                                        <td>
                                            <input type="hidden" name="details[{{$detail['product_id']}}][product_ref]" value="{{$detail['product_ref']}}" class="form-control">
                                            <input type="hidden" name="details[{{$detail['product_id']}}][product_coefficient]" value="{{$detail['product_coefficient']}}" id="coef_id_{{$detail['product_id']}}">
                                            <input type="hidden" name="details[{{$detail['product_id']}}][product_id]" value="{{$detail['product_id']}}">
                                            <input type="hidden" name="details[{{$detail['product_id']}}][product_category]" value="{{$detail['product_category']}}">
                                            <input type="hidden" id="name_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_name]" class="form-control name" readonly value="{{$detail['product_name']}}" style="width: 150px" required/>
                                            {{$detail['product_ref']}} - {{$detail['product_name']}}
                                        </td>
                                        @if($type=='admin')
                                        <td>
                                            <input type="number"  name="details[{{$detail['product_id']}}][product_quantity_sent]" class="form-control quantity withPopover quantity_id_{{$detail['product_id']}}" data-product_id="{{$detail['product_id']}}" value="{{$detail['product_quantity_sent']}}" step="1"
                                                   oninput="this.value = Math.floor(this.value);" required  autocomplete="off"
                                            >
                                        </td>
                                            <td>
                                            <input type="number"  name="details[{{$detail['product_id']}}][product_quantity_delivered]" class="form-control quantity withPopover quantity_id_{{$detail['product_id']}}" data-product_id="{{$detail['product_id']}}" value="{{$detail['product_quantity_delivered']}}" step="1"
                                                   oninput="this.value = Math.floor(this.value);" required  autocomplete="off"
                                            >
                                            </td>
                                            <td>
                                            <input type="number"  name="details[{{$detail['product_id']}}][product_quantity_received]" class="form-control quantity withPopover quantity_id_{{$detail['product_id']}}" data-product_id="{{$detail['product_id']}}" value="{{$detail['product_quantity_received']}}" step="1"
                                                   oninput="this.value = Math.floor(this.value);" required  autocomplete="off"
                                            >

                                        </td>
                                        @endif
                                        @if( $type=='driver')
                                        <td>
                                            <input type="hidden"  name="details[{{$detail['product_id']}}][product_quantity_sent]" class="form-control quantity withPopover quantity_id_{{$detail['product_id']}}" data-product_id="{{$detail['product_id']}}" value="{{$detail['product_quantity_sent']}}" step="1"
                                                   oninput="this.value = Math.floor(this.value);" required  autocomplete="off"
                                            >
                                            <input type="number"  name="details[{{$detail['product_id']}}][product_quantity_delivered]" class="form-control quantity withPopover quantity_id_{{$detail['product_id']}}" data-product_id="{{$detail['product_id']}}" value="{{$detail['product_quantity_delivered']}}" step="1"
                                                   oninput="this.value = Math.floor(this.value);" required  autocomplete="off"
                                            >
                                            <input type="hidden"  name="details[{{$detail['product_id']}}][product_quantity_received]" class="form-control quantity withPopover quantity_id_{{$detail['product_id']}}" data-product_id="{{$detail['product_id']}}" value="{{$detail['product_quantity_received']}}" step="1"
                                                   oninput="this.value = Math.floor(this.value);" required  autocomplete="off"
                                            >
                                            <input type="hidden" id="price_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_price]" class="form-control price price_u withPopover number_format_3" data-product_id="{{$detail['product_id']}}" data-product_grade="" value="{{$detail['product_price']??0}}">
                                            <input type="hidden" id="price_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_price_buying]" value="{{$detail['product_price_buying']??0}}"
                                                   >
                                        </td>
                                        @endif
                                        @if($type=='receiver')
                                        <td>
                                            <input type="hidden"  name="details[{{$detail['product_id']}}][product_quantity_sent]" class="form-control quantity withPopover quantity_id_{{$detail['product_id']}}" data-product_id="{{$detail['product_id']}}" value="{{$detail['product_quantity_sent']}}" step="1"
                                                   oninput="this.value = Math.floor(this.value);" required  autocomplete="off"
                                            >
                                            <input type="hidden"  name="details[{{$detail['product_id']}}][product_quantity_delivered]" class="form-control quantity withPopover quantity_id_{{$detail['product_id']}}" data-product_id="{{$detail['product_id']}}" value="{{$detail['product_quantity_delivered']}}" step="1"
                                                   oninput="this.value = Math.floor(this.value);" required  autocomplete="off"
                                            >
                                            <input type="number"  name="details[{{$detail['product_id']}}][product_quantity_received]" class="form-control quantity withPopover quantity_id_{{$detail['product_id']}}" data-product_id="{{$detail['product_id']}}" value="{{$detail['product_quantity_received']}}" step="1"
                                                   oninput="this.value = Math.floor(this.value);" required  autocomplete="off"
                                            >

                                            <input type="hidden" id="price_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_price]" class="form-control price price_u withPopover number_format_3" data-product_id="{{$detail['product_id']}}" data-product_grade="" value="{{$detail['product_price']??0}}"
                                                       required>
                                            <input type="hidden" id="price_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_price_buying]" value="{{$detail['product_price_buying']??0}}"
                                            >
                                        </td>
                                        @endif
                                        @if($type=='admin')
                                        <td>
                                            <input type="text" id="price_id_{{$detail['product_id']}}" name="details[{{$detail['product_id']}}][product_price]" class="form-control price price_u withPopover number_format_3" data-product_id="{{$detail['product_id']}}" data-product_grade="" value="{{$detail['product_price']??0}}"
                                            <input type="hidden"  name="details[{{$detail['product_id']}}][product_price_buying]" value="{{$detail['product_price_buying']??0}}"
                                                    >
                                        </td>
                                        <td>
                                            <input type="text" id="price_id_{{$detail['product_id']}}" name="" class="form-control price price_u withPopover number_format_3" data-product_id="{{$detail['product_id']}}" data-product_grade="" value="{{$detail['product_price']*$detail['product_quantity_sent']??0}}"
                                                    required>
                                        </td>
                                        @endif
                                        <td>
                                            <a data-tr="row{{$detail['product_id']}}" class="text-danger removeRow"><i class="fa fa-minus-circle"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr class="">
                                <td></td>
                                <td>
                                    <select class="form-control select2 productList" id="productList" name=""  style="width: 150px">
                                        <option value=""></option>
                                        @foreach($products as $product)

                                            <option value="{{$product->id}}"
                                                    data-name="{{$product->name??''}}"
                                                    data-category="{{$product->category_id??''}}"
                                                    data-ref="{{$product->ref??''}}"
                                                    data-price="{{$product->price??''}}"
                                                    data-coefficient="{{$product->coefficient??''}}"
                                            >
                                                {{$product->ref}} - {{$product->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><span class="withPopover" data-placement="top" data-toggle="popover" data-trigger="focus" title="" data-content="La quantité actuelle de cette article est " data-original-title="Raccourci"
                                    ><i class="fa fa-info-circle"></i></span> Recherche</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <audio src="{{asset('assets/sound/success.mp3')}}"></audio>
                    <audio src="{{asset('assets/sound/warning.mp3')}}"></audio>

                </div> <!-- end col-->

            </div> <!-- end col-->


        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-12">
                <div class="text-center mb-3">


                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light submit" id="sa-a4" value="a4" name="submit" >Enregistrer
                    </button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>



    <style>
        .select2 {
            width: 100%!important;
        }
        .popover-title{
            background: #ffff99;
        }
    </style>

@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/css/general.css')}}" rel="stylesheet" type="text/css"/>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

<!-- Loading buttons js -->
<script src="{{asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{asset('assets/libs/ladda/ladda.js')}}"></script>
<script src="{{asset('assets/js/pages/loading-btn.init.js')}}"></script>

<script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('assets/js/pages/add-product.init.js')}}"></script>
{{--<script src="{{asset('assets/js/validation.js')}}"></script>--}}

<script>

    $('.withPopover').popover({offset: 10});
    var productListAfterSelect2 = $("#productList").select2();
    // $(window).on('load', function() {
    //     productListAfterSelect2.select2("open");
    // });
    $(".datepicker").flatpickr();

    $(document).on('change', '#productList', function () {
        addProduct(this);
    });
    $(document).on('click', '.removeRow', function () {
        let tr = $(this).attr("data-tr");
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.value==true) {
                $('#'+tr).remove();
            }
        });

    });







    function addProduct(select) {
        let product = {
            id:select.value,
            name:select.options[select.selectedIndex].getAttribute('data-name'),
            ref:select.options[select.selectedIndex].getAttribute('data-ref'),
            category:select.options[select.selectedIndex].getAttribute('data-category'),
            coefficient:select.options[select.selectedIndex].getAttribute('data-coefficient'),
            // unity:select.options[select.selectedIndex].getAttribute('data-unity'),
            // tva:select.options[select.selectedIndex].getAttribute('data-tva'),
            // buying:select.options[select.selectedIndex].getAttribute('data-buying'),
            price:select.options[select.selectedIndex].getAttribute('data-price'),
            // quantity:select.options[select.selectedIndex].getAttribute('data-quantity'),

        };

        if($('#quantity_id_'+product.id).length){
            $('#quantity_id_'+product.id).val(parseFloat($('#quantity_id_'+product.id).val())+1);
            checkQuantity($('#quantity_id_'+product.id));
            playSound();
        }
        else{
            let alert='';

            tr=`<tr id="row${product.id}" class="item_cart"  data-id="${product.id}">
                    <td>
                        <input type="hidden" name="details[${product.id}][product_ref]" value="${product.ref}" readonly class="form-control">
                        <input type="hidden" name="details[${product.id}][product_id]" value="${product.id}">
                        <input type="hidden" name="details[${product.id}][product_coefficient]" value="${product.coefficient}" id="coef_id_${product.id}">
                        <input type="hidden" name="details[${product.id}][product_category]" value="${product.category}" id="category_id_${product.id}">
                        <input type="hidden" name="details[${product.id}][product_price]" value="${product.price}" id="price_id_${product.id}">
                        <input type="hidden" id="name_id_${product.id}" name="details[${product.id}][product_name]" class="form-control name" readonly value="${product.name}" style="width: 150px" required/>
                     ${product.ref} - ${product.name}
                    </td>
                    <td>
                       <input type="number" id="quantity_id_${product.id}" name="details[${product.id}][product_quantity_sent]" class="form-control  quantity withPopover " data-product_id="${product.id??0}" value="1" step="1" min="0">
                    </td>
                    <td>
                       <input type="number"  name="details[${product.id}][product_quantity_delivered]" class="form-control quantity withPopover quantity_id_${product.id}" data-product_id="${product.id}" value="0" step="1"
                       oninput="this.value = Math.floor(this.value);" required  autocomplete="off">
                    </td>
                    <td>
                        <input type="number"  name="details[${product.id}][product_quantity_received]" class="form-control quantity withPopover quantity_id_${product.id}" data-product_id="${product.id}" value="0" step="1"
                        oninput="this.value = Math.floor(this.value);" required  autocomplete="off">
                    </td>
                    <td>
                        <input type="text" id="price_id_${product.id}" name="details[${product.id}][product_price]" class="form-control price price_u withPopover number_format_3" data-product_id="${product.id}" data-product_grade="" value="${product.price}"
                        required>
                    </td>
                    <td>
                        <input type="text" id="price_id_${product.id}" name="" class="form-control price price_u withPopover number_format_3" data-product_id="${product.id}" data-product_grade="" value="${product.price}"
                        required>
                    </td>
                    <td>
                        <a data-tr="row${product.id}" class="text-danger removeRow"><i class="fa fa-minus-circle"></i></a>
                   </td>
                </tr>`;
            $('#cartItems').append(tr);
            $('.withPopover').popover({offset: 10});
            playSound();
        }

    }





    function playSound() {
        $('audio')[0].load();
        $('audio')[0].play();
    }



    function checkQuantity(element){
        // let max=parseFloat($(element).attr('data-max'));
        // let id=parseFloat($(element).attr('data-product_id'));
        // if(parseFloat($(element).val())>max){
        //     $.toast({
        //         heading: 'Dépassage du stock',
        //         text: 'La quantité actuelle est '+max,
        //         icon: 'error',
        //         loader: true,
        //         position:'top-right',// Change it to false to disable loader
        //         loaderBg: '#f1556c',  // To change the background
        //         bgColor: '#f1556c',  // To change the background
        //     });
        //     //$(this).val(max);
        //     $(element).addClass('alert-danger');
        // }else{
        //     $(element).removeClass('alert-danger');
        // }
    }




    function openAndFocus(element){
        $('#'+element).val('');
        $('#'+element).select2().select2('open');
        $('.select2-search__field').last().focus();
    }



</script>
<style>
    input.chk-btn {
        display: none;
    }

    .label-chk-btn {
        width: 60px;
    }

    .label-chk-btn img {
        filter: grayscale(100%);
        height: 60px;
        transition: ease .3s;
        width: 45px;
        margin: auto;
        display: block;
    }

    input.chk-btn:checked + label img {
        transform: scale(1.1);
        filter: grayscale(0);
    }

    .is-invalid {
        border-color: #dc3545;
        padding-right: calc(1.5em + .75rem);
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
        background-repeat: no-repeat;
        background-position: right calc(.375em + .1875rem) center;
        background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }
    #select2-productList-container{
        max-width: 300px;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
