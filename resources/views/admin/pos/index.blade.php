@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{config('global.raisonsociale')}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Ventes</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
                <h4 class="page-title">Liste Des Ventes</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->





    <!-- end row-->


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">




                        <!-- end row-->


                        <div class="col-12">
                            <div class="row justify-content-center mb-1">
                                <div class="col-md-3">
                                    <label>Du</label>
                                    <input autocomplete="off"  required type="date" class="form-control  filter-field"
                                           name="from" placeholder="De" id="from" >
                                </div>
                                <div class="col-md-3">
                                    <label>Au</label>
                                    <input autocomplete="off"  required type="date" class="form-control  filter-field"
                                           name="to" placeholder="Au" id="to">
                                </div>
                                <div class="col-md-3">
                                    <label>PTV</label>
                                    <input autocomplete="off"  required type="text" class="form-control  filter-field"
                                           name="client" placeholder="client" id="client">
                                </div>


                            </div>

                        </div>
                        <div class="col-12 text-center">

                            <label for="all" class="mb-0 btn btn btn-secondary"> <input type="radio" name="status" class="filter-field" id="all" value="all"> ALL (<span id="allSpan"></span>)</label>
                            <label for="paid" class="mb-0 btn btn badge-success"> <input type="radio" name="status" class="filter-field" id="paid" value="2"> Payé (<span id="paidSpan"></span>)</label>
                            <label for="p_paid" class="mb-0 btn btn badge-warning"> <input type="radio" name="status" class="filter-field" id="p_paid" value="1"> Partiellement Payé (<span id="p_paidSpan"></span>)</label>
                            <label for="unpaid" class="mb-0 btn btn btn-danger">  <input type="radio" name="status" class="filter-field" id="unpaid" value="0"> Non Payé (<span id="unpaidSpan"></span>)</label>



                        </div>
                        <div id="chart" class="col-12">

                        </div>



                        <div class="row mt-2">
                            <div class="col-12">


                                <div class="table-responsive">
                                        <table id="key-datatable" class="table table-striped dt-responsive nowrap">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Numéro</th>
                                                <th>Client</th>
                                                <th>Boutique</th>
                                                <th>MF</th>
                                                <th>Total</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                                <th>Créé à</th>
                                                <th>Modifié à</th>
                                                <th>User</th>
                                                <th width="100px">Action </th>

                                            </tr>
                                            </thead>
                                            <tfoot  style="display: table-row-group;">
                                            <tr>
                                                <th></th>
                                                <th class="searchable">Numéro</th>
                                                <th class="searchable">Client</th>
                                                <th class="searchable">Boutique</th>
                                                <th class="searchable">MF</th>
                                                <th class="searchable">Total</th>
                                                <th class="payment_status">Status</th>
                                                <th class="date">Date</th>
                                                <th class="date">Modifié à</th>
                                                <th class="date">Créé à</th>
                                                <th class="searchable">User</th>
                                                <th> </th>

                                            </tr>
                                            </tfoot>
                                            <tbody>

                                            </tbody>

                                        </table>
                                </div>

                            </div><!-- end col-->
                        </div>

                        <!-- end row-->



                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>



    <!-- end row-->





@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party css -->
<link href="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css"/>

<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .dt-buttons {
        float: right;
    }

    .dataTables_filter {
        text-align: center !important;
    }
</style>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- third party js -->
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js" type="text/javascript"></script>

<script src="{{URL::asset('assets/libs/morris-js/morris.min.js')}}"></script>

<script src="{{URL::asset('assets/libs/raphael/raphael.min.js')}}"></script>

<!-- third party js ends -->
<!-- Datatables init -->
<script>

    table = $('#key-datatable').DataTable({

        processing: true,
        serverSide: true,
        @if(auth()->user()->phone=='98611913' || auth()->user()->phone=='56525990' || auth()->user()->phone=='94896642' )
        drawCallback: function(settings) {
            $('#paidSpan').text(settings.json.order.paid+' | '+(((settings.json.order.paid/settings.json.order.all)*100).toFixed(2)) +'%');
            $('#p_paidSpan').text(settings.json.order.p_paid+' | '+(((settings.json.order.p_paid/settings.json.order.all)*100).toFixed(2)) +'%');
            $('#unpaidSpan').text(settings.json.order.unpaid+' | '+(((settings.json.order.unpaid/settings.json.order.all)*100).toFixed(2)) +'%');


            $('#allSpan').text(settings.json.order.all);
            $('#chart').html(`<div id="morris-donut-example" style="display: block;margin-left: auto;margin-right: auto;width: 300px" class="morris-chart"></div>`);
            var MorrisCharts = {
                createDonutChart: function (elementId, data, colors) {
                    Morris.Donut({
                        element: elementId,
                        data: data,
                        resize: true,
                        colors: colors
                    });
                },

                init: function () {
                    var data = [];
                    data.push({
                        label: "Payé",
                        value: settings.json.order.paid
                    });
                    data.push({
                        label: "P Payé",
                        value: settings.json.order.p_paid
                    });
                    data.push({
                        label: "Non Payé",
                        value: settings.json.order.unpaid
                    });




                    this.createDonutChart("morris-donut-example", data,
                        ["#1abc9c", "#f7b84b", "#f1556c"]);
                }
            };

            MorrisCharts.init();
        },
        @endif
        ajax: "{{ route('admin.pos.getData') }}",
        columns: [
            { data: 'checkbox', name: 'checkbox' },
            { data: 'id', name: 'id' },
            { data: 'client_details', name: 'client_details' },
            { data: 'store', name: 'stores.name' },
            { data: 'date', name: 'date' },
            { data: 'total', name: 'total' },
            { data: 'payment_status', name: 'payment_status' },

            { data: 'date', name: 'date' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'admin', name: 'admins.name' },
            { data: 'action', name: 'action' },

        ],
        order: [[1, 'desc']],



        aLengthMenu: [
            [10, 25, 50, 100, 200, -1],
            [10, 25, 50, 100, 200, "All"]
        ],

        dom: 'Blfrtip',
        responsive:false,
        initComplete: function () {
            // Apply the search
            this.api()
                .columns()
                .every(function () {
                    var that = this;
                    $('input,select', this.footer()).on('keyup change clear', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
        },

    });

    $('#key-datatable tfoot .searchable').each(function () {
        var title = $(this).text();
        $(this).html('<input class="form-control" type="text" placeholder="Search ' + title + '" />');
    });
    $('#key-datatable tfoot .date').each(function () {
        var title = $(this).text();
        $(this).html('<input class="form-control" type="date" placeholder="Search ' + title + '" />');
    });
    $('#key-datatable tfoot .payment_status').each(function () {
        var title = $(this).text();
        let select=`<select class="form-control">
                        <option value=""></option>
                        <option value="0">Non Payé</option>
                        <option value="1">Partiellement Payé</option>
                        <option value="2">Payé</option>
                    </select>`;
        $(this).html(select);
    });
    $('body').on('click', '.main_select', function (e) {
        var check = $('.orders_tbl').find('tbody > tr > td:first-child .order_check');
        if ($('.main_select').prop("checked") == true) {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', true);
        } else {
            $('.orders_tbl').find('tbody > tr > td:first-child .order_check').prop('checked', false);
        }

        $('.orders_tbl').find('tbody > tr > td:first-child .order_check').val();
    });
    $('.check').click(function () {

        var firstInput = $(this).find('input')[0];
        firstInput.checked = !firstInput.checked;
    });



    $('#sa-reset').click(function () {
        $("#StatusFilter,#customer_id").val("");
        $("#StatusFilter,#customer_id").select2({});
        $("#my_multi_select2").val('');
        $("#my_multi_select2").multiSelect('refresh');
    });
    function filter() {
        var queryString = '?' + new URLSearchParams(filter).toString();
        var url = @json(route('admin.pos.getData'));
        table.ajax.url(url + queryString).load();
        table.draw();
    }

    $(document).on('change', '.filter-field', function(e) {
        var key = $(this).attr('id');
        var value = $(this).val();
        filter[key] = value;
        filter();
    });
    $(document).on('change', 'input[name=status]', function(e) {
        var value = $(this).val();
        filter['status'] = value;
        if(value=='all') $('input[name=type]').prop('checked',false);
        filter();
    });

</script>



{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')

