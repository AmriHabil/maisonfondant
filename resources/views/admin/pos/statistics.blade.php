@section('content')




    <!-- end row-->
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="form-inline">
                        <div class="form-group">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control border-white dash-daterange">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-blue border-blue text-white">
                                        <i class="mdi mdi-calendar-range font-13"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control border-white dash-daterange">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-blue border-blue text-white">
                                        <i class="mdi mdi-calendar-range font-13"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-2">
                            <i class="mdi mdi-autorenew"></i>
                        </a>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-1">
                            <i class="mdi mdi-filter-variant"></i>
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                <h4 class="header-title mb-3">Evolution Mensuelle</h4>

                <div id="sales-analytics" class="flot-chart mt-4 pt-1" style="height: 280px;"></div>
            </div> <!-- end card-box -->
            <div class="card-box">
                <h4 class="header-title mb-3">Evolution Mensuelle Total</h4>

                <div id="total-analytics" class="flot-chart mt-4 pt-1" style="height: 280px;"></div>
            </div> <!-- end card-box -->
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="header-title mb-3">Evolution Hebdomadaire</h4>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <p class="text-muted font-15 font-family-secondary mb-0">
                                        @foreach($stores as $store)
                                            @switch ($store->id)
                                             @case(1)
                                            @endswitch
                                        <span class="mx-2 text-uppercase"><i class="mdi mdi-checkbox-blank-circle"
                                        @switch ($store->id)
                                            @case(1) style="color:#566676" @break
                                            @case(2) style="color:#4fc6e1" @break
                                            @case(3) style="color:#1abc9c" @break
                                            @case(4) style="color:#f7b84b" @break
                                            @case(7) style="color:#f1556c" @break
                                        @endswitch
                                                    ></i> {{$store->name}}</span>
                                        @endforeach

                                    </p>
                                </div>
                                <div id="morris-area-with-dotted" style="height: 320px;" class="morris-chart my-3 mb-lg-0"></div>
                            </div> <!-- end col -->

                        </div> <!-- end row-->
                    </div>  <!-- end card-box-->
                </div> <!-- end col -->
            </div>
        </div> <!-- end col-->

    </div>
    <!-- end row-->


@stop


@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<!-- Plugins css -->
<link href="{{URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<script src="{{URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/jquery-knob/jquery.knob.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.time.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.selection.js')}}"></script>
<script src="{{URL::asset('assets/libs/flot-charts/jquery.flot.crosshair.js')}}"></script>
<script src="{{URL::asset('assets/libs/raphael/raphael.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/morris-js/morris.min.js')}}"></script>


<script>
    !function (o) {
        "use strict";
        var t = function () {
            this.$body = o("body")
        };
        t.prototype.createCombineGraph = function (t, a, e, i) {
            var r =
                [
                        @foreach ($stores as $store)
                {   label: e[{{ $loop->index }}],
                    data: i[{{ $loop->index }}],
                    lines: {show: !0},
                    points: {show: !0}
                },
               @endforeach

            ], l = {
                series: {shadowSize: 0},
                grid: {hoverable: !0, clickable: !0, tickColor: "#f9f9f9", borderWidth: 1, borderColor: "#eeeeee"},
                colors: ["#566676", "#4fc6e1", "#1abc9c","#f7b84b","#f1556c","#f44336"],
                tooltip: !0,
                tooltipOpts: {defaultTheme: !1},
                legend: {
                    position: "ne",
                    margin: [0, -32],
                    noColumns: 0,
                    labelBoxBorderColor: null,
                    labelFormatter: function (o, t) {
                        return o + "&nbsp;&nbsp;"
                    },
                    width: 30,
                    height: 2
                },
                yaxis: {axisLabel: "Point Value (1000)", tickColor: "#f5f5f5", font: {color: "#bdbdbd"}},
                xaxis: {axisLabel: "Daily Hours", ticks: a, tickColor: "#f5f5f5", font: {color: "#bdbdbd"}}
            };
            o.plot(o(t), r,l)
        }, t.prototype.init = function () {
            var o = [
                @foreach ($stores as $store)
                [
                    @for($month = 1; $month <= 12; $month++)
                    [{{$month}}, {{$bls->where('store_id',$store->id)->where('month',$month)->first()->total??0}}],
                    @endfor
                ],
                @endforeach

            ];
            this.createCombineGraph("#sales-analytics", [ [1, "Janvier"], [2, "Fevrier"], [3, "Mars"], [4, "Avril"], [5, "Mai"], [6, "Juin"], [7, "Juillet"], [8, "Aout"], [9, "Septembre"], [10, "Octobre"], [11, "Novembre"], [12, "Decembre"]],
                [
                    @foreach ($stores as $store)
                        "{{$store->name}}",
                    @endforeach
                ],
                o)
        }, o.Dashboard1 = new t, o.Dashboard1.Constructor = t
    }(window.jQuery), function (o) {
        "use strict";
        o.Dashboard1.init()
    }(window.jQuery), $(".dash-daterange").flatpickr({
        altInput: !0,
        altFormat: "F j, Y",
        dateFormat: "Y-m-d",
        defaultDate: "today"
    });
</script>
<script>
    !function (o) {
        "use strict";
        var t = function () {
            this.$body = o("body")
        };
        t.prototype.createCombineGraph = function (t, a, e, i) {
            var r =
                [
                        @foreach ($stores as $store)
                    {   label: e[{{ $loop->index }}],
                        data: i[{{ $loop->index }}],
                        lines: {show: !0},
                        points: {show: !0}
                    },
                        @endforeach

                ], l = {
                series: {shadowSize: 0},
                grid: {hoverable: !0, clickable: !0, tickColor: "#f9f9f9", borderWidth: 1, borderColor: "#eeeeee"},
                colors: ["#1abc9c", "#4fc6e1", "#1abc9c","#f7b84b","#f1556c","#f44336"],
                tooltip: !0,
                tooltipOpts: {defaultTheme: !1},
                legend: {
                    position: "ne",
                    margin: [0, -32],
                    noColumns: 0,
                    labelBoxBorderColor: null,
                    labelFormatter: function (o, t) {
                        return o + "&nbsp;&nbsp;"
                    },
                    width: 30,
                    height: 2
                },
                yaxis: {axisLabel: "Point Value (1000)", tickColor: "#f5f5f5", font: {color: "#bdbdbd"}},
                xaxis: {axisLabel: "Daily Hours", ticks: a, tickColor: "#f5f5f5", font: {color: "#bdbdbd"}}
            };
            o.plot(o(t), r,l)
        }, t.prototype.init = function () {
            var o = [

                [
                        @for($month = 1; $month <= 12; $month++)
                    [{{$month}}, {{($bls->where('month',$month)->sum('total')??0) }}],
                    @endfor
                ],

            ];
            this.createCombineGraph("#total-analytics", [ [1, "Janvier"], [2, "Fevrier"], [3, "Mars"], [4, "Avril"], [5, "Mai"], [6, "Juin"], [7, "Juillet"], [8, "Aout"], [9, "Septembre"], [10, "Octobre"], [11, "Novembre"], [12, "Decembre"]],
                [

                        "Total",
                ],
                o)
        }, o.Dashboard1 = new t, o.Dashboard1.Constructor = t
    }(window.jQuery), function (o) {
        "use strict";
        o.Dashboard1.init()
    }(window.jQuery), $(".dash-daterange").flatpickr({
        altInput: !0,
        altFormat: "F j, Y",
        dateFormat: "Y-m-d",
        defaultDate: "today"
    });
</script>

<script>

    !function (e) {
        "use strict";
        var t = function () {
        };
        const monthNames = [@for($day = 1; $day <= 31; $day++)"{{$day}}", @endfor
        ];
        t.prototype.createAreaChartDotted = function (e, t, r, i, o, a, c, n, s, y) {
            Morris.Area({
                element: e,
                pointSize: 3,
                lineWidth: 2,
                data: i,
                xkey: o,
                ykeys: a,
                labels: c,
                hideHover: "auto",
                pointFillColors: n,
                pointStrokeColors: s,
                resize: !0,
                behaveLikeLine: !0,
                fillOpacity: .4,
                gridLineColor: "#eef0f2",
                lineColors: y,
                parseTime: false

            })
        }, t.prototype.init = function () {
            this.createAreaChartDotted("morris-area-with-dotted", 0, 0,
                [
                    @for($day = 1; $day <= 31; $day++)
                {
                    y: "{{$day}}",
                    @foreach($stores as $store)
                    _{{$store->id}}: {{$monthly->where('store_id',$store->id)->where('day',$day)->first()->total ??0}},
                    @endforeach

                },
                @endfor
                ],
                "y",
                [
                        @foreach($stores as $store)
                            "_{{$store->id}}",
                        @endforeach

                ],
                [
                    @foreach($stores as $store)
                        "{{$store->name}}",
                    @endforeach

                ],
                ["#ffffff"], ["#999999"],
                ["#566676", "#4fc6e1", "#1abc9c","#f7b84b","#f1556c","#f44336"]  )
        }, e.MorrisCharts = new t, e.MorrisCharts.Constructor = t
    }(window.jQuery), function (e) {
        "use strict";
        e.MorrisCharts.init()
    }(window.jQuery);
</script>
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')