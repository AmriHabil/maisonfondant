@section('content')




    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Celerity</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Admins</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
                <h4 class="page-title">Create Admins</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <form action="" method="POST">
        @csrf
        <div class="row">
            <div class="col-lg-12">

                <div class="card-box">
                    <h5 class="text-uppercase bg-light p-2 mt-0 mb-3">Receiver Details</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-3">
                                <label for="name">Nom & Prénom <span class="text-danger">*</span></label>
                                <input type="text" id="name" class="form-control" placeholder="Nom & Prénom " name="name"  value="{{$admin->name}}">
                                <ul class="parsley-errors-list filled" id="name_errors">

                                </ul>
                            </div>
                            <div class="form-group mb-3">
                                <label for="phone">Numéro <span class="text-danger">*</span></label>
                                <input type="text" id="phone" class="form-control mobile numeric" placeholder="Numéro" name="phone" maxlength="8" minlength="8"  value="{{$admin->phone}}">
                                <ul class="parsley-errors-list filled" id="phone_errors">

                                </ul>
                            </div>
                            <div class="form-group mb-3">
                                <label for="cin">CIN <span class="text-danger">*</span></label>
                                <input type="text" id="cin" class="form-control mobile numeric" placeholder="CIN" name="cin"  maxlength="8" minlength="8"  value="{{$admin->cin}}">
                                <ul class="parsley-errors-list filled" id="cin_errors">

                                </ul>
                            </div>
                            <div class="form-group mb-3">
                                <label for="email">Email </label>
                                <input type="email" id="email" class="form-control" placeholder="Email" name="email"  value="{{$admin->email}}">
                                <ul class="parsley-errors-list filled" id="email_errors">

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group mb-3">
                                <label for="address">Adresse <span class="text-danger">*</span></label>
                                <input type="text" id="address" class="form-control" placeholder="address" name="address" value="{{$admin->address}}">
                                <ul class="parsley-errors-list filled" id="address_errors">

                                </ul>
                            </div>

                            <div class="form-group mb-3">
                                <label for="type">Role <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="type" name="type">
                                    <option value="admin" @if($admin->type=="admin") selected @endif>Admin</option>
                                    <option value="franchise" @if($admin->type=="franchise") selected @endif>franchisé </option>
                                    <option value="livreur" @if($admin->type=="livreur") selected @endif>Livreur</option>

                                </select>

                                <ul class="parsley-errors-list filled" id="role_errors">

                                </ul>
                            </div>
                            <div class="form-group mb-3">
                                <label for="role">Permission <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="role" name="role">
                                    <option value=""></option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->name}}" @if($role->name==$admin->role) selected @endif>{{$role->name}}</option>
                                    @endforeach

                                </select>

                                <ul class="parsley-errors-list filled" id="role_errors">

                                </ul>
                            </div>
                            <div class="form-group mb-3">
                                <label for="store_id">Boutique <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="store_id" name="store_id">
                                    <option value=""></option>
                                    @foreach($stores as $store)
                                        <option value="{{$store->id}}" @if($store->id==$admin->store_id) selected @endif>{{$store->name}}</option>
                                    @endforeach

                                </select>

                                <ul class="parsley-errors-list filled" id="store_id_errors">

                                </ul>
                            </div>
                            <div class="form-group mb-3">
                                <label for="password">Password <span class="text-danger">*</span></label>
                                <input type="text" id="password" class="form-control" placeholder="password" name="password">
                                <ul class="parsley-errors-list filled" id="password_errors">

                                </ul>
                            </div>
                        </div>
                    </div>

                </div> <!-- end card-box -->

            </div> <!-- end col -->


        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="text-center mb-3">



                    <button type="submit" class="ladda-button  btn w-sm btn-success waves-effect waves-light"  data-style="expand-left" id="sa-success">Enregistrer</button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </form>





@stop
@include('admin.header')
@include('admin.topbar')
@include('admin.sidebar')
@include('admin.footer')

@yield('header')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
<link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />

<link href="{{URL::asset('assets/libs/summernote/summernote-bs4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Sweet Alert-->
<link href="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Loading button css -->
<link href="{{URL::asset('assets/libs/ladda/ladda-themeless.min.css')}}" rel="stylesheet" type="text/css" />




<link href="{{URL::asset('assets/css/general.css')}}" rel="stylesheet" type="text/css" />
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}
@yield('topbar')
@yield('sidebar')
@yield('content')
@yield('footer')
{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}



<!-- Select2 js-->
<script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>

<!-- Sweet Alerts js -->
<script src="{{URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- maxlength js-->
<script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
<!-- Loading buttons js -->
<script src="{{URL::asset('assets/libs/ladda/spin.js')}}"></script>
<script src="{{URL::asset('assets/libs/ladda/ladda.js')}}"></script>

<!-- Buttons init js-->
<script src="{{URL::asset('assets/js/pages/loading-btn.init.js')}}"></script>
<!-- Init js -->
<script src="{{URL::asset('assets/js/pages/add-product.init.js')}}"></script>
<script src="{{URL::asset('assets/js/validation.js')}}"></script>
<script>
    $("#sa-success").click(function (e) {
        e.preventDefault();
        $('.parsley-errors-list').html('');
        var Form=$(this).parents('form:first')[0];
        var formData= new FormData(Form);
        $.ajax({
            type: 'POST',
            url:"{{route('admin.admins.update',$admin->id)}}",
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success:function(data)
            {
                $('.is-invalid').removeClass('is-invalid');

                $.toast({
                    heading: 'Information',
                    text: 'Enregistré avec succès',
                    icon: 'success',
                    loader: true,
                    position:'top-right',// Change it to false to disable loader
                    loaderBg: '#5ba035',  // To change the background
                    bgColor: '#1abc9c',  // To change the background
                });
                $("#ID_Governorates").val("");
                $("#ID_Governorates").select2({});
            }, error: function (reject) {
                $('.is-invalid').removeClass('is-invalid');
                swal({
                    title: "Fail!",
                    text: "Please follow the instructions",
                    type: "error",
                    confirmButtonClass: "btn btn-confirm mt-2"
                });
                var response=$.parseJSON(reject.responseText);
                $.each(response.errors,function(key,val){
                    $("#" + key ).addClass('is-invalid');
                    $("#" + key + "_errors").html('<li class="parsley-required">'+val+'</li>');

                });
            }
        });

    });
</script>

{{--_____________________________________--}}
{{--_____________________________________--}}
{{--_____________________________________--}}

@include('admin.end')
