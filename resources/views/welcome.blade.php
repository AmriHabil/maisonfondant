<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Celerity</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- App css -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />


</head>

<body class="authentication-bg">

<div class="mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 justify-content-center">

                <img  loading="lazy" src="{{asset('assets/images/logo-l.png')}}" alt="" style="width: 300px;display: block;margin-right: auto;margin-left: auto">

                <div class="text-center">

                    <h3 class="mt-2 text-white">We are currently performing maintenance</h3>
                    <p class="text-muted">We're making the system more awesome. We'll be back shortly.</p>
                    <a href="{{route('login')}}" class="btn btn-success">Customers Portal</a>
                    <a href="{{route('showAdminLoginForm')}}" class="btn btn-primary">Managers Portal</a>
                    <div class="row mt-5 justify-content-center">
                        <div class="col-md-3">
                            <div class="text-center mt-3 pl-1 pr-1">
                                <div class="avatar-md rounded-circle bg-soft-secondary mx-auto">
                                    <i class="mdi mdi-chart-bar font-22 avatar-title text-white"></i>
                                </div>
                                <h5 class="text-uppercase mt-3 text-white">Interactive Dashboard</h5>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-3">
                            <div class="text-center mt-3 pl-1 pr-1">
                                <div class="avatar-md rounded-circle bg-soft-secondary mx-auto">
                                    <i class="mdi mdi-account-multiple font-22 avatar-title text-white"></i>
                                </div>
                                <h5 class="text-uppercase mt-3 text-white">Customers Management</h5>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-3">
                            <div class="text-center mt-3 pl-1 pr-1">
                                <div class="avatar-md rounded-circle bg-soft-secondary mx-auto">
                                    <i class="mdi mdi-file-check font-22 avatar-title text-white"></i>
                                </div>
                                <h5 class="text-uppercase mt-3 text-white">Customer Invoicing</h5>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-3">
                            <div class="text-center mt-3 pl-1 pr-1">
                                <div class="avatar-md rounded-circle bg-soft-secondary mx-auto">
                                    <i class="mdi mdi-map-marker-radius font-22 avatar-title text-white"></i>
                                </div>
                                <h5 class="text-uppercase mt-3 text-white">Live Tracking</h5>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-3">
                            <div class="text-center mt-3 pl-1 pr-1">
                                <div class="avatar-md rounded-circle bg-soft-secondary mx-auto">
                                    <i class="mdi mdi-share-variant font-22 avatar-title text-white"></i>
                                </div>
                                <h5 class="text-uppercase mt-3 text-white">Multi Hubs</h5>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-3">
                            <div class="text-center mt-3 pl-1 pr-1">
                                <div class="avatar-md rounded-circle bg-soft-secondary mx-auto">
                                    <i class="mdi mdi-barcode-scan font-22 avatar-title text-white"></i>
                                </div>
                                <h5 class="text-uppercase mt-3 text-white"> Barcode Scanning</h5>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-3">
                            <div class="text-center mt-3 pl-1 pr-1">
                                <div class="avatar-md rounded-circle bg-soft-secondary mx-auto">
                                    <i class="mdi mdi-lan-connect font-22 avatar-title text-white"></i>
                                </div>
                                <h5 class="text-uppercase mt-3 text-white">API Connection</h5>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-3">
                            <div class="text-center mt-3 pl-1 pr-1">
                                <div class="avatar-md rounded-circle bg-soft-secondary mx-auto">
                                    <i class="mdi mdi-lock-clock font-22 avatar-title text-white"></i>
                                </div>
                                <h5 class="text-uppercase mt-3 text-white">Users Permissions</h5>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-3">
                            <div class="text-center mt-3 pl-1 pr-1">
                                <div class="avatar-md rounded-circle bg-soft-secondary mx-auto">
                                    <i class="mdi mdi-database-check font-22 avatar-title text-white"></i>
                                </div>
                                <h5 class="text-uppercase mt-3 text-white">Fast DataBase</h5>
                            </div>
                        </div> <!-- end col-->
                        <div class="col-md-3">
                            <div class="text-center mt-3 pl-1 pr-1">
                                <div class="avatar-md rounded-circle bg-soft-secondary mx-auto">
                                    <i class="mdi mdi-car-pickup font-22 avatar-title text-white"></i>
                                </div>
                                <h5 class="text-uppercase mt-3 text-white">Fleet Management</h5>
                            </div>
                        </div> <!-- end col-->

                    </div> <!-- end row-->


                </div> <!-- end /.text-center-->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end page -->

<footer class="footer footer-alt">
    2021 - 2022 &copy; Celerity by <a href="https://badhrah.com" target="_blank">Badhrah</a>
</footer>


<!-- App js -->
<script src="{{asset('assets/js/vendor.min.js')}}"></script>
<script src="{{asset('assets/js/app.min.js')}}"></script>

</body>
</html>